﻿
using Cricket.DAL;
using Cricket.Model;
using Cricket.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace Cricket.Views
{
    /// <summary>
    /// Interaction logic for NewLocation.xaml
    /// </summary>
    public partial class NewLocation : UserControl
    {
        public NewLocationVM ObjLocationVM = new NewLocationVM();
        public NewLocation()
        {
            try
            {
                InitializeComponent();
                this.DataContext = ObjLocationVM;
                cbxPitchType.ItemsSource = Enum.GetValues(typeof(PitchType)).Cast<PitchType>();
                cbxzone.Text = User.Zone.ZoneName;
                ObjLocationVM.getGroundsinCurrentZone(User.Zone);
                btnnew_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void generateLocationkeyid()
        {
            try
            {
                int count = (ObjLocationVM.GroundsinCurrentZone.Where(p=>p.ZoneId ==User.Zone.ZoneId).Count()) + 1;

                if (count < 10)
                {
                    ObjLocationVM.CurrentLocation.GroundKeyId = User.Zone.ZoneNumber + "G0" + count;
                }
                else if (count >= 10 && count < 100)
                {
                    ObjLocationVM.CurrentLocation.GroundKeyId = User.Zone.ZoneNumber + "G" + count;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cbxzone_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                ZoneEntity selectedzone = User.Zone;




                if (selectedzone != null)
                {

                    if (ObjLocationVM.CurrentLocation.CZone != null)
                    {
                        if (selectedzone.ZoneId != ObjLocationVM.CurrentLocation.CZone.ZoneId)
                        {
                            ObjLocationVM.CurrentLocation.CZone = selectedzone;
                            generateLocationkeyid();
                            txt_locationname.Text = "";

                            txt_stadium.Text = "";
                            txt_address.Text = "";
                            btnsavelocation.Visibility = Visibility.Visible;
                        }
                    }
                    else
                    {
                        ObjLocationVM.CurrentLocation.CZone = selectedzone;
                        generateLocationkeyid();
                        txt_locationname.Text = "";

                        txt_stadium.Text = "";
                        txt_address.Text = "";
                        btnsavelocation.Visibility = Visibility.Visible;
                    }

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnsavelocation_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ObjLocationVM.CurrentLocation.CZone == null)
                {
                    ZoneEntity objZone = User.Zone;
                    ObjLocationVM.CurrentLocation.CZone = objZone;
                }
                foreach (LocationEntity locationtovalidate in ObjLocationVM.GroundsinCurrentZone )
                {
                    try
                    {
                        locationtovalidate.ValidateEntity();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        return;
                    }
                }
                try
                {
                    List<EntityCollection> lsttosave = new List<EntityCollection>();
                    Database.NewSaveEntity<LocationEntity>(ObjLocationVM.GroundsinCurrentZone, lsttosave);

                    using (var dbContext = Database.getDBContext())
                    {
                        Database.CommitChanges(lsttosave, dbContext);
                        ObjLocationVM.loadLocations();
                        ObjLocationVM.getGroundsinCurrentZone(User.Zone);
                    }
                    MessageBox.Show("Changes Saved", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error while Saving", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }


        private void btnnew_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ObjLocationVM.CurrentLocation ==null || ObjLocationVM.CurrentLocation.IsNew == false)
                {
                    LocationEntity newlocation = Database.getNewEntity<LocationEntity>();
                    ObjLocationVM.CurrentLocation = newlocation;
                    ObjLocationVM.ActiveLocations.Add(newlocation);
                    ObjLocationVM.GroundsinCurrentZone.Add(newlocation);
                    generateLocationkeyid();
                    txt_stadium.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void gridlocationvalues_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (ObjLocationVM.CurrentLocation == null || ObjLocationVM.CurrentLocation.IsNew == false)
                {
                    if (gridlocationvalues.SelectedItem != null)
                    {
                        LocationEntity objgridselected = (LocationEntity)gridlocationvalues.SelectedItem;
                        ZoneEntity ObjZone = Database.GetEntity<ZoneEntity>(objgridselected.ZoneId);

                        objgridselected.CZone = ObjZone;
                        // }
                        ObjLocationVM.CurrentLocation = objgridselected;
                        if (ObjLocationVM.CurrentLocation.CZone != null)
                            cbxzone.Text = ObjLocationVM.CurrentLocation.CZone.ZoneName;
                        cbxPitchType.Text = ObjLocationVM.CurrentLocation.PitchType.ToString();
                        txt_stadium.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cbxPitchType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (cbxPitchType.SelectedItem.ToString() == PitchType.Matting.ToString())
                    ObjLocationVM.CurrentLocation.PitchType = PitchType.Matting;
                else
                    ObjLocationVM.CurrentLocation.PitchType = PitchType.Turf;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ObjLocationVM.CurrentLocation != null)
                {
                    if (ObjLocationVM.CurrentLocation.IsNew)
                    {
                        ObjLocationVM.ActiveLocations.Remove(ObjLocationVM.CurrentLocation);
                        ObjLocationVM.CurrentLocation = null;
                    }
                    else
                    {
                        try
                        {
                            ObjLocationVM.ActiveLocations.Remove(ObjLocationVM.CurrentLocation);
                            ObjLocationVM.CurrentLocation = null;
                            MessageBox.Show("Location Deleted");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                if (ObjLocationVM.CurrentLocation != null && ObjLocationVM.CurrentLocation.IsNew)
                {

                    ObjLocationVM.ActiveLocations.Remove(ObjLocationVM.CurrentLocation);
                    ObjLocationVM.GroundsinCurrentZone.Remove(ObjLocationVM.CurrentLocation);
                    ObjLocationVM.CurrentLocation = null;
                    if (gridlocationvalues.SelectedItems != null)
                        gridlocationvalues_SelectionChanged(null, null);
                }
                else if (ObjLocationVM.CurrentLocation != null && ObjLocationVM.CurrentLocation.IsNew == false)
                {
                    LocationEntity objLocationfromDB = Database.GetEntity<LocationEntity>(ObjLocationVM.CurrentLocation.LocationId);
                    ObjLocationVM.ActiveLocations.Remove(ObjLocationVM.CurrentLocation);
                    ObjLocationVM.GroundsinCurrentZone.Remove(ObjLocationVM.CurrentLocation);

                    ObjLocationVM.CurrentLocation = objLocationfromDB;
                    ObjLocationVM.ActiveLocations.Add(objLocationfromDB);
                    ObjLocationVM.GroundsinCurrentZone.Add(objLocationfromDB);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
    }
}
