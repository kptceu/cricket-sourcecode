﻿
using Cricket.Model;
using Cricket.ViewModel;

using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cricket.Views
{
    /// <summary>
    /// Interaction logic for PlayerStats.xaml
    /// </summary>
    public partial class PlayerStats : UserControl
    {
        PlayerStatsVM vm = new PlayerStatsVM();

        public PlayerStats()
        {
            InitializeComponent();
            this.DataContext = vm;
            cbxseason.SelectedValue = User.Season.SeasonId;
            cbxteams.SelectedIndex = 0;
        }

        private void btnexportbatnbowl_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (vm.LstBatting.Count > 0||vm.LstBowling.Count>0)
                {
                    vm.GenerateExcel();
                }
                else
                {
                    MessageBox.Show("No Data To Export");
                }

            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }

        }

        private void gridallrounderdetails_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = (e.Row.GetIndex() + 1).ToString();

        }

        private void gridbowlingdetails_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = (e.Row.GetIndex() + 1).ToString();
        }

        private void gridbattingdetails_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = (e.Row.GetIndex() + 1).ToString();
        }

        private void cbxteams_DropDownClosed(object sender, EventArgs e)
        {
            if (cbxseason.SelectedIndex == -1)
                MessageBox.Show("Select Season", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            else if(cbxteams.SelectedIndex == -1)
                MessageBox.Show("Select Team", "Info", MessageBoxButton.OK, MessageBoxImage.Information);            
        }

        private void cbxseason_DropDownClosed(object sender, EventArgs e)
        {
            if (cbxseason.SelectedIndex == -1)
                MessageBox.Show("Select Season", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            else
            {
                if (cbxseason.SelectedIndex == 0)
                    vm.LoadTeams();
                else
                    vm.LoadTeams((SeasonEntity)cbxseason.SelectedItem);
            }
        }

        private void btnload_Click(object sender, RoutedEventArgs e)
        {
            if (cbxseason.SelectedIndex == -1)
                MessageBox.Show("Select Season", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            else if (cbxteams.SelectedIndex == -1)
                MessageBox.Show("Select Team", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            else
            {
                TeamEntity ob = (TeamEntity)cbxteams.SelectedItem;
                SeasonEntity os = (SeasonEntity)cbxseason.SelectedItem;


                if (ob.TeamName == "ALL" && os.SeasonName == "ALL")
                    vm.LoadForAll();
                else if (os.SeasonName == "ALL" && ob.TeamName != "ALL")
                    vm.LoadForAllSeasonForTeam(ob);
                else if (os.SeasonName != "ALL" && ob.TeamName == "ALL")
                    vm.LoadForSeason(os);
                else
                    vm.LoadForSeasonAndTeam(ob, os);
            }
        }
    }
}