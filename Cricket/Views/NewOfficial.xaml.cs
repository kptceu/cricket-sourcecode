﻿
using Cricket.DAL;
using Cricket.Model;
using Cricket.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cricket.Views
{
    /// <summary>
    /// Interaction logic for NewOfficial.xaml
    /// </summary>
    /// 

    public partial class NewOfficial : UserControl
    {
        public NewOfficialVM ObjOfficialVM;
        public NewOfficial()
        {
            try {
                InitializeComponent();
                ObjOfficialVM = new NewOfficialVM();
                this.DataContext = ObjOfficialVM;
                cbxGender.ItemsSource = Enum.GetValues(typeof(Gender)).Cast<Gender>();
                txt_zone.Text = User.Zone.ZoneName;
                btnnew_Click(null, null);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnSaveofficial_Click(object sender, RoutedEventArgs e)
        {
            try {
                if (cbxGender.SelectedItem == null)
                {
                    MessageBox.Show("Select Gender to continue..");
                    cbxGender.Focus();
                    return;
                }
                if (cbxGender.SelectedItem.ToString() == Gender.Male.ToString())

                    ObjOfficialVM.CurrentOfficial.Gender = Gender.Male.ToString();
                else
                    ObjOfficialVM.CurrentOfficial.Gender = Gender.Female.ToString();
                try
                {
                    foreach (OfficialEntity officialtovalidate in ObjOfficialVM.ActiveOfficials )
                    {
                        officialtovalidate.ValidateEntity();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                try
                {
                    List<EntityCollection> lsttosave = new List<EntityCollection>();
                    Database.NewSaveEntity<OfficialEntity>(ObjOfficialVM.ActiveOfficials, lsttosave);
                    using (var dbContext = Database.getDBContext())
                    {
                        Database.CommitChanges(lsttosave, dbContext);
                        ObjOfficialVM.LoadOfficials();
                    }

                    MessageBox.Show("Changes Saved", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void generateOfficialkeyid()
        {
            try {
                int count = (ObjOfficialVM.ActiveOfficials.Where(p => p.ZoneId == User.Zone.ZoneId).Count()) + 1;

                if (count < 10)
                {
                    ObjOfficialVM.CurrentOfficial.OfficialKeyId = User.Zone.ZoneNumber + "U0" + count;
                }
                else if (count >= 10 && count < 100)
                {
                    ObjOfficialVM.CurrentOfficial.OfficialKeyId = User.Zone.ZoneNumber + "U" + count;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }            

        private void btnnew_Click(object sender, RoutedEventArgs e)
        {
            try {
                if (ObjOfficialVM.CurrentOfficial==null || ObjOfficialVM.CurrentOfficial.IsNew == false)
                {
                    OfficialEntity ObjOfficial = Database.getNewEntity<OfficialEntity>();
                    ObjOfficialVM.CurrentOfficial = ObjOfficial;
                    ObjOfficialVM.ActiveOfficials.Add(ObjOfficial);
                    ObjOfficialVM.CurrentOfficial.CZone = User.Zone;
                    ObjOfficialVM.CurrentOfficial.Gender = Gender.Male.ToString();
                    generateOfficialkeyid();

                    btnSaveofficial.Visibility = Visibility.Visible;
                    txt_Name.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void gridofficialvalues_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try {
                if (gridofficialvalues.SelectedItem != null)
                {
                    if (ObjOfficialVM.CurrentOfficial == null || ObjOfficialVM.CurrentOfficial.IsNew == false)
                    {
                        OfficialEntity objgridselected = (OfficialEntity)gridofficialvalues.SelectedItem;
                        ObjOfficialVM.CurrentOfficial = objgridselected;
                        cbxGender.Text = ObjOfficialVM.CurrentOfficial.Gender.ToString();
                        txt_Name.Focus();
                        //txt_zone.Text = ObjOfficialVM.CurrentOfficial.CZone.ZoneName;
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cbxGender_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try {
                if (cbxGender.SelectedItem.ToString() == Gender.Male.ToString())

                    ObjOfficialVM.CurrentOfficial.Gender = Gender.Male.ToString();
                else
                    ObjOfficialVM.CurrentOfficial.Gender = Gender.Female.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ObjOfficialVM.CurrentOfficial.IsNew)
                {
                    ObjOfficialVM.ActiveOfficials.Remove(ObjOfficialVM.CurrentOfficial);
                }
                else
                {
                    Database.DeleteEntity<OfficialEntity>(ObjOfficialVM.CurrentOfficial);
                    ObjOfficialVM.ActiveOfficials.Remove(ObjOfficialVM.CurrentOfficial);
                }
                if (ObjOfficialVM.ActiveOfficials.Count > 0)
                {
                    ObjOfficialVM.CurrentOfficial = ObjOfficialVM.ActiveOfficials[0];
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ObjOfficialVM.CurrentOfficial != null && ObjOfficialVM.CurrentOfficial.IsNew)
                {

                    ObjOfficialVM.ActiveOfficials.Remove(ObjOfficialVM.CurrentOfficial);
                    ObjOfficialVM.CurrentOfficial = null;
                    //if (gridlocationvalues.SelectedItems != null)
                     //   gridlocationvalues_SelectionChanged(null, null);
                }
                else if (ObjOfficialVM.CurrentOfficial != null && ObjOfficialVM.CurrentOfficial.IsNew == false)
                {
                    OfficialEntity  objLocationfromDB = Database.GetEntity<OfficialEntity >(ObjOfficialVM.CurrentOfficial.OfficialId );
                    ObjOfficialVM.ActiveOfficials.Remove(ObjOfficialVM.CurrentOfficial);

                    ObjOfficialVM.CurrentOfficial = objLocationfromDB;
                    ObjOfficialVM.ActiveOfficials.Add(objLocationfromDB);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
    }


   
}
