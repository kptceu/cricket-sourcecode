﻿
using Cricket.Model;
using Cricket.ViewModel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cricket.Views
{
    /// <summary>
    /// Interaction logic for TeamStats.xaml
    /// </summary 
    /// 

    public partial class TeamStats : UserControl
    {

        TeamStatsVM ObjVM ;

        public TeamStats()
        {
            InitializeComponent();
            ObjVM = new TeamStatsVM(User.Zone, User.Season, User.Division);
            this.DataContext = ObjVM;
        }

        

        private void gridteamdetails_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            gridteamdetails.UnselectAll();
        }

        private void btnexportteamstats_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ObjVM.PlayedMatchStats.Count > 0)
                {
                    ObjVM.GenerateExcel((TeamEntity)cbxteam.SelectedItem);
                }
                else
                {
                    MessageBox.Show("No Data To Export");
                }

            }
            catch(Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void gridteamdetails2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            gridteamdetails2.UnselectAll();
        }

        private void cbxteam_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                if (cbxteam.SelectedItem != null)
                {
                    if (User.Division.NumberOfInningsPerSide == 2)
                    {
                        TeamEntity ObjTeam = (TeamEntity)cbxteam.SelectedItem; ;
                        ObjVM.GetPlayedMatchStats(ObjTeam, User.Zone, User.Season, User.Division);
                        gridteamdetails.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        TeamEntity ObjTeam = (TeamEntity)cbxteam.SelectedItem; ;
                        ObjVM.GetPlayedMatchStats(ObjTeam, User.Zone, User.Season, User.Division);
                        gridteamdetails2.Visibility = Visibility.Visible;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
    }
}
