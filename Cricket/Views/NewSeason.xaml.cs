﻿
using Cricket.DAL;
using Cricket.Model;
using Cricket.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;



namespace Cricket.Views
{
    /// <summary>
    /// Interaction logic for NewSeason.xaml
    /// </summary>
    public partial class NewSeason : UserControl
    {
        SeasonEntityManager  objseasonentitymanager = new SeasonEntityManager();
        public NewSeasonVM ObjNewSeasonVM = new NewSeasonVM();
        public NewSeason()
        {
            InitializeComponent();
            this.DataContext = ObjNewSeasonVM;
        }

        private void btnaddseason_Click(object sender, RoutedEventArgs e)
        {
            grpseasondetails.Visibility = Visibility.Visible;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (Startdatepick.SelectedDate == null)
            {
                MessageBox.Show("Select Start Date", "Info", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if (EndDatePick.SelectedDate == null)
            {
                MessageBox.Show("Select End Date", "Info", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {
                try
                {
                    int i = 0;
                    using (var dbContext = Database.getDBContext())
                    {
                        i = objseasonentitymanager.getRecentSeason(dbContext);
                    }
                    i = i + 1;
                    ObjNewSeasonVM.ObjSeason.SeasonName = i.ToString();
                    ObjNewSeasonVM.SaveSeason();                 
                    MessageBox.Show("Season With Name " + ObjNewSeasonVM.ObjSeason.SeasonName + " With StartDate " + ObjNewSeasonVM.ObjSeason.StartDate + " And End Date " + ObjNewSeasonVM.ObjSeason.EndDate + " Added Successfully", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
                    ObjNewSeasonVM.LoadSeasons();
                    ObjNewSeasonVM.ObjSeason = Database.getNewEntity<SeasonEntity>();
                    grpseasondetails.Visibility = Visibility.Hidden;

                    User.HomePageVM.getSeasonList();
                }
                catch (Exception es)
                {
                    MessageBox.Show(es.InnerException.ToString());
                }
            }
        }
    }
}
    