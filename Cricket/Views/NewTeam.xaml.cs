﻿
//using Cricket.BLL;
using Cricket.DAL;
using Cricket.Model;
using Cricket.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cricket.Views
{
    /// <summary>
    /// Interaction logic for NewTeam.xaml
    /// </summary>
    public partial class NewTeam : UserControl
    {

        private bool blnEditable;
        public NewTeamVM ObjTeamVM;
        public NewTeam()
        {
            InitializeComponent();
            ObjTeamVM = new NewTeamVM(); 
            this.DataContext = ObjTeamVM;
            this.cbxzone.SelectedValue    =((ZoneEntity) User.Zone).ZoneId  ;
            using (var Context = Database.getDBContext())
            {
                ObjTeamVM.getTeamsinCurrentZone(Context, User.Zone);
            }
                cbxzone.IsEnabled = false;
            // if (ObjTeamVM.ActiveZones.Count > 0)
            //    cbxzone.SelectedIndex = 0;
            btnnew_Click(null, null);
          }
        
        private void generateTeamkeyid()
        {
            try { 
            if (cbxzone.SelectedValue.ToString() != Guid.Empty.ToString())
            {
                ZoneEntity objzone = User.Zone ;//  LstZones.Where(p => p.ZoneId.ToString() == 

                int count = ObjTeamVM.ActiveTeams.Where(p => p.ZoneId.ToString() == cbxzone.SelectedValue.ToString()).Count();
                count += 1;
                if (count < 10)
                {
                    ObjTeamVM.CurrentTeam.TeamKeyId = objzone.ZoneNumber + "T00" + count;
                }
                else if (count >= 10 && count < 100)
                {
                    ObjTeamVM.CurrentTeam.TeamKeyId = objzone.ZoneNumber + "T0" + count;
                }
                else
                {
                    ObjTeamVM.CurrentTeam.TeamKeyId = objzone.ZoneNumber + "T" + count;
                }

                }
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        // Method to load dropdowns


        private void btnsaveteam_Click(object sender, RoutedEventArgs e)
        {
            
            foreach (TeamEntity TeamtoValidate in ObjTeamVM.TeamsinCurrentZone )
            {
                try
                {
                    TeamtoValidate.ValidateEntity();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
            //using (ISession session = Database.getSessionFactory().OpenSession())
            //{
            //    ITransaction transaction = session.BeginTransaction();

                try
                {
                List<EntityCollection> lsttosave = new List<EntityCollection>();

                Database.NewSaveEntity<TeamEntity>(ObjTeamVM.ActiveTeams, lsttosave);
                using (var dbContext = Database.getDBContext())
                {
                    Database.CommitChanges(lsttosave, dbContext);
                    ObjTeamVM.LoadTeams();
                    blnEditable = true;
                }
                //Database.SaveEntityList<Team>(ObjTeamVM.ActiveTeams, session);
                //transaction.Commit();
                //session.Flush();
                //session.Close();
                MessageBox.Show("Teams Saved Successfully", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                catch (Exception ex)
                {
                 MessageBox.Show(ex.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
                }
            //}
        }
           
        

        private void btnnew_Click(object sender, RoutedEventArgs e)
        {
            if (ObjTeamVM.CurrentTeam==null || ObjTeamVM.CurrentTeam.IsNew == false)
            {
                ObjTeamVM.CurrentTeam = Database.getNewEntity<TeamEntity>();
                ObjTeamVM.CurrentTeam.CZone = (ZoneEntity)cbxzone.SelectedItem;
                ObjTeamVM.ActiveTeams.Add(ObjTeamVM.CurrentTeam);
                ObjTeamVM.TeamsinCurrentZone.Add(ObjTeamVM.CurrentTeam);

                generateTeamkeyid();
                newgreen();
                blnEditable = false;
                txt_teamname.Focus();
            }
        }



        private void gridteamvalues_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (ObjTeamVM.CurrentTeam == null || ObjTeamVM.CurrentTeam.IsNew == false)
                {
                    if (gridteamvalues.SelectedItem != null)
                    {
                        TeamEntity objgridselected = (TeamEntity)gridteamvalues.SelectedItem;
                        // using (ISession session = Database.getSessionFactory().OpenSession())
                        //{
                        if (objgridselected.IsNew)

                            ObjTeamVM.CurrentTeam = objgridselected;
                        // gridteamvalues.IsEnabled = false;
                        else
                        {
                            ZoneEntity ObjZone = Database.GetEntity<ZoneEntity>(objgridselected.ZoneId);
                            objgridselected.CZone = ObjZone;
                            ObjTeamVM.CurrentTeam = objgridselected;

                        }
                        txt_teamname.Focus();
                        //cbxzone.Text = ObjTeamVM.CurrentTeam.CZone.ZoneName;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

     private void newgreen()
        {
            txt_teamkeyid.BorderBrush = Brushes.Green;
            txt_teamname.BorderBrush = Brushes.Green;
            txt_teamintname.BorderBrush = Brushes.Green;
            txt_teamname.BorderBrush = Brushes.Green;
        }
     private void neworange()
     {
         txt_teamkeyid.BorderBrush = Brushes.Orange;
         txt_teamname.BorderBrush = Brushes.Orange;
         txt_teamintname.BorderBrush = Brushes.Orange;
         txt_teamname.BorderBrush = Brushes.Orange;
     }

        private void cbxzone_Selected(object sender, SelectionChangedEventArgs e)
        {
            try { 
            if (ObjTeamVM.CurrentTeam.CZone != null)
            {
                if (ObjTeamVM.CurrentTeam.CZone.ZoneId != ((ZoneEntity)cbxzone.SelectedItem).ZoneId)
                {
                    ObjTeamVM.CurrentTeam.CZone = (ZoneEntity)cbxzone.SelectedItem;
                    
                    ObjTeamVM.CurrentTeam.CZone = (ZoneEntity)cbxzone.SelectedItem;
                  
                    generateTeamkeyid();
                    btnsaveteam.Visibility = Visibility.Visible;
                  
                    txt_teamname.Text = "";
                    txt_teamintname.Text = "";
                }
            }
            else
            {
                ObjTeamVM.CurrentTeam.CZone = (ZoneEntity)cbxzone.SelectedItem;
                List<TeamEntity> lstfilterteam = ObjTeamVM.ActiveTeams.Where(p => p.ZoneId.ToString() == cbxzone.SelectedValue.ToString()).ToList<TeamEntity>();
                ObjTeamVM.CurrentTeam.CZone = (ZoneEntity)cbxzone.SelectedItem;
              
            }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            ObjTeamVM.CurrentTeam.Indicator = RecordStatus.DeletedNotSaved;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            try { 
            if (ObjTeamVM.CurrentTeam != null && ObjTeamVM.CurrentTeam.IsNew)
            {
                
                ObjTeamVM.ActiveTeams.Remove(ObjTeamVM.CurrentTeam);
                ObjTeamVM.TeamsinCurrentZone.Remove(ObjTeamVM.CurrentTeam);
                ObjTeamVM.CurrentTeam = null;
                if (gridteamvalues.SelectedItems != null)
                    gridteamvalues_SelectionChanged(null, null);
            }
            else if(ObjTeamVM.CurrentTeam !=null && ObjTeamVM.CurrentTeam.IsNew==false)
                {
                TeamEntity objTeamfromDB = Database.GetEntity<TeamEntity>(ObjTeamVM.CurrentTeam.TeamId);
                ObjTeamVM.ActiveTeams.Remove(ObjTeamVM.CurrentTeam);
                ObjTeamVM.TeamsinCurrentZone.Remove(ObjTeamVM.CurrentTeam);
                ObjTeamVM.CurrentTeam = objTeamfromDB;
                ObjTeamVM.ActiveTeams.Add(objTeamfromDB);
                ObjTeamVM.TeamsinCurrentZone.Add(objTeamfromDB);
            }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
    }
}
