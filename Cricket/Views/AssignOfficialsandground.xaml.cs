﻿using Cricket.DAL;
using Cricket.Model;
using Cricket.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace Cricket.Views
{
    /// <summary>
    /// Interaction logic for AssignOfficialsandground.xaml
    /// </summary>
    public partial class AssignOfficialsandground : UserControl
    {
        ScheduleMatchesVM vm = new ScheduleMatchesVM();
        public AssignOfficialsandground()
        {
            InitializeComponent();
            this.DataContext = vm;
            loadvalues();
        }

        private void loadvalues()
        {
            if (vm.ObjFixture.GroupName == "Semi-Final" || vm.ObjFixture.GroupName == "Final")
            {
                finalLabel1.Visibility = Visibility.Visible;
                FinalLabel2.Visibility = Visibility.Visible;
                cbxTeam1.Visibility = Visibility.Visible;
                cbxTeam2.Visibility = Visibility.Visible;
                txtTeam1.Visibility = Visibility.Hidden;
                txtTeam2.Visibility = Visibility.Hidden;
                lblteam1league.Visibility = Visibility.Hidden;
                lblteam2league.Visibility = Visibility.Hidden;
                cbxTeam1.SelectedValue = vm.ObjFixture.TeamOneId;
                cbxTeam2.SelectedValue = vm.ObjFixture.TeamTwoId;
            }

            else
            {
                cbxTeam1.Visibility = Visibility.Hidden;
                cbxTeam2.Visibility = Visibility.Hidden;
                finalLabel1.Visibility = Visibility.Hidden;
                FinalLabel2.Visibility = Visibility.Hidden;
                lblteam1league.Visibility = Visibility.Visible;
                lblteam2league.Visibility = Visibility.Visible;
                txtTeam1.Visibility = Visibility.Visible;
                txtTeam2.Visibility = Visibility.Visible;
            }

            if (User.Division.NumberOfDays == 1)
            {
                enddate.Visibility = Visibility.Hidden;
                lbltodate.Visibility = Visibility.Hidden;
            }
            else
            {
                enddate.Visibility = Visibility.Visible;
                lbltodate.Visibility = Visibility.Visible;
            }
            PlayedMatchEntityManager objplayedem = new PlayedMatchEntityManager();
            if(objplayedem.GetByFixtureId(Database.getDBContext(),User.Fixture.FixtureId)!=null)
            {
                startDate.IsEnabled = false;
                enddate.IsEnabled = false;
                cbxVenue.IsEnabled = false;
                cbxumpire1.IsEnabled = false;
                cbxumpire2.IsEnabled = false;
                cbxscorer.IsEnabled = false;
                btnsaveschedule.IsEnabled = false;
                btnaddnewground.IsEnabled = false;
                btnaddnewofficial.IsEnabled = false;
            }
            LoadSelectedValues();
        }

        public void LoadSelectedValues()
        {
            if (vm.ObjFixture.UmpireOne != null)
                cbxumpire1.SelectedValue = vm.ObjFixture.UmpireOne.OfficialId;
            if (vm.ObjFixture.UmpireTwo != null)
                cbxumpire2.SelectedValue = vm.ObjFixture.UmpireTwo.OfficialId;
            if (vm.ObjFixture.Scorer != null)
                cbxscorer.SelectedValue = vm.ObjFixture.Scorer.OfficialId;
            if (vm.ObjFixture.Location != null)
                cbxVenue.SelectedValue = vm.ObjFixture.Location.LocationId;
        }

        private void btnsaveschedule_Click(object sender, RoutedEventArgs e)
        {
            try {
                vm.Save();
            }
            catch(Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void dropdownofficialduplicatesremove(ComboBox cbx1, ComboBox cbx2, ComboBox cbxsender)
        {
            try
            {
                if (cbx1.SelectedIndex != -1)
            {
                vm.LstOfficial.Where(p => p.OfficialId.ToString() == cbx1.SelectedValue.ToString()).ToList().ForEach(x =>
                {
                    x.AllowedColor = "#FFDB99";
                    x.IsSelectable = false;
                });
            }
            if (cbx2.SelectedIndex != -1)
            {
                vm.LstOfficial.Where(p => p.OfficialId.ToString() == cbx2.SelectedValue.ToString()).ToList().ForEach(x =>
                {
                    x.AllowedColor = "#FFDB99";
                    x.IsSelectable = false;
                });
                }
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void cbxumpire1_DropDownOpened(object sender, EventArgs e)
        {
            if (startDate.SelectedDate == null)
            {
                MessageBox.Show("Select StartDate");
            }
            else
            {
                dropdownofficialduplicatesremove(cbxumpire2, cbxscorer, cbxumpire1);
            }
        }

        private void cbxumpire2_DropDownOpened(object sender, EventArgs e)
        {
            if (startDate.SelectedDate == null)
            {
                MessageBox.Show("Select StartDate");
            }
            else
            {
                dropdownofficialduplicatesremove(cbxumpire1, cbxscorer, cbxumpire2);
            }
        }

        private void cbxscorer_DropDownOpened(object sender, EventArgs e)
        {
            if (startDate.SelectedDate == null)
            {
                MessageBox.Show("Select StartDate");
            }
            else
            {
               dropdownofficialduplicatesremove(cbxumpire2, cbxumpire1, cbxscorer);
            }
        }

        private void startDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (User.Division.NumberOfDays==2)
                {
                    if (startDate.SelectedDate != null)
                    {
                        DateTime enddatecheck = DateTime.Parse(startDate.SelectedDate.ToString()).AddDays(1);
                        //  fromdate.AddDays(1);
                        enddate.SelectedDate = enddatecheck;
                        loadavailablegroundandofficials();
                    }
                }
                else
                {
                    loadavailablegroundandofficialsotherdiv();
                }
            }
            catch(Exception es)
            {
                MessageBox.Show(es.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }            
        }

        private void cbxVenue_DropDownOpened(object sender, EventArgs e)
        {
            if (startDate.SelectedDate == null)
            {
                MessageBox.Show("Select StartDate");
            }
        }

        private void loadavailablegroundandofficials()
        {
            try { 
            if (startDate.SelectedDate != null)
            {
                ObservableCollection<FixtureEntity> LstFixture = User.lsTFixture;

                LstFixture.Where(p =>(p.FixtureId!=User.Fixture.FixtureId && (p.FromDate == DateTime.Parse(startDate.SelectedDate.ToString()) || p.ToDate == DateTime.Parse(startDate.SelectedDate.ToString())
                || p.ToDate == DateTime.Parse(enddate.SelectedDate.ToString()) || p.FromDate == DateTime.Parse(enddate.SelectedDate.ToString()))) && p.FixtureId != vm.ObjFixture.FixtureId).ToList().ForEach(obj =>
                 {
                     vm.LstLocation.Where(p => p.LocationId == obj.LocationId).ToList().ForEach(x =>
                         {
                             x.AllowedColor = "#FFDB99";
                             x.IsSelectable = false;
                         });

                     vm.LstOfficial.Where(p => p.OfficialId == obj.UmpireOneId || p.OfficialId == obj.UmpireTwoId || p.OfficialId == obj.ScorerId).ToList().ForEach(x =>
                         {
                             x.AllowedColor = "#FFDB99";
                             x.IsSelectable = false;
                         });
                 });
                }
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }

        }

        private void loadavailablegroundandofficialsotherdiv()
        {
            try
            {
                if (startDate.SelectedDate != null)
                {
                    ObservableCollection<FixtureEntity> LstFixture = User.lsTFixture;


                    //List<FixtureEntity> lst1 = LstFixture.Where(p =>( p.FromDate == DateTime.Parse(startDate.SelectedDate.ToString()) || p.ToDate == DateTime.Parse(startDate.SelectedDate.ToString())) && p.FixtureId != vm.ObjFixture.FixtureId).ToList();

                    LstFixture.Where(p => (p.FromDate == DateTime.Parse(startDate.SelectedDate.ToString()) || p.ToDate == DateTime.Parse(startDate.SelectedDate.ToString())) && p.FixtureId != vm.ObjFixture.FixtureId).ToList().ForEach(obj =>
                  {
                      vm.LstLocation.Where(p => p.LocationId == obj.LocationId).ToList().ForEach(x =>
                        {
                            x.AllowedColor = "#FFDB99";
                            x.IsSelectable = false;
                        });

                      vm.LstOfficial.Where(p => p.OfficialId == obj.UmpireOneId || p.OfficialId == obj.UmpireTwoId || p.OfficialId == obj.ScorerId).ToList().ForEach(x =>
                      {
                          x.AllowedColor = "#FFDB99";
                          x.IsSelectable = false;
                      });
                  });
                }
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        public void AddGround()
        {
            int count = (vm.LstLocation.Count) + 1;

            string groundkeyid = "";
            if (count < 10)
            {
                groundkeyid = "7G0" + count;
            }
            else if (count >= 10 && count < 100)
            {
                groundkeyid = "7G" + count;
            }

            LocationEntity objlocation = Database.getNewEntity<LocationEntity>();
            objlocation.StadiumName = txtnewgroundname.Text;
            objlocation.GroundKeyId = groundkeyid;
            objlocation.CZone = User.Zone;

            List<EntityCollection> lsttosave = new List<EntityCollection>();
            Database.NewSaveEntity<LocationEntity>(objlocation, lsttosave);
            using (var dbContext = Database.getDBContext())
                Database.CommitChanges(lsttosave, dbContext);
            txtnewgroundname.Text = "";
            MessageBox.Show("Ground with Name " + objlocation.StadiumName + " Added");
            vm.LstLocation = User.LoadLocations();
            LoadSelectedValues();
        }

        private void btnaddnewground_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtnewgroundname.Text == "")
                {
                    MessageBox.Show("Enter Ground Name");
                }
                else
                {
                    if ((vm.checkGround(txtnewgroundname.Text)) == true)
                    {
                        AddGround();
                    }
                    else
                    {
                        MessageBoxResult res = MessageBox.Show("Ground With The Same Name Already Exists Are You Sure You Want to Add New Ground With The Name "+ txtnewgroundname.Text, "Info", MessageBoxButton.YesNo, MessageBoxImage.Information);
                        if (res == MessageBoxResult.Yes)
                        {
                            AddGround();
                        }
                    }
                }
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        public void AddOfficial()
        {
            int count = (vm.LstOfficial.Count) + 1;

            string officialkeyid = "";
            if (count < 10)
            {
                officialkeyid = "7U0" + count;
            }
            else if (count >= 10 && count < 100)
            {
                officialkeyid = "7U" + count;
            }

            OfficialEntity objOfficial = Database.getNewEntity<OfficialEntity>();
            objOfficial.Name = txtnewofficialname.Text;
            objOfficial.OfficialKeyId = officialkeyid;
            objOfficial.Gender = "Male";
            objOfficial.DateOfBirth = DateTime.Now;
            objOfficial.CZone = User.Zone;
            List<EntityCollection> lsttosave = new List<EntityCollection>();
            Database.NewSaveEntity<OfficialEntity>(objOfficial, lsttosave);
            using (var dbContext = Database.getDBContext())
                Database.CommitChanges(lsttosave, dbContext);
            txtnewofficialname.Text = "";
            MessageBox.Show("Official with Name " + objOfficial.Name + " Added");
            vm.LstOfficial = User.LoadOfficials();
            LoadSelectedValues();
        }

        private void btnaddnewofficial_Click(object sender, RoutedEventArgs e)
        {
            try {
                if (txtnewofficialname.Text == "")
                {
                    MessageBox.Show("Enter Official Name");
                }
                else
                {
                    if ((vm.checkofficial(txtnewofficialname.Text)) == true)
                    {
                        AddOfficial();
                    }
                    else
                    {
                       MessageBoxResult res=MessageBox.Show("Official With The Same Name Already Exists,Are You Sure You Want to Add New Official With The Name "+txtnewofficialname.Text, "Info", MessageBoxButton.YesNo, MessageBoxImage.Information);
                        if (res == MessageBoxResult.Yes)
                        {
                            AddOfficial();
                        }
                    }
                }
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnscorenow_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PlayedMatchEntityManager objplayeem = new PlayedMatchEntityManager();
                PlayedMatchEntity obj = objplayeem.GetByFixtureId(Database.getDBContext(), User.Fixture.FixtureId);

                if (User.Fixture.FromDate != null)
                {
                    if (obj != null)
                    {
                        User.ObjPlayedMatch = obj;
                        MatchScoring page = new MatchScoring();
                        this.Content = page;
                    }
                    else
                    {
                        SelectPlayersForMatch page = new SelectPlayersForMatch();
                        this.Content = page;
                    }
                }
                else
                {
                    MessageBox.Show("Schedule Match First");
                }
            }
            catch (Exception es)
            {

                MessageBox.Show(es.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void cbxVenue_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbxVenue.SelectedItem!=null)
                {
                if (((LocationEntity)cbxVenue.SelectedItem).IsSelectable == false)
                {
                    cbxVenue.SelectedItem = null;
                }
            }
        }

        private void cbxofficial_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cbx = (ComboBox)sender;
            if (cbx.SelectedItem != null)
            {
                if (((OfficialEntity)cbx.SelectedItem).IsSelectable == false)
                {
                    cbx.SelectedItem = null;
                }
            }
        }
    }
}



   
