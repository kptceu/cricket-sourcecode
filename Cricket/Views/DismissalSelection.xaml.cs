﻿
using Cricket.Model;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cricket.Views
{
    /// <summary>
    /// Interaction logic for DismissalSelection.xaml
    /// </summary>
    public partial class DismissalSelection : ModernWindow
    {
        public ObservableCollection<PlayerEntity> LstPlayersBowling = new ObservableCollection<PlayerEntity>();
        public PlayerEntity ObjKeeper = new PlayerEntity();

        public DismissalSelection()
        {
            InitializeComponent();
        }

        public void LoadPlayers(PlayerEntity Obj,ObservableCollection<PlayerEntity> Lst2,PlayerEntity ObjKeep)
        {
            ObjKeeper = ObjKeep;
            txtbatsmanname.Text = Obj.PlayerName;
            LstPlayersBowling = new ObservableCollection<PlayerEntity>(Lst2);
            cbxbowler.ItemsSource = LstPlayersBowling;
            cbxfielder.ItemsSource = LstPlayersBowling;
        }

        private void btnsavedismissal_Click(object sender, RoutedEventArgs e)
        {
            try { 
            if(cbxdismissal.SelectedIndex==-1)
            {
                MessageBox.Show("Select Dismissal");
            }
            else if (cbxdismissal.SelectedIndex == 0)
            {
               if(cbxbowler.SelectedIndex!=-1 && cbxfielder.SelectedIndex!=-1)               
                   User.Dismissal = "c " + cbxfielder.Text + " b " + cbxbowler.Text;               
               else if(cbxfielder.SelectedIndex!=-1)               
                   User.Dismissal = "c " + cbxfielder.Text;               
               else
                    User.Dismissal = "c";
            }
            else if (cbxdismissal.SelectedIndex == 1)
            {
                if (cbxbowler.SelectedIndex != -1)
                    User.Dismissal = "c&b " + cbxbowler.Text;
                else
                    User.Dismissal = "c&b";
            }
            else if (cbxdismissal.SelectedIndex == 2)
            {
                if (cbxbowler.SelectedIndex != -1)
                    User.Dismissal= "b " + cbxbowler.Text;
                else
                    User.Dismissal = "b";
            }
            else if (cbxdismissal.SelectedIndex == 3)
            {
                if (cbxbowler.SelectedIndex != -1 && cbxfielder.SelectedIndex != -1)
                    User.Dismissal = "st " + cbxfielder.Text + " b " + cbxbowler.Text;
                else if (cbxfielder.SelectedIndex != -1)
                    User.Dismissal = "st " + cbxfielder.Text;
                else
                    User.Dismissal = "st";
            }
            else if (cbxdismissal.SelectedIndex == 4)
            {
                if (cbxfielder.SelectedIndex != -1)
                    User.Dismissal = "Run Out " + cbxfielder.Text;
                else
                    User.Dismissal = "Run Out";
            }
            else if (cbxdismissal.SelectedIndex == 5)
            {
                if (cbxbowler.SelectedIndex != -1)
                    User.Dismissal = "Lbw " + cbxbowler.Text;
                else
                    User.Dismissal = "Lbw";
            }
            else if (cbxdismissal.SelectedIndex == 6)
            {
                User.Dismissal = "Retd Hurt";
            }
            else if (cbxdismissal.SelectedIndex == 7)
            {
                if (cbxbowler.SelectedIndex != -1)
                    User.Dismissal = "Hit Wkt " + cbxbowler.Text;
                else
                    User.Dismissal = "Hit Wkt";
            }
            else
            {
                User.Dismissal = "Not Out";
            }
            this.Close();
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        public static class dismissal
        {
            public static string outdesc;
        }

        private void btncancel_Click(object sender, RoutedEventArgs e)
        {
            dismissal.outdesc = "";
            this.Close();
        }

        private void cbxdismissal_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try { 
            if(cbxdismissal.SelectedIndex!=-1)
            {
                if(cbxdismissal.SelectedIndex==0)
                {
                    cbxbowler.IsEnabled = true;
                    cbxfielder.IsEnabled = true;
                }
                else if(cbxdismissal.SelectedIndex==1)
                {
                    cbxbowler.IsEnabled = true;
                    cbxfielder.IsEnabled = false;
                }
                else if(cbxdismissal.SelectedIndex==2)
                {
                    cbxbowler.IsEnabled = true;
                    cbxfielder.IsEnabled = false;
                }
                else if(cbxdismissal.SelectedIndex==3)
                {
                    cbxbowler.IsEnabled = true;
                    cbxfielder.IsEnabled = true;
                    if(ObjKeeper!=null)
                    cbxfielder.SelectedValue = ObjKeeper.PlayerId;
                }
                else if(cbxdismissal.SelectedIndex==4)
                {
                    cbxfielder.IsEnabled = true;
                    cbxbowler.IsEnabled = false;
                }
                else if(cbxdismissal.SelectedIndex==5)
                {
                    cbxbowler.IsEnabled = true;
                    cbxfielder.IsEnabled = false;
                }
                else if(cbxdismissal.SelectedIndex==6)
                {
                    cbxbowler.IsEnabled = false;
                    cbxfielder.IsEnabled = false;
                }
                else
                {
                    cbxbowler.IsEnabled = false;
                    cbxfielder.IsEnabled = false;
                }
                }
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
    }
}
