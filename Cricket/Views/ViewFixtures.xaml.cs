﻿//using Cricket.DAL;
//using Cricket.Database;
//using Cricket.DAL;
//using Cricket.ReportData;
//using Microsoft.Reporting.WinForms;
using Cricket.DAL;
using Cricket.Model;
using Cricket.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace Cricket.Views
{
    /// <summary>
    /// Interaction logic for ViewFixtures.xaml
    /// </summary>
    public partial class ViewFixtures : UserControl
    {
        DataTable dtfixtures = new DataTable();
       public bool stat = true;
        public ViewFixtureVM ObjViewFixtureVM;
        public ViewFixtures()
        {
            InitializeComponent();
            ObjViewFixtureVM = new ViewFixtureVM();
            this.DataContext = ObjViewFixtureVM;
        }

        private void btnSchedule_Click(object sender, RoutedEventArgs e)
        {
            Button btnschedule = (Button)sender;
            User.Fixture = ObjViewFixtureVM.LstFixture.Where(p => p.FixtureId.ToString() == btnschedule.CommandParameter.ToString()).SingleOrDefault();

            AssignOfficialsandground page = new AssignOfficialsandground();
            this.Content = page;
        }

        public static class passfixtureid
        {
            public static string fixtureid;
        }

        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            try {
                ObjViewFixtureVM.Search(txtSearch.Text.ToUpper());
            }
            catch(Exception es)
            {
                MessageBox.Show(es.Message,"Info",MessageBoxButton.OK,MessageBoxImage.Information);
            }
        }

        private void btnviewscorecard_Click(object sender, RoutedEventArgs e)
        {
            try { 
            Button btn = (Button)sender;
            User.Fixture = Database.GetEntity<FixtureEntity>(Guid.Parse(btn.CommandParameter.ToString()));
            PlayedMatchEntityManager objplayeem = new PlayedMatchEntityManager();
            PlayedMatchEntity obj = objplayeem.GetByFixtureId(Database.getDBContext(), User.Fixture.FixtureId);

            if (User.Fixture.FromDate != null)
            {
                if (obj != null && !(obj.IsWalkOverForTeamOne == true || obj.IsWalkOverForTeamTwo==true))
                {
                    User.ObjPlayedMatch = obj;
                    MatchScoring page = new MatchScoring();
                    this.Content = page;                    
                }
                else
                {
                    SelectPlayersForMatch page = new SelectPlayersForMatch();
                    this.Content = page;
                }
            }
            else
            {
                MessageBox.Show("Schedule Match First");
            }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }

        }

        private void btnexportfixtures_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ObjViewFixtureVM.LstFixture.Count  > 0)
                {
                    ObjViewFixtureVM.GenerateExcel();
                }
                else
                {
                    MessageBox.Show("No Data To Export");
                }

            }
            catch(Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
    }
}
