﻿
using Cricket.Model;
using Cricket.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cricket.Views
{
    /// <summary>
    /// Interaction logic for PointsTable.xaml
    /// </summary>
    public partial class PointsTable : UserControl
    {
        PointsTableVM vm = new PointsTableVM();

        public PointsTable()
        {
            InitializeComponent();
            this.DataContext = vm;
            if (vm.Groupcount ==1)
            {
                gridgroupapoints.Visibility = Visibility.Visible;
                lblgroupa.Visibility = Visibility.Visible;
            }
            else if(vm.Groupcount==2)

            {
                gridgroupapoints.Visibility = Visibility.Visible;
                gridgroupbpoints.Visibility = Visibility.Visible;
                lblgroupa.Visibility = Visibility.Visible;
                lblgroupb.Visibility = Visibility.Visible;

            }
            else if (vm.Groupcount == 3)
            {
                gridgroupapoints.Visibility = Visibility.Visible;
                gridgroupbpoints.Visibility = Visibility.Visible;
                gridgroupcpoints.Visibility = Visibility.Visible;
                lblgroupa.Visibility = Visibility.Visible;
                lblgroupb.Visibility = Visibility.Visible;
                lblgroupc.Visibility = Visibility.Visible;
            }
            else if (vm.Groupcount == 4)
            {
                gridgroupapoints.Visibility = Visibility.Visible;
                gridgroupbpoints.Visibility = Visibility.Visible;
                gridgroupcpoints.Visibility = Visibility.Visible;
                gridgroupdpoints.Visibility = Visibility.Visible;
                lblgroupa.Visibility = Visibility.Visible;
                lblgroupb.Visibility = Visibility.Visible;
                lblgroupc.Visibility = Visibility.Visible;
                lblgroupd.Visibility = Visibility.Visible;

            }
        }
        
        private void btnexportpointstable_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (vm.ExportPointsTabletoExcel())
                    MessageBox.Show("File Exported");


            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message );
            }
        }

      
    }
}
