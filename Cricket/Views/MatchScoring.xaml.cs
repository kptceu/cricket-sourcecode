﻿using Cricket.DAL;
using Cricket.Model;
using Cricket.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Cricket.Views
{
    /// <summary>
    /// Interaction logic for MatchScoring.xaml
    /// </summary>
    public partial class MatchScoring : UserControl
    {
        MatchScoringVM vm = new MatchScoringVM();
        private DispatcherTimer dispatcherTimer;
        PlayedMatchDetailEntity objselectedbat = new PlayedMatchDetailEntity();
        PlayedMatchDetailEntity objselectedbowl = new PlayedMatchDetailEntity();

        PlayedMatchDetailEntity objdismissed = new PlayedMatchDetailEntity();

        BrushConverter bc = new BrushConverter();

        bool ismanualeditcommit = false;

        public MatchScoring()
        {
            InitializeComponent();
            this.DataContext = vm;
            loaddefaultvalues();
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            //Things which happen after 1 timer interval
            lblsavestatus.Visibility = Visibility.Collapsed;

            //Disable the timer
            dispatcherTimer.IsEnabled = false;
        }

        public void loaddefaultvalues()
        {
            dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 2);
            if (vm.ObjPlayedMatch.CompleteStatus == true)
            {
                gridteamonefirstinnings.IsEnabled = false;
                gridteamtwofirstinnings.IsEnabled = false;
                gridteamonesecondinnings.IsEnabled = false;
                gridteamtwosecondinnings.IsEnabled = false;
                gridbowlingteamonefirstinnings.IsEnabled = false;
                gridbowlingteamonesecondinnings.IsEnabled = false;
                gridbowlingteamtwofirstinnings.IsEnabled = false;
                gridbowlingteamtwosecondinnings.IsEnabled = false;

                btnsave.IsEnabled = false;
            }
            if (User.Division.NumberOfInningsPerSide == 1)
            {
                btnteam12ndinnings.Visibility = Visibility.Hidden;
                btnteam22ndinnings.Visibility = Visibility.Hidden;
                chkt1secondinningsnotplayed.Visibility = Visibility.Hidden;
                chkt1secondinningsnotplayed.IsChecked = true;
                chkt2secondinningsnotplayed.Visibility = Visibility.Hidden;
                chkt2secondinningsnotplayed.IsChecked = true;
            }
            else
            {
                btnteam12ndinnings.Visibility = Visibility.Visible;
                btnteam22ndinnings.Visibility = Visibility.Visible;
                chkt1secondinningsnotplayed.Visibility = Visibility.Visible;
                chkt1secondinningsnotplayed.IsChecked = false;
                chkt2secondinningsnotplayed.Visibility = Visibility.Visible;
                chkt2secondinningsnotplayed.IsChecked = false;

            }

            if(vm.ObjPlayedMatch.CompleteStatus==false)
            {
                btnedit.Background = (Brush)bc.ConvertFrom("#336699");
                btnedit.Foreground = Brushes.White;
            }

            txt_toss.Text = "Won By " + vm.ObjPlayedMatch.TossWonBy + " And elected to " + vm.ObjPlayedMatch.ElectedTo;
        }

        private void btnteam11stinnings_Click(object sender, RoutedEventArgs e)
        {
            try {
                btnteam11stinnings.Background = (Brush)bc.ConvertFrom("#336699");
                btnteam11stinnings.Foreground = Brushes.White;
                btnteam12ndinnings.Background = null;
                btnteam21stinnings.Background = null;
                btnteam22ndinnings.Background = null;
                btnteam12ndinnings.Foreground = Brushes.Black;
                btnteam21stinnings.Foreground = Brushes.Black;
                btnteam22ndinnings.Foreground = Brushes.Black;

                gridteamonefirstinnings.Visibility = Visibility.Visible;
                gridbowlingteamtwofirstinnings.Visibility = Visibility.Visible;

                gridteamonesecondinnings.Visibility = Visibility.Hidden;
                gridbowlingteamtwosecondinnings.Visibility = Visibility.Hidden;


                gridteamtwofirstinnings.Visibility = Visibility.Hidden;
                gridbowlingteamonefirstinnings.Visibility = Visibility.Hidden;

                gridteamtwosecondinnings.Visibility = Visibility.Hidden;
                gridbowlingteamonesecondinnings.Visibility = Visibility.Hidden;

                vm.T1FirstInnings = Visibility.Visible;
                vm.T2FirstInnings = Visibility.Hidden;
                vm.T1SecondInnings = Visibility.Hidden;
                vm.T2SecondInnings = Visibility.Hidden;

                LoadDefaultData();
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        public void LoadDefaultData()
        {
            loadwides();
            loadnoballs();
            loadtotalscore();
            loaddismissaltotal();

        }

        private void btnteam12ndinnings_Click(object sender, RoutedEventArgs e)
        {
            try {
                btnteam11stinnings.Background = null;
                btnteam12ndinnings.Background = (Brush)bc.ConvertFrom("#336699");
                btnteam12ndinnings.Foreground = Brushes.White;
                btnteam21stinnings.Background = null;
                btnteam22ndinnings.Background = null;

                btnteam21stinnings.Foreground = Brushes.Black;
                btnteam22ndinnings.Foreground = Brushes.Black;
                btnteam11stinnings.Foreground = Brushes.Black;

                gridteamonefirstinnings.Visibility = Visibility.Hidden;
                gridbowlingteamtwofirstinnings.Visibility = Visibility.Hidden;

                gridteamonesecondinnings.Visibility = Visibility.Visible;
                gridbowlingteamtwosecondinnings.Visibility = Visibility.Visible;

                gridteamtwofirstinnings.Visibility = Visibility.Hidden;
                gridbowlingteamonefirstinnings.Visibility = Visibility.Hidden;

                gridteamtwosecondinnings.Visibility = Visibility.Hidden;
                gridbowlingteamonesecondinnings.Visibility = Visibility.Hidden;

                vm.T1FirstInnings = Visibility.Hidden;
                vm.T2FirstInnings = Visibility.Hidden;
                vm.T1SecondInnings = Visibility.Visible;
                vm.T2SecondInnings = Visibility.Hidden;

                LoadDefaultData();

            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }

        }

        private void btnteam21stinnings_Click(object sender, RoutedEventArgs e)
        {
            try {
                btnteam11stinnings.Background = null;
                btnteam12ndinnings.Background = null;
                btnteam21stinnings.Background = (Brush)bc.ConvertFrom("#336699");
                btnteam21stinnings.Foreground = Brushes.White;
                btnteam22ndinnings.Background = null;

                btnteam11stinnings.Foreground = Brushes.Black;
                btnteam12ndinnings.Foreground = Brushes.Black;
                btnteam22ndinnings.Foreground = Brushes.Black;

                gridteamonefirstinnings.Visibility = Visibility.Hidden;
                gridbowlingteamtwofirstinnings.Visibility = Visibility.Hidden;

                gridteamonesecondinnings.Visibility = Visibility.Hidden;
                gridbowlingteamtwosecondinnings.Visibility = Visibility.Hidden;

                gridteamtwofirstinnings.Visibility = Visibility.Visible;
                gridbowlingteamonefirstinnings.Visibility = Visibility.Visible;

                gridteamtwosecondinnings.Visibility = Visibility.Hidden;
                gridbowlingteamonesecondinnings.Visibility = Visibility.Hidden;

                vm.T1FirstInnings = Visibility.Hidden;
                vm.T2FirstInnings = Visibility.Visible;
                vm.T1SecondInnings = Visibility.Hidden;
                vm.T2SecondInnings = Visibility.Hidden;

                LoadDefaultData();
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }

        }

        private void btnteam22ndinnings_Click(object sender, RoutedEventArgs e)
        {
            try {
                btnteam11stinnings.Background = null;
                btnteam12ndinnings.Background = null;
                btnteam21stinnings.Background = null;
                btnteam22ndinnings.Background = (Brush)bc.ConvertFrom("#336699");
                btnteam22ndinnings.Foreground = Brushes.White;

                btnteam11stinnings.Foreground = Brushes.Black;
                btnteam12ndinnings.Foreground = Brushes.Black;
                btnteam21stinnings.Foreground = Brushes.Black;

                gridteamonefirstinnings.Visibility = Visibility.Hidden;
                gridbowlingteamtwofirstinnings.Visibility = Visibility.Hidden;


                gridteamonesecondinnings.Visibility = Visibility.Hidden;
                gridbowlingteamtwosecondinnings.Visibility = Visibility.Hidden;


                gridteamtwofirstinnings.Visibility = Visibility.Hidden;
                gridbowlingteamonefirstinnings.Visibility = Visibility.Hidden;



                gridteamtwosecondinnings.Visibility = Visibility.Visible;
                gridbowlingteamonesecondinnings.Visibility = Visibility.Visible;

                vm.T1FirstInnings = Visibility.Hidden;
                vm.T2FirstInnings = Visibility.Hidden;
                vm.T1SecondInnings = Visibility.Hidden;
                vm.T2SecondInnings = Visibility.Visible;

                LoadDefaultData();
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }

        }

        private void gridbattingdetails_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            try {
                if (e.Column.Header.ToString() == "Dismissal" || (e.Column.Header.ToString() == "Runs")|| e.Column.Header.ToString() == "Mins" || e.Column.Header.ToString() == "Balls" || e.Column.Header.ToString() == "Fours" || e.Column.Header.ToString() == "Sixes")
                {
                    if (!ismanualeditcommit)
                    {
                        ismanualeditcommit = true;

                        if (e.Column.Header.ToString() == "Runs")
                        {
                            loadtotalscore();
                        }

                        //DataGrid dggrid = (DataGrid)sender;
                        //dggrid.CommitEdit(DataGridEditingUnit.Row, true);

                        
                            objselectedbat = (PlayedMatchDetailEntity)e.Row.Item;
                        ismanualeditcommit = false;
                    }
                }
                
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void loaddismissaltotal()
        {

            if (vm.T1FirstInnings == Visibility.Visible)
            {
                vm.LstPlayedMatchDetailTeamOne.Where(p => p.FirstInningsDismissal == null).ToList().ForEach(x => x.FirstInningsDismissal = "");
                vm.ObjPlayedMatch.TeamOneFirstInningsCatches = vm.LstPlayedMatchDetailTeamOne.Where(p => p.FirstInningsDismissal.Split(' ')[0].Trim() == "c" || p.FirstInningsDismissal.Split(' ')[0].Trim() == "c&b").ToList().Count();
                vm.ObjPlayedMatch.TeamOneFirstInningsBowled = vm.LstPlayedMatchDetailTeamOne.Where(p => p.FirstInningsDismissal.Split(' ')[0].Trim() == "b").ToList().Count();
                vm.ObjPlayedMatch.TeamOneFirstInningsLbws = vm.LstPlayedMatchDetailTeamOne.Where(p => p.FirstInningsDismissal.Split(' ')[0].Trim() == "Lbw").ToList().Count();
                vm.ObjPlayedMatch.TeamOneFirstInningsRO = vm.LstPlayedMatchDetailTeamOne.Where(p => p.FirstInningsDismissal.Split(' ')[0].Trim() == "Run").ToList().Count();
                vm.ObjPlayedMatch.TeamOneFirstInningsStumped = vm.LstPlayedMatchDetailTeamOne.Where(p => p.FirstInningsDismissal.Split(' ')[0].Trim() == "st").ToList().Count();
                vm.ObjPlayedMatch.TeamOneFirstInningsHitWickets = vm.LstPlayedMatchDetailTeamOne.Where(p => p.FirstInningsDismissal.Split(' ')[0].Trim() == "Hit").ToList().Count();
                vm.ObjPlayedMatch.TeamOneFirstInningsRetiredHurt = vm.LstPlayedMatchDetailTeamOne.Where(p => p.FirstInningsDismissal.Split(' ')[0].Trim() == "Retd").ToList().Count();
            }
            else if (vm.T2FirstInnings == Visibility.Visible)
            {
                vm.LstPlayedMatchDetailTeamTwo.Where(p => p.FirstInningsDismissal == null).ToList().ForEach(x => x.FirstInningsDismissal = "");
                vm.ObjPlayedMatch.TeamTwoFirstInningsCatches = vm.LstPlayedMatchDetailTeamTwo.Where(p => p.SecondInningsDismissal != "").ToList().Where(p => p.FirstInningsDismissal.Split(' ')[0].Trim() == "c" || p.FirstInningsDismissal.Split(' ')[0].Trim() == "c&b").ToList().Count();
                vm.ObjPlayedMatch.TeamTwoFirstInningsBowled = vm.LstPlayedMatchDetailTeamTwo.Where(p => p.FirstInningsDismissal.Split(' ')[0].Trim() == "b").ToList().Count();
                vm.ObjPlayedMatch.TeamTwoFirstInningsLbws = vm.LstPlayedMatchDetailTeamTwo.Where(p => p.FirstInningsDismissal.Split(' ')[0].Trim() == "Lbw").ToList().Count();
                vm.ObjPlayedMatch.TeamTwoFirstInningsRO = vm.LstPlayedMatchDetailTeamTwo.Where(p => p.FirstInningsDismissal.Split(' ')[0].Trim() == "Run").ToList().Count();
                vm.ObjPlayedMatch.TeamTwoFirstInningsStumped = vm.LstPlayedMatchDetailTeamTwo.Where(p => p.FirstInningsDismissal.Split(' ')[0].Trim() == "st").ToList().Count();
                vm.ObjPlayedMatch.TeamTwoFirstInningsHitWickets = vm.LstPlayedMatchDetailTeamTwo.Where(p => p.FirstInningsDismissal.Split(' ')[0].Trim() == "Hit").ToList().Count();
                vm.ObjPlayedMatch.TeamTwoFirstInningsRetiredHurt = vm.LstPlayedMatchDetailTeamTwo.Where(p => p.FirstInningsDismissal.Split(' ')[0].Trim() == "Retd").ToList().Count();

            }
            else if (vm.T2SecondInnings == Visibility.Visible)
            {
                vm.LstPlayedMatchDetailTeamTwo.Where(p => p.SecondInningsDismissal == null).ToList().ForEach(x => x.SecondInningsDismissal = "");
                vm.ObjPlayedMatch.TeamTwoSecondInningsCatches = vm.LstPlayedMatchDetailTeamTwo.Where(p => p.SecondInningsDismissal != "").ToList().Where(p => p.SecondInningsDismissal.Split(' ')[0].Trim() == "c" || p.SecondInningsDismissal.Split(' ')[0].Trim() == "c&b").ToList().Count();
                vm.ObjPlayedMatch.TeamTwoSecondInningsBowled = vm.LstPlayedMatchDetailTeamTwo.Where(p => p.SecondInningsDismissal.Split(' ')[0].Trim() == "b").ToList().Count();
                vm.ObjPlayedMatch.TeamTwoSecondInningsLbws = vm.LstPlayedMatchDetailTeamTwo.Where(p => p.SecondInningsDismissal.Split(' ')[0].Trim() == "Lbw").ToList().Count();
                vm.ObjPlayedMatch.TeamTwoSecondInningsRO = vm.LstPlayedMatchDetailTeamTwo.Where(p => p.SecondInningsDismissal.Split(' ')[0].Trim() == "Run").ToList().Count();
                vm.ObjPlayedMatch.TeamTwoSecondInningsStumped = vm.LstPlayedMatchDetailTeamTwo.Where(p => p.SecondInningsDismissal.Split(' ')[0].Trim() == "st").ToList().Count();
                vm.ObjPlayedMatch.TeamTwoSecondInningsHitWickets = vm.LstPlayedMatchDetailTeamTwo.Where(p => p.SecondInningsDismissal.Split(' ')[0].Trim() == "Hit").ToList().Count();
                vm.ObjPlayedMatch.TeamTwoSecondInningsRetiredHurt = vm.LstPlayedMatchDetailTeamTwo.Where(p => p.SecondInningsDismissal.Split(' ')[0].Trim() == "Retd").ToList().Count();

            }
            else
            {
                vm.LstPlayedMatchDetailTeamOne.Where(p => p.SecondInningsDismissal == null).ToList().ForEach(x => x.SecondInningsDismissal = "");
                vm.ObjPlayedMatch.TeamOneSecondInningsCatches = vm.LstPlayedMatchDetailTeamOne.Where(p=> p.SecondInningsDismissal.Split(' ')[0].Trim() == "c" || p.SecondInningsDismissal.Split(' ')[0].Trim() == "c&b").ToList().Count();
                vm.ObjPlayedMatch.TeamOneSecondInningsBowled = vm.LstPlayedMatchDetailTeamOne.Where(p => p.SecondInningsDismissal.Split(' ')[0].Trim() == "b").ToList().Count();
                vm.ObjPlayedMatch.TeamOneSecondInningsLbws = vm.LstPlayedMatchDetailTeamOne.Where(p => p.SecondInningsDismissal.Split(' ')[0].Trim() == "Lbw").ToList().Count();
                vm.ObjPlayedMatch.TeamOneSecondInningsRO = vm.LstPlayedMatchDetailTeamOne.Where(p => p.SecondInningsDismissal.Split(' ')[0].Trim() == "Run").ToList().Count();
                vm.ObjPlayedMatch.TeamOneSecondInningsStumped = vm.LstPlayedMatchDetailTeamOne.Where(p => p.SecondInningsDismissal.Split(' ')[0].Trim() == "st").ToList().Count();
                vm.ObjPlayedMatch.TeamOneSecondInningsHitWickets = vm.LstPlayedMatchDetailTeamOne.Where(p => p.SecondInningsDismissal.Split(' ')[0].Trim() == "Hit").ToList().Count();
                vm.ObjPlayedMatch.TeamOneSecondInningsRetiredHurt = vm.LstPlayedMatchDetailTeamOne.Where(p => p.SecondInningsDismissal.Split(' ')[0].Trim() == "Retd").ToList().Count();

            }

        }
        private void loaddismissaldata()
        {
           
                if (gridteamonefirstinnings.Visibility == Visibility.Visible)
                {
                    vm.LstPlayedMatchDetailTeamOne.Where(p => p.PlayedMatchDetailId == objdismissed.PlayedMatchDetailId).ToList().ForEach(x => x.FirstInningsDismissal = User.Dismissal);
                }
                else if (gridteamonesecondinnings.Visibility == Visibility.Visible)
                {
                    vm.LstPlayedMatchDetailTeamOne.Where(p => p.PlayedMatchDetailId == objdismissed.PlayedMatchDetailId).ToList().ForEach(x => x.SecondInningsDismissal = User.Dismissal);
                }
                else if (gridteamtwofirstinnings.Visibility == Visibility.Visible)
                {
                    vm.LstPlayedMatchDetailTeamTwo.Where(p => p.PlayedMatchDetailId == objdismissed.PlayedMatchDetailId).ToList().ForEach(x => x.FirstInningsDismissal = User.Dismissal);
                }
                else
                {
                    vm.LstPlayedMatchDetailTeamTwo.Where(p => p.PlayedMatchDetailId == objdismissed.PlayedMatchDetailId).ToList().ForEach(x => x.SecondInningsDismissal = User.Dismissal);
                }
            User.Dismissal = "";
           
        }

        public void loadtotalscore()
        {
                loadtotalextras();
                loaddismissaltotal();
                if (gridteamonefirstinnings.Visibility == Visibility.Visible)
                {
                    vm.ObjPlayedMatch.TeamOneFirstInningsScore = 0;
                    int total = 0;
                    vm.LstPlayedMatchDetailTeamOne.ToList().ForEach(x =>
                    {
                        total = total + x.FirstInningsRunsScored;

                    });
                    vm.ObjPlayedMatch.TeamOneFirstInningsScore = vm.ObjPlayedMatch.TeamOneFirstInningsTotalExtras + total;
                vm.ObjPlayedMatch.TeamOneFirstInningsTotalWickets = vm.ObjPlayedMatch.TeamOneFirstInningsCatches + vm.ObjPlayedMatch.TeamOneFirstInningsBowled +
                                                                        vm.ObjPlayedMatch.TeamOneFirstInningsLbws + vm.ObjPlayedMatch.TeamOneFirstInningsHitWickets +
                                                                        vm.ObjPlayedMatch.TeamOneFirstInningsStumped + vm.ObjPlayedMatch.TeamOneFirstInningsRO + vm.ObjPlayedMatch.TeamOneFirstInningsRetiredHurt;
                }
                else if (gridteamtwofirstinnings.Visibility == Visibility.Visible)
                {
                    vm.ObjPlayedMatch.TeamTwoFirstInningsScore = 0;
                    int total = 0;
                    vm.LstPlayedMatchDetailTeamTwo.ToList().ForEach(x =>
                    {
                        total = total + x.FirstInningsRunsScored;
                    });
                    vm.ObjPlayedMatch.TeamTwoFirstInningsScore = vm.ObjPlayedMatch.TeamTwoFirstInningsTotalExtras + total;
                    vm.ObjPlayedMatch.TeamTwoFirstInningsTotalWickets = vm.ObjPlayedMatch.TeamTwoFirstInningsCatches + vm.ObjPlayedMatch.TeamTwoFirstInningsBowled +
                                                            vm.ObjPlayedMatch.TeamTwoFirstInningsLbws + vm.ObjPlayedMatch.TeamTwoFirstInningsHitWickets +
                                                            vm.ObjPlayedMatch.TeamTwoFirstInningsStumped + vm.ObjPlayedMatch.TeamTwoFirstInningsRO+ vm.ObjPlayedMatch.TeamTwoFirstInningsRetiredHurt;

                }
                else if (gridteamtwosecondinnings.Visibility == Visibility.Visible)
                {
                    vm.ObjPlayedMatch.TeamTwoSecondInningsScore = 0;
                    int total = 0;
                    vm.LstPlayedMatchDetailTeamTwo.ToList().ForEach(x =>
                    {
                        total = total + x.SecondInningsRunsScored;
                    });
                    vm.ObjPlayedMatch.TeamTwoSecondInningsScore = vm.ObjPlayedMatch.TeamTwoSecondInningsTotalExtras + total;
                    vm.ObjPlayedMatch.TeamTwoSecondInningsTotalWickets = vm.ObjPlayedMatch.TeamTwoSecondInningsCatches + vm.ObjPlayedMatch.TeamTwoSecondInningsBowled +
                                            vm.ObjPlayedMatch.TeamTwoSecondInningsLbws + vm.ObjPlayedMatch.TeamTwoSecondInningsHitWickets +
                                            vm.ObjPlayedMatch.TeamTwoSecondInningsStumped + vm.ObjPlayedMatch.TeamTwoSecondInningsRO+ vm.ObjPlayedMatch.TeamTwoSecondInningsRetiredHurt;

                }
                else
                {
                    vm.ObjPlayedMatch.TeamOneSecondInningsScore = 0;
                    int total = 0;
                    vm.LstPlayedMatchDetailTeamOne.ToList().ForEach(x =>
                    {
                        total = total + x.SecondInningsRunsScored;
                    });
                    vm.ObjPlayedMatch.TeamOneSecondInningsScore = vm.ObjPlayedMatch.TeamOneSecondInningsTotalExtras + total;
                    vm.ObjPlayedMatch.TeamOneSecondInningsTotalWickets = vm.ObjPlayedMatch.TeamOneSecondInningsCatches + vm.ObjPlayedMatch.TeamOneSecondInningsBowled +
                            vm.ObjPlayedMatch.TeamOneSecondInningsLbws + vm.ObjPlayedMatch.TeamOneSecondInningsHitWickets +
                            vm.ObjPlayedMatch.TeamOneSecondInningsStumped + vm.ObjPlayedMatch.TeamOneSecondInningsRO+ vm.ObjPlayedMatch.TeamOneSecondInningsRetiredHurt;
                }
        }


        private void gridbattingdetails_CurrentCellChanged(object sender, EventArgs e)
        {
            try {
                DataGrid dgv = (DataGrid)sender;
                if (dgv.CurrentCell.Column != null)
                    if (dgv.CurrentCell.Column.Header.ToString() == "Dismissal")
                    {
                        if (vm.ObjPlayedMatch.CompleteStatus != true)
                        {
                            if (gridteamonefirstinnings.Visibility == Visibility.Visible || gridteamonesecondinnings.Visibility == Visibility.Visible)
                            {
                                DismissalSelection diswin = new DismissalSelection();
                                objdismissed = vm.LstPlayedMatchDetailTeamOne[dgv.Items.IndexOf(dgv.CurrentItem)];

                                PlayerEntity objkeep = vm.ObjPlayedMatch.TeamTwoKeeper;
                                ObservableCollection<PlayerEntity> LstPlayer = new ObservableCollection<PlayerEntity>(vm.LstPlayedMatchDetailTeamTwo.Select(p => p.Player));

                                diswin.LoadPlayers(objdismissed.Player, LstPlayer, objkeep);
                                diswin.ShowDialog();
                                loaddismissaldata();
                                loaddismissaltotal();
                            }
                            else
                            {
                                DismissalSelection diswin = new DismissalSelection();
                                objdismissed = vm.LstPlayedMatchDetailTeamTwo[dgv.Items.IndexOf(dgv.CurrentItem)];
                                PlayerEntity objkeep = vm.ObjPlayedMatch.TeamOneKeeper;
                                ObservableCollection<PlayerEntity> LstPlayer = new ObservableCollection<PlayerEntity>(vm.LstPlayedMatchDetailTeamOne.Select(p => p.Player));

                                diswin.LoadPlayers(objdismissed.Player, LstPlayer, objkeep);

                                diswin.ShowDialog();
                                loaddismissaldata();
                                loaddismissaltotal();
                            }
                        }
                    }
                    else if (dgv.CurrentCell.Column.Header.ToString() == "Runs")
                    {
                        loadtotalscore();
                    }
                    else if (objselectedbat != null)
                    {
                        Regex nameEx = new Regex(@"^[A-Za-z ]+$");
                        Regex numex = new Regex(@"^[0-9]+$");
                        try
                        {
                            if (vm.T1FirstInnings == Visibility.Visible)
                            {
                                if (objselectedbat.FirstInningsDismissal == "")
                                    throw new Exception("Invalid Dismissal Value");
                            }
                            else
                            {
                                if (objselectedbat.FirstInningsDismissal == "")
                                    throw new Exception("Invalid Dismissal Value");
                            }
                        }
                        catch (Exception es)
                        {
                            MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void loadtotalextras()
        {
                if (gridbowlingteamonefirstinnings.Visibility == Visibility.Visible)
                {                    
                        vm.ObjPlayedMatch.TeamTwoFirstInningsTotalExtras = vm.ObjPlayedMatch.TeamTwoFirstInningsByes + vm.ObjPlayedMatch.TeamTwoFirstInningsLegByes +
                        vm.ObjPlayedMatch.TeamTwoFirstInningsP + vm.ObjPlayedMatch.TeamTwoFirstInningsWides + vm.ObjPlayedMatch.TeamTwoFirstInningsNoBalls;
                    
                }
                else if (gridbowlingteamtwofirstinnings.Visibility == Visibility.Visible)
                {
                    
                        vm.ObjPlayedMatch.TeamOneFirstInningsTotalExtras = vm.ObjPlayedMatch.TeamOneFirstInningsByes + vm.ObjPlayedMatch.TeamOneFirstInningsLegByes +
                        vm.ObjPlayedMatch.TeamOneFirstInningsP + vm.ObjPlayedMatch.TeamOneFirstInningsWides + vm.ObjPlayedMatch.TeamOneFirstInningsNoBalls;
                    
                }
                else if (gridbowlingteamtwosecondinnings.Visibility == Visibility.Visible)
                {
                   
                        vm.ObjPlayedMatch.TeamOneSecondInningsTotalExtras = vm.ObjPlayedMatch.TeamOneSecondInningsByes + vm.ObjPlayedMatch.TeamOneSecondInningsLegByes +
                        vm.ObjPlayedMatch.TeamOneSecondInningsP + vm.ObjPlayedMatch.TeamOneSecondInningsWides + vm.ObjPlayedMatch.TeamOneSecondInningsNoBalls;
                    
                }
                else if(gridbowlingteamonesecondinnings.Visibility==Visibility.Visible)
                {
                    
                        vm.ObjPlayedMatch.TeamTwoSecondInningsTotalExtras = vm.ObjPlayedMatch.TeamTwoSecondInningsByes + vm.ObjPlayedMatch.TeamTwoSecondInningsLegByes +
                        vm.ObjPlayedMatch.TeamTwoSecondInningsP + vm.ObjPlayedMatch.TeamTwoSecondInningsWides + vm.ObjPlayedMatch.TeamTwoSecondInningsNoBalls;                    
                }
           
        }

        private void txt_byes_LostFocus(object sender, RoutedEventArgs e)
        {
            loadtotalextras();
        }

        private void txt_legbyes_LostFocus(object sender, RoutedEventArgs e)
        {
            loadtotalextras();
        }

        private void txt_p_LostFocus(object sender, RoutedEventArgs e)
        {
            loadtotalextras();
        }

        private void txt_RO_LostFocus(object sender, RoutedEventArgs e)
        {
            loadtotalextras();
        }

        private void gridbowlingdetails_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            try {
                if (e.Column.Header.ToString() == "OversBowled" || e.Column.Header.ToString() == "Maidens" || e.Column.Header.ToString() == "RunsGiven" || e.Column.Header.ToString() == "Wickets" || e.Column.Header.ToString() == "NoBalls" || e.Column.Header.ToString() == "Wides" || e.Column.Header.ToString() == "Economy")
                {

                    if (!ismanualeditcommit)
                    {
                        ismanualeditcommit = true;

                        if (e.Column.Header.ToString() == "NoBalls")
                            loadnoballs();

                        else if (e.Column.Header.ToString() == "Wides")
                            loadwides();

                        //DataGrid dggrid = (DataGrid)sender;
                        //dggrid.CommitEdit(DataGridEditingUnit.Row, true);

                        objselectedbowl = (PlayedMatchDetailEntity)e.Row.Item;
                        ismanualeditcommit = false;
                    }
                }
                
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void loadnoballs()
        {
            try {
                vm.ObjPlayedMatch.TeamOneFirstInningsNoBalls = 0;
                vm.ObjPlayedMatch.TeamTwoSecondInningsNoBalls = 0;
                vm.ObjPlayedMatch.TeamTwoFirstInningsNoBalls = 0;
                vm.ObjPlayedMatch.TeamOneSecondInningsNoBalls = 0;

                if (gridbowlingteamonefirstinnings.Visibility == Visibility.Visible)
                {
                    vm.LstPlayedMatchDetailTeamOne.ToList().ForEach(x =>
                    {
                        vm.ObjPlayedMatch.TeamTwoFirstInningsNoBalls = vm.ObjPlayedMatch.TeamTwoFirstInningsNoBalls + x.FirstInningsNoBalls;
                    });
                }
                else if (gridbowlingteamtwofirstinnings.Visibility == Visibility.Visible)
                {
                    vm.LstPlayedMatchDetailTeamTwo.ToList().ForEach(x =>
                    {
                        vm.ObjPlayedMatch.TeamOneFirstInningsNoBalls = vm.ObjPlayedMatch.TeamOneFirstInningsNoBalls + x.FirstInningsNoBalls;
                    });
                }
                else if (gridbowlingteamtwosecondinnings.Visibility == Visibility.Visible)
                {
                    vm.LstPlayedMatchDetailTeamTwo.ToList().ForEach(x =>
                    {
                        vm.ObjPlayedMatch.TeamOneSecondInningsNoBalls = vm.ObjPlayedMatch.TeamOneSecondInningsNoBalls + x.SecondInningsNoBalls;
                    });
                }
                else
                {
                    vm.LstPlayedMatchDetailTeamOne.ToList().ForEach(x =>
                    {
                        vm.ObjPlayedMatch.TeamTwoSecondInningsNoBalls = vm.ObjPlayedMatch.TeamTwoSecondInningsNoBalls + x.SecondInningsNoBalls;
                    });
                }
                loadtotalextras();
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void loadwides()
        {
            try {
                vm.ObjPlayedMatch.TeamOneFirstInningsWides = 0;
                vm.ObjPlayedMatch.TeamOneSecondInningsWides = 0;
                vm.ObjPlayedMatch.TeamTwoFirstInningsWides = 0;
                vm.ObjPlayedMatch.TeamTwoSecondInningsWides = 0;
                if (gridbowlingteamonefirstinnings.Visibility == Visibility.Visible)
                {
                    vm.LstPlayedMatchDetailTeamOne.ToList().ForEach(x =>
                    {
                        vm.ObjPlayedMatch.TeamTwoFirstInningsWides = vm.ObjPlayedMatch.TeamTwoFirstInningsWides + x.FirstInningsWides;
                    });
                }
                else if (gridbowlingteamtwofirstinnings.Visibility == Visibility.Visible)
                {
                    vm.LstPlayedMatchDetailTeamTwo.ToList().ForEach(x =>
                    {
                        vm.ObjPlayedMatch.TeamOneFirstInningsWides = vm.ObjPlayedMatch.TeamOneFirstInningsWides + x.FirstInningsWides;
                    });
                }
                else if (gridbowlingteamtwosecondinnings.Visibility == Visibility.Visible)
                {
                    vm.LstPlayedMatchDetailTeamTwo.ToList().ForEach(x =>
                    {
                        vm.ObjPlayedMatch.TeamOneSecondInningsWides = vm.ObjPlayedMatch.TeamOneSecondInningsWides + x.SecondInningsWides;
                    });
                }
                else
                {
                    vm.LstPlayedMatchDetailTeamOne.ToList().ForEach(x =>
                    {
                        vm.ObjPlayedMatch.TeamTwoSecondInningsWides = vm.ObjPlayedMatch.TeamTwoSecondInningsWides + x.SecondInningsWides;
                    });
                }
                loadtotalextras();
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void gridbowlingdetails_CurrentCellChanged(object sender, EventArgs e)
        {
            try {

                if (objselectedbowl != null)
                {
                    DataGrid dgv = (DataGrid)sender;
                    Regex numex = new Regex(@"^[0-9]+$");
                    Regex numexOver = new Regex(@"^[0-9]+(\.[0-9]+)?$");
                    if (!numexOver.Match(objselectedbowl.FirstInningsOversBowled.ToString()).Success)
                    {
                        objselectedbowl.FirstInningsOversBowled = 0;
                        MessageBox.Show("Invalid Overs Format");
                    }
                    if (!numexOver.Match(objselectedbowl.FirstInningsEconomy.ToString()).Success)
                    {
                        objselectedbowl.FirstInningsEconomy = 0;
                        MessageBox.Show("Invalid Average Format");
                    }
                    if (dgv.CurrentCell.Column != null)
                    {
                        if (dgv.CurrentCell.Column.Header.ToString() == "NoBalls")
                            loadnoballs();

                        else if (dgv.CurrentCell.Column.Header.ToString() == "Wides")
                            loadwides();
                    }
                }
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btncomplete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                vm.ObjPlayedMatch.CompleteStatus = true;
                if (vm.ObjPlayedMatch.MaximumOvers <= 0)
                    throw new Exception("Enter Maximum Overs Per Innings");
                else
                {
                    if (Save() == true)
                    {

                        ViewFixtures page = new ViewFixtures();
                        this.Content = page;
                    }
                }
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void editstatus()
        {
            gridteamonefirstinnings.IsEnabled = true;
            gridteamtwofirstinnings.IsEnabled = true;
            gridteamonesecondinnings.IsEnabled = true;
            gridteamtwosecondinnings.IsEnabled = true;
            gridbowlingteamonefirstinnings.IsEnabled = true;
            gridbowlingteamonesecondinnings.IsEnabled = true;
            gridbowlingteamtwofirstinnings.IsEnabled = true;
            gridbowlingteamtwosecondinnings.IsEnabled = true;
            btnsave.IsEnabled = true;
        }
        private void btnedit_Click(object sender, RoutedEventArgs e)
        {
            try {
                btnedit.Background = (Brush)bc.ConvertFrom("#336699");
                btnedit.Foreground = Brushes.White;

                editstatus();

                vm.ObjPlayedMatch.CompleteStatus = false;
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }


        //public void validatewicketsandovers()
        //{
        //    loadtotalscore();
        //    //if (gridteamonefirstinnings.Visibility == Visibility.Visible)
        //    //{
        //    //    int totwickets = vm.LstPlayedMatchDetailTeamTwo.Select(p => p.FirstInningsWickets).Sum();
        //    //    if (vm.ObjPlayedMatch.TeamOneFirstInningsTotalWickets != totwickets)
        //    //        throw new Exception("Total Wickets Do Not Match");
        //    //}
        //    //else if (gridteamtwofirstinnings.Visibility == Visibility.Visible)
        //    //{
        //    //    int totwickets = vm.LstPlayedMatchDetailTeamOne.Select(p => p.FirstInningsWickets).Sum();
        //    //    if (vm.ObjPlayedMatch.TeamTwoFirstInningsTotalWickets != totwickets)
        //    //        throw new Exception("Total Wickets Do Not Match");
        //    //}
        //    //else if (gridteamtwosecondinnings.Visibility == Visibility.Visible)
        //    //{
        //    //    int totwickets = vm.LstPlayedMatchDetailTeamOne.Select(p => p.SecondInningsWickets).Sum();
        //    //    if (vm.ObjPlayedMatch.TeamTwoSecondInningsTotalWickets != totwickets)
        //    //        throw new Exception("Total Wickets Do Not Match");
        //    //}
        //    //else
        //    //{
        //    //    int totwickets = vm.LstPlayedMatchDetailTeamTwo.Select(p => p.FirstInningsWickets).Sum();
        //    //    if (vm.ObjPlayedMatch.TeamOneSecondInningsTotalWickets != totwickets)
        //    //        throw new Exception("Total Wickets Do Not Match");
        //    //}
        //} 

        public void validatetotalovers()
        {
            
            if (gridteamonefirstinnings.Visibility == Visibility.Visible)
            {
                if (string.IsNullOrEmpty(vm.ObjPlayedMatch.TeamOneFirstInningsOvers))
                    throw new Exception("Enter Total Overs");
            }
            else if (gridteamtwofirstinnings.Visibility == Visibility.Visible)
            {
                if (string.IsNullOrEmpty(vm.ObjPlayedMatch.TeamTwoFirstInningsOvers))
                    throw new Exception("Enter Total Overs");

            }
            else if (gridteamtwosecondinnings.Visibility == Visibility.Visible)
            {
                if (string.IsNullOrEmpty(vm.ObjPlayedMatch.TeamTwoSecondInningsOvers))
                    throw new Exception("Enter Total Overs");
            }
            else
            {
                if (string.IsNullOrEmpty(vm.ObjPlayedMatch.TeamOneSecondInningsOvers))
                    throw new Exception("Enter Total Overs");
                }
            
            
        }

        public bool Save()
        {
            try
            {
                if (vm.ObjPlayedMatch.CompleteStatus == true)
                {
                    //if (vm.ObjPlayedMatch.Result == Guid.Empty)
                    //{
                    //    if (User.Division.NumberOfDays == 2)
                    //    {
                    //        MessageBoxResult res = MessageBox.Show("Do You Want to Load Points Based On First Innings Lead?", "Info", MessageBoxButton.YesNo, MessageBoxImage.Information);
                    //        if (vm.ObjPlayedMatch.TeamOneFirstInningsScore > vm.ObjPlayedMatch.TeamTwoFirstInningsScore)
                    //        {
                    //            if (res == MessageBoxResult.Yes)
                    //            {
                    //                vm.ObjPlayedMatch.TeamOnePoints = 3;
                    //                vm.ObjPlayedMatch.TeamTwoPoints = 1;
                    //            }
                    //            else
                    //            {
                    //                vm.ObjPlayedMatch.TeamOnePoints = 2;
                    //                vm.ObjPlayedMatch.TeamTwoPoints = 2;

                    //            }
                    //        }
                    //        else
                    //        {
                    //            if (res == MessageBoxResult.Yes)
                    //            {
                    //                vm.ObjPlayedMatch.TeamOnePoints = 1;
                    //                vm.ObjPlayedMatch.TeamTwoPoints = 3;
                    //            }
                    //            else
                    //            {
                    //                vm.ObjPlayedMatch.TeamOnePoints = 2;
                    //                vm.ObjPlayedMatch.TeamTwoPoints = 2;
                    //            }
                    //        }
                    //    }
                    //}
                }
                //validations
                loadtotalscore();
                validatetotalovers();

                List<EntityCollection> lsttosave = new List<EntityCollection>();
                Database.NewSaveEntity<PlayedMatchEntity>(vm.ObjPlayedMatch, lsttosave);
                Database.NewSaveEntity<PlayedMatchDetailEntity>(vm.LstPlayedMatchDetailTeamOne, lsttosave);
                Database.NewSaveEntity<PlayedMatchDetailEntity>(vm.LstPlayedMatchDetailTeamTwo, lsttosave);

                Database.CommitChanges(lsttosave, Database.getDBContext());

                User.ObjPlayedMatch = Database.GetEntity<PlayedMatchEntity>(vm.ObjPlayedMatch.PlayedMatchId);
                vm.LoadDefaultValues();

                
                MessageBox.Show("Saved","Info", MessageBoxButton.OK, MessageBoxImage.Information);
                return true;

            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }
        }
        private void btnsave_Click(object sender, RoutedEventArgs e)
        {
            Save();
        }

        private void chkt1firstinningsnotplayed_Checked(object sender, RoutedEventArgs e)
        {
            gridteamonefirstinnings.IsEnabled = false;
            gridbowlingteamtwofirstinnings.IsEnabled = false;
        }

        private void chkt1secondinningsnotplayed_Checked(object sender, RoutedEventArgs e)
        {
            gridteamonesecondinnings.IsEnabled = false;
            gridbowlingteamtwosecondinnings.IsEnabled = false;

        }

        private void chkt2firstinningsnotplayed_Checked(object sender, RoutedEventArgs e)
        {
            gridteamonesecondinnings.IsEnabled = false;
            gridbowlingteamtwosecondinnings.IsEnabled = false;
        }

        private void chkt2secondinningsnotplayed_Checked(object sender, RoutedEventArgs e)
        {
            gridteamtwosecondinnings.IsEnabled = false;
            gridbowlingteamonesecondinnings.IsEnabled = false;
        }

        private void chkt1firstinningsnotplayed_Unchecked(object sender, RoutedEventArgs e)
        {
            gridteamonefirstinnings.IsEnabled = true;
            gridbowlingteamtwofirstinnings.IsEnabled = true;
        }

        private void chkt1secondinningsnotplayed_Unchecked(object sender, RoutedEventArgs e)
        {
            gridteamonesecondinnings.IsEnabled = true;
            gridbowlingteamtwosecondinnings.IsEnabled = true;
        }

        private void chkt2firstinningsnotplayed_Unchecked(object sender, RoutedEventArgs e)
        {
            gridteamonesecondinnings.IsEnabled = true;
            gridbowlingteamtwosecondinnings.IsEnabled = true;
        }

        private void chkt2secondinningsnotplayed_Unchecked(object sender, RoutedEventArgs e)
        {
            gridteamtwosecondinnings.IsEnabled = true;
            gridbowlingteamonesecondinnings.IsEnabled = true;
        }

        
    }
}
