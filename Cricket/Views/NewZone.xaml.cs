﻿
//using Cricket.BLL;
using Cricket.DAL;
using Cricket.Model;
using Cricket.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
namespace Cricket.Views
{
    /// <summary>
    /// Interaction logic for NewZone.xaml
    /// </summary>
    public partial class NewZone : UserControl
    {
        public NewZoneVM ObjNewZoneVM=new NewZoneVM();
        public NewZone()
        {
            try {
                InitializeComponent();
                this.DataContext = ObjNewZoneVM;
                btnnewzone_Click(null,null );
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnsavezone_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ObjNewZoneVM.ActiveZones.Where(p => p.ZoneNumber == ObjNewZoneVM.CurrentZone.ZoneNumber).Count() > 0)
                    throw new Exception("Zone With Number " + ObjNewZoneVM.CurrentZone.ZoneNumber + " Already Exists");
                else {
                    ObjNewZoneVM.SaveZone();
                    MessageBox.Show("Zone Saved");
                    ObjNewZoneVM.NewZone();
                    ObjNewZoneVM.LoadZones();
                    User.HomePageVM.getZoneList();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void gridzonevalues_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (gridzonevalues.SelectedItem != null)
                {
                    if (ObjNewZoneVM.CurrentZone  == null || ObjNewZoneVM.CurrentZone.IsNew == false)
                    {
                        ZoneEntity  objgridselected = (ZoneEntity )gridzonevalues.SelectedItem;
                        ObjNewZoneVM.CurrentZone  = objgridselected;
                        
                        txt_ZoneName.Focus();
                        
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        

        private void btnnewzone_Click(object sender, RoutedEventArgs e)
        {
            try {
                if (ObjNewZoneVM.CurrentZone != null)
                {
                    if (ObjNewZoneVM.CurrentZone.IsNew == false)
                    {
                        ObjNewZoneVM.NewZone();
                    }
                }
                else
                {
                    ObjNewZoneVM.NewZone();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ObjNewZoneVM.CurrentZone  != null && ObjNewZoneVM.CurrentZone.IsNew)
                {

                    ObjNewZoneVM.ActiveZones.Remove(ObjNewZoneVM.CurrentZone);
                    ObjNewZoneVM.CurrentZone = null;
                    if (gridzonevalues.SelectedItems != null) 
                    gridzonevalues_SelectionChanged(null, null);
                }
                else if (ObjNewZoneVM.CurrentZone != null && ObjNewZoneVM.CurrentZone.IsNew == false)
                {
                    ZoneEntity  objZonefromDB = Database.GetEntity<ZoneEntity >(ObjNewZoneVM.CurrentZone.ZoneId );
                    ObjNewZoneVM.ActiveZones.Remove(ObjNewZoneVM.CurrentZone);

                    ObjNewZoneVM.CurrentZone = objZonefromDB;
                    ObjNewZoneVM.ActiveZones.Add(objZonefromDB);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
    }
}
