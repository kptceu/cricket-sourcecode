﻿using Cricket.Model;
using Cricket.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cricket.Views
{
    /// <summary>
    /// Interaction logic for Groups.xaml
    /// </summary>
    public partial class Groups : UserControl
    {
        GroupsVM vm = new GroupsVM();
        public static bool Status = false;      
        public Groups()
        {
            InitializeComponent();
            this.DataContext = vm;
            cbx_groups.SelectedIndex = vm.GroupCount - 1;
        }

        public void validate(ListBox lst)
        {
            try { 
            if (lst.Items.Count == 0)
            {
                Status = false;
                throw new Exception("Add Teams To Group");
            }
            else
                Status = true;
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnsave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cbx_groups.SelectedIndex == -1)
                    throw new Exception("Select No Of Groups");
                if (cbx_groups.SelectedIndex == 0)
                {
                    validate(lbx_grpa);
                }
                if (cbx_groups.SelectedIndex == 1)
                {
                    validate(lbx_grpa);
                    validate(lbx_grpb);
                }
                if (cbx_groups.SelectedIndex == 2)
                {
                    validate(lbx_grpa);
                    validate(lbx_grpb);
                    validate(lbx_grpc);
                }
                if (cbx_groups.SelectedIndex == 3)
                {
                    validate(lbx_grpa);
                    validate(lbx_grpb);
                    validate(lbx_grpc);
                    validate(lbx_grpd);
                }
                if(Status==true)
                {
                    if (vm.Save(cbx_groups.SelectedIndex + 1) == true)
                        MessageBox.Show("Groups Saved", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch(Exception es)
            {
                MessageBox.Show(es.Message);
            }            
        }

        private void txt_search_TextChanged(object sender, TextChangedEventArgs e)
        {
            try { 
            vm.Search(txt_search.Text.ToUpper());
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnreverse_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LoadDivisionEntity Obj = new LoadDivisionEntity();
                if (rad_grpa.IsChecked == true)
                {
                    if (lbx_grpa.SelectedIndex != -1)
                    {
                        Obj = (LoadDivisionEntity)lbx_grpa.SelectedItem;
                        vm.RemoveFromGroup(vm.LstGroupA, Obj);
                        lbx_grpa.SelectedIndex = 0;
                    }
                }
                else if (rad_grpb.IsChecked == true)
                {
                    if (lbx_grpb.SelectedIndex != -1)
                    {
                        Obj = (LoadDivisionEntity)lbx_grpb.SelectedItem;
                        vm.RemoveFromGroup(vm.LstGroupB, Obj);
                        lbx_grpb.SelectedIndex = 0;
                    }
                }
                else if (rad_grpc.IsChecked == true)
                {
                    if (lbx_grpc.SelectedIndex != -1)
                    {
                        Obj = (LoadDivisionEntity)lbx_grpc.SelectedItem;
                        vm.RemoveFromGroup(vm.LstGroupC, Obj);
                        lbx_grpc.SelectedIndex = 0;
                    }
                }
                else
                {
                    if (lbx_grpd.SelectedIndex != -1)
                    {
                        Obj = (LoadDivisionEntity)lbx_grpd.SelectedItem;
                        vm.RemoveFromGroup(vm.LstGroupD, Obj);
                    }
                }
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }
        }

        private void btntransfer_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cbx_groups.SelectedIndex != -1)
                {
                    if (lbx_teams.SelectedIndex != -1)
                    {
                        TeamEntity Obj = new TeamEntity();
                        GroupEntity Objgroup = new GroupEntity();
                        Obj = (TeamEntity)lbx_teams.SelectedItem;
                        if (rad_grpa.IsChecked == true)
                        {
                            Objgroup = User.Lstgroups.Where(p => p.GroupName == "A").SingleOrDefault();
                            vm.AddtoGroup(vm.LstGroupA, Obj, Objgroup);
                        }
                        else if (rad_grpb.IsChecked == true)
                        {
                            Objgroup = User.Lstgroups.Where(p => p.GroupName == "B").SingleOrDefault();
                            vm.AddtoGroup(vm.LstGroupB, Obj, Objgroup);
                        }
                        else if (rad_grpc.IsChecked == true)
                        {
                            Objgroup = User.Lstgroups.Where(p => p.GroupName == "C").SingleOrDefault();
                            vm.AddtoGroup(vm.LstGroupC, Obj, Objgroup);
                        }
                        else
                        {
                            Objgroup = User.Lstgroups.Where(p => p.GroupName == "D").SingleOrDefault();
                            vm.AddtoGroup(vm.LstGroupD, Obj, Objgroup);
                        }
                        lbx_teams.SelectedIndex = 0;
                    }
                    else
                    {
                        throw new Exception("Select Team To Transfer");
                    }
                }
                else
                {
                    throw new Exception("Select No. of Groups");
                }
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message);
            }
        }

        private void cbx_groups_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try { 
            if (cbx_groups.SelectedIndex != -1)
            {
                if (cbx_groups.SelectedIndex == 0)
                {
                    grp_groupa.Visibility = Visibility.Visible;
                    grp_groupb.Visibility = Visibility.Hidden;
                    grp_groupc.Visibility = Visibility.Hidden;
                    grp_groupd.Visibility = Visibility.Hidden;
                }
                else if (cbx_groups.SelectedIndex == 1)
                {
                    grp_groupa.Visibility = Visibility.Visible;
                    grp_groupb.Visibility = Visibility.Visible;
                    grp_groupc.Visibility = Visibility.Hidden;
                    grp_groupd.Visibility = Visibility.Hidden;
                }
                else if (cbx_groups.SelectedIndex == 2)
                {
                    grp_groupa.Visibility = Visibility.Visible;
                    grp_groupb.Visibility = Visibility.Visible;
                    grp_groupc.Visibility = Visibility.Visible;
                    grp_groupd.Visibility = Visibility.Hidden;
                }
                else
                {
                    grp_groupa.Visibility = Visibility.Visible;
                    grp_groupb.Visibility = Visibility.Visible;
                    grp_groupc.Visibility = Visibility.Visible;
                    grp_groupd.Visibility = Visibility.Visible;
                }
                }
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btngeneratefixtures_Click(object sender, RoutedEventArgs e)
        {
            try {
                btnsave_Click(null, null);
                vm.GenerateFixtures(cbx_groups.SelectedIndex + 1);
                ViewFixtures page = new ViewFixtures();
                this.Content = page;
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnviewfixtures_Click(object sender, RoutedEventArgs e)
        {
            ViewFixtures page = new ViewFixtures();
            this.Content = page;
        }
    }
}
