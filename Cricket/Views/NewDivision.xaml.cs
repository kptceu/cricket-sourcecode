﻿
using Cricket.DAL;
using Cricket.Model;
using Cricket.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
namespace Cricket.Views
{
    /// <summary>
    /// Interaction logic for NewDivision.xaml
    /// </summary>
    public partial class NewDivision : UserControl
    {

        string selecteddivisionid = "";
        NewDivisionVM ObjDivisionVM;
        public NewDivision()
        {
            try
            {
                InitializeComponent();
                txt_divisionname.Text = "";
                //loaddivisionvalues();
                ObjDivisionVM = new NewDivisionVM();
                this.DataContext = ObjDivisionVM;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }







        private void griddivisionvalues_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {

                if (ObjDivisionVM.CurrentDivision == null || ObjDivisionVM.CurrentDivision.IsNew == false)
                {
                    if (griddivisionvalues.SelectedItem != null)
                    {
                        DivisionEntity objgridselected = (DivisionEntity)griddivisionvalues.SelectedItem;
                        ObjDivisionVM.CurrentDivision = objgridselected;

                        neworange();
                        if (objgridselected.DivisionName == "1st Division" || objgridselected.DivisionName == "2nd Division" || objgridselected.DivisionName == "3rd Division")
                        {
                            txt_divisionname.IsEnabled = false;
                            txt_noofdaysofplay.IsEnabled = false;
                            txt_noofinnings.IsEnabled = false;
                            txt_noofoversperinnings.IsEnabled = false;
                        }
                        else
                        {
                            txt_divisionname.IsEnabled = true;
                            txt_noofdaysofplay.IsEnabled = true;
                            txt_noofinnings.IsEnabled = true;
                            txt_noofoversperinnings.IsEnabled = true;
                        }
                    }
                    txt_divisionname.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void newgreen()
        {
            try
            {
                txt_divisionname.BorderBrush = Brushes.Green;

                txtballname.BorderBrush = Brushes.Green;
                txt_noofinnings.BorderBrush = Brushes.Green;
                txt_noofoversperinnings.BorderBrush = Brushes.Green;
                txt_noofdaysofplay.BorderBrush = Brushes.Green;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void neworange()
        {
            try
            {
                txt_divisionname.BorderBrush = Brushes.Orange;

                txtballname.BorderBrush = Brushes.Orange;
                txt_noofinnings.BorderBrush = Brushes.Orange;
                txt_noofoversperinnings.BorderBrush = Brushes.Orange;
                txt_noofdaysofplay.BorderBrush = Brushes.Orange;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnsavedivision_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                foreach (DivisionEntity Divisiontovalidate in ObjDivisionVM.DivisionList)
                {
                    try
                    {
                        if (!Divisiontovalidate.ValidateEntity())
                            return;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                }

                List<EntityCollection> lsttosave = new List<EntityCollection>();

                Database.NewSaveEntity<DivisionEntity>(ObjDivisionVM.DivisionList, lsttosave);

                using (var dbContext = Database.getDBContext())
                {
                    Database.CommitChanges(lsttosave, dbContext);
                }

                User.HomePageVM.getDivisionList();

            }
            catch (Exception ex)
            {

                MessageBox.Show("Error " + ex.Message + " while saving", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void btnnew_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ObjDivisionVM.CurrentDivision = Database.getNewEntity<DivisionEntity>();
                ObjDivisionVM.DivisionList.Add(ObjDivisionVM.CurrentDivision);
                txt_divisionname.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ObjDivisionVM.CurrentDivision != null && ObjDivisionVM.CurrentDivision.IsNew)
                {

                    ObjDivisionVM.DivisionList.Remove(ObjDivisionVM.CurrentDivision);
                    ObjDivisionVM.CurrentDivision = null;
                    if (griddivisionvalues.SelectedItems != null) ;
                    griddivisionvalues_SelectionChanged(null, null);
                }
                else if (ObjDivisionVM.CurrentDivision != null && ObjDivisionVM.CurrentDivision.IsNew == false)
                {
                    DivisionEntity objDivisionfromDB = Database.GetEntity<DivisionEntity>(ObjDivisionVM.CurrentDivision.DivisionId);
                    ObjDivisionVM.DivisionList.Remove(ObjDivisionVM.CurrentDivision);

                    ObjDivisionVM.CurrentDivision = objDivisionfromDB;
                    ObjDivisionVM.DivisionList.Add(objDivisionfromDB);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
    }
}
