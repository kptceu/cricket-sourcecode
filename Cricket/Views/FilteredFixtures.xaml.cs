﻿using Cricket.Reports;
using Cricket.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Reporting.WinForms;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Cricket.Report_Models;
using System.Collections.ObjectModel;

namespace Cricket.Views
{
    /// <summary>
    /// Interaction logic for FilteredFixtures.xaml
    /// </summary>
    public partial class FilteredFixtures : UserControl
    {
        FilteredFixturesVM vm = new FilteredFixturesVM();
        public FilteredFixtures()
        {
            InitializeComponent();
            this.DataContext = vm;
        }

        private void btnloadfilteredfixtures_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dat_fromdate.SelectedDate == null)
                    MessageBox.Show("Select From Date", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
                else if (dat_todate.SelectedDate == null)
                    MessageBox.Show("Select From Date", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
                else if(dat_fromdate.SelectedDate> dat_todate.SelectedDate)
                    MessageBox.Show("From Date Cannot be greater than To Date", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
                else
                {
                    vm.LoadFilteredFixtures(DateTime.Parse(dat_fromdate.SelectedDate.ToString()), DateTime.Parse(dat_todate.SelectedDate.ToString()));
                    if (vm.LstFixture.Count == 0)
                        MessageBox.Show("No Data Found", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnexportfixtures_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dat_fromdate.SelectedDate == null)
                    MessageBox.Show("Select From Date", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
                else if (dat_todate.SelectedDate == null)
                    MessageBox.Show("Select From Date", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
                else if (dat_fromdate.SelectedDate > dat_todate.SelectedDate)
                    MessageBox.Show("From Date Cannot be greater than To Date", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
                else
                {
                    if (vm.LstFixture.Count > 0)
                    {
                        ObservableCollection<FilterFixturesPrint> LstForPrint = new ObservableCollection<FilterFixturesPrint>();
                        LstForPrint=vm.GenerateReports();
                        if (LstForPrint.Count > 0)
                        {
                            FilterFixturesReport frm = new FilterFixturesReport();
                            ReportViewer rv = frm.GetReportViewer();
                            rv.Reset();
                            rv.LocalReport.EnableExternalImages = true;
                            rv.LocalReport.ReportEmbeddedResource = @"Cricket.Rdlc.FilteredFixtures.rdlc";
                            ReportDataSource rds = new ReportDataSource("FilterFixturesDataSet", LstForPrint);
                            rv.LocalReport.DataSources.Add(rds);
                            rv.Refresh();
                            frm.ShowDialog();
                           // vm.GenerateExcel(DateTime.Parse(dat_fromdate.SelectedDate.ToString()), DateTime.Parse(dat_todate.SelectedDate.ToString()));
                        }
                    }
                    else
                    {
                        MessageBox.Show("No Data To Export");
                    }
                }
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void dgvfixtures_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = (e.Row.GetIndex() + 1).ToString();
        }
    }
}
