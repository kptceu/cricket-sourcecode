﻿using Cricket.DAL;
using Cricket.Model;
using Cricket.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cricket.Views
{
    /// <summary>
    /// Interaction logic for SelectPlayersForMatch.xaml
    /// </summary>
    public partial class SelectPlayersForMatch : UserControl
    {
        SelectPlayersForMatchVM vm = new SelectPlayersForMatchVM();

        public SelectPlayersForMatch()
        {
            InitializeComponent();
            this.DataContext = vm;
            loaddefaultvalues();
        }

        private void loaddefaultvalues()
        {
            if (vm.ObjFixture.UmpireOne != null)
                cbxumpire1.SelectedValue = vm.ObjFixture.UmpireOne.OfficialId;
            if (vm.ObjFixture.UmpireTwo != null)
                cbxumpire2.SelectedValue = vm.ObjFixture.UmpireTwo.OfficialId;
            if (vm.ObjFixture.Scorer != null)
                cbxscorer.SelectedValue = vm.ObjFixture.Scorer.OfficialId;
            if (vm.ObjFixture.Location != null)
                cbxVenue.SelectedValue = vm.ObjFixture.Location.LocationId;          
            
                cbxmatchtype.Text = vm.ObjFixture.MatchType.ToString();
            
            if (User.Division.NumberOfDays == 2)
            {
                EndDate.Visibility = Visibility.Visible;
                lblenddate.Visibility = Visibility.Visible;
            }
            else
            {
                EndDate.Visibility = Visibility.Hidden;
                lblenddate.Visibility = Visibility.Hidden;
            }

            if (vm.ObjPlayedMatch.IsWalkOverForTeamOne == true)
            {
                chkwalkover.IsChecked = true;
                cbx_walkOver.SelectedValue = vm.ObjPlayedMatch.TeamOneId.ToString();
                MessageBox.Show(cbx_walkOver.Text + " Has Won By WalkOver");
            }
            else if (vm.ObjPlayedMatch.IsWalkOverForTeamTwo == true)
            {
                chkwalkover.IsChecked = true;
                cbx_walkOver.SelectedValue = vm.ObjPlayedMatch.TeamTwoId.ToString();
                MessageBox.Show(cbx_walkOver.Text + " Has Won By WalkOver");
            }
        }


        private void transferplayers(PlayerEntity ObjPlayer,ObservableCollection<PlayerEntity> lstplayers,ObservableCollection<PlayerEntity> lstmaster)
       {
           try
           {
               if (lstplayers.Count < 11)
               {
                   if (ObjPlayer != null)
                   {
                       lstplayers.Add(ObjPlayer);
                        lstmaster.Remove(ObjPlayer);
                   }
                   else
                   {
                       MessageBox.Show("No players to Transfer");
                   }
               }
               else
               {
                   MessageBox.Show("Max 11 Players are Allowed");
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }

        private void reverseplayers(PlayerEntity ObjPlayer, ObservableCollection<PlayerEntity> lstplayers,ObservableCollection<PlayerEntity> lstmaster)
        {
            try
            {
                if (ObjPlayer != null)
                {
                    lstmaster.Add(ObjPlayer);
                    lstplayers.Remove(ObjPlayer);
                }
                else if (lstplayers.Count == 0)
                {
                    MessageBox.Show("No players to Reverse");
                }
                else
                {
                    MessageBox.Show("Select Player to Reverse");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       private void btnTransferedTeamA_Click(object sender, RoutedEventArgs e)
       {
           transferplayers((PlayerEntity) lbxTeam1.SelectedItem, vm.LstPlaying11TeamOne,vm.LstPlayerTeamOne);
            lbxTeam1.SelectedIndex = 0;
       }

       private void btnReverseTeamA_Click(object sender, RoutedEventArgs e)
       {
           reverseplayers((PlayerEntity)lbltransferedTeamA.SelectedItem, vm.LstPlaying11TeamOne, vm.LstPlayerTeamOne);
            lbltransferedTeamA.SelectedIndex = 0;
       }

       private void btnTransferedTeamB_Click(object sender, RoutedEventArgs e)
       {
           transferplayers((PlayerEntity)lbxTeam2.SelectedItem, vm.LstPlaying11TeamTwo,vm.LstPlayerTeamTwo);
            lbxTeam2.SelectedIndex = 0;
       }

       private void btnReverseTeamB_Click(object sender, RoutedEventArgs e)
       {
           reverseplayers((PlayerEntity)lbltransferedTeamB.SelectedItem, vm.LstPlaying11TeamTwo, vm.LstPlayerTeamTwo);
            lbltransferedTeamB.SelectedIndex = 0;
       }

        private void movelistboxitemsup(ListBox lst, ObservableCollection<PlayerEntity> LstPlayers)
       {
           try
           {
               var selectedIndex = lst.SelectedIndex;

               if (selectedIndex > 0)
               {
                   var itemToMoveUp = LstPlayers[selectedIndex];
                   LstPlayers.RemoveAt(selectedIndex);
                   LstPlayers.Insert(selectedIndex - 1, itemToMoveUp);
                   lst.SelectedIndex = selectedIndex - 1;
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
       }

        private void movelistboxitemsdown(ListBox lst,ObservableCollection<PlayerEntity> LstPlayers)
        {
            try
            {
                var selectedIndex = lst.SelectedIndex;
                if (selectedIndex + 1 < lst.Items.Count)
                {
                    var itemToMoveDown = LstPlayers[selectedIndex];
                    LstPlayers.RemoveAt(selectedIndex);                   
                    LstPlayers.Insert(selectedIndex + 1, itemToMoveDown);
                    lst.SelectedIndex = selectedIndex + 1;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       private void btnUp_Click(object sender, RoutedEventArgs e)
       {
           movelistboxitemsup(lbltransferedTeamA,vm.LstPlaying11TeamOne);
       }

       private void btnDown_Click(object sender, RoutedEventArgs e)
       {
           movelistboxitemsdown(lbltransferedTeamA, vm.LstPlaying11TeamOne);
       }

       private void btnUp2_Click(object sender, RoutedEventArgs e)
       {
           movelistboxitemsup(lbltransferedTeamB, vm.LstPlaying11TeamTwo);
       }

       private void btnDown2_Click(object sender, RoutedEventArgs e)
       {
           movelistboxitemsdown(lbltransferedTeamB, vm.LstPlaying11TeamTwo);
       }

        public void Save()
        {

            if (chkwalkover.IsChecked == true)
            {
                if (cbx_walkOver.SelectedItem == null)
                    throw new Exception("Select Team Which Has Won");
                else
                {
                    vm.ObjPlayedMatch.Fixture = vm.ObjFixture;
                    vm.ObjPlayedMatch.TeamOne = vm.ObjFixture.TeamOne;
                    vm.ObjPlayedMatch.TeamTwo = vm.ObjFixture.TeamTwo;
                    if (vm.ObjPlayedMatch.TeamOne.TeamId == Guid.Parse(cbx_walkOver.SelectedValue.ToString()))
                    {
                        vm.ObjPlayedMatch.IsWalkOverForTeamOne = true;
                        vm.ObjPlayedMatch.Result = vm.ObjFixture.TeamOneId.Value;
                    }
                    else
                    {
                        vm.ObjPlayedMatch.IsWalkOverForTeamTwo = true;
                        vm.ObjPlayedMatch.Result = vm.ObjFixture.TeamTwoId.Value;
                    }

                    vm.ObjPlayedMatch.CompleteStatus = true;

                    List<EntityCollection> lsttosave = new List<EntityCollection>();
                    Database.NewSaveEntity<FixtureEntity>(vm.ObjFixture, lsttosave);
                    Database.NewSaveEntity<PlayedMatchEntity>(vm.ObjPlayedMatch, lsttosave);

                    Database.CommitChanges(lsttosave, Database.getDBContext());
                    MessageBox.Show("Saved");
                }
            }
            else {

                int count = 0;
                if (vm.LstPlaying11TeamOne.Count >= 9 && vm.LstPlaying11TeamTwo.Count >= 9)
                {
                    if (txtteam1points.Text == "")
                    {
                        throw new Exception("Enter Team 1 Points");
                    }
                    else if (txtteam2points.Text == "")
                    {
                        throw new Exception("Enter Team 2 Points");
                    }
                    else if (raddtn1won.IsChecked != true && radbtn1Lost.IsChecked != true && radbtn1Draw.IsChecked != true)
                    {
                        throw new Exception("Choose A Result");
                    }
                    else if (txt_resultremarks.Text == "")
                    {
                        throw new Exception("Enter Result Remarks");
                    }
                    else if (cbxteam1captain.SelectedIndex == -1)
                    {
                        throw new Exception("Select Team1 Captain");
                    }
                    else if (cbxteam1keeper.SelectedIndex == -1)
                    {
                        throw new Exception("Select Team2 Captain");
                    }
                    else if (cbxteam2captain.SelectedIndex == -1)
                    {
                        throw new Exception("Select Team2 Captain");
                    }
                    else if (cbxteam2keeper.SelectedIndex == -1)
                    {
                        throw new Exception("Select Team2 Keeper");
                    }
                    else if (cbxtoss.SelectedIndex == -1)
                    {
                        throw new Exception("Select Team that has won the toss");
                    }
                    else if (cbxelected.SelectedIndex == -1)
                    {
                        throw new Exception("Select Bat/Bowl");
                    }
                    else if (cbxVenue.SelectedIndex == -1)
                    {
                        throw new Exception("Select Ground");
                    }
                    else if (cbxumpire1.SelectedIndex == -1)
                    {
                        throw new Exception("Select Umpire1");
                    }
                    else if (cbxumpire2.SelectedIndex == -1)
                    {
                        throw new Exception("Select Umpire2");
                    }
                    else if (cbxscorer.SelectedIndex == -1)
                    {
                        throw new Exception("Select Scorer");
                    }
                    else
                    {
                        vm.ObjPlayedMatch.Fixture = vm.ObjFixture;
                        vm.ObjPlayedMatch.TeamOne = vm.ObjFixture.TeamOne;
                        vm.ObjPlayedMatch.TeamTwo = vm.ObjFixture.TeamTwo;
                        vm.ObjPlayedMatch.TossWonBy = cbxtoss.Text;
                        vm.ObjPlayedMatch.TeamOneCaptain = (PlayerEntity)cbxteam1captain.SelectedItem;
                        vm.ObjPlayedMatch.TeamTwoCaptain = (PlayerEntity)cbxteam2captain.SelectedItem;
                        vm.ObjPlayedMatch.TeamOneKeeper = (PlayerEntity)cbxteam1keeper.SelectedItem;
                        vm.ObjPlayedMatch.TeamTwoKeeper = (PlayerEntity)cbxteam2keeper.SelectedItem;
                        vm.ObjPlayedMatch.MaximumOvers = User.Division.MaxOverPerInnings;

                        count = 1;
                        vm.LstPlaying11TeamOne.ToList().ForEach(x =>
                        {
                            PlayedMatchDetailEntity objnew = Database.getNewEntity<PlayedMatchDetailEntity>();
                            objnew.BattingOrder = count;
                            objnew.Player = x;
                            objnew.Fixture = vm.ObjFixture;
                            objnew.Team = vm.ObjFixture.TeamOne;
                            vm.LstPlayedMatchDetail.Add(objnew);
                            count++;
                        });
                        count = 1;

                        vm.LstPlaying11TeamTwo.ToList().ForEach(x =>
                            {
                                PlayedMatchDetailEntity objnew = Database.getNewEntity<PlayedMatchDetailEntity>();
                                objnew.BattingOrder = count;
                                objnew.Player = x;
                                objnew.Fixture = vm.ObjFixture;
                                objnew.Team = vm.ObjFixture.TeamTwo;
                                vm.LstPlayedMatchDetail.Add(objnew);
                                count++;
                            });


                        //vm.ObjPlayedMatch.TossWonBy = cbxtoss.Text;

                        List<EntityCollection> lsttosave = new List<EntityCollection>();
                        Database.NewSaveEntity<FixtureEntity>(vm.ObjFixture, lsttosave);
                        Database.NewSaveEntity<PlayedMatchEntity>(vm.ObjPlayedMatch, lsttosave);
                        Database.NewSaveEntity<PlayedMatchDetailEntity>(vm.LstPlayedMatchDetail, lsttosave);


                        Database.CommitChanges(lsttosave, Database.getDBContext());

                        MessageBox.Show("Saved");

                        User.Fixture = Database.GetEntity<FixtureEntity>(User.Fixture.FixtureId);
                        User.ObjPlayedMatch = Database.GetEntity<PlayedMatchEntity>(vm.ObjPlayedMatch.PlayedMatchId);
                    }
                }
                else
                {
                    throw new Exception("Teams Should Have A Minimum Of 9 Players");
                }
            }           

        }

       private void btngotoscorecard_Click(object sender, RoutedEventArgs e)
       {
            try
            {
                Save();

                if (chkwalkover.IsChecked == false)
                {
                    MatchScoring page = new MatchScoring();
                    this.Content = page;
                }
                else
                {
                    throw new Exception("Team has Walked Over");
                }                
            }
            catch(Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }

       }

       private void raddtn1won_Checked(object sender, RoutedEventArgs e)
       {
           radbtn2Lost.IsChecked = true;
            vm.ObjPlayedMatch.Result = vm.ObjFixture.TeamOneId.Value;
           if (User.Division.NumberOfDays==2)
           {
               vm.ObjPlayedMatch.TeamOnePoints = 5;
                vm.ObjPlayedMatch.TeamTwoPoints = -1;
           }
           else
           {
                vm.ObjPlayedMatch.TeamOnePoints = 4;
                vm.ObjPlayedMatch.TeamTwoPoints = 0;
            }

        }

       private void radbtn2Won_Checked(object sender, RoutedEventArgs e)
       {
           radbtn1Lost.IsChecked = true;
            vm.ObjPlayedMatch.Result = vm.ObjFixture.TeamTwoId.Value;
            if (User.Division.NumberOfDays == 2)
            {
                vm.ObjPlayedMatch.TeamOnePoints = -1;
                vm.ObjPlayedMatch.TeamTwoPoints = 5;
            }
            else
           {
                vm.ObjPlayedMatch.TeamOnePoints = 0;
                vm.ObjPlayedMatch.TeamTwoPoints = 4;
            }
        }

       private void radbtn2Lost_Checked(object sender, RoutedEventArgs e)
       {
           raddtn1won.IsChecked = true;
            vm.ObjPlayedMatch.Result = vm.ObjFixture.TeamOneId.Value;
            if (User.Division.NumberOfDays == 2)
            {
                vm.ObjPlayedMatch.TeamOnePoints = 5;
                vm.ObjPlayedMatch.TeamTwoPoints = -1;
            }
            else
           {
                vm.ObjPlayedMatch.TeamOnePoints = 4;
                vm.ObjPlayedMatch.TeamTwoPoints = 0;
            }
        }

       private void radbtn1Lost_Checked(object sender, RoutedEventArgs e)
       {
           radbtn2Won.IsChecked = true;
            vm.ObjPlayedMatch.Result = vm.ObjFixture.TeamTwoId.Value;
            if (User.Division.NumberOfDays == 2)
            {
                vm.ObjPlayedMatch.TeamOnePoints = -1;
                vm.ObjPlayedMatch.TeamTwoPoints = 5;
            }
            else
           {
                vm.ObjPlayedMatch.TeamOnePoints = 0;
                vm.ObjPlayedMatch.TeamTwoPoints = 4;
            }
        }

       private void radbtn1Draw_Checked(object sender, RoutedEventArgs e)
       {
           radbtn2Draw.IsChecked = true;
            vm.ObjPlayedMatch.Result = Guid.Empty;
            if (User.Division.NumberOfDays == 2)
            {
                vm.ObjPlayedMatch.TeamOnePoints = 2;
                vm.ObjPlayedMatch.TeamTwoPoints = 2;
            }
            else
           {
                vm.ObjPlayedMatch.TeamOnePoints = 2;
                vm.ObjPlayedMatch.TeamTwoPoints = 2;
            }
        }

       private void radbtn2Draw_Checked(object sender, RoutedEventArgs e)
       {
           radbtn1Draw.IsChecked = true;
            vm.ObjPlayedMatch.Result = Guid.Empty;
            if (User.Division.NumberOfDays == 2)
            {
               txtteam1points.Text = "2";
               txtteam2points.Text = "2";
           }
           else
           {
               txtteam1points.Text = "2";
               txtteam2points.Text = "2";
           }
       }


        public void AddPlayer(PlayerEntity objplayer,string playername,TeamEntity Objteam)
        {
            try { 
            int count = Database.GetEntityList<PlayerEntity>().Count + 1;
            string kscauid = "";
            if (count < 10)
            {
                kscauid = "7P0000" + count;
            }
            else if (count >= 10 && count < 100)
            {
                kscauid = "7P000" + count;
            }
            else if (count >= 100 && count < 1000)
            {
                kscauid = "7P00" + count;
            }
            else if (count >= 1000 && count < 10000)
            {
                kscauid = "7P0" + count;
            }
            else
            {
                kscauid = "7P" + count;
            }

            objplayer.PlayerName = playername.ToUpper();
            objplayer.KSCAUID = kscauid;
            objplayer.CZone = User.Zone;
            objplayer.Team = Objteam;
            objplayer.DateOfBirth = DateTime.Now;
            objplayer.Gender = "Male";
            objplayer.WicketKeeper = false;
            objplayer.BattingStyle = "Right Handed";
            objplayer.BowlingStyle = "Right Arm";
            objplayer.BowlingSubStyle = "Fast";

            List<EntityCollection> lsttosave = new List<EntityCollection>();
            Database.NewSaveEntity<PlayerEntity>(objplayer, lsttosave);
            Database.CommitChanges(lsttosave, Database.getDBContext());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        public  void AddTeam1Player()
        {
            PlayerEntity objplayer = Database.getNewEntity<PlayerEntity>();
            AddPlayer(objplayer, txtteam1player.Text, vm.ObjFixture.TeamOne);

            objplayer = Database.GetEntity<PlayerEntity>(objplayer.PlayerId);

            vm.LstPlayerTeamOne.Add(objplayer);
            txtteam1player.Text = "";
            MessageBox.Show("Player with Name " + objplayer.PlayerName + " Added");
        }


        private void btnaddteam1player_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtteam1player.Text == "")
                {
                    MessageBox.Show("Enter Player Name");
                }
                else
                {
                    if ((vm.checkplayer(txtteam1player.Text, vm.ObjFixture.TeamOneId.Value)) == true)
                    {
                        AddTeam1Player();
                    }
                    else
                    {
                        MessageBoxResult res = MessageBox.Show("Player With The Same Name Already Exists In The Team, Are You Sure You Want to Add Player With The Name " + txtteam1player.Text, "Info", MessageBoxButton.YesNo, MessageBoxImage.Information);
                        if (res == MessageBoxResult.Yes)
                        {
                            AddTeam1Player();
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        public void Addteam2Player()
        {
            PlayerEntity objplayer = Database.getNewEntity<PlayerEntity>();
            AddPlayer(objplayer, txtteam2player.Text, User.Fixture.TeamTwo);

            objplayer = Database.GetEntity<PlayerEntity>(objplayer.PlayerId);

            vm.LstPlayerTeamTwo.Add(objplayer);
            txtteam2player.Text = "";
            MessageBox.Show("Player with Name " + objplayer.PlayerName + " Added");

        }

        private void btnaddteam2player_Click(object sender, RoutedEventArgs e)
       {
            try
            {
                if (txtteam2player.Text == "")
                {
                    MessageBox.Show("Enter Player Name");
                }
                else
                {
                    if ((vm.checkplayer(txtteam2player.Text, vm.ObjFixture.TeamTwoId.Value)) == true)
                    {
                        Addteam2Player();
                    }
                    else
                    {
                        MessageBoxResult res = MessageBox.Show("Player With The Same Name Already Exists In The Team, Are You Sure You Want to Add Player With The Name " + txtteam2player.Text, "Info", MessageBoxButton.YesNo, MessageBoxImage.Information);
                        if (res == MessageBoxResult.Yes)
                        {
                            Addteam2Player();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        public void AddGround()
        {
            int count = vm.LstLocation.Count + 1;

            string groundkeyid = "";
            if (count < 10)
            {
                groundkeyid = "7G0" + count;
            }
            else if (count >= 10 && count < 100)
            {
                groundkeyid = "7G" + count;
            }

            LocationEntity objlocation = Database.getNewEntity<LocationEntity>();
            objlocation.StadiumName = txtnewground.Text;
            objlocation.GroundKeyId = groundkeyid;
            objlocation.CZone = User.Zone;
            List<EntityCollection> lsttosave = new List<EntityCollection>();
            Database.NewSaveEntity<LocationEntity>(objlocation, lsttosave);
            Database.CommitChanges(lsttosave, Database.getDBContext());

            objlocation = Database.GetEntity<LocationEntity>(objlocation.LocationId);
            vm.LstLocation.Add(objlocation);

            txtnewground.Text = "";
            MessageBox.Show("Ground with Name " + objlocation.StadiumName + " Added");
        }

        private void btnaddnewground_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtnewground.Text == "")
                {
                    MessageBox.Show("Enter Ground Name");
                }
                else
                {
                    if ((vm.checkGround(txtnewground.Text)) == true)
                    {
                        AddGround();
                    }
                    else
                    {
                        MessageBoxResult res = MessageBox.Show("Ground With The Same Name Already Exists Are You Sure You Want to Add New Ground With The Name " + txtnewground.Text, "Info", MessageBoxButton.YesNo, MessageBoxImage.Information);
                        if (res == MessageBoxResult.Yes)
                        {
                            AddGround();
                        }
                    }
                }
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        public  void AddOfficial()
        {
            int count = vm.LstOfficial.Count + 1;

            string officialkeyid = "";
            if (count < 10)
            {
                officialkeyid = "7U0" + count;
            }
            else if (count >= 10 && count < 100)
            {
                officialkeyid = "7U" + count;
            }

            OfficialEntity objOfficial = Database.getNewEntity<OfficialEntity>();
            objOfficial.CZone = User.Zone;
            objOfficial.Name = txtnewofficial.Text;
            objOfficial.OfficialKeyId = officialkeyid;
            objOfficial.Gender = "Male";
            objOfficial.DateOfBirth = DateTime.Now;

            List<EntityCollection> lsttosave = new List<EntityCollection>();
            Database.NewSaveEntity<OfficialEntity>(objOfficial, lsttosave);
            Database.CommitChanges(lsttosave, Database.getDBContext());
            txtnewofficial.Text = "";

            objOfficial = Database.GetEntity<OfficialEntity>(objOfficial.OfficialId);
            vm.LstOfficial.Add(objOfficial);

            MessageBox.Show("Official with Name " + objOfficial.Name + " Added");
        }

        private void btnaddnewofficial_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtnewofficial.Text == "")
                {
                    MessageBox.Show("Enter Official Name");
                }
                else
                {
                    if ((vm.checkofficial(txtnewofficial.Text)) == true)
                    {
                        AddOfficial();
                    }
                    else
                    {
                        MessageBoxResult res = MessageBox.Show("Official With The Same Name Already Exists,Are You Sure You Want to Add New Official With The Name " + txtnewofficial.Text, "Info", MessageBoxButton.YesNo, MessageBoxImage.Information);
                        if (res == MessageBoxResult.Yes)
                        {
                            AddOfficial();
                        }
                    }
                }
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }


       private void dropdownofficialduplicatesremove(ComboBox cbx1, ComboBox cbx2, ComboBox cbxsender)
       {
            try { 
            if (cbx1.SelectedIndex != -1)
            {
                vm.LstOfficial.Where(p => p.OfficialId.ToString() == cbx1.SelectedValue.ToString()).ToList().ForEach(x =>
                {
                    x.AllowedColor = "#FFDB99";
                    x.IsSelectable = false;
                });
            }
            if (cbx2.SelectedIndex != -1)
            {
                vm.LstOfficial.Where(p => p.OfficialId.ToString() == cbx2.SelectedValue.ToString()).ToList().ForEach(x =>
                {
                    x.AllowedColor = "#FFDB99";
                    x.IsSelectable = false;
                });
            }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }

        }

        private void loadavailablegroundandofficials()
        {
            try
            {
                if (startDate.SelectedDate != null)
                {
                    ObservableCollection<FixtureEntity> LstFixture = User.lsTFixture;

                    LstFixture.Where(p => (p.FixtureId != User.Fixture.FixtureId && (p.FromDate == DateTime.Parse(startDate.SelectedDate.ToString()) || p.ToDate == DateTime.Parse(startDate.SelectedDate.ToString())
                    || p.ToDate == DateTime.Parse(EndDate.SelectedDate.ToString()) || p.FromDate == DateTime.Parse(EndDate.SelectedDate.ToString()))) && p.FixtureId != vm.ObjFixture.FixtureId).ToList().ForEach(obj =>
                    {
                        vm.LstLocation.Where(p => p.LocationId == obj.LocationId).ToList().ForEach(x =>
                        {
                            x.AllowedColor = "#FFDB99";
                            x.IsSelectable = false;
                        });

                        vm.LstOfficial.Where(p => p.OfficialId == obj.UmpireOneId || p.OfficialId == obj.UmpireTwoId || p.OfficialId == obj.ScorerId).ToList().ForEach(x =>
                        {
                            x.AllowedColor = "#FFDB99";
                            x.IsSelectable = false;
                        });
                    });
                }
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }

        }

        private void loadavailablegroundandofficialsotherdiv()
        {
            try
            {
                if (startDate.SelectedDate != null)
                {
                    ObservableCollection<FixtureEntity> LstFixture = User.lsTFixture;


                    //List<FixtureEntity> lst1 = LstFixture.Where(p =>( p.FromDate == DateTime.Parse(startDate.SelectedDate.ToString()) || p.ToDate == DateTime.Parse(startDate.SelectedDate.ToString())) && p.FixtureId != vm.ObjFixture.FixtureId).ToList();

                    LstFixture.Where(p => (p.FromDate == DateTime.Parse(startDate.SelectedDate.ToString()) || p.ToDate == DateTime.Parse(startDate.SelectedDate.ToString())) && p.FixtureId != vm.ObjFixture.FixtureId).ToList().ForEach(obj =>
                    {
                        vm.LstLocation.Where(p => p.LocationId == obj.LocationId).ToList().ForEach(x =>
                        {
                            x.AllowedColor = "#FFDB99";
                            x.IsSelectable = false;
                        });

                        vm.LstOfficial.Where(p => p.OfficialId == obj.UmpireOneId || p.OfficialId == obj.UmpireTwoId || p.OfficialId == obj.ScorerId).ToList().ForEach(x =>
                        {
                            x.AllowedColor = "#FFDB99";
                            x.IsSelectable = false;
                        });
                    });
                }
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void cbxumpire1_DropDownOpened(object sender, EventArgs e)
       {
           if (startDate.SelectedDate == null)
           {
               MessageBox.Show("Select StartDate");
           }
           else
           {
               dropdownofficialduplicatesremove(cbxumpire2, cbxscorer, cbxumpire1);
           }
       }

       private void cbxumpire2_DropDownOpened(object sender, EventArgs e)
       {
           if (startDate.SelectedDate == null)
           {
               MessageBox.Show("Select StartDate");
           }
           else
           {
               dropdownofficialduplicatesremove(cbxumpire1, cbxscorer, cbxumpire2);
           }
       }

       private void cbxscorer_DropDownOpened(object sender, EventArgs e)
       {
           if (startDate.SelectedDate == null)
           {
               MessageBox.Show("Select StartDate");
           }
           else
           {
               dropdownofficialduplicatesremove(cbxumpire2, cbxumpire1, cbxscorer);
           }
       }

        private void startDate_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (User.Division.NumberOfDays == 2)
                {
                    if (startDate.SelectedDate != null)
                    {
                        DateTime enddatecheck = DateTime.Parse(startDate.SelectedDate.ToString()).AddDays(1);
                        //  fromdate.AddDays(1);
                        EndDate.SelectedDate = enddatecheck;
                        loadavailablegroundandofficials();
                    }
                }
                else
                {
                    loadavailablegroundandofficialsotherdiv();
                }
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void chkwalkover_Checked(object sender, RoutedEventArgs e)
        {
            cbx_walkOver.Visibility = Visibility.Visible;
        }

        private void chkwalkover_Unchecked(object sender, RoutedEventArgs e)
        {
            cbx_walkOver.Visibility = Visibility.Hidden;
        }

        private void cbx_walkOver_DropDownClosed(object sender, EventArgs e)
        {
            if (cbx_walkOver.SelectedIndex == -1)
                MessageBox.Show("Select Team Which Has Won");
            else
            {
                if (cbx_walkOver.SelectedValue.ToString() == vm.ObjFixture.TeamOne.TeamId.ToString())
                    raddtn1won.IsChecked = true;
                else
                    radbtn2Won.IsChecked = true;
            }
        }

        private void cbxVenue_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbxVenue.SelectedItem != null)
            {
                if (((LocationEntity)cbxVenue.SelectedItem).IsSelectable == false)
                {
                    cbxVenue.SelectedItem = null;
                }
            }
        }

        private void cbxofficial_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cbx = (ComboBox)sender;
            if (cbx.SelectedItem != null)
            {
                if (((OfficialEntity)cbx.SelectedItem).IsSelectable == false)
                {
                    cbx.SelectedItem = null;
                }
            }
        }

        private void cbxtoss_SelectionChanged(object sender, EventArgs e)
        {
            if (cbxtoss.SelectedIndex == -1)
                MessageBox.Show("Select Team That Has Won the Toss", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            else
            {
                vm.ObjPlayedMatch.TossWonBy = cbxtoss.Text;
            }
        }
    }
}
