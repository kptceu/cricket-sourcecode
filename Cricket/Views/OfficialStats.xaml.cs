﻿
using Cricket.Model;
using Cricket.ViewModel;

using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cricket.Views
{
    /// <summary>
    /// Interaction logic for OfficialStats.xaml
    /// </summary>
    public partial class OfficialStats : UserControl
    {
        OfficialStatsVM vm = new OfficialStatsVM();

        public OfficialStats()
        {
            InitializeComponent();
            this.DataContext = vm;
        }

        private void cbxOfficial_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                if (cbxOfficial.SelectedIndex == -1)
                    MessageBox.Show("Select Location To Load Statistics");
                else
                {
                    dat_fromdate.SelectedDate = null;
                    dattodate.SelectedDate = null;
                    cbxdivision.SelectedIndex = -1;
                    vm.LoadOfficialStats(Guid.Parse(cbxOfficial.SelectedValue.ToString()));
                    if (vm.LstStatsScorer.Count == 0 && vm.LstStatsUmpire.Count == 0)
                        throw new Exception("No Data Found");
                }
            }
            catch(Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnexportofficialdata_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (vm.LstStatsUmpire.Count > 0 ||vm.LstStatsScorer.Count>0) 
                {
                    vm.GenerateExcel((OfficialEntity)cbxOfficial.SelectedItem);
                }
                else
                {
                    MessageBox.Show("No Data To Export");
                }

            }
            catch(Exception es )
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void gridscorerdetails_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = (e.Row.GetIndex() + 1).ToString();
        }

        private void gridumpiredetails_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = (e.Row.GetIndex() + 1).ToString();
        }

        private void btnfilterdata_Click(object sender, RoutedEventArgs e)
        {
            if (vm.LstStatsUmpire.Count == 0 && vm.LstStatsScorer.Count == 0)
                MessageBox.Show("No Stats To Filter", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            else if (cbxdivision.SelectedIndex == -1 && dat_fromdate.SelectedDate == null && dattodate.SelectedDate == null)
                MessageBox.Show("Select Dates Or Division To Filter", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            else if (cbxdivision.SelectedIndex == -1)
            {
                if (dat_fromdate.SelectedDate == null)
                    MessageBox.Show("Select from Date", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
                else if (dattodate.SelectedDate == null)
                    MessageBox.Show("Select to Date", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
                else if (dat_fromdate.SelectedDate > dattodate.SelectedDate)
                    MessageBox.Show("From Date Cannot be Greater Than to Date", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
                else
                {
                    vm.FilterStats(DateTime.Parse(dat_fromdate.SelectedDate.ToString()), DateTime.Parse(dattodate.SelectedDate.ToString()));
                }
            }
            else
            {
                if (cbxdivision.SelectedIndex != -1 && dat_fromdate.SelectedDate != null && dattodate.SelectedDate != null && ((DivisionEntity)cbxdivision.SelectedItem).DivisionName=="ALL")
                    vm.FilterStats(DateTime.Parse(dat_fromdate.SelectedDate.ToString()), DateTime.Parse(dattodate.SelectedDate.ToString()));
               else if (cbxdivision.SelectedIndex != -1 && dat_fromdate.SelectedDate != null && dattodate.SelectedDate != null)
                    vm.FilterStats(DateTime.Parse(dat_fromdate.SelectedDate.ToString()), DateTime.Parse(dattodate.SelectedDate.ToString()), (DivisionEntity)cbxdivision.SelectedItem);
                else
                    vm.FilterStats((DivisionEntity)cbxdivision.SelectedItem);
            }
        }
    }
}