﻿
using Cricket.DAL;
using Cricket.Model;
using Cricket.ViewModel;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace Cricket.Views
{
    /// <summary>
    /// Interaction logic for NewPlayer.xaml
    /// </summary>
    public partial class NewPlayer : UserControl
    {
        string destinationFilename;
        NewPlayerVM ObjNewPlayerVM = new NewPlayerVM();
        public NewPlayer()
        {
            InitializeComponent();
            //ObjNewPlayerVM.CurrentPlayer = Database.getNewEntity<PlayerEntity>();
            
            this.DataContext = ObjNewPlayerVM;
            cbxzone.SelectedValue = ((ZoneEntity)User.Zone).ZoneId;
            if (User.Zone == null)
            {
                MessageBox.Show("Select Zone to continue", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (User.Season == null)
            {
                MessageBox.Show("Select Season to continue", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (User.Division == null)
            {
                MessageBox.Show("Select Division to continue", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            ObjNewPlayerVM.getCurrentSeasonZoneDivisionPlayers(User.Zone, User.Season, User.Division);
            using (var CricketContext = Database.getDBContext())
            {
                ObjNewPlayerVM.getTeamsinCurrentZone(CricketContext, User.Zone);
            }

                btnnew_Click(null, null);
        }




        private void btnupload_Click(object sender, RoutedEventArgs e)
        {

            OpenFileDialog ofd = new OpenFileDialog();
            string extension = ".jpg";
            try
            {
                Nullable<bool> result = ofd.ShowDialog();
                if (result == true)
                {
                    BitmapImage bip = new BitmapImage(new Uri(ofd.FileName));
                    string name = ofd.FileName;
                    destinationFilename = User.PlayerImagePath + ObjNewPlayerVM.CurrentPlayer.KSCAUID + extension;
                    if (!File.Exists(destinationFilename))
                    {
                        playerimage.Source = bip;

                        File.Copy(name, destinationFilename);
                        ObjNewPlayerVM.CurrentPlayer.PlayerImagePath = destinationFilename;
                    }
                    else
                    {
                        destinationFilename = User.PlayerImagePath + txt_kscauid.Text + Guid.NewGuid() + extension;
                        playerimage.Source = bip;
                        File.Copy(name, destinationFilename);
                        ObjNewPlayerVM.CurrentPlayer.PlayerImagePath = destinationFilename;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("File Not in Correct Format", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnsave_Click(object sender, RoutedEventArgs e)
        {
            PlayerEntity PlayerValidated = new PlayerEntity();
           // ObjNewPlayerVM.CurrentPlayer.Division = User.Division;
           // ObjNewPlayerVM.CurrentPlayer.Season = User.Season;
            try
            {
                ObjNewPlayerVM.CurrentPlayer.Team = (TeamEntity)cbxTeam.SelectedItem;
                foreach (PlayerEntity playertovalidate in ObjNewPlayerVM.CurrentZoneDivisionSeasonPlayers )
                {
                    PlayerValidated = playertovalidate;
                    playertovalidate.ValidateEntity();
                }
                try
                {
                    List<EntityCollection> lsttosave = new List<EntityCollection>();

                    Database.NewSaveEntity<PlayerEntity>(ObjNewPlayerVM.CurrentPlayer, lsttosave);
                    using (var dbContext = Database.getDBContext())
                    {
                        Database.CommitChanges(lsttosave, dbContext);
                        ObjNewPlayerVM.LoadPlayers();
                        ObjNewPlayerVM.getCurrentSeasonZoneDivisionPlayers(User.Zone, User.Season, User.Division, (TeamEntity)cbxTeam.SelectedItem);

                    }
                    MessageBox.Show("Player Saved Successfully", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
                    //ObjNewPlayerVM.getCurrentSeasonZoneDivisionPlayers(User.Zone, User.Season, User.Division, (TeamEntity)cbxTeam.SelectedItem);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    ObjNewPlayerVM.CurrentPlayer = PlayerValidated;
                    return;
                }


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            // }
        }

        private void newgreen()
        {
            rbtnfemale.BorderBrush = Brushes.Green;
            rbtnleftbat.BorderBrush = Brushes.Green;
            rbtnleftbal.BorderBrush = Brushes.Green;
            cbkeeper.BorderBrush = Brushes.Green;
            txt_kscauid.BorderBrush = Brushes.Green;
            txt_fname.BorderBrush = Brushes.Green;
            txt_Fathername.BorderBrush = Brushes.Green;
            txt_mobno.BorderBrush = Brushes.Green;
            dateselect.BorderBrush = Brushes.Green;
            txt_emailid.BorderBrush = Brushes.Green;
            rbtnmale.BorderBrush = Brushes.Green;
            txt_address.BorderBrush = Brushes.Green;
            rbtnrightbat.BorderBrush = Brushes.Green;
            rbtnrightbal.BorderBrush = Brushes.Green;
            cbxbalstyle.BorderBrush = Brushes.Green;
        }

        private void neworange()
        {
            rbtnfemale.BorderBrush = Brushes.Orange;
            rbtnleftbat.BorderBrush = Brushes.Orange;
            rbtnleftbal.BorderBrush = Brushes.Orange;
            cbkeeper.BorderBrush = Brushes.Orange;
            txt_kscauid.BorderBrush = Brushes.Orange;
            txt_fname.BorderBrush = Brushes.Orange;
            txt_Fathername.BorderBrush = Brushes.Orange;
            txt_mobno.BorderBrush = Brushes.Orange;
            dateselect.BorderBrush = Brushes.Orange;
            txt_emailid.BorderBrush = Brushes.Orange;
            rbtnmale.BorderBrush = Brushes.Orange;
            txt_address.BorderBrush = Brushes.Orange;
            rbtnrightbat.BorderBrush = Brushes.Orange;
            rbtnrightbal.BorderBrush = Brushes.Orange;
            cbxbalstyle.BorderBrush = Brushes.Orange;
        }

        private void cbxzone_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try { 
            if (cbxzone.SelectedItem != null)
            {
                ObjNewPlayerVM.CurrentPlayer.CZone = (ZoneEntity)cbxzone.SelectedItem;
                generatekscauid();
            }
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void generatekscauid()
        {
            try { 
            if (cbxTeam.SelectedItem != null)
            {
                if (cbxzone.SelectedValue.ToString() != Guid.Empty.ToString())
                {
                    ZoneEntity objzone = (ZoneEntity)ObjNewPlayerVM.ActiveZones.Where(p => p.ZoneId.ToString() == cbxzone.SelectedValue.ToString()).SingleOrDefault();
                    int count;
                    if (ObjNewPlayerVM.ActivePlayers.Where(p => string.IsNullOrEmpty(p.KSCAUID) == false).ToList<PlayerEntity>().Count > 0)
                    {
                        List<string> liststr = ObjNewPlayerVM.ActivePlayers.Where(p => p.ZoneId == User.Zone.ZoneId && string.IsNullOrEmpty(p.KSCAUID) == false).Select(p => p.KSCAUID.Substring(2)).ToList<string>();
                        if (liststr != null && liststr.Count > 0)
                        { 
                        count = (int)liststr.Select(p => Convert.ToInt16(p)).Max();
                        count = count + 1;
                    }
                        else
                        {
                            count = 1;
                        }
                    }
                    else
                    {
                        count = 1;
                    }//(ObjNewPlayerVM.ActivePlayers.Count) ;
                    string num = "0000" + count.ToString();
                    ObjNewPlayerVM.CurrentPlayer.KSCAUID = User.Zone.ZoneNumber.ToString() + "P" + num.Substring(num.Length - 5, 5);


                }
                }
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        //private void cbxTeam_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    if (cbxTeam.SelectedItem != null)
        //    {
        //        ObjNewPlayerVM.CurrentPlayer.Team = (TeamEntity)cbxTeam.SelectedItem;
        //        generatekscauid();
        //    }
        //}



        private void gridplayervalues_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try { 
            if (ObjNewPlayerVM.CurrentPlayer == null || ObjNewPlayerVM.CurrentPlayer.IsNew == false)
            {
                if (gridplayervalues.SelectedItem != null)
                {
                    PlayerEntity objgridselected = (PlayerEntity)gridplayervalues.SelectedItem;
                    ObjNewPlayerVM.CurrentPlayer = objgridselected;


                    if (objgridselected.Gender == "Male")
                    {
                        rbtnmale.IsChecked = true;
                    }
                    else
                    {
                        rbtnfemale.IsChecked = true;
                    }
                    txt_address.Text = objgridselected.Address;
                    if (objgridselected.BattingStyle == "Right Handed")
                    {
                        rbtnrightbat.IsChecked = true;
                    }
                    else
                    {
                        rbtnleftbat.IsChecked = true;
                    }
                    if (objgridselected.BowlingStyle == "Right Arm")
                    {
                        rbtnrightbal.IsChecked = true;
                    }
                    else
                    {
                        rbtnleftbal.IsChecked = true;
                    }
                    cbxbalstyle.Text = objgridselected.BowlingSubStyle;
                    if (!string.IsNullOrEmpty(objgridselected.PlayerImagePath))
                    {
                        if (System.IO.File.Exists(objgridselected.PlayerImagePath.ToString()))
                            playerimage.Source = new BitmapImage(new Uri(objgridselected.PlayerImagePath.ToString()));
                    }
                    if (objgridselected.CZone != null)
                        //cbxzone.Text = objgridselected.CZone.ZoneName;
                        cbxzone.SelectedValue = objgridselected.CZone.ZoneId;
                    else
                        cbxzone.Text = "";
                    if (objgridselected.Team != null)
                        cbxTeam.SelectedValue = objgridselected.Team.TeamId;
                    else
                        cbxTeam.Text = "";
                    btnsave.Visibility = Visibility.Visible;
                    neworange();
                }
                }
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

     


        private void btnnew_Click(object sender, RoutedEventArgs e)
        {
            try { 
            if (ObjNewPlayerVM.CurrentPlayer ==null || ObjNewPlayerVM.CurrentPlayer.IsNew == false)
            {

                PlayerEntity ObjNewPlayer = Database.getNewEntity<PlayerEntity>();
                ObjNewPlayerVM.CurrentPlayer = ObjNewPlayer;
                ObjNewPlayerVM.ActivePlayers.Add(ObjNewPlayer);
                ObjNewPlayerVM.CurrentZoneDivisionSeasonPlayers.Add(ObjNewPlayer);
                rbtnfemale.IsChecked = false;
                rbtnmale.IsChecked = true;
                rbtnrightbat.IsChecked = true;
                rbtnleftbat.IsChecked = false;
                rbtnrightbal.IsChecked = true;
                rbtnleftbal.IsChecked = false;
                    cbxbalstyle.SelectedIndex = 0;
                cbxTeam.Text = "";
                playerimage.Source = null;
                if (cbxzone.SelectedItem != null)
                {
                    ObjNewPlayerVM.CurrentPlayer.CZone = (ZoneEntity)cbxzone.SelectedItem;
                    generatekscauid();
                }
                if (cbxTeam.SelectedItem != null)
                {
                    ObjNewPlayerVM.CurrentPlayer.Team = (TeamEntity)cbxTeam.SelectedItem;
                    generatekscauid();
                }
                ObjNewPlayerVM.CurrentPlayer.PlayerImagePath = null;
                newgreen();
                }
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void rbtnrightbat_Checked(object sender, RoutedEventArgs e)
        {
            ObjNewPlayerVM.CurrentPlayer.BattingStyle = "Right Handed";
        }

        private void rbtnleftbat_Checked(object sender, RoutedEventArgs e)
        {
            ObjNewPlayerVM.CurrentPlayer.BattingStyle = "Left Handed";
        }

        private void rbtnrightbal_Checked(object sender, RoutedEventArgs e)
        {
            ObjNewPlayerVM.CurrentPlayer.BowlingStyle = "Right Arm";
        }

        private void rbtnleftbal_Checked(object sender, RoutedEventArgs e)
        {
            ObjNewPlayerVM.CurrentPlayer.BowlingStyle = "Left Arm";
        }

        private void rbtnmale_Checked(object sender, RoutedEventArgs e)
        {
            ObjNewPlayerVM.CurrentPlayer.Gender = "Male";
        }

        private void rbtnfemale_Checked(object sender, RoutedEventArgs e)
        {
            ObjNewPlayerVM.CurrentPlayer.Gender = "Female";
        }

        private void cbxbalstyle_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbxbalstyle.SelectedItem != null)
            {
                ObjNewPlayerVM.CurrentPlayer.BowlingSubStyle = ((ComboBoxItem)e.AddedItems[0]).Content.ToString();
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            try { 
            if (ObjNewPlayerVM.CurrentPlayer != null && ObjNewPlayerVM.CurrentPlayer.IsNew == true)
            {
                ObjNewPlayerVM.ActivePlayers.Remove(ObjNewPlayerVM.CurrentPlayer);
                ObjNewPlayerVM.CurrentZoneDivisionSeasonPlayers.Remove (ObjNewPlayerVM.CurrentPlayer);
                ObjNewPlayerVM.CurrentPlayer = null;
                if (gridplayervalues.SelectedItem != null)
                {

                    gridplayervalues_SelectionChanged(gridplayervalues, null);
                }
            }
            //ObjNewPlayerVM.CurrentPlayer = null;

            else if (ObjNewPlayerVM.CurrentPlayer != null && ObjNewPlayerVM.CurrentPlayer.IsNew == false)
            {
                PlayerEntity  ObjDBObject = Database.GetEntity<PlayerEntity>(ObjNewPlayerVM.CurrentPlayer.PlayerId);
                ObjNewPlayerVM.CurrentZoneDivisionSeasonPlayers.Remove(ObjNewPlayerVM.CurrentPlayer);
                ObjNewPlayerVM.CurrentZoneDivisionSeasonPlayers.Add(ObjDBObject);

                ObjNewPlayerVM.ActivePlayers.Remove(ObjNewPlayerVM.CurrentPlayer);
                ObjNewPlayerVM.ActivePlayers.Add(ObjDBObject);

                ObjNewPlayerVM.CurrentPlayer = ObjDBObject;


                }
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void cbxTeam_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try { 
            if (cbxTeam.SelectedItem != null && ObjNewPlayerVM.CurrentPlayer.IsNew)
            {
                    ObjNewPlayerVM.CurrentPlayer.Team = (TeamEntity)cbxTeam.SelectedItem;
                    //ObjNewPlayerVM.CurrentZoneDivisionSeasonPlayers = new ObservableCollection<PlayerEntity>(ObjNewPlayerVM.CurrentZoneDivisionSeasonPlayers.Where(p => p.TeamId == ObjNewPlayerVM.CurrentPlayer.Team.TeamId));
                    ObjNewPlayerVM.getCurrentSeasonZoneDivisionPlayers(User.Zone, User.Season, User.Division, ObjNewPlayerVM.CurrentPlayer.Team);
                    generatekscauid();
                }
            else
                {
                    ObjNewPlayerVM.getCurrentSeasonZoneDivisionPlayers(User.Zone, User.Season, User.Division, (TeamEntity)cbxTeam.SelectedItem);
                }
            }
            catch (Exception es)
            {
                MessageBox.Show(es.Message, "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

       
    }
}
