﻿using Cricket.DAL;
using Cricket.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Cricket.ViewModel;
namespace Cricket.Pages
{
    /// <summary>
    /// Interaction logic for Home.xaml
    /// </summary>
    public partial class Home : UserControl
    {
        
        SeasonEntityManager objseasonentitymanager = new SeasonEntityManager();
        public Home()
        {
            InitializeComponent();
            User.LoggedInUser = Database.GetEntity<UserEntity>(Guid.Parse("315650AB-19BC-4DDC-9334-6B2A0809AF00"));
            User.PlayerImagePath=@"D:\Cricket\PlayerImages\";
            HomeViewModel objHomeVM = new HomeViewModel();
            this.DataContext = objHomeVM;
        }

        


        private void cbxSeason_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbxSeason.SelectedIndex != -1)
            {
                User.Season = (SeasonEntity)cbxSeason.SelectedItem;
            }
        }

        private void cbxzone_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbxzone.SelectedIndex != -1)
            {
                User.Zone = (ZoneEntity)cbxzone.SelectedItem;
            }
        }

        private void cbxdivision_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbxdivision.SelectedIndex != -1)
            {
                User.Division = (DivisionEntity)cbxdivision.SelectedItem;
            }

        }
        private void btnplayers_Click(object sender, RoutedEventArgs e)
        {
            if (cbxSeason.SelectedIndex == -1)
            {
                MessageBox.Show("Select Season");
            }
            else
            {
                btnplayers.Command = NavigationCommands.GoToPage;
                btnplayers.CommandParameter = "/Views/NewPlayer.xaml";
            }
        }

        private void btnofficials_Click(object sender, RoutedEventArgs e)
        {
            if (cbxSeason.SelectedIndex == -1)
            {
                MessageBox.Show("Select Season");
            }
            else
            {
                btnofficials.Command = NavigationCommands.GoToPage;
                btnofficials.CommandParameter = "/Views/NewOfficial.xaml";
            }
        }

        private void btngrounds_Click(object sender, RoutedEventArgs e)
        {
            if (cbxSeason.SelectedIndex == -1)
            {
                MessageBox.Show("Select Season");
            }
            else
            {
                btngrounds.Command = NavigationCommands.GoToPage;
                btngrounds.CommandParameter = "/Views/NewLocation.xaml";
            }
        }

        private void btnseasons_Click(object sender, RoutedEventArgs e)
        {
            if (cbxSeason.SelectedIndex == -1)
            {
                //MessageBox.Show("Select Season");
                SeasonEntity season = new SeasonEntity();
                btnseasons.Command = NavigationCommands.GoToPage;
                btnseasons.CommandParameter = "/Views/NewSeason.xaml";
            }
            else
            {
                btnseasons.Command = NavigationCommands.GoToPage;
                btnseasons.CommandParameter = "/Views/NewSeason.xaml";
            }
        }

        private void btnteams_Click(object sender, RoutedEventArgs e)
        {
            if (Validate() == true)
            {
                btnteams.Command = NavigationCommands.GoToPage;
                btnteams.CommandParameter = "/Views/NewTeam.xaml";
            }
        }

        private void btndivisions_Click(object sender, RoutedEventArgs e)
        {
            //if (Validate() == true)
            {
                btndivisions.Command = NavigationCommands.GoToPage;
                btndivisions.CommandParameter = "/Views/NewDivision.xaml";
            }
        }

        private void btngroups_Click(object sender, RoutedEventArgs e)
        {
            if (Validate() == true)
            {
                btngroups.Command = NavigationCommands.GoToPage;
                btngroups.CommandParameter = "/Views/TransferTeamsToDivision.xaml";
            }
        }

        private void btnfixtures_Click(object sender, RoutedEventArgs e)
        {
            if (Validate() == true)
            {
                btnfixtures.Command = NavigationCommands.GoToPage;
                btnfixtures.CommandParameter = "/Views/Fixtures.xaml";
            }
        }

        private void btnviewfixtures_Click(object sender, RoutedEventArgs e)
        {
            if (Validate() == true)
            {
                btnviewfixtures.Command = NavigationCommands.GoToPage;
                btnviewfixtures.CommandParameter = "/Views/ViewFixtures.xaml";
            }
        }

        private void btnplayerstats_Click(object sender, RoutedEventArgs e)
        {
            if (Validate() == true)
            {
                btnplayerstats.Command = NavigationCommands.GoToPage;
                btnplayerstats.CommandParameter = "/Views/PlayerStats.xaml";
            }
        }

        private void btnteamstats_Click(object sender, RoutedEventArgs e)
        {
            if (Validate() == true)
            {
                btnteamstats.Command = NavigationCommands.GoToPage;
                btnteamstats.CommandParameter = "/Views/TeamStats.xaml";
            }
        }

        private void btnpointstable_Click(object sender, RoutedEventArgs e)
        {
            if (Validate() == true)
            {
                btnpointstable.Command = NavigationCommands.GoToPage;
                btnpointstable.CommandParameter = "/Views/PointsTable.xaml";
            }
        }

        private void btnlocationstats_Click(object sender, RoutedEventArgs e)
        {
            if (Validate() == true)
            {
                btnlocationstats.Command = NavigationCommands.GoToPage;
                btnlocationstats.CommandParameter = "/Views/LocationStats.xaml";
            }
        }

        private void btnofficialstats_Click(object sender, RoutedEventArgs e)
        {
            if (Validate() == true)
            {
                btnofficialstats.Command = NavigationCommands.GoToPage;
                btnofficialstats.CommandParameter = "/Views/OfficialStats.xaml";
            }            
        }

        public bool Validate()
        {
            if (cbxSeason.SelectedIndex == -1)
            {
                MessageBox.Show("Select Season");
                return false;
            }
            else if (cbxzone.SelectedIndex == -1)
            {
                MessageBox.Show("Select Zone");
                return false;
            }
            else if (cbxdivision.SelectedIndex == -1)
            {
                MessageBox.Show("Select Division");
                return false;
            }
            else
                return true;
        }

        
    }
}
