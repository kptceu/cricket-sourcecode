﻿#pragma checksum "..\..\..\Views\NewPlayer.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "24F0778276811319F477DF6DB8870E1B"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using FirstFloor.ModernUI.Windows.Converters;
using FirstFloor.ModernUI.Windows.Navigation;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Cricket.Views {
    
    
    /// <summary>
    /// NewPlayer
    /// </summary>
    public partial class NewPlayer : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 15 "..\..\..\Views\NewPlayer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton rbtnrightbat;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\..\Views\NewPlayer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton rbtnleftbat;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\..\Views\NewPlayer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton rbtnrightbal;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\..\Views\NewPlayer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton rbtnleftbal;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\..\Views\NewPlayer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbxbalstyle;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\..\Views\NewPlayer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnsave;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\..\Views\NewPlayer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbxzone;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\..\Views\NewPlayer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbxTeam;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\..\Views\NewPlayer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_kscauid;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\..\Views\NewPlayer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_fname;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\..\Views\NewPlayer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_mobno;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\..\Views\NewPlayer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton rbtnmale;
        
        #line default
        #line hidden
        
        
        #line 68 "..\..\..\Views\NewPlayer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_Fathername;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\..\Views\NewPlayer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dateselect;
        
        #line default
        #line hidden
        
        
        #line 70 "..\..\..\Views\NewPlayer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_emailid;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\..\Views\NewPlayer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton rbtnfemale;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\..\Views\NewPlayer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_address;
        
        #line default
        #line hidden
        
        
        #line 76 "..\..\..\Views\NewPlayer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid gridplayervalues;
        
        #line default
        #line hidden
        
        
        #line 99 "..\..\..\Views\NewPlayer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox cbkeeper;
        
        #line default
        #line hidden
        
        
        #line 101 "..\..\..\Views\NewPlayer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnupload;
        
        #line default
        #line hidden
        
        
        #line 104 "..\..\..\Views\NewPlayer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image playerimage;
        
        #line default
        #line hidden
        
        
        #line 107 "..\..\..\Views\NewPlayer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnnew;
        
        #line default
        #line hidden
        
        
        #line 110 "..\..\..\Views\NewPlayer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCancel;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Cricket;component/views/newplayer.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Views\NewPlayer.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.rbtnrightbat = ((System.Windows.Controls.RadioButton)(target));
            
            #line 15 "..\..\..\Views\NewPlayer.xaml"
            this.rbtnrightbat.Checked += new System.Windows.RoutedEventHandler(this.rbtnrightbat_Checked);
            
            #line default
            #line hidden
            return;
            case 2:
            this.rbtnleftbat = ((System.Windows.Controls.RadioButton)(target));
            
            #line 16 "..\..\..\Views\NewPlayer.xaml"
            this.rbtnleftbat.Checked += new System.Windows.RoutedEventHandler(this.rbtnleftbat_Checked);
            
            #line default
            #line hidden
            return;
            case 3:
            this.rbtnrightbal = ((System.Windows.Controls.RadioButton)(target));
            
            #line 29 "..\..\..\Views\NewPlayer.xaml"
            this.rbtnrightbal.Checked += new System.Windows.RoutedEventHandler(this.rbtnrightbal_Checked);
            
            #line default
            #line hidden
            return;
            case 4:
            this.rbtnleftbal = ((System.Windows.Controls.RadioButton)(target));
            
            #line 30 "..\..\..\Views\NewPlayer.xaml"
            this.rbtnleftbal.Checked += new System.Windows.RoutedEventHandler(this.rbtnleftbal_Checked);
            
            #line default
            #line hidden
            return;
            case 5:
            this.cbxbalstyle = ((System.Windows.Controls.ComboBox)(target));
            
            #line 31 "..\..\..\Views\NewPlayer.xaml"
            this.cbxbalstyle.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.cbxbalstyle_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 6:
            this.btnsave = ((System.Windows.Controls.Button)(target));
            
            #line 44 "..\..\..\Views\NewPlayer.xaml"
            this.btnsave.Click += new System.Windows.RoutedEventHandler(this.btnsave_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.cbxzone = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 8:
            this.cbxTeam = ((System.Windows.Controls.ComboBox)(target));
            
            #line 50 "..\..\..\Views\NewPlayer.xaml"
            this.cbxTeam.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.cbxTeam_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 9:
            this.txt_kscauid = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.txt_fname = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.txt_mobno = ((System.Windows.Controls.TextBox)(target));
            return;
            case 12:
            this.rbtnmale = ((System.Windows.Controls.RadioButton)(target));
            
            #line 67 "..\..\..\Views\NewPlayer.xaml"
            this.rbtnmale.Checked += new System.Windows.RoutedEventHandler(this.rbtnmale_Checked);
            
            #line default
            #line hidden
            return;
            case 13:
            this.txt_Fathername = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.dateselect = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 15:
            this.txt_emailid = ((System.Windows.Controls.TextBox)(target));
            return;
            case 16:
            this.rbtnfemale = ((System.Windows.Controls.RadioButton)(target));
            
            #line 71 "..\..\..\Views\NewPlayer.xaml"
            this.rbtnfemale.Checked += new System.Windows.RoutedEventHandler(this.rbtnfemale_Checked);
            
            #line default
            #line hidden
            return;
            case 17:
            this.txt_address = ((System.Windows.Controls.TextBox)(target));
            return;
            case 18:
            this.gridplayervalues = ((System.Windows.Controls.DataGrid)(target));
            
            #line 76 "..\..\..\Views\NewPlayer.xaml"
            this.gridplayervalues.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.gridplayervalues_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 19:
            this.cbkeeper = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 20:
            this.btnupload = ((System.Windows.Controls.Button)(target));
            
            #line 101 "..\..\..\Views\NewPlayer.xaml"
            this.btnupload.Click += new System.Windows.RoutedEventHandler(this.btnupload_Click);
            
            #line default
            #line hidden
            return;
            case 21:
            this.playerimage = ((System.Windows.Controls.Image)(target));
            return;
            case 22:
            this.btnnew = ((System.Windows.Controls.Button)(target));
            
            #line 107 "..\..\..\Views\NewPlayer.xaml"
            this.btnnew.Click += new System.Windows.RoutedEventHandler(this.btnnew_Click);
            
            #line default
            #line hidden
            return;
            case 23:
            this.btnCancel = ((System.Windows.Controls.Button)(target));
            
            #line 110 "..\..\..\Views\NewPlayer.xaml"
            this.btnCancel.Click += new System.Windows.RoutedEventHandler(this.btnCancel_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

