﻿#pragma checksum "..\..\..\Views\TeamStats.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "A73702E2B9A3D3C18C757D9618C32D12"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using FirstFloor.ModernUI.Windows.Converters;
using FirstFloor.ModernUI.Windows.Navigation;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Cricket.Views {
    
    
    /// <summary>
    /// TeamStats
    /// </summary>
    public partial class TeamStats : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\..\Views\TeamStats.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid gridteamdetails;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\Views\TeamStats.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbxteam;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\..\Views\TeamStats.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid gridteamdetails2;
        
        #line default
        #line hidden
        
        
        #line 117 "..\..\..\Views\TeamStats.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnexportteamstats;
        
        #line default
        #line hidden
        
        
        #line 120 "..\..\..\Views\TeamStats.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblinnings;
        
        #line default
        #line hidden
        
        
        #line 124 "..\..\..\Views\TeamStats.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblwon;
        
        #line default
        #line hidden
        
        
        #line 128 "..\..\..\Views\TeamStats.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbllost;
        
        #line default
        #line hidden
        
        
        #line 132 "..\..\..\Views\TeamStats.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbldrawn;
        
        #line default
        #line hidden
        
        
        #line 136 "..\..\..\Views\TeamStats.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblrunsscored;
        
        #line default
        #line hidden
        
        
        #line 140 "..\..\..\Views\TeamStats.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblrunsconceded;
        
        #line default
        #line hidden
        
        
        #line 144 "..\..\..\Views\TeamStats.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblwicketslost;
        
        #line default
        #line hidden
        
        
        #line 148 "..\..\..\Views\TeamStats.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblwicketstaken;
        
        #line default
        #line hidden
        
        
        #line 152 "..\..\..\Views\TeamStats.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblhundreds;
        
        #line default
        #line hidden
        
        
        #line 156 "..\..\..\Views\TeamStats.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblfifties;
        
        #line default
        #line hidden
        
        
        #line 160 "..\..\..\Views\TeamStats.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblfivewickets;
        
        #line default
        #line hidden
        
        
        #line 164 "..\..\..\Views\TeamStats.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblpoints;
        
        #line default
        #line hidden
        
        
        #line 168 "..\..\..\Views\TeamStats.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblrunrate;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Cricket;component/views/teamstats.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Views\TeamStats.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.gridteamdetails = ((System.Windows.Controls.DataGrid)(target));
            
            #line 10 "..\..\..\Views\TeamStats.xaml"
            this.gridteamdetails.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.gridteamdetails_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 2:
            this.cbxteam = ((System.Windows.Controls.ComboBox)(target));
            
            #line 64 "..\..\..\Views\TeamStats.xaml"
            this.cbxteam.DropDownClosed += new System.EventHandler(this.cbxteam_DropDownClosed);
            
            #line default
            #line hidden
            return;
            case 3:
            this.gridteamdetails2 = ((System.Windows.Controls.DataGrid)(target));
            
            #line 65 "..\..\..\Views\TeamStats.xaml"
            this.gridteamdetails2.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.gridteamdetails2_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 4:
            this.btnexportteamstats = ((System.Windows.Controls.Button)(target));
            
            #line 117 "..\..\..\Views\TeamStats.xaml"
            this.btnexportteamstats.Click += new System.Windows.RoutedEventHandler(this.btnexportteamstats_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.lblinnings = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.lblwon = ((System.Windows.Controls.Label)(target));
            return;
            case 7:
            this.lbllost = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.lbldrawn = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.lblrunsscored = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            this.lblrunsconceded = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.lblwicketslost = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.lblwicketstaken = ((System.Windows.Controls.Label)(target));
            return;
            case 13:
            this.lblhundreds = ((System.Windows.Controls.Label)(target));
            return;
            case 14:
            this.lblfifties = ((System.Windows.Controls.Label)(target));
            return;
            case 15:
            this.lblfivewickets = ((System.Windows.Controls.Label)(target));
            return;
            case 16:
            this.lblpoints = ((System.Windows.Controls.Label)(target));
            return;
            case 17:
            this.lblrunrate = ((System.Windows.Controls.Label)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

