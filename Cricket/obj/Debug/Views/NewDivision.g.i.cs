﻿#pragma checksum "..\..\..\Views\NewDivision.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "FB5ACC459F26FC3B7C27D39279A7E942"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using FirstFloor.ModernUI.Windows.Converters;
using FirstFloor.ModernUI.Windows.Navigation;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Cricket.Views {
    
    
    /// <summary>
    /// NewDivision
    /// </summary>
    public partial class NewDivision : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 17 "..\..\..\Views\NewDivision.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnsavedivision;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\..\Views\NewDivision.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnnew;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\..\Views\NewDivision.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid griddivisionvalues;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\..\Views\NewDivision.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_divisionname;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\..\Views\NewDivision.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_noofinnings;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\..\Views\NewDivision.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_noofoversperinnings;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\..\Views\NewDivision.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_noofdaysofplay;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\..\Views\NewDivision.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbxballspermatch;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\..\Views\NewDivision.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtballname;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\..\Views\NewDivision.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txt_serial;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\..\Views\NewDivision.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCancel;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Cricket;component/views/newdivision.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Views\NewDivision.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.btnsavedivision = ((System.Windows.Controls.Button)(target));
            
            #line 17 "..\..\..\Views\NewDivision.xaml"
            this.btnsavedivision.Click += new System.Windows.RoutedEventHandler(this.btnsavedivision_Click);
            
            #line default
            #line hidden
            return;
            case 2:
            this.btnnew = ((System.Windows.Controls.Button)(target));
            
            #line 21 "..\..\..\Views\NewDivision.xaml"
            this.btnnew.Click += new System.Windows.RoutedEventHandler(this.btnnew_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.griddivisionvalues = ((System.Windows.Controls.DataGrid)(target));
            
            #line 23 "..\..\..\Views\NewDivision.xaml"
            this.griddivisionvalues.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.griddivisionvalues_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 4:
            this.txt_divisionname = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.txt_noofinnings = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.txt_noofoversperinnings = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.txt_noofdaysofplay = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.cbxballspermatch = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 9:
            this.txtballname = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.txt_serial = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.btnCancel = ((System.Windows.Controls.Button)(target));
            
            #line 59 "..\..\..\Views\NewDivision.xaml"
            this.btnCancel.Click += new System.Windows.RoutedEventHandler(this.btnCancel_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

