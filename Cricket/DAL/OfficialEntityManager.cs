﻿using Cricket.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cricket.DAL
{
    public class OfficialEntityManager : IEntityManager<OfficialEntity>
    {
        public OfficialEntity GetEntityById(CricketContext db, Guid EntityID)
        {
            OfficialEntity objnew = (from obj in db.Officials where obj.OfficialId == EntityID select obj).SingleOrDefault();
            objnew = (OfficialEntity)Database.CleanObjectGraph(objnew);
            return objnew;
        }

        public ObservableCollection<OfficialEntity> getEntityList(CricketContext db)
        {
            ObservableCollection<OfficialEntity> Lst = new ObservableCollection<OfficialEntity>(from obj in db.Officials.Include(p=>p.CZone)
                                                                                               where obj.ZoneId==User.Zone.ZoneId select obj);
            var lsttoclean = Lst.OrderBy(p=>p.Name ).ToList<object>();
            var lstcleaned = Database.CleanObjectGraph(lsttoclean);
            Lst = new ObservableCollection<OfficialEntity>((lstcleaned.Cast<OfficialEntity>()));
            Lst.ToList().ForEach(x => x.IsSelectable = true);
            return Lst;
        }

        public bool DeleteEntity(CricketContext dbcontext, OfficialEntity EntitytoDelete)
        {
            dbcontext.Entry(EntitytoDelete).State = EntityState.Deleted;
            return true;
        }

        public bool CheckOfficial(CricketContext db, string officialname)
        {
            OfficialEntity objoff = (from obj in db.Officials where obj.Name.ToUpper() == officialname.ToUpper() && obj.ZoneId == User.Zone.ZoneId select obj).FirstOrDefault();
            if (objoff == null)
                return false;
            else
                return true;
        }
    }
}
