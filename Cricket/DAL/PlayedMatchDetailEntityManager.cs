﻿using Cricket.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cricket.DAL
{
    public class PlayedMatchDetailEntityManager : IEntityManager<PlayedMatchDetailEntity>
    {
        public PlayedMatchDetailEntity GetEntityById(CricketContext db, Guid EntityID)
        {
            PlayedMatchDetailEntity objnew = (from obj in db.PlayedMatchDetails where obj.PlayedMatchDetailId == EntityID select obj).SingleOrDefault();
            objnew = (PlayedMatchDetailEntity)Database.CleanObjectGraph(objnew);
            return objnew;
        }

        public ObservableCollection<PlayedMatchDetailEntity> getEntityList(CricketContext db)
        {
            ObservableCollection<PlayedMatchDetailEntity> Lst = new ObservableCollection<PlayedMatchDetailEntity>(from obj in db.PlayedMatchDetails select obj);
            var lsttoclean = Lst.ToList<object>();
            var lstcleaned = Database.CleanObjectGraph(lsttoclean);
            Lst = new ObservableCollection<PlayedMatchDetailEntity>((lstcleaned.Cast<PlayedMatchDetailEntity>()));
            return Lst;
        }

        public bool DeleteEntity(CricketContext dbcontext, PlayedMatchDetailEntity EntitytoDelete)
        {
            dbcontext.Entry(EntitytoDelete).State = EntityState.Deleted;
            return true;
        }

        public ObservableCollection<PlayedMatchDetailEntity> GetByFixtureIdAndTeamId(CricketContext db,Guid FixtureId,Guid TeamId)
        {
            ObservableCollection<PlayedMatchDetailEntity> Lst = new ObservableCollection<PlayedMatchDetailEntity>(from obj in db.PlayedMatchDetails
                                                                                                                  .Include(p=>p.Player)
                                                                                                                  where obj.FixtureId==FixtureId && obj.TeamId==TeamId select obj);
            var lsttoclean = Lst.ToList<object>();
            var lstcleaned = Database.CleanObjectGraph(lsttoclean);
            Lst = new ObservableCollection<PlayedMatchDetailEntity>((lstcleaned.Cast<PlayedMatchDetailEntity>()));
            return Lst;
        }

        public ObservableCollection<PlayedMatchDetailEntity> getListByPlayerId(CricketContext db,Guid PlayerId)
        {
            PlayedMatchEntityManager objplayedem = new PlayedMatchEntityManager();

            ObservableCollection<PlayedMatchDetailEntity> Lst = new ObservableCollection<PlayedMatchDetailEntity>(from obj in db.PlayedMatchDetails
                                                                                                                  where obj.PlayerId==PlayerId && obj.Fixture.Season.SeasonId==User.Season.SeasonId select obj);
            var lsttoclean = Lst.ToList<object>();
            var lstcleaned = Database.CleanObjectGraph(lsttoclean);
            Lst = new ObservableCollection<PlayedMatchDetailEntity>((lstcleaned.Cast<PlayedMatchDetailEntity>()));
            return Lst;
        }

        public ObservableCollection<PlayedMatchDetailEntity> getListByPlayerIdAndSeasonId(CricketContext db, Guid PlayerId,Guid SeasonId)
        {
            PlayedMatchEntityManager objplayedem = new PlayedMatchEntityManager();

            ObservableCollection<PlayedMatchDetailEntity> Lst = new ObservableCollection<PlayedMatchDetailEntity>(from obj in db.PlayedMatchDetails
                                                                                                                  where obj.PlayerId == PlayerId && obj.Fixture.Season.SeasonId == SeasonId
                                                                                                                  select obj);
            var lsttoclean = Lst.ToList<object>();
            var lstcleaned = Database.CleanObjectGraph(lsttoclean);
            Lst = new ObservableCollection<PlayedMatchDetailEntity>((lstcleaned.Cast<PlayedMatchDetailEntity>()));
            return Lst;
        }

        public ObservableCollection<PlayedMatchDetailEntity> getListByPlayerIdAcrossSeasons(CricketContext db, Guid PlayerId)
        {
            PlayedMatchEntityManager objplayedem = new PlayedMatchEntityManager();

            ObservableCollection<PlayedMatchDetailEntity> Lst = new ObservableCollection<PlayedMatchDetailEntity>(from obj in db.PlayedMatchDetails
                                                                                                                  where obj.PlayerId == PlayerId 
                                                                                                                  select obj);
            var lsttoclean = Lst.ToList<object>();
            var lstcleaned = Database.CleanObjectGraph(lsttoclean);
            Lst = new ObservableCollection<PlayedMatchDetailEntity>((lstcleaned.Cast<PlayedMatchDetailEntity>()));
            return Lst;
        }
    }
}

