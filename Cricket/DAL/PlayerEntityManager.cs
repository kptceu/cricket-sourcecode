﻿using Cricket.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cricket.DAL
{
   public class PlayerEntityManager : IEntityManager<PlayerEntity>
    {
        public PlayerEntity GetEntityById(CricketContext db, Guid EntityID)
        {
            PlayerEntity objnew = (from obj in db.Players.Include("CZone").Include("Team") where obj.PlayerId == EntityID select obj).SingleOrDefault();
            objnew = (PlayerEntity)Database.CleanObjectGraph(objnew);
            return objnew;
        }

        public ObservableCollection<PlayerEntity> getEntityList(CricketContext db)
        {
            ObservableCollection<PlayerEntity> Lst = new ObservableCollection<PlayerEntity>(from obj in db.Players where obj.ZoneId==User.Zone.ZoneId select obj);
            var lsttoclean = Lst.OrderBy(p => p.PlayerName).ToList<object>();
            var lstcleaned = Database.CleanObjectGraph(lsttoclean);
            Lst = new ObservableCollection<PlayerEntity>((lstcleaned.Cast<PlayerEntity>()));
            return Lst;
        }

        public ObservableCollection<PlayerEntity> getEntityListwithReferences(CricketContext db)
        {
            ObservableCollection<PlayerEntity> Lst = new ObservableCollection<PlayerEntity>(from obj in db.Players.Include (p=>p.CZone).Include(p=>p.Team ) select obj);
            var lsttoclean = Lst.OrderBy(p => p.PlayerName).ToList<object>();
            var lstcleaned = Database.CleanObjectGraph(lsttoclean);
            Lst = new ObservableCollection<PlayerEntity>((lstcleaned.Cast<PlayerEntity>()));
            return Lst;
        }

        
            


        public ObservableCollection<PlayerEntity> getPlayersfromZoneDidvisionSeasonwithReferences(CricketContext db, ZoneEntity objZone, SeasonEntity objSeason, DivisionEntity ObjDivision)
        {
            ObservableCollection<PlayerEntity> Lst = new ObservableCollection<PlayerEntity>(from obj in db.Players.Include(p => p.CZone).Include(p => p.Team).Where (p=>p.ZoneId==objZone.ZoneId )      select obj);
            var lsttoclean = Lst.OrderBy(p => p.PlayerName).ToList<object>();
            var lstcleaned = Database.CleanObjectGraph(lsttoclean);
            Lst = new ObservableCollection<PlayerEntity>((lstcleaned.Cast<PlayerEntity>()));
            return Lst;
        }

        public ObservableCollection<PlayerEntity> getPlayersfromTeamID(CricketContext db, Guid TeamId)
        {
            ObservableCollection<PlayerEntity> Lst = new ObservableCollection<PlayerEntity>(from obj in db.Players.Include(p => p.CZone).Include(p => p.Team).Where(p=>p.TeamId==TeamId)
                                                                                            select obj);
            var lsttoclean = Lst.OrderBy(p => p.PlayerName).ToList<object>();
            var lstcleaned = Database.CleanObjectGraph(lsttoclean);
            Lst = new ObservableCollection<PlayerEntity>((lstcleaned.Cast<PlayerEntity>()));
            return Lst;
        }

        public bool DeleteEntity(CricketContext dbcontext, PlayerEntity EntitytoDelete)
        {
            dbcontext.Entry(EntitytoDelete).State = EntityState.Deleted;
            return true;
        }

        public ObservableCollection<PlayerEntity> GetByTeamId(CricketContext db,Guid TeamId)
        {
            ObservableCollection<PlayerEntity> LstPlayer = new ObservableCollection<PlayerEntity>(from obj in db.Players where obj.TeamId == TeamId select obj);
            LstPlayer = new ObservableCollection<PlayerEntity>(LstPlayer.OrderBy(p => p.PlayerName));
            return LstPlayer;
        }

        public ObservableCollection<PlayerEntity> getListByTeamList(CricketContext db,List<Guid> LstTeamId) 
        {
            ObservableCollection<PlayerEntity> Lst = new ObservableCollection<PlayerEntity>(from obj in db.Players.Include(p=>p.Team) where  LstTeamId.Contains(obj.TeamId) select obj);
            var lsttoclean = Lst.OrderBy(p => p.PlayerName).ToList<object>();
            var lstcleaned = Database.CleanObjectGraph(lsttoclean);
            Lst = new ObservableCollection<PlayerEntity>((lstcleaned.Cast<PlayerEntity>()));
            return Lst;
        }

        public bool CheckPlayer(CricketContext db, string playername,Guid Teamid)
        {
            PlayerEntity objoff = (from obj in db.Players where obj.PlayerName.ToUpper() == playername.ToUpper() && obj.TeamId==Teamid select obj).FirstOrDefault();
            if (objoff == null)
                return false;
            else
                return true;
        }

        
    }
}



