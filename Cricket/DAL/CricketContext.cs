﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cricket.Configurations;
using Cricket.Model;

namespace Cricket.DAL
{
   public class CricketContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CricketContext"/> class.
        /// </summary>
        public CricketContext()
            : base("CricketContext")
        {
            System.Data.Entity.Database.SetInitializer(new MigrateDatabaseToLatestVersion<CricketContext, Cricket.Migrations.Configuration>("CricketContext"));//"EFModelContainer"

        }
        /// <summary>
        /// This method is called when the model for a derived context has been initialized, but
        /// before the model has been locked down and used to initialize the context.  The default
        /// implementation of this method does nothing, but it can be overridden in a derived class
        /// such that the model can be further configured before it is locked down.
        /// </summary>
        /// <param name="modelBuilder">The builder that defines the model for the context being created.</param>
        /// <remarks>
        /// Typically, this method is called only once when the first instance of a derived context
        /// is created.  The model for that context is then cached and is for all further instances of
        /// the context in the app domain.  This caching can be disabled by setting the ModelCaching
        /// property on the given ModelBuidler, but note that this can seriously degrade performance.
        /// More control over caching is provided through use of the DbModelBuilder and DbContextFactory
        /// classes directly.
        /// </remarks>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.HasDefaultSchema("dbo");

            modelBuilder.Configurations.Add(new DivisionEntityConfiguration());
            modelBuilder.Configurations.Add(new ErrorLogEntityConfiguration());
            modelBuilder.Configurations.Add(new FixtureEntityConfiguration());
            modelBuilder.Configurations.Add(new LoadDivisionEntityConfigurations());
            modelBuilder.Configurations.Add(new LocationEntityConfiguration());
            modelBuilder.Configurations.Add(new OfficialEntityConfiguration());
            modelBuilder.Configurations.Add(new PlayedMatchDetailEntityConfiguration());
            modelBuilder.Configurations.Add(new PlayedMatchEntityConfiguration());
            modelBuilder.Configurations.Add(new PlayerEntityConfiguration());
            modelBuilder.Configurations.Add(new SeasonEntityConfiguration());
            modelBuilder.Configurations.Add(new TeamEntityConfiguration());
            modelBuilder.Configurations.Add(new UserEntityConfiguration());
            modelBuilder.Configurations.Add(new ZoneEntityConfiguration());
            modelBuilder.Configurations.Add(new GroupEntityConfiguration());
        }

        public DbSet<DivisionEntity> Divisions { get; set; }
        public DbSet<ErrorLogEntity> ErrorLogs { get; set; }
        public DbSet<FixtureEntity> Fixtures { get; set; }
        public DbSet<LoadDivisionEntity> LoadDivisions { get; set; }
        public DbSet<LocationEntity> Locations { get; set; }
        public DbSet<OfficialEntity> Officials { get; set; }
        public DbSet<PlayedMatchDetailEntity> PlayedMatchDetails { get; set; }
        public DbSet<PlayedMatchEntity> PlayedMatches { get; set; }
        public DbSet<PlayerEntity> Players { get; set; }
        public DbSet<SeasonEntity> Seasons { get; set; }
        public DbSet<TeamEntity> Teams { get; set; }
        public DbSet<UserEntity> Users { get; set; }
        public DbSet<ZoneEntity> Zones { get; set; }
        public DbSet<GroupEntity> Groups { get; set; }
    }
}