﻿using Cricket.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Cricket.DAL
{
    public class Database
    {
        /// <summary>
        /// Creates A New Entity.
        /// </summary>
        /// <typeparam name="t"></typeparam>
        /// <param name="entitytosave">The entitytosave.</param>
        /// <param name="lsttosave">The lsttosave.</param>
        /// <returns></returns>
        public static bool NewSaveEntity<t>(BaseEntity entitytosave,
            List<EntityCollection> lsttosave) where t : class
        {
            getDirtyEntities<t>(entitytosave, lsttosave);
            // CricketContext dbcontext = getDBContext();


            return true;
        }

        /// <summary>
        ///  saves entity.
        /// </summary>
        /// <typeparam name="t"></typeparam>
        /// <param name="lstentitytosave">The lstentitytosave.</param>
        /// <param name="lsttosave">The lsttosave.</param>
        /// <returns></returns>
        public static bool NewSaveEntity<t>(ObservableCollection<t> lstentitytosave,
            List<EntityCollection> lsttosave) where t : class
        {
            foreach (t entitytosave in lstentitytosave)
            {
                getDirtyEntities<t>(entitytosave as BaseEntity, lsttosave);
            }
            // CricketContext dbcontext = getDBContext();


            return true;
        }

        /// <summary>
        /// Clears the collections.
        /// </summary>
        /// <param name="lsttoClear">The lstto clear.</param>
        /// <returns></returns>
        public static List<EntityCollection> ClearCollections(List<EntityCollection> lsttoClear)
        {
            List<EntityCollection> lsttoreturn = new List<EntityCollection>();
            foreach (EntityCollection ec in lsttoClear)
            {
                object Entitytocheck = ec.Entity;
                foreach (PropertyInfo pi in (Entitytocheck.GetType().GetProperties()))
                {


                    if (pi.PropertyType.GetInterfaces().Any(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(ICollection<>)) && pi.Name != "Rowversion")
                    {
                        IEnumerable<object> lsttocheck = pi.GetValue(Entitytocheck) as IEnumerable<object>;
                        //lsttocheck = new List<object>();
                        lsttocheck.ToList<object>().RemoveAll(p => p.GetType().IsSubclassOf(typeof(BaseEntity)));
                        pi.SetValue(Entitytocheck, null);
                    }
                }

                ec.Entity = Entitytocheck;


                lsttoreturn.Add(ec);
                //}
            }
            return lsttoreturn;
        }

        /// <summary>
        /// Cleans the object graph.
        /// </summary>
        /// <param name="ObjecttoClean">The objectto clean.</param>
        /// <returns></returns>
        public static object CleanObjectGraph(Object ObjecttoClean)
        {
            //t ToClean = ObjecttoClean as t;
            //ToClean.IsDirty = false;
            if (ObjecttoClean != null)
            {
                ObjecttoClean.GetType().GetProperty("IsDirty").SetValue(ObjecttoClean, false);
                foreach (PropertyInfo pi in ObjecttoClean.GetType().GetProperties().Where(p => p.PropertyType.IsSubclassOf(typeof(BaseEntity))))
                {
                    if (pi.GetValue(ObjecttoClean) != null)
                        CleanObjectGraph(pi.GetValue(ObjecttoClean));
                }

                foreach (PropertyInfo pi in ObjecttoClean.GetType().GetProperties().Where(p => p.PropertyType.GetInterfaces().Any(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(ICollection<>)) && p.Name != "Rowversion"))
                {
                    IEnumerable<object> lsttocheck = pi.GetValue(ObjecttoClean) as IEnumerable<object>;
                    if (lsttocheck != null)
                    {
                        if (pi.Name == "DirtyProperties")
                        {
                            pi.SetValue(ObjecttoClean, new List<string>());
                            //if (ObjecttoClean.GetType() == typeof(EmployeeEntity))
                              //  Console.WriteLine(lsttocheck.ToList().Count().ToString());
                        }
                        else
                        {
                            foreach (object o in lsttocheck)
                            {
                                if (o != null)
                                {
                                    o.GetType().GetProperty("IsDirty").SetValue(o, false);
                                    o.GetType().GetProperty("DirtyProperties").SetValue(o, new List<string>());

                                }
                            }
                        }
                    }
                }
            }
            return ObjecttoClean;
        }


        /// <summary>
        /// Cleans the object graph.
        /// </summary>
        /// <param name="lstObjecttoClean">The LST objectto clean.</param>
        /// <returns></returns>
        public static List<object> CleanObjectGraph(List<object> lstObjecttoClean)
        {
            foreach (object ObjecttoClean in lstObjecttoClean)
            {
                ObjecttoClean.GetType().GetProperty("IsDirty").SetValue(ObjecttoClean, false);

                foreach (PropertyInfo pi in ObjecttoClean.GetType().GetProperties().Where(p => p.PropertyType.IsSubclassOf(typeof(BaseEntity))))
                {
                    if (pi.GetValue(ObjecttoClean) != null)
                    {
                        CleanObjectGraph(pi.GetValue(ObjecttoClean));
                    }
                }

                foreach (PropertyInfo pi in ObjecttoClean.GetType().GetProperties().Where(p => p.PropertyType.GetInterfaces().Any(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(ICollection<>)) && p.Name != "Rowversion"))
                {
                    IEnumerable<object> lsttocheck = pi.GetValue(ObjecttoClean) as IEnumerable<object>;

                    if (lsttocheck != null)
                    {
                        if (pi.Name == "DirtyProperties")
                            pi.SetValue(ObjecttoClean, new List<string>());
                        else
                        {
                            foreach (object o in lsttocheck)
                            {
                                o.GetType().GetProperty("IsDirty").SetValue(o, false);
                                o.GetType().GetProperty("DirtyProperties").SetValue(o, new List<string>());
                            }
                        }
                    }
                }
            }
            return (lstObjecttoClean);
        }
        /// <summary>
        /// Commits the changes.
        /// </summary>
        /// <param name="lsttosave">The lsttosave.</param>
        /// <param name="dbContext">The database context.</param>
        /// <returns></returns>
        /// <exception cref="Exception">
        /// </exception>
        public static bool CommitChanges(List<EntityCollection> lsttosave, CricketContext dbContext)
        {
            try
            {
                //dbContext.Dispose();
                // using (dbContext = Database1.getDBContext())
                //{
                //List<EntityCollection> lstCleared = lsttosave; //ClearCollections(lsttosave);
                foreach (EntityCollection entity in lsttosave)
                {
                    object EntitytoCheck = entity.Entity;

                    BaseEntity EntitytoSave = entity.Entity as BaseEntity;
                    if (EntitytoSave.IsNew)
                    {
                        User.Created(EntitytoSave);

                        dbContext.Entry(EntitytoSave).State = EntityState.Added;

                    }
                    else if (EntitytoSave.IsDirty)
                    {



                        if (EntitytoSave.DirtyProperties.Count > 0)
                        {
                            User.Updated(EntitytoSave);
                            dbContext.Entry(EntitytoSave).State = EntityState.Unchanged;
                            foreach (string changedproperty in EntitytoSave.DirtyProperties)
                            {
                                dbContext.Entry(EntitytoSave).Property(changedproperty).IsModified = true;
                            }
                        }
                    }
                    else
                        dbContext.Entry(EntitytoSave).State = EntityState.Unchanged;


                }

                dbContext.SaveChanges();
                lsttosave = ApplyChanges(lsttosave);


                return true;
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                var outputLines = new List<string>();
                foreach (var eve in ex.EntityValidationErrors)
                {
                    outputLines.Add(string.Format(
                        "{0}: Entity of type \"{1}\" in state \"{2}\" has the following validation errors:",
                        DateTime.Now, eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        outputLines.Add(string.Format(
                            "- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage));
                    }
                }
                //Write to file
                System.IO.File.AppendAllLines(@"d:\errors.txt", outputLines);
                throw;

                // Showing it on screen
                throw new Exception(string.Join(",", outputLines.ToArray()));


            }
            catch (DbUpdateConcurrencyException dex)
            {
                List<string> LstModifiedEntities = new List<string>();
                string message = "";
                foreach (DbEntityEntry be in dex.Entries)
                {
                    //if (be.Entity.GetType() == typeof(NumberGenerationEntity))
                    //    throw;
                    //else
                    //{
                        Type typ = be.Entity.GetType();
                        Object O = Activator.CreateInstance(typ);
                        O = be.Entity;
                        PropertyInfo IdentifierName = O.GetType().GetProperties().FirstOrDefault(p => p.GetCustomAttributes(false)
                    .Any(a => a.GetType() == typeof(IdentifierAttribute)));
                        string strIdentifier = "";
                        if (IdentifierName != null)
                        {
                            strIdentifier = IdentifierName.GetValue(O).ToString();
                        }
                        if (!string.IsNullOrEmpty(strIdentifier))
                            message = message + typ.Name + " with " + IdentifierName.Name + "=" + strIdentifier + " has been changed by other user, Please re query ";
                        else
                            message = message + typ.Name + " has been changed by other user, Please re query ";
                        message = message + Environment.NewLine;


                    }
                //}
                throw new Exception(message);
            }
            return true;
        }



        /// <summary>
        /// Deletes the entity.
        /// </summary>
        /// <typeparam name="t"></typeparam>
        /// <param name="entitytodelete">The entitytodelete.</param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public static bool DeleteEntity<t>(BaseEntity entitytodelete) where t : class
        {
            string strType = getEntityManager(entitytodelete);

            Type typType = Type.GetType("Cricket.DAL." + strType + "Manager");
            IEntityManager<t> IEManager = Activator.CreateInstance(typType) as IEntityManager<t>;

            CricketContext dbContext = new CricketContext();
            dbContext.Configuration.AutoDetectChangesEnabled = false;
            dbContext.Configuration.ProxyCreationEnabled = false;
            //dbContext.EMDcontext.
            dbContext.Database.Log = Logger;

            if (IEManager.DeleteEntity(dbContext, entitytodelete as t))
            {
                try
                {
                    dbContext.SaveChanges();
                    return true;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException ex)
                {
                    var outputLines = new List<string>();
                    foreach (var eve in ex.EntityValidationErrors)
                    {
                        outputLines.Add(string.Format(
                            "{0}: Entity of type \"{1}\" in state \"{2}\" has the following validation errors:",
                            DateTime.Now, eve.Entry.Entity.GetType().Name, eve.Entry.State));
                        foreach (var ve in eve.ValidationErrors)
                        {
                            outputLines.Add(string.Format(
                                "- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage));
                        }
                    }
                    //Write to file
                    System.IO.File.AppendAllLines(@"d:\errors.txt", outputLines);
                    throw;

                    // Showing it on screen
                    throw new Exception(string.Join(",", outputLines.ToArray()));
                }
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// Gets the entity.
        /// </summary>
        /// <typeparam name="t"></typeparam>
        /// <param name="id">The identifier.</param>
        /// <param name="Entitytoget">The entitytoget.</param>
        /// <returns></returns>
        public static t GetEntity<t>(Guid id) where t : BaseEntity
        {
            object Entitytoget = Activator.CreateInstance(typeof(t));
            string strType = getEntityManager(Entitytoget as BaseEntity);
            Type typType = Type.GetType("Cricket.DAL." + strType + "Manager");
            IEntityManager<t> IEManager = Activator.CreateInstance(typType) as IEntityManager<t>;
            CricketContext dbContext = new CricketContext();
            dbContext.Database.Log = Logger;
            dbContext.Configuration.AutoDetectChangesEnabled = false;
            dbContext.Configuration.ProxyCreationEnabled = false;
            t Entitytoreturn = (t)IEManager.GetEntityById(dbContext, id);
            CleanEntities<t>(Entitytoreturn);
            return Entitytoreturn;
        }
        /// <summary>
        /// Gets the entity list.
        /// </summary>
        /// <typeparam name="t"></typeparam>
        /// <param name="entityList">The entity list.</param>
        /// <returns></returns>
        public static ObservableCollection<t> GetEntityList<t>() where t : BaseEntity
        {
            object entityList=  Activator.CreateInstance(typeof(t));
            string strType = getEntityManager(entityList as BaseEntity );
            Type typType = Type.GetType("Cricket.DAL." + strType + "Manager");
            IEntityManager<t> IEManager = Activator.CreateInstance(typType) as IEntityManager<t>;
            CricketContext dbContext = new CricketContext();
            dbContext.Database.Log = Logger;
            dbContext.Configuration.AutoDetectChangesEnabled = false;
            dbContext.Configuration.ProxyCreationEnabled = false;
            ObservableCollection<t> EntityList = IEManager.getEntityList(dbContext);
            CleanEntityList<t>(EntityList);
            return EntityList;
        }

        /// <summary>
        /// Gets the entity manager.
        /// </summary>
        /// <param name="Entitytosave">The entitytosave.</param>
        /// <returns></returns>
        public static string getEntityManager(BaseEntity Entitytosave)
        {
            String[] strTypeName = Entitytosave.GetType().ToString().Split('.');
            Console.WriteLine(strTypeName.Count().ToString());
            Console.WriteLine(strTypeName[strTypeName.Count() - 1]);
            string strType = strTypeName[strTypeName.Count() - 1];

            return strType;
        }

        /// <summary>
        /// Gets the new entity.
        /// </summary>
        /// <typeparam name="t"></typeparam>
        /// <returns></returns>
        public static t getNewEntity<t>() where t : BaseEntity
        {
            String strEntityName = typeof(t).ToString();//.GetType().ToString().Split('.');
            String ModelName = strEntityName.Substring(strEntityName.IndexOf('.') + 1);
            String ClassName = ModelName.Substring(ModelName.IndexOf('.') + 1);
            BaseEntity NewObject = (BaseEntity)Activator.CreateInstance(typeof(t));
            NewObject.ActiveIndicator = true;
            CricketContext dbContext = new CricketContext();
            ObjectContext objectContext = ((IObjectContextAdapter)dbContext).ObjectContext;
            ObjectSet<t> set = objectContext.CreateObjectSet<t>();
            IEnumerable<string> keyNames = set.EntitySet.ElementType
                                                        .KeyMembers
                                                        .Select(k => k.Name);
            NewObject.IsNew = true;
            //NewObject.Indicator = true;
            foreach (string keyField in keyNames)
            {
                PropertyInfo propinfo = typeof(t).GetProperty(keyField);
                propinfo.SetValue(NewObject as t, Guid.NewGuid());
            }
            return NewObject as t;
        }

        /// <summary>
        /// Logs the specified log string.
        /// </summary>
        /// <param name="logString">The log string.</param>
        private static void Logger(string logString)
        {
            Debug.WriteLine(logString);
        }
        /// <summary>
        /// Gets the database context.
        /// </summary>
        /// <returns></returns>
        public static CricketContext getDBContext()
        {
            CricketContext dbContext = new CricketContext();
            dbContext.Configuration.AutoDetectChangesEnabled = false;
            dbContext.Configuration.ProxyCreationEnabled = false;
            dbContext.Configuration.LazyLoadingEnabled = false;
            dbContext.Database.Log = Logger;
            dbContext.Database.CommandTimeout = 0;

            return dbContext;
        }

        /// <summary>
        /// Applies the changes.
        /// </summary>
        /// <param name="lstobjectsSaved">The lstobjects saved.</param>
        /// <returns></returns>
        public static List<EntityCollection> ApplyChanges(List<EntityCollection> lstobjectsSaved)
        {
            foreach (EntityCollection e in lstobjectsSaved)
            {
                object savedentity = e.Entity;
                object originalobject = e.OriginalEntity;
                originalobject.GetType().GetProperty("Rowversion").SetValue(originalobject, savedentity.GetType().GetProperty("Rowversion").GetValue(savedentity));
                originalobject.GetType().GetProperty("IsDirty").SetValue(originalobject, false);
                originalobject.GetType().GetProperty("IsNew").SetValue(originalobject, false);


            }
            return lstobjectsSaved;
        }

        /// <summary>
        /// Gets the dirty entities.
        /// </summary>
        /// <typeparam name="t"></typeparam>
        /// <param name="Entitytocheck">The entitytocheck.</param>
        /// <param name="lstobjectstoSave">The lstobjectsto save.</param>
        /// <returns></returns>
        public static List<EntityCollection> getDirtyEntities<t>(BaseEntity Entitytocheck, List<EntityCollection> lstobjectstoSave)
        {

            if (Entitytocheck.IsNew || Entitytocheck.IsDirty)
            {
                //deep clone the object that should be saved
                object ObjecttoSave = DeepClone(Entitytocheck);//Entitytocheck.getObjecttoSave();
                //get Key Field of the Object that should be saved
                PropertyInfo KeyPI = ObjecttoSave.GetType().GetProperties().Where(p => p.GetCustomAttribute(typeof(KeyAttribute)) != null).SingleOrDefault();
                EntityCollection ec = new EntityCollection();
                //Get Key field value that should be saved
                Guid newkey = (Guid)KeyPI.GetValue(ObjecttoSave);

                if (newkey != null)
                {
                    //Check if object with this ID is allready added to the list of objects to be saved
                    if (lstobjectstoSave.Where(p => p.EntityKey == newkey).ToList().Count == 0)
                    {
                        ec.EntityKey = (Guid)KeyPI.GetValue(ObjecttoSave);
                        ec.Entity = ObjecttoSave;
                        ec.OriginalEntity = Entitytocheck;
                        //if Object is not added add it
                        lstobjectstoSave.Add(ec);
                        foreach (PropertyInfo pi in (Entitytocheck.GetType().GetProperties()))
                        {
                            //if property is of Reference type (Link field) and Property is not parent reference of a collection 
                            if (pi.PropertyType.IsSubclassOf(typeof(BaseEntity)) && pi.GetCustomAttribute(typeof(ParentAttribute)) == null)
                            {
                                object objecttocheck = pi.GetValue(Entitytocheck);
                                if (objecttocheck != null)
                                {
                                    getDirtyEntities<t>(objecttocheck as BaseEntity, lstobjectstoSave);
                                    pi.SetValue(ObjecttoSave, null);
                                    //PropertyInfo fkpi = Entitytocheck.GetType().GetProperties().Where(p => p.GetCustomAttribute(ForeignKey.Gettype()))
                                    object[] attrs = pi.GetCustomAttributes(false);
                                    foreach (object Cattr in attrs)
                                    {
                                        ForeignKeyAttribute FK = Cattr as ForeignKeyAttribute;
                                        if (FK != null)
                                        {
                                            string FKName = FK.Name;
                                            PropertyInfo pifk = Entitytocheck.GetType().GetProperty(FKName);
                                            PropertyInfo KeyName = objecttocheck.GetType().GetProperties().FirstOrDefault(p => p.GetCustomAttributes(false)
                     .Any(a => a.GetType() == typeof(KeyAttribute)));
                                            pifk.SetValue(ObjecttoSave, KeyName.GetValue(objecttocheck));
                                        }
                                    }

                                }
                            }
                            //if property is a collection
                            else if (pi.PropertyType.GetInterfaces().Any(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(ICollection<>)) && pi.Name != "Rowversion" && pi.Name != "DirtyProperties")
                            {
                                IEnumerable<object> lsttocheck = pi.GetValue(Entitytocheck) as IEnumerable<object>;
                                if (lsttocheck != null)
                                {
                                    foreach (object o in lsttocheck)
                                    {
                                        getDirtyEntities<t>(o as BaseEntity, lstobjectstoSave);
                                    }

                                    pi.SetValue(ObjecttoSave, null);
                                }
                            }
                            //If Property is a Parent reference
                            else if (pi.PropertyType.IsSubclassOf(typeof(BaseEntity)) && pi.GetCustomAttribute(typeof(ParentAttribute)) != null)
                            {
                                object objecttocheck = pi.GetValue(ObjecttoSave);
                                if (objecttocheck != null)
                                {
                                    pi.SetValue(ObjecttoSave, null);
                                    object[] attrs = pi.GetCustomAttributes(false);
                                    foreach (object Cattr in attrs)
                                    {
                                        ForeignKeyAttribute FK = Cattr as ForeignKeyAttribute;
                                        if (FK != null)
                                        {
                                            string FKName = FK.Name;
                                            PropertyInfo pifk = Entitytocheck.GetType().GetProperty(FKName);
                                            PropertyInfo KeyName = objecttocheck.GetType().GetProperties().FirstOrDefault(p => p.GetCustomAttributes(false)
                     .Any(a => a.GetType() == typeof(KeyAttribute)));
                                            pifk.SetValue(ObjecttoSave, KeyName.GetValue(objecttocheck));
                                        }
                                    }
                                }
                            }


                        }
                    }
                }
            }

            return lstobjectstoSave;
        }



        /// <summary>
        /// Cleans the entity list.
        /// </summary>
        /// <typeparam name="t"></typeparam>
        /// <param name="EntityList">The entity list.</param>
        public static void CleanEntityList<t>(ObservableCollection<t> EntityList) where t : BaseEntity
        {
            EntityList.ToList().ForEach(c => c.IsDirty = false);
        }
        /// <summary>
        /// Cleans the entities.
        /// </summary>
        /// <typeparam name="t"></typeparam>
        /// <param name="EntitytoClean">The entityto clean.</param>
        public static void CleanEntities<t>(BaseEntity EntitytoClean) where t : BaseEntity
        {
            if (EntitytoClean != null)
            {
                EntitytoClean.IsDirty = false;

                List<PropertyInfo> properties = EntitytoClean.GetType().GetProperties().Where(p => p.GetMethod.IsVirtual).ToList<PropertyInfo>();
                foreach (PropertyInfo p in properties)
                {
                    if (p.PropertyType.GetInterface("IEnumerable") == null && p.GetCustomAttribute(typeof(IgnoreSaveAttribute)) == null)
                    {
                        BaseEntity B = (BaseEntity)p.GetValue(EntitytoClean);
                        if (B != null)
                            B.IsDirty = false;
                    }
                }
            }
        }


        /// <summary>
        /// Deep clones the object.
        /// </summary>
        /// <typeparam name="t"></typeparam>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public static t DeepClone<t>(t obj)
        {
            using (var ms = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(ms, obj);
                ms.Position = 0;

                return (t)formatter.Deserialize(ms);
            }
        }
    }
}
