﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cricket.DAL
{
    public interface IEntityManager<TEntity>
    {
        /// <summary>
        /// Gets the entity by identifier.
        /// </summary>
        /// <param name="dbcontext">The dbcontext.</param>
        /// <param name="EntityID">The entity identifier.</param>
        /// <returns></returns>
        TEntity GetEntityById(CricketContext dbcontext, Guid EntityID);
        /// <summary>
        /// Deletes the entity.
        /// </summary>
        /// <param name="dbcontext">The dbcontext.</param>
        /// <param name="Entitytodelete">The entitytodelete.</param>
        /// <returns></returns>
        bool DeleteEntity(CricketContext dbcontext, TEntity Entitytodelete);
        /// <summary>
        /// Gets the entity list.
        /// </summary>
        /// <param name="dbcontext">The dbcontext.</param>
        /// <returns></returns>
        ObservableCollection<TEntity> getEntityList(CricketContext dbcontext);
    }
}
