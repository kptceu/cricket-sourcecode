﻿using Cricket.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cricket.DAL
{
   public class UserEntityManager : IEntityManager<UserEntity>
    {
        public UserEntity GetEntityById(CricketContext db, Guid EntityID)
        {
            UserEntity objnew = (from obj in db.Users where obj.UserId == EntityID select obj).SingleOrDefault();
            objnew = (UserEntity)Database.CleanObjectGraph(objnew);
            return objnew;
        }

        public ObservableCollection<UserEntity> getEntityList(CricketContext db)
        {
            ObservableCollection<UserEntity> Lst = new ObservableCollection<UserEntity>(from obj in db.Users select obj);
            var lsttoclean = Lst.ToList<object>();
            var lstcleaned = Database.CleanObjectGraph(lsttoclean);
            Lst = new ObservableCollection<UserEntity>((lstcleaned.Cast<UserEntity>()));
            return Lst;
        }

        public bool DeleteEntity(CricketContext dbcontext, UserEntity EntitytoDelete)
        {
            dbcontext.Entry(EntitytoDelete).State = EntityState.Deleted;
            return true;
        }
    }
}


