﻿using Cricket.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cricket.DAL
{
    public class SeasonEntityManager : IEntityManager<SeasonEntity>
    {
        public SeasonEntity GetEntityById(CricketContext db, Guid EntityID)
        {
            SeasonEntity objnew = (from obj in db.Seasons where obj.SeasonId == EntityID select obj).SingleOrDefault();
            objnew = (SeasonEntity)Database.CleanObjectGraph(objnew);
            return objnew;
        }

        public ObservableCollection<SeasonEntity> getEntityList(CricketContext db)
        {
            ObservableCollection<SeasonEntity> Lst = new ObservableCollection<SeasonEntity>(from obj in db.Seasons select obj);
            var lsttoclean = Lst.ToList<object>();
            var lstcleaned = Database.CleanObjectGraph(lsttoclean);
            Lst = new ObservableCollection<SeasonEntity>((lstcleaned.Cast<SeasonEntity>()));
            return Lst;
        }

        public bool DeleteEntity(CricketContext dbcontext, SeasonEntity EntitytoDelete)
        {
            dbcontext.Entry(EntitytoDelete).State = EntityState.Deleted;
            return true;
        }

        public int getRecentSeason(CricketContext db)
        {
            int recentseason = 0;
            try
            {
                List<SeasonEntity> seasons = db.Seasons.Where(p => p.Indicator != RecordStatus.Deleted).ToList<SeasonEntity>();
                //recentseason = seasons.Max(Convert.ToInt32(p => p.SeasonName));
                if (seasons.Count > 0)
                {
                    seasons.OrderByDescending(p => p.SeasonName);
                    recentseason = Convert.ToInt32(seasons[0].SeasonName);

                }
                else
                {
                    recentseason = 2013;
                }
                return recentseason;
            }
            catch (Exception ex)
            {
                //conn.Close();
                throw;
            }

        }
    }
}
