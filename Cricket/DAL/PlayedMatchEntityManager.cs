﻿using Cricket.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cricket.DAL
{
    public class PlayedMatchEntityManager : IEntityManager<PlayedMatchEntity>
    {
        public PlayedMatchEntity GetEntityById(CricketContext db, Guid EntityID)
        {
            PlayedMatchEntity objnew = (from obj in db.PlayedMatches.Include(p => p.TeamOneKeeper).Include(p => p.TeamOneCaptain)
                                        .Include(p => p.TeamTwoCaptain).Include(p => p.TeamTwoKeeper)
                                        where obj.PlayedMatchId == EntityID
                                        select obj).SingleOrDefault();
            objnew = (PlayedMatchEntity)Database.CleanObjectGraph(objnew);
            return objnew;
        }

        public ObservableCollection<PlayedMatchEntity> getEntityList(CricketContext db)
        {
            ObservableCollection<PlayedMatchEntity> Lst = new ObservableCollection<PlayedMatchEntity>(from obj in db.PlayedMatches.Include(p => p.TeamOneKeeper)
                                                                                                      .Include(p => p.TeamOneCaptain)
                                                                                                      .Include(p => p.TeamTwoCaptain).Include(p => p.TeamTwoKeeper)
                                                                                                      where obj.Fixture.SeasonId == User.Season.SeasonId &&
                                                                                                      obj.Fixture.DivisionId == User.Division.DivisionId &&
                                                                                                      obj.Fixture.ZoneId == User.Zone.ZoneId
                                                                                                      select obj);
            var lsttoclean = Lst.ToList<object>();
            var lstcleaned = Database.CleanObjectGraph(lsttoclean);
            Lst = new ObservableCollection<PlayedMatchEntity>((lstcleaned.Cast<PlayedMatchEntity>()));
            return Lst;
        }

        public bool DeleteEntity(CricketContext dbcontext, PlayedMatchEntity EntitytoDelete)
        {
            dbcontext.Entry(EntitytoDelete).State = EntityState.Deleted;
            return true;
        }

        public PlayedMatchEntity GetByFixtureId(CricketContext db, Guid FixtureId)
        {
            PlayedMatchEntity objnew = (from obj in db.PlayedMatches.Include(p => p.TeamOneKeeper).Include(p => p.TeamOneCaptain)
                                        .Include(p => p.TeamTwoCaptain).Include(p => p.TeamTwoKeeper)
                                        where obj.FixtureId == FixtureId
                                        select obj).SingleOrDefault();
            return objnew;
        }

        public ObservableCollection<PlayedMatchEntity> GetPlayedMatchesbyTeam(CricketContext context, TeamEntity ObjTeam, ZoneEntity ObjZone, SeasonEntity ObjSeason, DivisionEntity ObjDivision)
        {
            ObservableCollection<PlayedMatchEntity> lstPlayedMatchesByaTeam = new ObservableCollection<PlayedMatchEntity>(context.PlayedMatches.Include(path => path.TeamOne).Include(p => p.TeamTwo).Include(p => p.Fixture).Include("Fixture.Location").Where(p => (p.TeamOne.TeamId == ObjTeam.TeamId || p.TeamTwo.TeamId == ObjTeam.TeamId) && p.Fixture.ZoneId == ObjZone.ZoneId && p.Fixture.SeasonId == ObjSeason.SeasonId && p.Fixture.DivisionId == ObjDivision.DivisionId).ToList().OrderBy (p=>p.Fixture.FromDate ));

            return lstPlayedMatchesByaTeam;
        }
    }
}



