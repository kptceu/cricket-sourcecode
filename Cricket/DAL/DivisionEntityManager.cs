﻿using Cricket.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cricket.DAL
{
    public class DivisionEntityManager : IEntityManager<DivisionEntity>
    {
        public DivisionEntity GetEntityById(CricketContext db, Guid EntityID)
        {
            DivisionEntity objnew = (from obj in db.Divisions where obj.DivisionId == EntityID select obj).SingleOrDefault();
            objnew = (DivisionEntity)Database.CleanObjectGraph(objnew);
            return objnew;
        }
      
        public ObservableCollection<DivisionEntity> getEntityList(CricketContext db)
        {
            ObservableCollection<DivisionEntity> Lst = new ObservableCollection<DivisionEntity>(from obj in db.Divisions select obj);
            var lsttoclean = Lst.OrderBy(p=>p.DivisionName).ToList<object>();
            var lstcleaned = Database.CleanObjectGraph(lsttoclean);
            Lst = new ObservableCollection<DivisionEntity>((lstcleaned.Cast<DivisionEntity>()));
            return Lst;
        }

        public bool DeleteEntity(CricketContext dbcontext, DivisionEntity EntitytoDelete)
        {
            dbcontext.Entry(EntitytoDelete).State = EntityState.Deleted;
            return true;
        }

        
    }
}
