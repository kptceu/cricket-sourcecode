﻿using Cricket.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cricket.DAL
{
   public class LocationEntityManager : IEntityManager<LocationEntity>
    {
        public LocationEntity GetEntityById(CricketContext db, Guid EntityID)
        {
            LocationEntity objnew = (from obj in db.Locations.Include ("CZone") where obj.LocationId == EntityID select obj).SingleOrDefault();
            objnew = (LocationEntity)Database.CleanObjectGraph(objnew);
            return objnew;
        }

        public ObservableCollection<LocationEntity> getEntityList(CricketContext db)
        {
            ObservableCollection<LocationEntity> Lst = new ObservableCollection<LocationEntity>(from obj in db.Locations.Include("CZone") select obj);
            var lsttoclean = Lst.OrderBy(p=>p.LocationName).ToList<object>();
            var lstcleaned = Database.CleanObjectGraph(lsttoclean);
            Lst = new ObservableCollection<LocationEntity>((lstcleaned.Cast<LocationEntity>()));
            Lst.ToList().ForEach(x => x.IsSelectable = true);
            return Lst;
        }


        public ObservableCollection<LocationEntity> getGroundsinCurrentZone(CricketContext db,ZoneEntity ObjZone)
        {
            ObservableCollection<LocationEntity> Lst = new ObservableCollection<LocationEntity>(from obj in db.Locations.Include("CZone") where obj.ZoneId==ObjZone.ZoneId  select obj);
            var lsttoclean = Lst.OrderBy(p=>p.LocationName).ToList<object>();
            var lstcleaned = Database.CleanObjectGraph(lsttoclean);
            Lst = new ObservableCollection<LocationEntity>((lstcleaned.Cast<LocationEntity>()));
            return Lst;
        }

        public bool DeleteEntity(CricketContext dbcontext, LocationEntity EntitytoDelete)
        {
            dbcontext.Entry(EntitytoDelete).State = EntityState.Deleted;
            return true;
        }

        public bool CheckGround(CricketContext db, string groundname)
        {
            LocationEntity objloc = (from obj in db.Locations where obj.StadiumName.ToUpper() == groundname.ToUpper() && obj.ZoneId==User.Zone.ZoneId select obj).FirstOrDefault();
            if (objloc == null)
                return false;
            else
                return true;
        }
    }
}