﻿using Cricket.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cricket.DAL
{
    public class ZoneEntityManager : IEntityManager<ZoneEntity>
    {
        public ZoneEntity GetEntityById(CricketContext db, Guid EntityID)
        {
            ZoneEntity objnew = (from obj in db.Zones where obj.ZoneId == EntityID select obj).SingleOrDefault();
            objnew = (ZoneEntity)Database.CleanObjectGraph(objnew);
            return objnew;
        }

        public ObservableCollection<ZoneEntity> getEntityList(CricketContext db)
        {
            ObservableCollection<ZoneEntity> Lst = new ObservableCollection<ZoneEntity>(from obj in db.Zones select obj);
            var lsttoclean = Lst.OrderBy(p=>p.ZoneName).ToList<object>();
            var lstcleaned = Database.CleanObjectGraph(lsttoclean);
            Lst = new ObservableCollection<ZoneEntity>((lstcleaned.Cast<ZoneEntity>()));
            return Lst;
        }

        public bool DeleteEntity(CricketContext dbcontext, ZoneEntity EntitytoDelete)
        {
            dbcontext.Entry(EntitytoDelete).State = EntityState.Deleted;
            return true;
        }

        public void GetMaxZoneNumber()
        {

        }
    }
}



