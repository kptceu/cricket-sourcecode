﻿using Cricket.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cricket.DAL
{
    public class TeamEntityManager : IEntityManager<TeamEntity>
    {
        public TeamEntity GetEntityById(CricketContext db, Guid EntityID)
        {
            TeamEntity objnew = (from obj in db.Teams where obj.TeamId == EntityID select obj).SingleOrDefault();
            objnew = (TeamEntity)Database.CleanObjectGraph(objnew);
            return objnew;
        }

        public ObservableCollection<TeamEntity> getEntityList(CricketContext db)
        {
            ObservableCollection<TeamEntity> Lst = new ObservableCollection<TeamEntity>(from obj in db.Teams.Include(p=>p.CZone ) where obj.ZoneId==User.Zone.ZoneId select obj);
            
            var lsttoclean = Lst.OrderBy(p=>p.TeamName).ToList<object>();
            var lstcleaned = Database.CleanObjectGraph(lsttoclean);
            Lst = new ObservableCollection<TeamEntity>((lstcleaned.Cast<TeamEntity>()));
            return Lst;
        }

        public bool DeleteEntity(CricketContext dbcontext, TeamEntity EntitytoDelete)
        {
            dbcontext.Entry(EntitytoDelete).State = EntityState.Deleted;
            return true;
        }

        public ObservableCollection<TeamEntity> getTeamsinCurrentZone(CricketContext db,ZoneEntity  objzone )
        {
            ObservableCollection<TeamEntity> Lst = new ObservableCollection<TeamEntity>(from obj in db.Teams.Include("CZone") where obj.ZoneId==objzone.ZoneId   select obj);
            //Lst.OrderBy(p => p.TeamName);
            var lsttoclean = Lst.OrderBy(p => p.TeamName).ToList<object>();
            var lstcleaned = Database.CleanObjectGraph(lsttoclean);
            Lst = new ObservableCollection<TeamEntity>((lstcleaned.Cast<TeamEntity>()));
            return Lst;
        }

        public ObservableCollection<TeamEntity> getTeamsinCurrentZoneSeasonDivision(CricketContext db, ZoneEntity objzone,SeasonEntity ObjSeason,DivisionEntity ObjDivision)
        {
            ObservableCollection<TeamEntity > Teams = new ObservableCollection<TeamEntity >();

            Teams = new ObservableCollection<TeamEntity> ( db.LoadDivisions.Where(p => p.ZoneId == objzone.ZoneId && p.SeasonId == ObjSeason.SeasonId && p.DivisionId == ObjDivision.DivisionId).Select(p => p.Team ).ToList<TeamEntity >());
            
            var lsttoclean = Teams.OrderBy(p => p.TeamName).ToList<object>();
            var lstcleaned = Database.CleanObjectGraph(lsttoclean);
            Teams = new ObservableCollection<TeamEntity>((lstcleaned.Cast<TeamEntity>()));
            return Teams;
        }
    }
}

