﻿using Cricket.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cricket.DAL
{
   public class GroupEntityManager : IEntityManager<GroupEntity>
    {
        public GroupEntity GetEntityById(CricketContext db, Guid EntityID)
        {
            GroupEntity objnew = (from obj in db.Groups where obj.GroupId == EntityID select obj).SingleOrDefault();
            objnew = (GroupEntity)Database.CleanObjectGraph(objnew);
            return objnew;
        }

        public ObservableCollection<GroupEntity> getEntityList(CricketContext db)
        {
            ObservableCollection<GroupEntity> Lst = new ObservableCollection<GroupEntity>(from obj in db.Groups select obj);
            var lsttoclean = Lst.ToList<object>();
            var lstcleaned = Database.CleanObjectGraph(lsttoclean);
            Lst = new ObservableCollection<GroupEntity>((lstcleaned.Cast<GroupEntity>()));
            return Lst;
        }

        public bool DeleteEntity(CricketContext dbcontext, GroupEntity EntitytoDelete)
        {
            dbcontext.Entry(EntitytoDelete).State = EntityState.Deleted;
            return true;
        }
    }
}