﻿using Cricket.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cricket.DAL
{
    public class ErrorLogEntityManager : IEntityManager<ErrorLogEntity>
    {
        public ErrorLogEntity GetEntityById(CricketContext db, Guid EntityID)
        {
            ErrorLogEntity objnew = (from obj in db.ErrorLogs where obj.ErrorLogId == EntityID select obj).SingleOrDefault();
            objnew = (ErrorLogEntity)Database.CleanObjectGraph(objnew);
            return objnew;
        }

        public ObservableCollection<ErrorLogEntity> getEntityList(CricketContext db)
        {
            ObservableCollection<ErrorLogEntity> Lst = new ObservableCollection<ErrorLogEntity>(from obj in db.ErrorLogs select obj);
            var lsttoclean = Lst.ToList<object>();
            var lstcleaned = Database.CleanObjectGraph(lsttoclean);
            Lst = new ObservableCollection<ErrorLogEntity>((lstcleaned.Cast<ErrorLogEntity>()));
            return Lst;
        }

        public bool DeleteEntity(CricketContext dbcontext, ErrorLogEntity EntitytoDelete)
        {
            dbcontext.Entry(EntitytoDelete).State = EntityState.Deleted;
            return true;
        }
    }
}
   
