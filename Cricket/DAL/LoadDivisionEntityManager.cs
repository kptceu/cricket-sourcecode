﻿using Cricket.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cricket.DAL
{
    public class LoadDivisionEntityManager : IEntityManager<LoadDivisionEntity>
    {
        public LoadDivisionEntity GetEntityById(CricketContext db, Guid EntityID)
        {
            LoadDivisionEntity objnew = (from obj in db.LoadDivisions where obj.LoadDivisionId == EntityID select obj).SingleOrDefault();
            objnew = (LoadDivisionEntity)Database.CleanObjectGraph(objnew);
            return objnew;
        }

        public ObservableCollection<LoadDivisionEntity> getEntityList(CricketContext db)
        {
            ObservableCollection<LoadDivisionEntity> Lst = new ObservableCollection<LoadDivisionEntity>(from obj in db.LoadDivisions.Include(p=>p.Team).Include(p=>p.Group) where
                                                                                                        obj.DivisionId==User.Division.DivisionId && obj.ZoneId==
                                                                                                        User.Zone.ZoneId && obj.SeasonId==User.Season.SeasonId select obj);
            var lsttoclean = Lst.ToList<object>();
            var lstcleaned = Database.CleanObjectGraph(lsttoclean);
            Lst = new ObservableCollection<LoadDivisionEntity>((lstcleaned.Cast<LoadDivisionEntity>()));
            return Lst;
        }

        public bool DeleteEntity(CricketContext dbcontext, LoadDivisionEntity EntitytoDelete)
        {
            dbcontext.Entry(EntitytoDelete).State = EntityState.Deleted;
            return true;
        }

        public ObservableCollection<TeamEntity> GetUnaddedTeams(CricketContext db,Guid ZoneId,Guid SeasonId)
        {
            ObservableCollection<TeamEntity> LstTeam = new ObservableCollection<TeamEntity>(from obj in db.LoadDivisions
                                                                                                    where obj.SeasonId == SeasonId && obj.ZoneId == ZoneId
                                                                                                    select obj.Team);
            return LstTeam;
        }


        public ObservableCollection<LoadDivisionEntity> getBySeasonId(CricketContext db,Guid Seasonid)
        {
            ObservableCollection<LoadDivisionEntity> Lst = new ObservableCollection<LoadDivisionEntity>(from obj in db.LoadDivisions.Include(p => p.Team).Include(p => p.Group)
                                                                                                        where
                                    obj.DivisionId == User.Division.DivisionId && obj.ZoneId ==
                                    User.Zone.ZoneId && obj.SeasonId == Seasonid
                                                                                                        select obj);
            var lsttoclean = Lst.ToList<object>();
            var lstcleaned = Database.CleanObjectGraph(lsttoclean);
            Lst = new ObservableCollection<LoadDivisionEntity>((lstcleaned.Cast<LoadDivisionEntity>()));
            return Lst;
        }

        public ObservableCollection<LoadDivisionEntity> getForAllSeasons(CricketContext db)
        {
            ObservableCollection<LoadDivisionEntity> Lst = new ObservableCollection<LoadDivisionEntity>(from obj in db.LoadDivisions.Include(p => p.Team).Include(p => p.Group)
                                                                                                        where
                                    obj.DivisionId == User.Division.DivisionId && obj.ZoneId ==
                                    User.Zone.ZoneId select obj);
            var lsttoclean = Lst.ToList<object>();
            var lstcleaned = Database.CleanObjectGraph(lsttoclean);
            Lst = new ObservableCollection<LoadDivisionEntity>((lstcleaned.Cast<LoadDivisionEntity>()));
            return Lst;
        }


    }
}