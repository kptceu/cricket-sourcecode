﻿using Cricket.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cricket.DAL
{
    public class FixtureEntityManager : IEntityManager<FixtureEntity>
    {
        public FixtureEntity GetEntityById(CricketContext db, Guid EntityID)
        {
            FixtureEntity objnew = (from obj in db.Fixtures.Include(p => p.TeamOne).Include(p => p.TeamTwo).Include(p=>p.UmpireOne)
                                    .Include(p=>p.UmpireTwo).Include(p=>p.Scorer).Include(p=>p.Location)
                                    where obj.FixtureId == EntityID select obj).SingleOrDefault();
            objnew = (FixtureEntity)Database.CleanObjectGraph(objnew);
            return objnew;
        }

        public ObservableCollection<FixtureEntity> getEntityList(CricketContext db)
        {
            ObservableCollection<FixtureEntity> Lst = new ObservableCollection<FixtureEntity>(from obj in db.Fixtures.Include(p => p.TeamOne).Include(p => p.TeamTwo)
                                                                                   .Include(p => p.UmpireOne).Include(p => p.UmpireTwo).Include(p => p.Scorer)
                                                                                   .Include(p => p.Location)
                                                                                              where obj.DivisionId ==
                                                              User.Division.DivisionId && obj.SeasonId ==
                                                              User.Season.SeasonId && obj.ZoneId == User.Zone.ZoneId
                                                                                              select obj);
            var lsttoclean = Lst.ToList<object>();
            var lstcleaned = Database.CleanObjectGraph(lsttoclean);
            Lst = new ObservableCollection<FixtureEntity>((lstcleaned.Cast<FixtureEntity>()));
            return Lst;
        }

        public bool DeleteEntity(CricketContext dbcontext, FixtureEntity EntitytoDelete)
        {
            dbcontext.Entry(EntitytoDelete).State = EntityState.Deleted;
            return true;
        }

        public ObservableCollection<FixtureEntity> getListByLocationId(CricketContext db,Guid LocationId)
        {
            ObservableCollection<FixtureEntity> Lst = new ObservableCollection<FixtureEntity>((from obj in db.Fixtures.Include(p=>p.TeamOne).Include(p=>p.TeamTwo).Include(p => p.Division)
                                                                                               where obj.SeasonId ==
                                                              User.Season.SeasonId && obj.ZoneId == User.Zone.ZoneId && obj.LocationId==LocationId
                                                                                              select obj).ToList().OrderBy(p=>p.FromDate).ThenBy(p=>p.GroupName));
            Lst = new ObservableCollection<FixtureEntity>(Lst.OrderBy(p => p.FromDate));
            var lsttoclean = Lst.ToList<object>();
            var lstcleaned = Database.CleanObjectGraph(lsttoclean);
            Lst = new ObservableCollection<FixtureEntity>((lstcleaned.Cast<FixtureEntity>()));
            return Lst;
        }

        public ObservableCollection<FixtureEntity> getListByOfficialId(CricketContext db,Guid OfficialId)
        {
            ObservableCollection<FixtureEntity> Lst = new ObservableCollection<FixtureEntity>(from obj in db.Fixtures.Include(p => p.TeamOne).Include(p => p.TeamTwo).Include(p=>p.Division)
                                                                                   .Include(p => p.UmpireOne).Include(p => p.UmpireTwo).Include(p => p.Scorer)
                                                                                   .Include(p => p.Location) where
                                                                                                obj.SeasonId ==
                                                              User.Season.SeasonId && obj.ZoneId == User.Zone.ZoneId && (obj.UmpireOneId==OfficialId || obj.UmpireTwoId==OfficialId
                                                              || obj.ScorerId==OfficialId)
                                                                                              select obj);
            Lst = new ObservableCollection<FixtureEntity>(Lst.OrderBy(p => p.FromDate));
            var lsttoclean = Lst.ToList<object>();
            var lstcleaned = Database.CleanObjectGraph(lsttoclean);
            Lst = new ObservableCollection<FixtureEntity>((lstcleaned.Cast<FixtureEntity>()));
            return Lst;
        }

        public ObservableCollection<FixtureEntity> getListByFromAndToDate(CricketContext db, DateTime fromdate,DateTime todate)
        {
            ObservableCollection<FixtureEntity> Lst = new ObservableCollection<FixtureEntity>(from obj in db.Fixtures.Include(p => p.TeamOne).Include(p => p.TeamTwo).Include(p => p.Division)
                                                                                   .Include(p => p.UmpireOne).Include(p => p.UmpireTwo).Include(p => p.Scorer).Include(P=>P.Season)
                                                                                   .Include(p => p.Location).Include(P=>P.Zone)
                                                                                              where
                                                                                 obj.SeasonId ==
                                               User.Season.SeasonId && obj.ZoneId == User.Zone.ZoneId && (DbFunctions.TruncateTime(obj.FromDate)) >= fromdate.Date && (DbFunctions.TruncateTime(obj.FromDate))
                                               <= todate.Date   select obj);
            Lst = new ObservableCollection<FixtureEntity>(Lst.OrderBy(p => p.FromDate));
            var lsttoclean = Lst.ToList<object>();
            var lstcleaned = Database.CleanObjectGraph(lsttoclean);
            Lst = new ObservableCollection<FixtureEntity>((lstcleaned.Cast<FixtureEntity>()));
            return Lst;
        }
    }
}
   