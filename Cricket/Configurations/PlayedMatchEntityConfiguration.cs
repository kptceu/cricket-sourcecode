﻿using Cricket.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cricket.Configurations
{
    public class PlayedMatchEntityConfiguration : EntityTypeConfiguration<PlayedMatchEntity>
    {
        public PlayedMatchEntityConfiguration()
        {
            ToTable("PlayedMatches");
            //primary key
            HasKey(p => p.PlayedMatchId);

            HasRequired(p => p.Fixture).WithMany().HasForeignKey(p => p.FixtureId).WillCascadeOnDelete(false);
            HasRequired(p => p.TeamOne).WithMany().HasForeignKey(p => p.TeamOneId).WillCascadeOnDelete(false);
            HasRequired(p => p.TeamTwo).WithMany().HasForeignKey(p => p.TeamTwoId).WillCascadeOnDelete(false);




            Ignore(p => p.IsDirty);
            Ignore(p => p.IsNew);
            Ignore(p => p.Visibility);
            Ignore(p => p.IsSelected);
            Ignore(p => p.ActiveIndicator);
            Ignore(p => p.IsEnabled);
        }


    }
}