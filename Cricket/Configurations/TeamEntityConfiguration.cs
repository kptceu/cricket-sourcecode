﻿using Cricket.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cricket.Configurations
{
    public class TeamEntityConfiguration : EntityTypeConfiguration<TeamEntity>
    {
        public TeamEntityConfiguration()
        {
            ToTable("Teams");
            //primary key
            HasKey(p => p.TeamId);

            HasRequired(p => p.CZone).WithMany().HasForeignKey(p => p.ZoneId).WillCascadeOnDelete(false);


            Ignore(p => p.IsDirty);
            Ignore(p => p.IsNew);
            Ignore(p => p.Visibility);
            Ignore(p => p.IsSelected);
            Ignore(p => p.ActiveIndicator);
            Ignore(p => p.IsEnabled);
        }


    }
}