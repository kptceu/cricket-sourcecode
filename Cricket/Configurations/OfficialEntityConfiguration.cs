﻿using Cricket.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cricket.Configurations
{
    public class OfficialEntityConfiguration : EntityTypeConfiguration<OfficialEntity>
    {
        public OfficialEntityConfiguration()
        {
            ToTable("Officials");
            //primary key
            HasKey(p => p.OfficialId);

            HasRequired(p => p.CZone).WithMany().WillCascadeOnDelete(false);


            Ignore(p => p.IsDirty);
            Ignore(p => p.IsNew);
            Ignore(p => p.Visibility);
            Ignore(p => p.IsSelected);
            Ignore(p => p.ActiveIndicator);
            Ignore(p => p.IsEnabled);
            Ignore(p => p.IsSelectable);
            Ignore(p => p.AllowedColor);
        }


    }
}