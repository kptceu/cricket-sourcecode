﻿using Cricket.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cricket.Configurations
{
    public class LoadDivisionEntityConfigurations : EntityTypeConfiguration<LoadDivisionEntity>
    {
        public LoadDivisionEntityConfigurations()
        {
            ToTable("LoadDivisions");
            //primary key
            HasKey(p => p.LoadDivisionId);

            HasRequired(p => p.Season).WithMany().HasForeignKey(p => p.SeasonId).WillCascadeOnDelete(false);
            HasRequired(p => p.Division).WithMany().HasForeignKey(p => p.DivisionId).WillCascadeOnDelete(false);
            HasRequired(p => p.Team).WithMany().HasForeignKey(p => p.TeamId).WillCascadeOnDelete(false);
            HasRequired(p => p.Zone).WithMany().HasForeignKey(p => p.ZoneId).WillCascadeOnDelete(false);
            HasRequired(p => p.Group).WithMany().HasForeignKey(p => p.GroupId).WillCascadeOnDelete(false);


            Ignore(p => p.IsDirty);
            Ignore(p => p.IsNew);
            Ignore(p => p.Visibility);
            Ignore(p => p.IsSelected);
            Ignore(p => p.ActiveIndicator);
            Ignore(p => p.IsEnabled);
        }


    }
}
