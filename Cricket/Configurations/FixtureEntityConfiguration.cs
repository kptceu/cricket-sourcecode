﻿using Cricket.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cricket.Configurations
{
    public class FixtureEntityConfiguration : EntityTypeConfiguration<FixtureEntity>
    {
        public FixtureEntityConfiguration()
        {
            ToTable("Fixtures");
            //primary key
            HasKey(p => p.FixtureId);

            HasRequired(p => p.Division).WithMany().HasForeignKey(p => p.DivisionId).WillCascadeOnDelete(false);
            //HasRequired(p => p.TeamOne).WithMany().HasForeignKey(p => p.TeamOneId).WillCascadeOnDelete(false);
            //HasRequired(p => p.TeamTwo).WithMany().HasForeignKey(p => p.TeamTwoId).WillCascadeOnDelete(false);
            HasRequired(p => p.Zone).WithMany().HasForeignKey(p => p.ZoneId).WillCascadeOnDelete(false);
            //HasRequired(p => p.Group).WithMany().HasForeignKey(p => p.GroupId).WillCascadeOnDelete(false);


            Ignore(p => p.IsDirty);
            Ignore(p => p.IsNew);
            Ignore(p => p.Visibility);
            Ignore(p => p.IsSelected);
            Ignore(p => p.ActiveIndicator);
            Ignore(p => p.IsEnabled);
        }


    }
}
