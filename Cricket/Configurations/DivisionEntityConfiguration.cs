﻿using Cricket.Model;
using System.Data.Entity.ModelConfiguration;

namespace Cricket.Configurations
{
    public class DivisionEntityConfiguration : EntityTypeConfiguration<DivisionEntity>
    {
       public DivisionEntityConfiguration()
        {
            ToTable("Divisions");
            //primary key
            HasKey(p => p.DivisionId);

            Ignore(p => p.IsDirty);
            Ignore(p => p.IsNew);
            Ignore(p => p.Visibility); 
           Ignore(p => p.IsSelected);
           Ignore(p => p.ActiveIndicator);
           Ignore(p => p.IsEnabled);
        }  
    

    }
}
