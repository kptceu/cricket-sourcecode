﻿using Cricket.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cricket.Configurations
{
   public class PlayedMatchDetailEntityConfiguration : EntityTypeConfiguration<PlayedMatchDetailEntity>
    {
        public PlayedMatchDetailEntityConfiguration()
        {
            ToTable("PlayedMatchDetails");
            //primary key
            HasKey(p => p.PlayedMatchDetailId);

            HasRequired(p => p.Fixture).WithMany().HasForeignKey(p => p.FixtureId).WillCascadeOnDelete(false);
            HasRequired(p => p.Team).WithMany().HasForeignKey(p => p.TeamId).WillCascadeOnDelete(false);
            HasRequired(p => p.Player).WithMany().HasForeignKey(p => p.PlayerId).WillCascadeOnDelete(false);




            Ignore(p => p.IsDirty);
            Ignore(p => p.IsNew);
            Ignore(p => p.Visibility);
            Ignore(p => p.IsSelected);
            Ignore(p => p.ActiveIndicator);
            Ignore(p => p.IsEnabled);
        }


    }
}