﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cricket.Reports
{
    public partial class FilterFixturesReport : Form
    {
        public FilterFixturesReport()
        {
            InitializeComponent();
        }

        private void FilterFixturesReport_Load(object sender, EventArgs e)
        {
            this.reportViewer1.RefreshReport();
        }

        private void initializecomponent()
        {
            this.SuspendLayout();
            this.WindowState = FormWindowState.Maximized;
        }

        public ReportViewer GetReportViewer()
        {
            return this.reportViewer1;
        }

    }
}
