﻿using Cricket.DAL;
using Cricket.Model;
using Cricket.ViewModel;
using Cricket.Views;
using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cricket
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : ModernWindow
    {
        public MainWindow()
        {
            InitializeComponent();

            User.LoggedInUser = Database.GetEntity<UserEntity>(Guid.Parse("315650AB-19BC-4DDC-9334-6B2A0809AF00"));
            
            User.PlayerImagePath = @"D:\Cricket\PlayerImages\";
            HomeViewModel objHomeVM = new HomeViewModel();
            this.DataContext = objHomeVM;

            Typeface tyface = new Typeface(
                   new FontFamily(),
                   FontStyles.Italic,
                   FontWeights.Bold,
                   FontStretches.Normal);

            FormattedText fortxt = new FormattedText("∞", CultureInfo.GetCultureInfo("en-us"), FlowDirection.LeftToRight, tyface, 16, Brushes.Blue);
            Geometry gm = fortxt.BuildGeometry(new Point(0, 0));
            this.LogoData = gm;
        }

        private void btnseasons_Click(object sender, RoutedEventArgs e)
        {
            NewSeason page = new NewSeason();
            FrameContainer.Content = page;
        }

        public bool Validate()
        {
            if (cbxSeason.SelectedIndex == -1)
            {
                MessageBox.Show("Select Season");
                return false;
            }
            else if (cbxzone.SelectedIndex == -1)
            {
                MessageBox.Show("Select Zone");
                return false;
            }
            else if (cbxdivision.SelectedIndex == -1)
            {
                MessageBox.Show("Select Division");
                return false;
            }
            else
                return true;
        }

        private void btnzones_Click(object sender, RoutedEventArgs e)
        {
            NewZone page = new NewZone();
            FrameContainer.Content = page;
        }

        private void btndivisions_Click(object sender, RoutedEventArgs e)
        {
            NewDivision page = new NewDivision();
            FrameContainer.Content = page;
        }

        private void btnofficials_Click(object sender, RoutedEventArgs e)
        {
            if(Validate()==true)
            {
                NewOfficial page = new NewOfficial();
                FrameContainer.Content = page;
            }
        }

        private void btngrounds_Click(object sender, RoutedEventArgs e)
        {
            if (Validate() == true)
            {
                NewLocation page = new NewLocation();
                FrameContainer.Content = page;
            }
        }

        private void btnteams_Click(object sender, RoutedEventArgs e)
        {
            if (Validate() == true)
            {
                NewTeam page = new NewTeam();
                FrameContainer.Content = page;
            }
        }

        private void btnplayers_Click(object sender, RoutedEventArgs e)
        {
            if (Validate() == true)
            {
                NewPlayer page = new NewPlayer();
                FrameContainer.Content = page;
            }
        }

        private void cbxSeason_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbxSeason.SelectedIndex != -1)
            {
                User.Season = (SeasonEntity)cbxSeason.SelectedItem;
                FrameContainer.Content = null;
            }
        }

        private void cbxzone_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbxzone.SelectedIndex != -1)
            {
                User.Zone = (ZoneEntity)cbxzone.SelectedItem;
                FrameContainer.Content = null;
            }
        }

        private void cbxdivision_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbxdivision.SelectedIndex != -1)
            {
                User.Division = (DivisionEntity)cbxdivision.SelectedItem;
                FrameContainer.Content = null;
            }
        }

        private void btngroups_Click(object sender, RoutedEventArgs e)
        {
            if (Validate() == true)
            {
                Groups page = new Groups();
                FrameContainer.Content = page;
            }
        }

        private void btnfixtures_Click(object sender, RoutedEventArgs e)
        {
            if (Validate() == true)
            {
                ViewFixtures page = new ViewFixtures();
                FrameContainer.Content = page;
            }
        }

        private void btnpointstable_Click(object sender, RoutedEventArgs e)
        {
            if(Validate()==true)
            {
                PointsTable page = new PointsTable();
                FrameContainer.Content = page;
            }
        }

        private void btnofficialstats_Click(object sender, RoutedEventArgs e)
        {
            if (Validate() == true)
            {
                OfficialStats page = new OfficialStats();
                FrameContainer.Content = page;
            }
        }

        private void btngroundstats_Click(object sender, RoutedEventArgs e)
        {
            if (cbxzone.SelectedIndex == -1)
                MessageBox.Show("Select Zone");
            else if (cbxSeason.SelectedIndex == -1)
                MessageBox.Show("Select Season");
            else
            {
                LocationStats page = new LocationStats();
                FrameContainer.Content = page;
            }
        }

        private void btnTeamstats_Click(object sender, RoutedEventArgs e)
        {
            if (cbxzone.SelectedIndex == -1)
                MessageBox.Show("Select Zone");
            else if (cbxSeason.SelectedIndex == -1)
                MessageBox.Show("Select Season");
            else { 
            TeamStats page = new TeamStats();
                FrameContainer.Content = page;
            }
        }

        private void btnplayerstats_Click(object sender, RoutedEventArgs e)
        {
            if (Validate() == true)
            {
                PlayerStats page = new PlayerStats();
                FrameContainer.Content = page;
            }
        }

        private void btnfilterfixtures_Click(object sender, RoutedEventArgs e)
        {
            if (cbxzone.SelectedIndex == -1)
                MessageBox.Show("Select Zone");
            else if (cbxSeason.SelectedIndex == -1)
                MessageBox.Show("Select Season");
            else
            {
                FilteredFixtures page = new FilteredFixtures();
                FrameContainer.Content = page;
            }
        }
    }
}
