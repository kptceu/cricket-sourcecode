﻿using Cricket.DAL;
using Cricket.Model;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cricket.ViewModel
{
    public class LocationStatsVM : ViewModelBase
    {
        public LocationStatsVM()
        {
            LstLocation = User.LoadLocations();
            LstDivision = User.LoadDivision();
            DivisionEntity obj = new DivisionEntity();
            obj.DivisionName = "ALL";
            LstDivision.Insert(0, obj);
        }


        private ObservableCollection<DivisionEntity> _LstDivision = new ObservableCollection<DivisionEntity>();
        public ObservableCollection<DivisionEntity> LstDivision
        {
            get { return _LstDivision; }
            set { _LstDivision = value; InvokePropertyChanged("LstDivision"); }
        }

        private ObservableCollection<LocationEntity> _LstLocation = new ObservableCollection<LocationEntity>();
        public ObservableCollection<LocationEntity> LstLocation
        {
            get { return _LstLocation; }
            set { _LstLocation = value;InvokePropertyChanged("LstLocation"); }
        }

        private ObservableCollection<Location> _LstStats = new ObservableCollection<Location>();
        public ObservableCollection<Location> LstStats
        {
            get { return _LstStats; }
            set { _LstStats = value; InvokePropertyChanged("LstStats"); }
        }

        private LocStat _ObjLocStat = new LocStat();
        public LocStat ObjLocStat
        {
            get { return _ObjLocStat; }
            set { _ObjLocStat = value; InvokePropertyChanged("ObjLocStat"); }
        }

        public void LoadLocationStats(Guid LocationId)
        {
            LstStats = new ObservableCollection<Location>();
               FixtureEntityManager ObjFixEm = new FixtureEntityManager();
            PlayedMatchEntityManager objPlayedEm = new PlayedMatchEntityManager();
            ObservableCollection<FixtureEntity> LstFixtures = ObjFixEm.getListByLocationId(Database.getDBContext(), LocationId);
            LstFixtures.ToList().ForEach(x =>
            {
            Location obj = new Location();
            PlayedMatchEntity objplayed = objPlayedEm.GetByFixtureId(Database.getDBContext(), x.FixtureId);
                if (objplayed != null && objplayed.CompleteStatus==true)
                {
                    if (x.Division.NumberOfDays == 1)
                        obj.Date = x.FromDate.Value.ToShortDateString();
                    else
                        obj.Date = x.FromDate.Value.ToShortDateString() + " and " + x.ToDate.Value.ToShortDateString();

                    obj.fromdate = x.FromDate.Value;
                    if(x.ToDate!=null)
                    obj.todate = x.ToDate.Value;

                    obj.Division = x.Division.DivisionName;
                    obj.Group = x.GroupName;
                    obj.Teams = x.Teams;
                    obj.NumberOfDays = x.Division.NumberOfDays;
                    if (x.TeamOneId == objplayed.Result)
                        obj.Result = " Won By " + x.TeamOne.TeamName;
                    else
                        obj.Result = "Won By " + x.TeamTwo.TeamName;
                    obj.Remarks = objplayed.ResultRemarks;
                    obj.MatchId = x.MatchKeyId;
                    LstStats.Add(obj);
                }
            });

            ObjLocStat.Matches = LstStats.Count;
            ObjLocStat.NumberOfDays = LstStats.Select(p => p.NumberOfDays).Sum();
        }



        public void FilterStats(DateTime datfrom, DateTime datto)
        {
            LstStats.ToList().ForEach(x =>
            {
                if (x.todate != null)
                {
                    if (x.fromdate >= datfrom && x.todate <= datto)
                        x.Visibility = System.Windows.Visibility.Visible;
                    else
                        x.Visibility = System.Windows.Visibility.Collapsed;

                }
                else
                {
                    if (x.fromdate >= datfrom && x.fromdate <= datfrom)
                        x.Visibility = System.Windows.Visibility.Visible;
                    else
                        x.Visibility = System.Windows.Visibility.Collapsed;

                }
            });
            ObjLocStat.Matches = LstStats.Where(p => p.Visibility == System.Windows.Visibility.Visible).Count();
            ObjLocStat.NumberOfDays = LstStats.Where(p => p.Visibility == System.Windows.Visibility.Visible).Select(p => p.NumberOfDays).Sum();

        }

        public void FilterStats(DateTime datfrom, DateTime datto, DivisionEntity objdiv)
        {
            if (objdiv.DivisionName != "ALL")
            {
                LstStats.ToList().ForEach(x =>
                {
                    if (objdiv.DivisionName.ToUpper() == objdiv.DivisionName.ToUpper())
                    {
                        if (objdiv.NumberOfDays == 1)
                        {
                            if (x.Division.ToUpper() == objdiv.DivisionName.ToUpper() && (x.fromdate >= datfrom && x.fromdate <= datfrom))
                                x.Visibility = System.Windows.Visibility.Visible;
                            else
                                x.Visibility = System.Windows.Visibility.Collapsed;

                        }
                        else
                        {
                            if (x.Division.ToUpper() == objdiv.DivisionName.ToUpper() && (x.fromdate >= datfrom && x.todate <= datto))
                                x.Visibility = System.Windows.Visibility.Visible;
                            else
                                x.Visibility = System.Windows.Visibility.Collapsed;

                        }
                    }
                    else
                        x.Visibility = System.Windows.Visibility.Collapsed;
                });

             
            }
            else
            {
                LstStats.ToList().ForEach(x =>
                {
                    if (objdiv.NumberOfDays == 1)
                    {
                        if (x.fromdate >= datfrom && x.fromdate <= datfrom)
                            x.Visibility = System.Windows.Visibility.Visible;
                        else
                            x.Visibility = System.Windows.Visibility.Collapsed;

                    }
                    else
                    {
                        if (x.fromdate >= datfrom && x.todate <= datto)
                            x.Visibility = System.Windows.Visibility.Visible;
                        else
                            x.Visibility = System.Windows.Visibility.Collapsed;

                    }

                });

             
            }

            ObjLocStat.Matches = LstStats.Where(p => p.Visibility == System.Windows.Visibility.Visible).Count();
            ObjLocStat.NumberOfDays = LstStats.Where(p => p.Visibility == System.Windows.Visibility.Visible).Select(p => p.NumberOfDays).Sum();

        }


        public void FilterStats(DivisionEntity objdiv)
        {
            if (objdiv.DivisionName != "ALL")
            {
                LstStats.ToList().ForEach(x =>
                {
                    if (x.Division.ToUpper() == objdiv.DivisionName.ToUpper())
                        x.Visibility = System.Windows.Visibility.Visible;
                    else
                        x.Visibility = System.Windows.Visibility.Collapsed;

                });
            }
            else
            {
                LstStats.ToList().ForEach(x => x.Visibility = System.Windows.Visibility.Visible);
            }

            ObjLocStat.Matches = LstStats.Where(p => p.Visibility == System.Windows.Visibility.Visible).Count();
            ObjLocStat.NumberOfDays = LstStats.Where(p => p.Visibility == System.Windows.Visibility.Visible).Select(p => p.NumberOfDays).Sum();
        }

        public void GenerateExcel(LocationEntity ObjLoc)
        {
            ExcelPackage pck = new ExcelPackage();
            pck.Workbook.Properties.Title = "Location Stats";
            pck.Workbook.Properties.Author = "Infinite Strings";
            pck.Workbook.Properties.Company = "Infinite Strings";

            var ws = pck.Workbook.Worksheets.Add("Location Stats");
            ws.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            ws.Cells[1, 1].Value = "Location Id";
            ws.Cells[1, 1].Style.Font.Bold = true;

            ws.Cells[1, 2].Value = ObjLoc.GroundKeyId;
            ws.Cells[1, 2].Style.Font.Bold = true;

            ws.Cells[2, 1].Value = "Stadium Name";
            ws.Cells[2, 1].Style.Font.Bold = true;

            ws.Cells[2, 2].Value = ObjLoc.StadiumName;
            ws.Cells[2, 2].Style.Font.Bold = true;


            ws.Cells[3, 1].Value = "Total Matches";
            ws.Cells[3, 1].Style.Font.Bold = true;

            ws.Cells[3, 2].Value = ObjLocStat.Matches;
            ws.Cells[3, 2].Style.Font.Bold = true;

            ws.Cells[4, 1].Value = "Total No. Of Days";
            ws.Cells[4, 1].Style.Font.Bold = true;

            ws.Cells[4, 2].Value = ObjLocStat.NumberOfDays;
            ws.Cells[4, 2].Style.Font.Bold = true;



            ws.Cells[6, 1].Value = "MatchId";
            ws.Cells[6, 1].Style.Font.Bold = true;
            ws.Cells[6, 2].Value = "Date";
            ws.Cells[6, 2].Style.Font.Bold = true;
            ws.Cells[6, 3].Value = "Group";
            ws.Cells[6, 3].Style.Font.Bold = true;
            ws.Cells[6, 4].Value = "NumberOfDays";
            ws.Cells[6, 4].Style.Font.Bold = true;
            ws.Cells[6, 5].Value = "Teams";
            ws.Cells[6, 5].Style.Font.Bold = true;
            ws.Cells[6, 6].Value = "Result";
            ws.Cells[6, 6].Style.Font.Bold = true;
            ws.Cells[6, 7].Value = "Remarks";
            ws.Cells[6, 7].Style.Font.Bold = true;

            //Column Value Section
            int i = 0;
            LstStats.Where(p=>p.Visibility==System.Windows.Visibility.Visible).ToList().ForEach(x =>
            {
                ws.Cells[7 + i, 1].Value = x.MatchId;
                ws.Cells[7 + i, 2].Value = x.Date;
                ws.Cells[7 + i, 3].Value = x.Group;
                ws.Cells[7 + i, 4].Value = x.NumberOfDays;
                ws.Cells[7 + i, 5].Value = x.Teams;
                ws.Cells[7 + i, 6].Value = x.Result;
                ws.Cells[7 + i, 7].Value = x.Remarks;
                i++;
            });

            byte[] fileText = pck.GetAsByteArray();

            System.Windows.Forms.SaveFileDialog dialog = new System.Windows.Forms.SaveFileDialog()
            {
                Filter = "Excel Worksheets (*.xlsx)|*.xlsx"
            };

            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                File.WriteAllBytes(dialog.FileName, fileText);
                throw new Exception("Exported To " + dialog.FileName);
            }
        }

    }

    public class Location : ViewModelBase
    {
        public DateTime fromdate { get; set; }
        public DateTime? todate { get; set; }

        public string Date { get; set; }
        public string Division { get; set; }
        public string Group { get; set; }
        public string Teams { get; set; }
        public string Result { get; set; }
        public string Remarks { get; set; }
        public int NumberOfDays { get; set; }
        public string MatchId { get; set; }
        private System.Windows.Visibility _visibility = System.Windows.Visibility.Visible;
        public System.Windows.Visibility Visibility
        {
            get { return _visibility; }
            set { _visibility = value; InvokePropertyChanged("Visibility"); }
        }
    }

    public class LocStat:ViewModelBase
    {
        private int _matches = 0;
        public int Matches { get { return _matches; }set { _matches = value; InvokePropertyChanged("Matches"); } }
        private int _numberofdays = 0;
        public int NumberOfDays { get { return _numberofdays; } set { _numberofdays = value; InvokePropertyChanged("NumberOfDays"); } }
    }
}
