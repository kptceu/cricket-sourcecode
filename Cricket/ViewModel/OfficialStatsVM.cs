﻿using Cricket.DAL;
using Cricket.Model;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cricket.ViewModel
{
    public class OfficialStatsVM : ViewModelBase
    {
        public OfficialStatsVM()
        {
            LstOfficial = User.LoadOfficials();
            LstDivision = User.LoadDivision();
            DivisionEntity obj = new DivisionEntity();
            obj.DivisionName = "ALL";
            LstDivision.Insert(0, obj);
        }

        private ObservableCollection<DivisionEntity> _LstDivision = new ObservableCollection<DivisionEntity>();
        public ObservableCollection<DivisionEntity> LstDivision
        {
            get { return _LstDivision; }
            set { _LstDivision = value; InvokePropertyChanged("LstDivision"); }
        }


        private ObservableCollection<OfficialEntity> _LstOfficial = new ObservableCollection<OfficialEntity>();
        public ObservableCollection<OfficialEntity> LstOfficial
        {
            get { return _LstOfficial; }
            set { _LstOfficial = value; InvokePropertyChanged("LstOfficial"); }
        }

        private ObservableCollection<Official> _LstStatsUmpire = new ObservableCollection<Official>();
        public ObservableCollection<Official> LstStatsUmpire
        {
            get { return _LstStatsUmpire; }
            set { _LstStatsUmpire = value; InvokePropertyChanged("LstStatsUmpire"); }
        }

        private ObservableCollection<Official> _LstStatsScorer = new ObservableCollection<Official>();
        public ObservableCollection<Official> LstStatsScorer
        {
            get { return _LstStatsScorer; }
            set { _LstStatsScorer = value; InvokePropertyChanged("LstStatsScorer"); }
        }

        private OffStat _ObjOffStatUmpire = new OffStat();
        public OffStat ObjOffStatUmpire
        {
            get { return _ObjOffStatUmpire; }
            set { _ObjOffStatUmpire = value; InvokePropertyChanged("ObjOffStatUmpire"); }
        }

        private OffStat _ObjOffStatScorer = new OffStat();
        public OffStat ObjOffStatScorer
        {
            get { return _ObjOffStatScorer; }
            set { _ObjOffStatScorer = value; InvokePropertyChanged("ObjOffStatScorer"); }
        }

        public void LoadStats(ObservableCollection<Official> Lst, FixtureEntity x, Guid OfficialId)
        {
            Official obj = new Official();
            PlayedMatchEntityManager objPlayedEm = new PlayedMatchEntityManager();
            PlayedMatchEntity objplayed = objPlayedEm.GetByFixtureId(Database.getDBContext(), x.FixtureId);
            if (objplayed != null && objplayed.CompleteStatus == true)
            {
                if (x.Division.NumberOfDays == 1)
                    obj.Date = x.FromDate.Value.ToShortDateString();
                else
                    obj.Date = x.FromDate.Value.ToShortDateString() + " and " + x.ToDate.Value.ToShortDateString();
                obj.Division = x.Division.DivisionName;
                obj.Group = x.GroupName;
                obj.Teams = x.Teams;
                obj.NumberOfDays = x.Division.NumberOfDays;
                if (x.TeamOneId == objplayed.Result)
                    obj.Result = " Won By " + x.TeamOne.TeamName;
                else
                    obj.Result = "Won By " + x.TeamTwo.TeamName;
                obj.Remarks = objplayed.ResultRemarks;
                obj.MatchId = x.MatchKeyId;

                obj.fromdate = x.FromDate.Value;
                if (x.ToDate != null)
                    obj.todate = x.ToDate.Value;

                if (x.UmpireOneId == OfficialId)
                {
                    obj.OtherUmpire1 = x.UmpireTwo.Name;
                    obj.UmpireorScorer = x.Scorer.Name;
                }
                else if (x.UmpireTwoId == OfficialId)
                {
                    obj.OtherUmpire1 = x.UmpireOne.Name;
                    obj.UmpireorScorer = x.Scorer.Name;
                }
                else
                {
                    obj.OtherUmpire1 = x.UmpireOne.Name;
                    obj.UmpireorScorer = x.UmpireTwo.Name;
                }
                obj.Location = x.Location.LocationName;
                Lst.Add(obj);
            }
        }

        public void LoadOfficialStats(Guid OfficialId)
        {
            FixtureEntityManager ObjFixEm = new FixtureEntityManager();
            ObservableCollection<FixtureEntity> LstFixtures = ObjFixEm.getListByOfficialId(Database.getDBContext(), OfficialId);

            ObservableCollection<FixtureEntity> LstUmpire = new ObservableCollection<FixtureEntity>(LstFixtures.Where(p => p.UmpireOneId == OfficialId || p.UmpireTwoId == OfficialId));
            ObservableCollection<FixtureEntity> LstScorer = new ObservableCollection<FixtureEntity>(LstFixtures.Where(p => p.ScorerId == OfficialId));

            LstStatsScorer = new ObservableCollection<Official>();
            LstStatsUmpire = new ObservableCollection<Official>();

            LstUmpire.ToList().ForEach(x =>
            {
                LoadStats(LstStatsUmpire, x, OfficialId);
            });

            LstScorer.ToList().ForEach(x =>
            {
                LoadStats(LstStatsScorer, x, OfficialId);
            });

            ObjOffStatUmpire.Matches = LstStatsUmpire.Count;
            ObjOffStatUmpire.NumberOfDays = LstStatsUmpire.Select(p => p.NumberOfDays).Sum();

            ObjOffStatScorer.Matches = LstStatsScorer.Count;
            ObjOffStatScorer.NumberOfDays = LstStatsScorer.Select(p => p.NumberOfDays).Sum();

        }

        public void ToExcel(ExcelPackage pck, ObservableCollection<Official> Lst, string header, OfficialEntity obj)
        {
            var ws = pck.Workbook.Worksheets.Add(header);
            ws.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            ws.Cells[1, 1].Value = "Official ID";
            ws.Cells[1, 1].Style.Font.Bold = true;

            ws.Cells[1, 2].Value = obj.OfficialKeyId;
            ws.Cells[1, 2].Style.Font.Bold = true;

            ws.Cells[2, 1].Value = "Official Name";
            ws.Cells[2, 1].Style.Font.Bold = true;

            ws.Cells[2, 2].Value = obj.Name;
            ws.Cells[2, 2].Style.Font.Bold = true;

            ws.Cells[3, 1].Value = "MatchId";
            ws.Cells[3, 1].Style.Font.Bold = true;
            ws.Cells[3, 2].Value = "Date";
            ws.Cells[3, 2].Style.Font.Bold = true;
            ws.Cells[3, 3].Value = "Group";
            ws.Cells[3, 3].Style.Font.Bold = true;
            ws.Cells[3, 4].Value = "Teams";
            ws.Cells[3, 4].Style.Font.Bold = true;
            ws.Cells[3, 5].Value = "Location";
            ws.Cells[3, 5].Style.Font.Bold = true;
            ws.Cells[3, 6].Value = "OtherUmpire1";
            ws.Cells[3, 6].Style.Font.Bold = true;
            ws.Cells[3, 7].Value = "UmpireorScorer";
            ws.Cells[3, 7].Style.Font.Bold = true;
            ws.Cells[3, 8].Value = "Result";
            ws.Cells[3, 8].Style.Font.Bold = true;
            ws.Cells[3, 9].Value = "Remarks";
            ws.Cells[3, 9].Style.Font.Bold = true;

            //Column Value Section
            int i = 0;
            Lst.Where(p => p.Visibility == System.Windows.Visibility.Visible).ToList().ForEach(x =>
                {
                    ws.Cells[4 + i, 1].Value = x.MatchId;
                    ws.Cells[4 + i, 2].Value = x.Date;
                    ws.Cells[4 + i, 3].Value = x.Group;
                    ws.Cells[4 + i, 4].Value = x.Teams;
                    ws.Cells[4 + i, 5].Value = x.Location;
                    ws.Cells[4 + i, 6].Value = x.OtherUmpire1;
                    ws.Cells[4 + i, 7].Value = x.UmpireorScorer;
                    ws.Cells[4 + i, 8].Value = x.Result;
                    ws.Cells[4 + i, 9].Value = x.Remarks;
                    i++;
                });
        }

        public void GenerateExcel(OfficialEntity ObjOffi)
        {
            ExcelPackage pck = new ExcelPackage();
            pck.Workbook.Properties.Title = "Official Stats";
            pck.Workbook.Properties.Author = "Infinite Strings";
            pck.Workbook.Properties.Company = "Infinite Strings";

            ToExcel(pck, LstStatsUmpire, "Umpire Stats", ObjOffi);
            ToExcel(pck, LstStatsScorer, "Scorer Stats", ObjOffi);

            byte[] fileText = pck.GetAsByteArray();

            System.Windows.Forms.SaveFileDialog dialog = new System.Windows.Forms.SaveFileDialog()
            {
                Filter = "Excel Worksheets (*.xlsx)|*.xlsx"
            };

            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                File.WriteAllBytes(dialog.FileName, fileText);
                throw new Exception("Exported To " + dialog.FileName);
            }
        }

        public void FilterStats(DateTime datfrom, DateTime datto)
        {
            LstStatsUmpire.ToList().ForEach(x =>
            {
                if (x.todate != null)
                {
                    if (x.fromdate >= datfrom && x.todate <= datto)
                        x.Visibility = System.Windows.Visibility.Visible;
                    else
                        x.Visibility = System.Windows.Visibility.Collapsed;

                }
                else
                {
                    if (x.fromdate >= datfrom && x.fromdate <= datfrom)
                        x.Visibility = System.Windows.Visibility.Visible;
                    else
                        x.Visibility = System.Windows.Visibility.Collapsed;

                }
            });
            LstStatsScorer.ToList().ForEach(x =>
            {
                if (x.todate != null)
                {
                    if (x.fromdate >= datfrom && x.todate <= datto)
                        x.Visibility = System.Windows.Visibility.Visible;
                    else
                        x.Visibility = System.Windows.Visibility.Collapsed;

                }
                else
                {
                    if (x.fromdate >= datfrom && x.todate <= datfrom)
                        x.Visibility = System.Windows.Visibility.Visible;
                    else
                        x.Visibility = System.Windows.Visibility.Collapsed;
                }
            });


            ObjOffStatUmpire.Matches = LstStatsUmpire.Where(p => p.Visibility == System.Windows.Visibility.Visible).Count();
            ObjOffStatUmpire.NumberOfDays = LstStatsUmpire.Where(p => p.Visibility == System.Windows.Visibility.Visible).Select(p => p.NumberOfDays).Sum();

            ObjOffStatScorer.Matches = LstStatsScorer.Where(p => p.Visibility == System.Windows.Visibility.Visible).Count();
            ObjOffStatScorer.NumberOfDays = LstStatsScorer.Where(p => p.Visibility == System.Windows.Visibility.Visible).Select(p => p.NumberOfDays).Sum();

        }

        public void FilterStats(DateTime datfrom, DateTime datto, DivisionEntity objdiv)
        {
            if (objdiv.DivisionName != "ALL")
            {
                LstStatsUmpire.ToList().ForEach(x =>
                {
                    if (objdiv.DivisionName.ToUpper() == objdiv.DivisionName.ToUpper())
                    {
                        if (objdiv.NumberOfDays == 1)
                        {
                            if (x.Division.ToUpper() == objdiv.DivisionName.ToUpper() && (x.fromdate >= datfrom && x.fromdate <= datfrom))
                                x.Visibility = System.Windows.Visibility.Visible;
                            else
                                x.Visibility = System.Windows.Visibility.Collapsed;

                        }
                        else
                        {
                            if (x.Division.ToUpper() == objdiv.DivisionName.ToUpper() && (x.fromdate >= datfrom && x.todate <= datto))
                                x.Visibility = System.Windows.Visibility.Visible;
                            else
                                x.Visibility = System.Windows.Visibility.Collapsed;

                        }
                    }
                    else
                        x.Visibility = System.Windows.Visibility.Collapsed;
                });

                LstStatsScorer.ToList().ForEach(x =>
                {
                    if (objdiv.DivisionName.ToUpper() == objdiv.DivisionName.ToUpper())
                    {
                        if (objdiv.NumberOfDays == 1)
                        {
                            if (x.Division.ToUpper() == objdiv.DivisionName.ToUpper() && (x.fromdate >= datfrom && x.fromdate <= datfrom))
                                x.Visibility = System.Windows.Visibility.Visible;
                            else
                                x.Visibility = System.Windows.Visibility.Collapsed;

                        }
                        else
                        {
                            if (x.Division.ToUpper() == objdiv.DivisionName.ToUpper() && (x.fromdate >= datfrom && x.todate <= datto))
                                x.Visibility = System.Windows.Visibility.Visible;
                            else
                                x.Visibility = System.Windows.Visibility.Collapsed;

                        }
                    }
                    else
                        x.Visibility = System.Windows.Visibility.Collapsed;
                });
            }
            else
            {
                LstStatsUmpire.ToList().ForEach(x =>
                {
                        if (objdiv.NumberOfDays == 1)
                        {
                            if (x.fromdate >= datfrom && x.fromdate <= datfrom)
                                x.Visibility = System.Windows.Visibility.Visible;
                            else
                                x.Visibility = System.Windows.Visibility.Collapsed;

                        }
                        else
                        {
                            if (x.fromdate >= datfrom && x.todate <= datto)
                                x.Visibility = System.Windows.Visibility.Visible;
                            else
                                x.Visibility = System.Windows.Visibility.Collapsed;

                        }
                    
                });

                LstStatsScorer.ToList().ForEach(x =>
                {
                        if (objdiv.NumberOfDays == 1)
                        {
                            if (x.fromdate >= datfrom && x.fromdate <= datfrom)
                                x.Visibility = System.Windows.Visibility.Visible;
                            else
                                x.Visibility = System.Windows.Visibility.Collapsed;

                        }
                        else
                        {
                            if (x.fromdate >= datfrom && x.todate <= datto)
                                x.Visibility = System.Windows.Visibility.Visible;
                            else
                                x.Visibility = System.Windows.Visibility.Collapsed;

                        }
                });
            }

            ObjOffStatUmpire.Matches = LstStatsUmpire.Where(p => p.Visibility == System.Windows.Visibility.Visible).Count();
            ObjOffStatUmpire.NumberOfDays = LstStatsUmpire.Where(p => p.Visibility == System.Windows.Visibility.Visible).Select(p => p.NumberOfDays).Sum();

            ObjOffStatScorer.Matches = LstStatsScorer.Where(p => p.Visibility == System.Windows.Visibility.Visible).Count();
            ObjOffStatScorer.NumberOfDays = LstStatsScorer.Where(p => p.Visibility == System.Windows.Visibility.Visible).Select(p => p.NumberOfDays).Sum();

        }


        public void FilterStats(DivisionEntity objdiv)
        {
            if (objdiv.DivisionName != "ALL")
            {
                LstStatsUmpire.ToList().ForEach(x =>
                {
                    if (x.Division.ToUpper() == objdiv.DivisionName.ToUpper())
                        x.Visibility = System.Windows.Visibility.Visible;
                    else
                        x.Visibility = System.Windows.Visibility.Collapsed;

                });


                LstStatsScorer.ToList().ForEach(x =>
                {
                    if (x.Division.ToUpper() == objdiv.DivisionName.ToUpper())
                        x.Visibility = System.Windows.Visibility.Visible;
                    else
                        x.Visibility = System.Windows.Visibility.Collapsed;


                });
            }
            else
            {
                LstStatsUmpire.ToList().ForEach(x => x.Visibility = System.Windows.Visibility.Visible);
                LstStatsScorer.ToList().ForEach(x => x.Visibility = System.Windows.Visibility.Visible);
            }

            ObjOffStatUmpire.Matches = LstStatsUmpire.Where(p => p.Visibility == System.Windows.Visibility.Visible).Count();
            ObjOffStatUmpire.NumberOfDays = LstStatsUmpire.Where(p => p.Visibility == System.Windows.Visibility.Visible).Select(p => p.NumberOfDays).Sum();

            ObjOffStatScorer.Matches = LstStatsScorer.Where(p => p.Visibility == System.Windows.Visibility.Visible).Count();
            ObjOffStatScorer.NumberOfDays = LstStatsScorer.Where(p => p.Visibility == System.Windows.Visibility.Visible).Select(p => p.NumberOfDays).Sum();
        }

    }

    public class Official : ViewModelBase
    {
        public  DateTime fromdate { get; set; }
        public DateTime? todate { get; set; }
        public string Date { get; set; }
        public string Division { get; set; }
        public string Group { get; set; }
        public string Teams { get; set; }
        public string Result { get; set; }
        public string Remarks { get; set; }
        public int NumberOfDays { get; set; }
        public string MatchId { get; set; }
        public string OtherUmpire1 { get; set; }
        public string UmpireorScorer { get; set; }
        public string Location { get; set; }
        private System.Windows.Visibility _visibility = System.Windows.Visibility.Visible;
        public System.Windows.Visibility Visibility
        {
            get { return _visibility; }
            set { _visibility = value;InvokePropertyChanged("Visibility"); }
        }
    }

    public class OffStat : ViewModelBase
    {
        private int _matches = 0;
        public int Matches { get { return _matches; } set { _matches = value; InvokePropertyChanged("Matches"); } }
        private int _numberofdays = 0;
        public int NumberOfDays { get { return _numberofdays; } set { _numberofdays = value; InvokePropertyChanged("NumberOfDays"); } }
    }
}