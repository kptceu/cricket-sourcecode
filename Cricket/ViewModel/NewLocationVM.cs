﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Cricket.Model;
using Cricket.DAL;
using Cricket.ViewModel;




namespace Cricket.ViewModel
{
    public class NewLocationVM : ViewModelBase 
    {
        private ObservableCollection<LocationEntity> _lstlocations = new ObservableCollection<LocationEntity>();
        private ObservableCollection<ZoneEntity> _lstZones = new ObservableCollection<ZoneEntity>();
        private ObservableCollection<LocationEntity> _lstgroundsincurrentzone = new ObservableCollection<LocationEntity>();
        private LocationEntity _currentlocation;
        public NewLocationVM()
        {
            loadLocations();
            //ActiveZones = Database.GetEntityList<ZoneEntity>();
            //CurrentLocation = new LocationEntity();
        }   

        public ObservableCollection <LocationEntity> ActiveLocations
        {
            get { return _lstlocations; }
            set { _lstlocations = value; InvokePropertyChanged("ActiveLocations"); }
        }

        public ObservableCollection<ZoneEntity> ActiveZones
        {
            get { return _lstZones ; }
            set { _lstZones = value; InvokePropertyChanged("ActiveZones"); }
        }

        public LocationEntity CurrentLocation
        {
            get { return _currentlocation; }
            set { _currentlocation  = value; InvokePropertyChanged("CurrentLocation"); }
        }

        public ObservableCollection<LocationEntity > GroundsinCurrentZone
        {
            get { return _lstgroundsincurrentzone; }
            set { _lstgroundsincurrentzone = value; InvokePropertyChanged("GroundsinCurrentZone"); }
        }

        public void loadLocations()
        {
            ActiveLocations = Database.GetEntityList<LocationEntity>();
        }




        public void getGroundsinCurrentZone(ZoneEntity ObjZone)
        {
            LocationEntityManager ObjLocEM = new LocationEntityManager();
            using (var Context = Database.getDBContext())
            {
                GroundsinCurrentZone = ObjLocEM.getGroundsinCurrentZone(Context, ObjZone);
            }
        }
    }
}
