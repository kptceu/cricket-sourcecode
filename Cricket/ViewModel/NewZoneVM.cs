﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using Cricket.Model;
using Cricket.DAL;
namespace Cricket.ViewModel
{
  
    public class NewZoneVM : ViewModelBase 
    {
        private ObservableCollection<ZoneEntity> _zones;
        private ZoneEntity ObjZone;
        public NewZoneVM ()
        {
            CurrentZone  = Database.getNewEntity<ZoneEntity>();            
            LoadZones();
        }

        public void NewZone()
        {
            CurrentZone = Database.getNewEntity<ZoneEntity>();
            ActiveZones.Add(CurrentZone);
        }

        public ObservableCollection<ZoneEntity> ActiveZones
        {
            get { return _zones; }
            set { _zones = value; InvokePropertyChanged("ActiveZones"); }            
        }
        
        public void LoadZones()
        {           
          ActiveZones = Database.GetEntityList<ZoneEntity>(); 
           
            
        }

        public ZoneEntity CurrentZone
        {
            get { return ObjZone; }
            set { ObjZone = value; InvokePropertyChanged("CurrentZone"); }
        }

       

        public void SaveZone()
        {
            
            List<EntityCollection> lsttosave = new List<EntityCollection>();
            Database.NewSaveEntity<ZoneEntity>(CurrentZone,lsttosave);
            using (var dbContext = Database.getDBContext())
            {
                Database.CommitChanges(lsttosave, dbContext);
            }
        }

        public void DeleteZone()
        {
            Database.DeleteEntity<ZoneEntity>(CurrentZone);
        }
    }
}
