﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Cricket.DAL;
using Cricket.Model;


using System.Collections.ObjectModel;




namespace Cricket.ViewModel
{
    public class NewSeasonVM : ViewModelBase
    {
        private SeasonEntity _objseason;
        public SeasonEntity ObjSeason
        {
            get { return _objseason; }
            set { _objseason = value;InvokePropertyChanged("ObjSeason"); }

        }
        private ObservableCollection<SeasonEntity> ObcSeasons = new ObservableCollection<SeasonEntity>();

        public NewSeasonVM()
        {
            LoadSeasons();
            ObjSeason = Database.getNewEntity<SeasonEntity>();
        }

        public void LoadSeasons()
        {           
          Seasons = Database.GetEntityList<SeasonEntity>();            
        }

        public ObservableCollection<SeasonEntity> Seasons
        {
            set { ObcSeasons = value; InvokePropertyChanged("Seasons"); }
            get { return ObcSeasons; }
        }

        public void SaveSeason()
        {
            List<EntityCollection> lsttosave = new List<EntityCollection>();
            Database.NewSaveEntity<SeasonEntity>(ObjSeason, lsttosave);
            using (var dbContext = Database.getDBContext())
                Database.CommitChanges(lsttosave, dbContext);

        }
    }
}
