﻿using Cricket.DAL;
using Cricket.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cricket.ViewModel
{
    public class ScheduleMatchesVM:ViewModelBase
    {
        public ScheduleMatchesVM()
        {
            LstOfficial=User.LoadOfficials();
            ObservableCollection<LoadDivisionEntity> Lstdiv = User.LoadDivisionsWithTeams();
            LstTeams =new ObservableCollection<TeamEntity>(Lstdiv.Select(p => p.Team).OrderBy(p=>p.TeamName));
            LstLocation = User.LoadLocations();
            ObjFixture = User.Fixture;
        }

        public bool checkGround(string grdname)
        {
            LocationEntityManager objlocem = new LocationEntityManager();
            if ((objlocem.CheckGround(Database.getDBContext(), grdname)) == false)
                return true;
            else
                return false;
        }

        public bool checkofficial(string offname)
        {
            OfficialEntityManager objoffem = new OfficialEntityManager();
            if ((objoffem.CheckOfficial(Database.getDBContext(), offname)) == false)
                return true;
            else
                return false;
        }

        private ObservableCollection<TeamEntity> _LstTeams = new ObservableCollection<TeamEntity>();
        public ObservableCollection<TeamEntity> LstTeams
        {
            get { return _LstTeams; }
            set { _LstTeams = value; InvokePropertyChanged("LstTeams"); }
        }

        private ObservableCollection<OfficialEntity> _lstofficial = new ObservableCollection<OfficialEntity>();
        public ObservableCollection<OfficialEntity> LstOfficial
        {
            get { return _lstofficial; }
            set { _lstofficial = value;InvokePropertyChanged("LstOfficial"); }
        }

        private ObservableCollection<LocationEntity> _lstlocation = new ObservableCollection<LocationEntity>();
        public ObservableCollection<LocationEntity> LstLocation
        {
            get { return _lstlocation; }
            set { _lstlocation = value; InvokePropertyChanged("LstLocation"); }
        }

        private FixtureEntity _objfixture = new FixtureEntity();
        public FixtureEntity ObjFixture
        {
            get { return _objfixture; }
            set { _objfixture = value;InvokePropertyChanged("ObjFixture"); }
        }

        private LocationEntity _objlocation = new LocationEntity();
        public LocationEntity ObjLocation
        {
            get { return _objlocation; }
            set { _objlocation = value;InvokePropertyChanged("ObjLocation"); }
        }

        private OfficialEntity _objofficial = new OfficialEntity();
        public OfficialEntity ObjOfficial
        {
            get { return _objofficial; }
            set { _objofficial = value;InvokePropertyChanged("ObjOfficial"); }
        }

        public void Save()
        {

            if (ObjFixture.TeamOne == null)
                throw new Exception("Select Team One");
            else if (ObjFixture.TeamTwo == null)
                throw new Exception("Select Team Two");
            else if (ObjFixture.FromDate == null)
                throw new Exception("Select From Date");
            else if (ObjFixture.Location == null)
                throw new Exception("Select Location");
            else
            {
                List<EntityCollection> lsttosave = new List<EntityCollection>();
                Database.NewSaveEntity<FixtureEntity>(ObjFixture, lsttosave);
                using (var dbContext = Database.getDBContext())
                {
                    Database.CommitChanges(lsttosave, dbContext);
                }
                User.Fixture = Database.GetEntity<FixtureEntity>(ObjFixture.FixtureId);
                ObjFixture = User.Fixture;

                throw new Exception("Fixture Saved With Schedule Information");
            }
        }
    }
}
