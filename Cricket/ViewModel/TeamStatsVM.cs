﻿using Cricket.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.IO;
using Cricket.DAL;

namespace Cricket.ViewModel
{
    public class TeamStatsVM : ViewModelBase
    {

        private ZoneEntity _zone=new ZoneEntity();
        private SeasonEntity _season=new SeasonEntity();
        private DivisionEntity _division = new DivisionEntity();
        private TeamStatsReportVM _objteamstatsreport = new TeamStatsReportVM();
        public TeamStatsVM(ZoneEntity Objzone,SeasonEntity ObjSeason,DivisionEntity ObjDivision)
        {
           
            _zone = Objzone;
            _season = ObjSeason;
            _division = ObjDivision;
            LoadTeams();
                        
            

        }

        public TeamStatsReportVM TeamStatsReport
        {
            get { return _objteamstatsreport; }
            set { _objteamstatsreport = value; InvokePropertyChanged("TeamStatsReport"); }
        }

        private ObservableCollection<TeamMatchStats> _lstplayedmatchstats=new ObservableCollection<TeamMatchStats >();
        public ObservableCollection <TeamMatchStats > PlayedMatchStats
        {
            get { return _lstplayedmatchstats; }
            set { _lstplayedmatchstats = value; InvokePropertyChanged("PlayedMatchStats"); }
        }
            
        public int Groupcount = 0;

        private ObservableCollection<TeamEntity> _lstTeams = new ObservableCollection<TeamEntity>();

        public ObservableCollection<TeamEntity > LstTeams
        {
            get { return _lstTeams; }
            set { _lstTeams = value; InvokePropertyChanged("LstTeams"); }
        }

       
       public void LoadTeams()
        {
            TeamEntityManager ObjTeamEM = new TeamEntityManager();
            using (CricketContext context = Database.getDBContext())
            {
               LstTeams = ObjTeamEM.getTeamsinCurrentZoneSeasonDivision(context, _zone, _season, _division);
            }
        }
       
      


        public bool  GetPlayedMatchStats(TeamEntity ObjTeam, ZoneEntity ObjZone, SeasonEntity ObjSeason, DivisionEntity ObjDivision)
        {
            ObservableCollection<PlayedMatchEntity> LstPlayedMatches = new ObservableCollection<PlayedMatchEntity>();
            using (CricketContext context = Database.getDBContext())
            {
                PlayedMatchEntityManager ObjPMEM = new PlayedMatchEntityManager();
                LstPlayedMatches = ObjPMEM.GetPlayedMatchesbyTeam(context, ObjTeam, ObjZone, ObjSeason, ObjDivision);

                if (LstPlayedMatches.Count > 0)
                {
                    LoadMatchStats(context, LstPlayedMatches, ObjTeam);
                    LoadPoints(context, LstPlayedMatches, ObjTeam);
                    return true;
                }

                else
                {
                    return false;
                }
            }

               
        }


       
        public void LoadMatchStats(CricketContext context, ObservableCollection<PlayedMatchEntity > Lst,TeamEntity ObjTeam)
        {
            int intSLNo = 1;
            ObservableCollection<TeamMatchStats> lstPlayedMatchStats = new ObservableCollection<TeamMatchStats>();
            lstPlayedMatchStats.Clear();
            Lst.ToList().ForEach(x =>
            {

                TeamMatchStats objTeamMatchStats = new TeamMatchStats();

                objTeamMatchStats.SerialNumber = intSLNo;
                objTeamMatchStats.MatchId = x.Fixture.MatchKeyId;
                if (User.Division.NumberOfDays == 2)
                    objTeamMatchStats.MatchDate = x.Fixture.FromDate.Value.ToShortDateString() + " to " + x.Fixture.ToDate.Value.ToShortDateString();
                else if (User.Division.NumberOfDays == 1)
                    objTeamMatchStats.MatchDate = x.Fixture.FromDate.Value.ToShortDateString();

                objTeamMatchStats.Venue = x.Fixture.Location.LocationName;

                objTeamMatchStats.TossWonBy = x.TossWonBy;
                objTeamMatchStats.ElectedTo = x.ElectedTo;
                if (x.TeamOneId == ObjTeam.TeamId)
                {
                    if (string.IsNullOrEmpty(x.TeamTwoFirstInningsOvers)) x.TeamTwoFirstInningsOvers = "0";
                    
                    if (string.IsNullOrEmpty(x.TeamTwoSecondInningsOvers)) x.TeamTwoSecondInningsOvers = "0";
                    if (string.IsNullOrEmpty(x.TeamOneFirstInningsOvers)) x.TeamOneFirstInningsOvers = "0";
                    if (string.IsNullOrEmpty(x.TeamOneSecondInningsOvers)) x.TeamOneSecondInningsOvers = "0";

                    objTeamMatchStats.Opponent = x.TeamTwo.TeamName;
                    objTeamMatchStats.FirtsInningsSummary = (x.TeamOneFirstInningsScore.ToString()) + "//" + (x.TeamOneFirstInningsTotalWickets.ToString()) + " In " + (x.TeamOneFirstInningsOvers.ToString());

                    objTeamMatchStats.SecondInningsSummary = (x.TeamOneSecondInningsScore.ToString() ) + "//" + (x.TeamOneSecondInningsTotalWickets.ToString() ) + " In " + (x.TeamOneSecondInningsOvers.ToString() );

                    objTeamMatchStats.OppFirtsInningsSummary = (x.TeamTwoFirstInningsScore.ToString() ) + "//" + (x.TeamTwoFirstInningsTotalWickets.ToString()) + " In " + (x.TeamTwoFirstInningsOvers.ToString() );

                    objTeamMatchStats.OppSecondInningsSummary = (x.TeamTwoSecondInningsScore.ToString()) + "//" + (x.TeamTwoSecondInningsTotalWickets.ToString() ) + " In " + (x.TeamTwoSecondInningsOvers.ToString());
                    objTeamMatchStats.Points = x.TeamOnePoints.ToString();



                }
                else
                {
                    objTeamMatchStats.Opponent = x.TeamOne.TeamName;
                    if (string.IsNullOrEmpty(x.TeamTwoFirstInningsOvers)) x.TeamTwoFirstInningsOvers = "0";
                    if (string.IsNullOrEmpty(x.TeamTwoSecondInningsOvers)) x.TeamTwoSecondInningsOvers = "0";
                    if (string.IsNullOrEmpty(x.TeamOneFirstInningsOvers)) x.TeamOneFirstInningsOvers = "0";
                    if (string.IsNullOrEmpty(x.TeamOneSecondInningsOvers)) x.TeamOneSecondInningsOvers = "0";

                    objTeamMatchStats.FirtsInningsSummary = (x.TeamTwoFirstInningsScore.ToString()) + "//" + (x.TeamTwoFirstInningsTotalWickets.ToString()) + " In " + (x.TeamTwoFirstInningsOvers.ToString());

                    objTeamMatchStats.SecondInningsSummary = (x.TeamTwoSecondInningsScore.ToString()) + "//" + (x.TeamTwoSecondInningsTotalWickets.ToString()) + " In " + (x.TeamTwoSecondInningsOvers.ToString());


                    objTeamMatchStats.OppFirtsInningsSummary = (x.TeamOneFirstInningsScore.ToString()) + "//" + (x.TeamOneFirstInningsTotalWickets.ToString()) + " In " + (x.TeamOneFirstInningsOvers.ToString());

                    objTeamMatchStats.OppSecondInningsSummary = (x.TeamOneSecondInningsScore.ToString() )+ "//" + (x.TeamOneSecondInningsTotalWickets.ToString()) + " In " + (x.TeamOneSecondInningsOvers.ToString());
                    objTeamMatchStats.Points = x.TeamTwoPoints.ToString();
                }
                if (x.Result == Guid.Empty)
                    objTeamMatchStats.Result = "Drawn";
                else if (ObjTeam.TeamId==x.Result )
                 objTeamMatchStats.Result="Won";
                else
                objTeamMatchStats.Result = "Lost";
                intSLNo += 1;
                lstPlayedMatchStats.Add(objTeamMatchStats);
                });
         
            PlayedMatchStats = lstPlayedMatchStats;
            
        }

        public void LoadPoints(CricketContext context, ObservableCollection<PlayedMatchEntity> Lst,TeamEntity ObjTeam )
        {
            PlayedMatchDetailEntityManager objPMDEM = new PlayedMatchDetailEntityManager();

            TeamStatsReportVM x = new TeamStatsReportVM();
            Lst.ToList().ForEach(obj =>
                {
                    //PlayedMatchEntity obj = LstPlayedMatch.Where(p => p.FixtureId == y.FixtureId).SingleOrDefault();
                    if (obj != null)
                    {
                        if (!(obj.TeamOnePoints == 0 && obj.TeamTwoPoints == 0))
                        {
                            if(obj.TeamOneId==ObjTeam.TeamId)
                            {
                                if (obj.TeamOneFirstInningsScore > 0)
                                    x.InningsPlayed = x.InningsPlayed + 1;
                                if(obj.TeamOneSecondInningsScore >0)
                                    x.InningsPlayed = x.InningsPlayed + 1;
                                x.WicketsLost = x.WicketsLost + obj.TeamOneFirstInningsTotalWickets + obj.TeamOneSecondInningsTotalWickets ;
                                x.WicketsTaken = x.WicketsTaken + obj.TeamTwoFirstInningsTotalWickets + obj.TeamTwoSecondInningsTotalWickets ;
                            }
                            else
                            {
                                if(obj.TeamTwoFirstInningsScore >0)
                                    x.InningsPlayed = x.InningsPlayed + 1;
                                if(obj.TeamTwoSecondInningsScore >0)
                                    x.InningsPlayed = x.InningsPlayed + 1;

                                x.WicketsLost = x.WicketsLost + obj.TeamTwoFirstInningsTotalWickets + obj.TeamTwoSecondInningsTotalWickets;
                                x.WicketsTaken = x.WicketsTaken + obj.TeamOneFirstInningsTotalWickets + obj.TeamOneSecondInningsTotalWickets;
                            }

                            
                            if (obj.Result == Guid.Empty)
                                x.MatchesDrawn = x.MatchesDrawn + 1;
                            else if (obj.Result == ObjTeam.TeamId)
                                x.MatchesWon = x.MatchesWon + 1;
                            else
                                x.MatchesLost = x.MatchesLost + 1;

                            if (string.IsNullOrEmpty(obj.TeamOneFirstInningsOvers))
                                obj.TeamOneFirstInningsOvers = "0";
                            if (string.IsNullOrEmpty(obj.TeamOneSecondInningsOvers))
                                obj.TeamOneSecondInningsOvers = "0";
                            if (string.IsNullOrEmpty(obj.TeamTwoFirstInningsOvers))
                                obj.TeamTwoFirstInningsOvers = "0";
                            if (string.IsNullOrEmpty(obj.TeamTwoSecondInningsOvers))
                                obj.TeamTwoSecondInningsOvers = "0";

                            if (obj.TeamOneId == ObjTeam.TeamId)
                            {
                                if(obj.Fixture.GroupName !="Semi-Final" && obj.Fixture.GroupName!="Final")
                                x.Points = x.Points + obj.TeamOnePoints;
                                x.RunsScored = x.RunsScored + obj.TeamOneFirstInningsScore + obj.TeamOneSecondInningsScore;
                                x.RunsConceded = x.RunsConceded + obj.TeamTwoFirstInningsScore + obj.TeamTwoSecondInningsScore;


                                if (obj.TeamTwoFirstInningsTotalWickets == 10)
                                    obj.TeamTwoFirstInningsOvers = obj.MaximumOvers.ToString();
                                if (obj.TeamTwoSecondInningsTotalWickets == 10)
                                    obj.TeamTwoSecondInningsOvers = obj.MaximumOvers.ToString();

                                if (obj.TeamOneFirstInningsTotalWickets == 10)
                                    obj.TeamOneFirstInningsOvers = obj.MaximumOvers.ToString();
                                if (obj.TeamOneSecondInningsTotalWickets == 10)
                                    obj.TeamOneSecondInningsOvers = obj.MaximumOvers.ToString();

                                x.OversFaced = (Convert.ToInt16(x.OversFaced) + User.GetTotalBalls(Convert.ToDecimal(obj.TeamOneFirstInningsOvers), Convert.ToDecimal(obj.TeamOneSecondInningsOvers))).ToString();
                                x.OversBowled = (Convert.ToInt16(x.OversBowled) + User.GetTotalBalls(Convert.ToDecimal(obj.TeamTwoFirstInningsOvers), Convert.ToDecimal(obj.TeamTwoSecondInningsOvers))).ToString();

                            }
                            else
                            {
                                if (obj.TeamTwoFirstInningsTotalWickets == 10)
                                    obj.TeamTwoFirstInningsOvers = obj.MaximumOvers.ToString();
                                if (obj.TeamTwoSecondInningsTotalWickets == 10)
                                    obj.TeamTwoSecondInningsOvers = obj.MaximumOvers.ToString();

                                if (obj.TeamOneFirstInningsTotalWickets == 10)
                                    obj.TeamOneFirstInningsOvers = obj.MaximumOvers.ToString();
                                if (obj.TeamOneSecondInningsTotalWickets == 10)
                                    obj.TeamOneSecondInningsOvers = obj.MaximumOvers.ToString();
                                if (obj.Fixture.GroupName != "Semi-Final" && obj.Fixture.GroupName != "Final")
                                    x.Points = x.Points + obj.TeamTwoPoints;
                                x.RunsScored = x.RunsScored + obj.TeamTwoFirstInningsScore + obj.TeamTwoSecondInningsScore;
                                x.RunsConceded = x.RunsConceded + obj.TeamOneFirstInningsScore + obj.TeamOneSecondInningsScore;
                                x.OversFaced = (Convert.ToInt16(x.OversFaced) + User.GetTotalBalls(Convert.ToDecimal(obj.TeamTwoFirstInningsOvers), Convert.ToDecimal(obj.TeamTwoSecondInningsOvers))).ToString();

                                x.OversBowled = (Convert.ToInt16(x.OversBowled) + User.GetTotalBalls(Convert.ToDecimal(obj.TeamOneFirstInningsOvers), Convert.ToDecimal(obj.TeamOneSecondInningsOvers))).ToString();


                            }

                            ObservableCollection<PlayedMatchDetailEntity> lstPMD = objPMDEM.GetByFixtureIdAndTeamId(context, obj.FixtureId, ObjTeam.TeamId);
                            //Hundreds
                            x.Hundreds = x.Hundreds + lstPMD.Where(p => p.FirstInningsRunsScored >= 100).Count() + lstPMD.Where(p => p.SecondInningsRunsScored >= 100).Count();
                            //Fifties
                            x.Fifties= x.Fifties + lstPMD.Where(p => p.FirstInningsRunsScored >=50 && p.FirstInningsRunsScored < 100).Count() + lstPMD.Where(p => p.SecondInningsRunsScored >= 50 && p.SecondInningsRunsScored < 100).Count();

                            x.FiveWickets = x.FiveWickets + lstPMD.Where(p => p.FirstInningsWickets >= 5 || p.SecondInningsWickets >= 5).Count();

                           
                        }
                    }
                });
                x.RunRate = (((Convert.ToDouble((x.RunsScored / Convert.ToDouble(x.OversFaced)))) - Convert.ToDouble((x.RunsConceded / Convert.ToDouble(x.OversBowled)))) * 6);
                x.RunRate = Math.Round(x.RunRate, 3);
                x.OversBowled = User.getOvers(Convert.ToInt16(x.OversBowled));
                x.OversFaced = User.getOvers(Convert.ToInt16(x.OversFaced));
            this.TeamStatsReport  = x;
            }
        

        public void LoadPointsTable()
        {
            

           
        }

        public void GenerateExcel(TeamEntity ObjTeam)
        {
            ExcelPackage pck = new ExcelPackage();
            pck.Workbook.Properties.Title = "Team Stats";
            pck.Workbook.Properties.Author = "Infinite Strings";
            pck.Workbook.Properties.Company = "Infinite Strings";

            var ws = pck.Workbook.Worksheets.Add("Team Stats");
            ws.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


            ws.Cells[1, 1].Value = "Team Id";
            ws.Cells[1, 1].Style.Font.Bold = true;

            ws.Cells[1, 2].Value = ObjTeam.TeamKeyId;
            ws.Cells[1, 2].Style.Font.Bold = true;

            ws.Cells[2, 1].Value = "Team Name";
            ws.Cells[2, 1].Style.Font.Bold = true;

            ws.Cells[2, 2].Value = ObjTeam.TeamName;
            ws.Cells[2, 2].Style.Font.Bold = true;

            ws.Cells[3, 1].Value = "Innings Played";
            ws.Cells[3, 1].Style.Font.Bold = true;

            ws.Cells[3, 2].Value = TeamStatsReport.InningsPlayed;
            ws.Cells[3, 2].Style.Font.Bold = true;

            ws.Cells[4, 1].Value = "Matches Won";
            ws.Cells[4, 1].Style.Font.Bold = true;

            ws.Cells[4, 2].Value = TeamStatsReport.MatchesWon;
            ws.Cells[4, 2].Style.Font.Bold = true;

            ws.Cells[5, 1].Value = "Matches Lost";
            ws.Cells[5, 1].Style.Font.Bold = true;

            ws.Cells[5, 2].Value = TeamStatsReport.MatchesLost;
            ws.Cells[5, 2].Style.Font.Bold = true;

            ws.Cells[6, 1].Value = "Matches Drawn";
            ws.Cells[6, 1].Style.Font.Bold = true;

            ws.Cells[6, 2].Value = TeamStatsReport.MatchesDrawn;
            ws.Cells[6, 2].Style.Font.Bold = true;

            ws.Cells[7, 1].Value = "Runs Scored";
            ws.Cells[7, 1].Style.Font.Bold = true;

            ws.Cells[7, 2].Value = TeamStatsReport.RunsScored;
            ws.Cells[7, 2].Style.Font.Bold = true;

            ws.Cells[8, 1].Value = "Runs Conceded";
            ws.Cells[8, 1].Style.Font.Bold = true;

            ws.Cells[8, 2].Value = TeamStatsReport.RunsConceded;
            ws.Cells[8, 2].Style.Font.Bold = true;

            ws.Cells[9, 1].Value = "Wickets Lost";
            ws.Cells[9, 1].Style.Font.Bold = true;

            ws.Cells[9, 2].Value = TeamStatsReport.WicketsLost;
            ws.Cells[9, 2].Style.Font.Bold = true;

            ws.Cells[10, 1].Value = "Wickets Taken";
            ws.Cells[10, 1].Style.Font.Bold = true;

            ws.Cells[10, 2].Value = TeamStatsReport.WicketsTaken;
            ws.Cells[10, 2].Style.Font.Bold = true;

            ws.Cells[11, 1].Value = "100's";
            ws.Cells[11, 1].Style.Font.Bold = true;

            ws.Cells[11, 2].Value = TeamStatsReport.Hundreds;
            ws.Cells[11, 2].Style.Font.Bold = true;

            ws.Cells[12, 1].Value = "50's";
            ws.Cells[12, 1].Style.Font.Bold = true;

            ws.Cells[12, 2].Value = TeamStatsReport.Fifties;
            ws.Cells[12, 2].Style.Font.Bold = true;

            ws.Cells[13, 1].Value = "5 Wickets";
            ws.Cells[13, 1].Style.Font.Bold = true;

            ws.Cells[13, 2].Value = TeamStatsReport.FiveWickets;
            ws.Cells[13, 2].Style.Font.Bold = true;

            ws.Cells[14, 1].Value = "Points";
            ws.Cells[14, 1].Style.Font.Bold = true;

            ws.Cells[14, 2].Value = TeamStatsReport.Points;
            ws.Cells[14, 2].Style.Font.Bold = true;

            ws.Cells[15, 1].Value = "Run Rate";
            ws.Cells[15, 1].Style.Font.Bold = true;

            ws.Cells[15, 2].Value = TeamStatsReport.RunRate;
            ws.Cells[15, 2].Style.Font.Bold = true;










            ws.Cells[17, 1].Value = "MatchId";
            ws.Cells[17, 1].Style.Font.Bold = true;
            ws.Cells[17, 2].Value = "Date";
            ws.Cells[17, 2].Style.Font.Bold = true;
            ws.Cells[17, 3].Value = "Opponent";
            ws.Cells[17, 3].Style.Font.Bold = true;
            ws.Cells[17, 4].Value = "Venue";
            ws.Cells[17, 4].Style.Font.Bold = true;
            ws.Cells[17, 5].Value = "Toss Won By";
            ws.Cells[17, 5].Style.Font.Bold = true;
            ws.Cells[17, 6].Value = "Elected To";
            ws.Cells[17, 6].Style.Font.Bold = true;
            ws.Cells[17, 7].Value = "FirtsInningsSummary";
            ws.Cells[17, 7].Style.Font.Bold = true;
            ws.Cells[17, 8].Value = "SecondInningsSummary";
            ws.Cells[17, 8].Style.Font.Bold = true;
            ws.Cells[17, 9].Value = "OppFirtsInningsSummary";
            ws.Cells[17, 9].Style.Font.Bold = true;
            ws.Cells[17, 10].Value = "OppSecondInningsSummary";
            ws.Cells[17, 10].Style.Font.Bold = true;
            ws.Cells[17, 11].Value = "Points";
            ws.Cells[17, 11].Style.Font.Bold = true;
            ws.Cells[17, 12].Value = "Result";
            ws.Cells[17, 12].Style.Font.Bold = true;
            ws.Cells[17, 13].Value = "Remarks";
            ws.Cells[17, 13].Style.Font.Bold = true;
            //Column Value Section
            int i = 0;
            PlayedMatchStats.ToList().ForEach(x =>
            {
                ws.Cells[18 + i, 1].Value = x.MatchId;
                ws.Cells[18 + i, 2].Value = x.MatchDate;
                ws.Cells[18 + i, 3].Value = x.Opponent;
                ws.Cells[18 + i, 4].Value = x.Venue;
                ws.Cells[18 + i, 5].Value = x.TossWonBy;
                ws.Cells[18 + i, 6].Value = x.ElectedTo;
                ws.Cells[18 + i, 7].Value = x.FirtsInningsSummary;
                ws.Cells[18 + i, 8].Value = x.SecondInningsSummary;
                ws.Cells[18 + i, 9].Value = x.OppFirtsInningsSummary;
                ws.Cells[18 + i, 10].Value = x.OppSecondInningsSummary;
                ws.Cells[18 + i, 11].Value = x.Points;
                ws.Cells[18 + i, 12].Value = x.Result;
                ws.Cells[18 + i, 13].Value = x.Remarks;
                i++;
            });

            byte[] fileText = pck.GetAsByteArray();

            System.Windows.Forms.SaveFileDialog dialog = new System.Windows.Forms.SaveFileDialog()
            {
                Filter = "Excel Worksheets (*.xlsx)|*.xlsx"
            };

            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                File.WriteAllBytes(dialog.FileName, fileText);
                throw new Exception("Exported To " + dialog.FileName);
            }
        }
    }

    public class TeamStatsReportVM : ViewModelBase
    {
    public TeamEntity Team { get; set; }
    public int InningsPlayed { get; set; }
    public int MatchesWon { get; set; }
    public int MatchesLost { get; set; }
    public int MatchesDrawn { get; set; }
    public int Points { get; set; }
    public int RunsScored { get; set; }
    public int RunsConceded { get; set; }
    public string OversFaced { get; set; }
    public string OversBowled { get; set; }
    public double RunRate { get; set; }
    public int Hundreds { get; set; }
        public int Fifties { get; set; }
        public int FiveWickets { get; set; }
        public int WicketsTaken { get; set; }
        public int WicketsLost { get; set; }
    }

    public class TeamMatchStats : ViewModelBase
    {
        public int SerialNumber { get; set; }

        public TeamEntity Team { get; set; }
        
        public string MatchId { get; set; }

        public string MatchDate { get; set; }

        public string Opponent { get; set; }

        public string Venue { get; set; }

        public string TossWonBy { get; set; }

        public string ElectedTo { get; set; }

        public string FirtsInningsSummary { get; set; }

        public string SecondInningsSummary { get; set; }


        public string OppFirtsInningsSummary { get; set; }

        public string OppSecondInningsSummary { get; set; }

        public string Result { get; set; }

        public string Points { get; set; }

        public string Remarks { get; set; }
    }


}
