﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using Cricket.Model;
using Cricket.DAL;


namespace Cricket.ViewModel
{
    public class NewOfficialVM : ViewModelBase 
    {
        private ObservableCollection<ZoneEntity> _zones = new ObservableCollection<ZoneEntity>();
        private ObservableCollection <OfficialEntity> _officials=new ObservableCollection<OfficialEntity>();

       // private ObservableCollection<OfficialEntity> _officialsincurrentzone = new ObservableCollection<OfficialEntity>();

        private OfficialEntity _currentofficial;

        public NewOfficialVM()
        {
            OfficialEntityManager OEM = new OfficialEntityManager();
            //CurrentOfficial = Database.getNewEntity<OfficialEntity>();
            LoadZones();
            LoadOfficials();
            
        }

        public OfficialEntity CurrentOfficial
        {
            get { return _currentofficial; }
            set { _currentofficial = value; InvokePropertyChanged("CurrentOfficial"); }
        }

        public ObservableCollection<ZoneEntity> ActiveZones
        {
            get { return _zones; }
            set { _zones = value; InvokePropertyChanged("ActiveZones"); }
        }

        public ObservableCollection<OfficialEntity> ActiveOfficials
        {
            get { return _officials; }
            set { _officials = value; InvokePropertyChanged("ActiveOfficials"); }
        }



        public void LoadOfficials()
        {
            ActiveOfficials = Database.GetEntityList<OfficialEntity>();
        }

        public void LoadZones()
        {
            ActiveZones = Database.GetEntityList<ZoneEntity>();
        }
    }
}
