﻿using Cricket.DAL;
using Cricket.Model;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cricket.ViewModel
{
    public class PlayerStatsVM : ViewModelBase
    {
        public PlayerStatsVM()
        {
          //  ObservableCollection<LoadDivisionEntity> Lst = User.LoadDivisionsWithTeams();
           // LstTeam = new ObservableCollection<TeamEntity>(Lst.Select(p => p.Team));
           // LstTeam = new ObservableCollection<TeamEntity>(LstTeam.OrderBy(p => p.TeamName));
           // TeamEntity oj = Database.getNewEntity<TeamEntity>();
          //  oj.TeamName = "ALL";
         //   LstTeam.Insert(0,oj);

            LstSeason = User.LoadSeason();
            SeasonEntity obsea = new SeasonEntity();
            obsea.SeasonName = "ALL";
            LstSeason.Insert(0, obsea);
            LoadTeams(User.Season);
            LoadForSeason(User.Season);
            // LoadStatistics();
            //LstBatting = new ObservableCollection<Batting>(LstBatting.OrderByDescending(p => p.TotalRuns).ThenByDescending(p => p.InningsPlayed).ThenByDescending(p => p.Average).ThenByDescending(p => p.Matches).Take(20));
            // LstBowling = new ObservableCollection<Bowling>(LstBowling.OrderByDescending(p => p.TotalWickets).ThenByDescending(p=>p.BallsBowled).ThenByDescending(p => p.Average).ThenByDescending(p=>p.Economy).Take(20));
            // LstAllRounder = new ObservableCollection<AllRounder>(LstAllRounder.OrderByDescending(p => p.ObjBatting.TotalRuns).ThenByDescending(p => p.ObjBowling.TotalWickets).Take(20));
        }

        private ObservableCollection<TeamEntity> _LstTeam = new ObservableCollection<TeamEntity>();
        public ObservableCollection<TeamEntity> LstTeam
        {
            get { return _LstTeam; }
            set { _LstTeam = value; InvokePropertyChanged("LstTeam"); }
        }

        public void LoadTeams(SeasonEntity objsea = null)
        {
            LoadDivisionEntityManager ObjLoadDivEm = new LoadDivisionEntityManager();
            if (objsea != null)
                LstLoadDivision = new ObservableCollection<LoadDivisionEntity>(ObjLoadDivEm.getBySeasonId(Database.getDBContext(), objsea.SeasonId));
            else
                LstLoadDivision = new ObservableCollection<LoadDivisionEntity>(ObjLoadDivEm.getForAllSeasons(Database.getDBContext()));

            LstTeam = new ObservableCollection<TeamEntity>(LstLoadDivision.Select(p => p.Team));
            LstTeam = new ObservableCollection<TeamEntity>(LstTeam.OrderBy(p => p.TeamName));
            TeamEntity oj = Database.getNewEntity<TeamEntity>();
            oj.TeamName = "ALL";
            LstTeam.Insert(0, oj);

        }




        private ObservableCollection<LoadDivisionEntity> _LstLoadDivision = new ObservableCollection<LoadDivisionEntity>();
        public ObservableCollection<LoadDivisionEntity> LstLoadDivision
        {
            get { return _LstLoadDivision; }
            set { _LstLoadDivision = value; InvokePropertyChanged("LstLoadDivision"); }
        }

        public void LoadForAll()
        {
            InitializeLists();
            List<Guid> LstTeamId = LstLoadDivision.Select(p => p.Team.TeamId).Distinct().ToList<Guid>();
            PlayerEntityManager objplayerem = new PlayerEntityManager();
            LstPlayers = objplayerem.getListByTeamList(Database.getDBContext(), LstTeamId);
            LstPlayers.ToList().ForEach(x =>
            {
                PlayedMatchDetailEntityManager objPlayeddetailem = new PlayedMatchDetailEntityManager();
                ObservableCollection<PlayedMatchDetailEntity> LstDetail = objPlayeddetailem.getListByPlayerIdAcrossSeasons(Database.getDBContext(), x.PlayerId);
                if (LstDetail.Count > 0)
                {
                    BattingStatistics(LstBatting, x, LstDetail);
                    BowlingStatistics(LstBowling, x, LstDetail);
                    AllRounderStatistics(x);
                }
            });
            LstBatting = new ObservableCollection<Batting>(LstBatting.OrderByDescending(p => p.TotalRuns).ThenByDescending(p => p.InningsPlayed).ThenByDescending(p => p.Average).ThenByDescending(p => p.Matches).Take(20));
            LstBowling = new ObservableCollection<Bowling>(LstBowling.OrderByDescending(p => p.TotalWickets).ThenByDescending(p => p.BallsBowled).ThenByDescending(p => p.Average).ThenByDescending(p => p.Economy).Take(20));
            LstAllRounder = new ObservableCollection<AllRounder>(LstAllRounder.OrderByDescending(p => p.ObjBatting.TotalRuns).ThenByDescending(p => p.ObjBowling.TotalWickets).Take(20));

        }

        public void LoadForSeason(SeasonEntity objsea)
        {
            InitializeLists();
            List<Guid> LstTeamId = LstLoadDivision.Select(p => p.Team.TeamId).Distinct().ToList<Guid>();
            PlayerEntityManager objplayerem = new PlayerEntityManager();
            LstPlayers = objplayerem.getListByTeamList(Database.getDBContext(), LstTeamId);
            LstPlayers.ToList().ForEach(x =>
            {
                PlayedMatchDetailEntityManager objPlayeddetailem = new PlayedMatchDetailEntityManager();
                ObservableCollection<PlayedMatchDetailEntity> LstDetail = objPlayeddetailem.getListByPlayerIdAndSeasonId(Database.getDBContext(), x.PlayerId,objsea.SeasonId);
                if (LstDetail.Count > 0)
                {
                    BattingStatistics(LstBatting, x, LstDetail);
                    BowlingStatistics(LstBowling, x, LstDetail);
                    AllRounderStatistics(x);
                }
            });

            LstBatting = new ObservableCollection<Batting>(LstBatting.OrderByDescending(p => p.TotalRuns).ThenByDescending(p => p.InningsPlayed).ThenByDescending(p => p.Average).ThenByDescending(p => p.Matches).Take(20));
            LstBowling = new ObservableCollection<Bowling>(LstBowling.OrderByDescending(p => p.TotalWickets).ThenByDescending(p => p.BallsBowled).ThenByDescending(p => p.Average).ThenByDescending(p => p.Economy).Take(20));
            LstAllRounder = new ObservableCollection<AllRounder>(LstAllRounder.OrderByDescending(p => p.ObjBatting.TotalRuns).ThenByDescending(p => p.ObjBowling.TotalWickets).Take(20));

        }

        public void LoadForAllSeasonForTeam(TeamEntity obj)
        {
            InitializeLists();
            List<Guid> LstTeamId = new List<Guid>();
            LstTeamId.Add(obj.TeamId);
            PlayerEntityManager objplayerem = new PlayerEntityManager();
            LstPlayers = objplayerem.getListByTeamList(Database.getDBContext(), LstTeamId);
            LstPlayers.ToList().ForEach(x =>
            {
                PlayedMatchDetailEntityManager objPlayeddetailem = new PlayedMatchDetailEntityManager();
                ObservableCollection<PlayedMatchDetailEntity> LstDetail = objPlayeddetailem.getListByPlayerIdAcrossSeasons(Database.getDBContext(), x.PlayerId);
                if (LstDetail.Count > 0)
                {
                    BattingStatistics(LstBatting, x, LstDetail);
                    BowlingStatistics(LstBowling, x, LstDetail);
                    AllRounderStatistics(x);
                }
            });
            LstBatting = new ObservableCollection<Batting>(LstBatting.OrderByDescending(p => p.TotalRuns).ThenByDescending(p => p.InningsPlayed).ThenByDescending(p => p.Average).ThenByDescending(p => p.Matches).Take(20));
            LstBowling = new ObservableCollection<Bowling>(LstBowling.OrderByDescending(p => p.TotalWickets).ThenByDescending(p => p.BallsBowled).ThenByDescending(p => p.Average).ThenByDescending(p => p.Economy).Take(20));
            LstAllRounder = new ObservableCollection<AllRounder>(LstAllRounder.OrderByDescending(p => p.ObjBatting.TotalRuns).ThenByDescending(p => p.ObjBowling.TotalWickets).Take(20));


        }

        public void LoadForSeasonAndTeam(TeamEntity obj,SeasonEntity obs)
        {
            InitializeLists();
            List<Guid> LstTeamId = new List<Guid>();
            LstTeamId.Add(obj.TeamId);
            PlayerEntityManager objplayerem = new PlayerEntityManager();
            LstPlayers = objplayerem.getListByTeamList(Database.getDBContext(), LstTeamId);
            LstPlayers.ToList().ForEach(x =>
            {
                PlayedMatchDetailEntityManager objPlayeddetailem = new PlayedMatchDetailEntityManager();
                ObservableCollection<PlayedMatchDetailEntity> LstDetail = objPlayeddetailem.getListByPlayerIdAndSeasonId(Database.getDBContext(), x.PlayerId,obs.SeasonId);
                if (LstDetail.Count > 0)
                {
                    BattingStatistics(LstBatting, x, LstDetail);
                    BowlingStatistics(LstBowling, x, LstDetail);
                    AllRounderStatistics(x);
                }
            });
            LstBatting = new ObservableCollection<Batting>(LstBatting.OrderByDescending(p => p.TotalRuns).ThenByDescending(p => p.InningsPlayed).ThenByDescending(p => p.Average).ThenByDescending(p => p.Matches).Take(20));
            LstBowling = new ObservableCollection<Bowling>(LstBowling.OrderByDescending(p => p.TotalWickets).ThenByDescending(p => p.BallsBowled).ThenByDescending(p => p.Average).ThenByDescending(p => p.Economy).Take(20));
            LstAllRounder = new ObservableCollection<AllRounder>(LstAllRounder.OrderByDescending(p => p.ObjBatting.TotalRuns).ThenByDescending(p => p.ObjBowling.TotalWickets).Take(20));

        }

        //public void LoadOnTeam(TeamEntity obj=null)
        //{
        //    if (obj != null)            
        //        LoadStatistics(obj.TeamId);            
        //    else
        //        LoadStatistics();

        //    LstBatting = new ObservableCollection<Batting>(LstBatting.OrderByDescending(p => p.TotalRuns).ThenByDescending(p => p.InningsPlayed).ThenByDescending(p => p.Average).ThenByDescending(p => p.Matches).Take(20));
        //    LstBowling = new ObservableCollection<Bowling>(LstBowling.OrderByDescending(p => p.TotalWickets).ThenByDescending(p => p.BallsBowled).ThenByDescending(p => p.Average).ThenByDescending(p => p.Economy).Take(20));
        //    LstAllRounder = new ObservableCollection<AllRounder>(LstAllRounder.OrderByDescending(p => p.ObjBatting.TotalRuns).ThenByDescending(p => p.ObjBowling.TotalWickets).Take(20));

        //}

        private ObservableCollection<SeasonEntity> _LstSeason = new ObservableCollection<SeasonEntity>();
        public ObservableCollection<SeasonEntity> LstSeason
        {
            get { return _LstSeason; }
            set { _LstSeason = value; InvokePropertyChanged("LstSeason"); }
        }

        private ObservableCollection<PlayerEntity> _LstPlayers = new ObservableCollection<PlayerEntity>();
        public ObservableCollection<PlayerEntity> LstPlayers
        {
            get { return _LstPlayers; }
            set { _LstPlayers = value; InvokePropertyChanged("LstPlayers"); }

        }


        private ObservableCollection<Batting> _LstBatting = new ObservableCollection<Batting>();
        public ObservableCollection<Batting> LstBatting
        {
            get { return _LstBatting; }
            set { _LstBatting = value; InvokePropertyChanged("LstBatting"); }
        }
        private ObservableCollection<Bowling> _LstBowling = new ObservableCollection<Bowling>();
        public ObservableCollection<Bowling> LstBowling
        {
            get { return _LstBowling; }
            set { _LstBowling = value; InvokePropertyChanged("LstBowling"); }
        }

        private ObservableCollection<AllRounder> _LstAllRounder = new ObservableCollection<AllRounder>();
        public ObservableCollection<AllRounder> LstAllRounder
        {
            get { return _LstAllRounder; }
            set { _LstAllRounder = value; InvokePropertyChanged("LstAllRounder"); }
        }

        public void AllRounderStatistics(PlayerEntity x)
        {
            AllRounder obj = new AllRounder();
            obj.ObjBatting = LstBatting.Where(p => p.Player.PlayerId == x.PlayerId).SingleOrDefault();
            obj.ObjBowling = LstBowling.Where(p => p.Player.PlayerId == x.PlayerId).SingleOrDefault();
            if (obj.ObjBatting != null && obj.ObjBowling != null)
            {
                if (obj.ObjBatting.TotalRuns >= 100 && obj.ObjBowling.TotalWickets >= 10)
                    LstAllRounder.Add(obj);
            }
        }

        public void BattingStatistics(ObservableCollection<Batting> Lst, PlayerEntity xp, ObservableCollection<PlayedMatchDetailEntity> LstDetails)
        {
            int runs = LstDetails.Select(p => p.FirstInningsRunsScored).Sum() + LstDetails.Select(p => p.SecondInningsRunsScored).Sum();
            if (runs > 0)
            {
                Batting obj = new Batting();
                obj.Player = xp;
                obj.KSCAUID = xp.KSCAUID;
                obj.TeamId = xp.TeamId;
                obj.TeamName = xp.Team.TeamName;
                obj.HighestScore = Math.Max(LstDetails.Select(p => p.FirstInningsRunsScored).Max(), LstDetails.Select(p => p.SecondInningsRunsScored).Max());
                obj.TotalRuns = LstDetails.Select(p => p.FirstInningsRunsScored).Sum() + LstDetails.Select(p => p.SecondInningsRunsScored).Sum();
                LstDetails.ToList().ForEach(x =>
                {
                    obj.Matches = obj.Matches + 1;
                    if (x.FirstInningsBallsFaced > 0)
                    {
                        obj.InningsPlayed = obj.InningsPlayed + 1;
                        if (x.FirstInningsDismissal == "Not Out")
                            obj.NotOuts = obj.NotOuts + 1;
                        if (x.FirstInningsRunsScored >= 100)
                            obj.Hundreds = obj.Hundreds + 1;
                        if (x.FirstInningsRunsScored >= 50 && x.FirstInningsRunsScored < 100)
                            obj.Fifties = obj.Fifties + 1;
                    }
                    if (x.SecondInningsBallsFaced > 0)
                    {
                        obj.InningsPlayed = obj.InningsPlayed + 1;
                        if (x.SecondInningsDismissal == "Not Out")
                            obj.NotOuts = obj.NotOuts + 1;
                        if (x.SecondInningsRunsScored >= 100)
                            obj.Hundreds = obj.Hundreds + 1;
                        if (x.SecondInningsRunsScored >= 50 && x.SecondInningsRunsScored < 100)
                            obj.Fifties = obj.Fifties + 1;
                    }
                    obj.Fours = obj.Fours + x.FirstInningsFours + x.SecondInningsFours;
                    obj.Sixes = obj.Sixes + x.FirstInningsSixes + x.SecondInningsSixes;
                    obj.BallsFaced = obj.BallsFaced + x.FirstInningsBallsFaced + x.SecondInningsBallsFaced;
                });
                if (obj.InningsPlayed - obj.NotOuts != 0)
                    obj.Average = obj.TotalRuns / (obj.InningsPlayed - obj.NotOuts);
                else
                    obj.Average = obj.TotalRuns;
                obj.StrikeRate = (obj.TotalRuns*100) / obj.BallsFaced;
                Lst.Add(obj);
            }
        }

        public void BowlingStatistics(ObservableCollection<Bowling> Lst, PlayerEntity xp, ObservableCollection<PlayedMatchDetailEntity> LstDetails)
        {
            double overs = LstDetails.Select(p => p.FirstInningsOversBowled).Sum() + LstDetails.Select(p => p.SecondInningsOversBowled).Sum();
            if (overs > 0)
            {
                Bowling obj = new Bowling();
                obj.Player = xp;
                obj.KSCAUID = xp.KSCAUID;
                obj.TeamId = xp.TeamId;
                obj.TeamName = xp.Team.TeamName;
                LstDetails.ToList().ForEach(x =>
                {
                    obj.Matches = obj.Matches + 1;
                    obj.BallsBowled = obj.BallsBowled + User.ConvertToBalls(Convert.ToDecimal(x.FirstInningsOversBowled)) + User.ConvertToBalls(Convert.ToDecimal(x.SecondInningsOversBowled));
                    obj.RunsConceded = obj.RunsConceded + x.FirstInningsRunsGiven + x.SecondInningsRunsGiven;
                    if (x.FirstInningsWickets >= 5)
                        obj.FivePlusWickets = obj.FivePlusWickets + 1;
                    if (x.SecondInningsWickets >= 5)
                        obj.FivePlusWickets = obj.FivePlusWickets + 1;
                    obj.TotalWickets = obj.TotalWickets + x.FirstInningsWickets + x.SecondInningsWickets;
                    obj.Maidens = obj.Maidens + x.FirstInningsMaidens + x.SecondInningsMaidens;
                    obj.Wides = obj.Wides + x.FirstInningsWides + x.SecondInningsWides;
                    obj.NoBalls = obj.NoBalls + x.FirstInningsNoBalls + x.SecondInningsNoBalls;
                });


                if (obj.TotalWickets > 0)
                {
                    double ab = double.Parse(obj.RunsConceded.ToString()) /double.Parse (obj.TotalWickets.ToString());
                    obj.Average = Math.Round(ab, 2);                   
                }
                else
                    obj.Average = 0;

                obj.OversBowled =Convert.ToDouble(User.getOvers(obj.BallsBowled));
                obj.Economy = obj.RunsConceded / obj.OversBowled;
                obj.Economy=Math.Round(obj.Economy, 2);

                Lst.Add(obj);
            }
        }

        public void InitializeLists()
        {
            LstBatting = new ObservableCollection<Batting>();
            LstBowling = new ObservableCollection<Bowling>();
            LstAllRounder = new ObservableCollection<AllRounder>();
        }

        //public void LoadStatistics()
        //{
        //    InitializeLists();
        //    ObservableCollection<LoadDivisionEntity> Lst = User.LoadDivisionsWithTeams();
        //    List<Guid> LstTeamId = Lst.Select(p => p.Team.TeamId).ToList<Guid>();
        //    PlayerEntityManager objplayerem = new PlayerEntityManager();
        //    LstPlayers = objplayerem.getListByTeamList(Database.getDBContext(), LstTeamId);
        //    LstPlayers.ToList().ForEach(x =>
        //    {
        //        PlayedMatchDetailEntityManager objPlayeddetailem = new PlayedMatchDetailEntityManager();
        //        ObservableCollection<PlayedMatchDetailEntity> LstDetail = objPlayeddetailem.getListByPlayerId(Database.getDBContext(), x.PlayerId);
        //        if (LstDetail.Count > 0)
        //        {
        //            BattingStatistics(LstBatting, x, LstDetail);
        //            BowlingStatistics(LstBowling, x, LstDetail);
        //            AllRounderStatistics(x);
        //        }
        //    });
        //}
        //public void LoadStatistics(Guid Id)
        //{
        //    InitializeLists();
        //    List<Guid> LstTeamId = new List<Guid>();
        //    LstTeamId.Add(Id);
        //    PlayerEntityManager objplayerem = new PlayerEntityManager();
        //    LstPlayers = objplayerem.getListByTeamList(Database.getDBContext(), LstTeamId);
        //    LstPlayers.ToList().ForEach(x =>
        //    {
        //        PlayedMatchDetailEntityManager objPlayeddetailem = new PlayedMatchDetailEntityManager();
        //        ObservableCollection<PlayedMatchDetailEntity> LstDetail = objPlayeddetailem.getListByPlayerId(Database.getDBContext(), x.PlayerId);
        //        if (LstDetail.Count > 0)
        //        {
        //            BattingStatistics(LstBatting, x, LstDetail);
        //            BowlingStatistics(LstBowling, x, LstDetail);
        //            AllRounderStatistics(x);
        //        }
        //    });
        //}

        public void GenerateExcel()
        {
            ExcelPackage pck = new ExcelPackage();
            pck.Workbook.Properties.Title = "Player Stats";
            pck.Workbook.Properties.Author = "Infinite Strings";
            pck.Workbook.Properties.Company = "Infinite Strings";

            var ws = pck.Workbook.Worksheets.Add("Batting Stats");
            ws.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            ws.Cells[1, 1].Value = "KSCAUID";
            ws.Cells[1, 1].Style.Font.Bold = true;
            ws.Cells[1, 2].Value = "Player";
            ws.Cells[1, 2].Style.Font.Bold = true;
            ws.Cells[1, 3].Value = "Team";
            ws.Cells[1, 3].Style.Font.Bold = true;
            ws.Cells[1, 4].Value = "Matches";
            ws.Cells[1, 4].Style.Font.Bold = true;
            ws.Cells[1, 5].Value = "Innings";
            ws.Cells[1, 5].Style.Font.Bold = true;
            ws.Cells[1, 6].Value = "Total Runs";
            ws.Cells[1, 6].Style.Font.Bold = true;
            ws.Cells[1, 7].Value = "Not Outs";
            ws.Cells[1, 7].Style.Font.Bold = true;
            ws.Cells[1, 8].Value = "100's";
            ws.Cells[1, 8].Style.Font.Bold = true;
            ws.Cells[1, 9].Value = "50's";
            ws.Cells[1, 9].Style.Font.Bold = true;
            ws.Cells[1, 10].Value = "4's";
            ws.Cells[1, 10].Style.Font.Bold = true;
            ws.Cells[1, 11].Value = "6's";
            ws.Cells[1, 1].Style.Font.Bold = true;
            ws.Cells[1, 12].Value = "Highest Score";
            ws.Cells[1, 12].Style.Font.Bold = true;
            ws.Cells[1, 13].Value = "Average";
            ws.Cells[1, 13].Style.Font.Bold = true;
            ws.Cells[1, 14].Value = "Strike Rate";
            ws.Cells[1, 14].Style.Font.Bold = true;

            //Column Value Section
            int i = 0;
            LstBatting.ToList().ForEach(x =>
            {
                ws.Cells[2 + i, 1].Value = x.KSCAUID;
                ws.Cells[2 + i, 2].Value = x.Player.PlayerName;
                ws.Cells[2 + i, 3].Value = x.TeamName;
                ws.Cells[2 + i, 4].Value = x.Matches;
                ws.Cells[2 + i, 5].Value = x.InningsPlayed;
                ws.Cells[2 + i, 6].Value = x.TotalRuns;
                ws.Cells[2 + i, 7].Value = x.NotOuts;
                ws.Cells[2 + i, 8].Value = x.Hundreds;
                ws.Cells[2 + i, 9].Value = x.Fifties;
                ws.Cells[2 + i, 10].Value = x.Fours;
                ws.Cells[2 + i, 11].Value = x.Sixes;
                ws.Cells[2 + i, 12].Value = x.HighestScore;
                ws.Cells[2 + i, 13].Value = x.Average;
                ws.Cells[2 + i, 14].Value = x.StrikeRate;
                i++;
            });

            var wsb = pck.Workbook.Worksheets.Add("Bowling Stats");
            ws.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            wsb.Cells[1, 1].Value = "KSCAUID";
            wsb.Cells[1, 1].Style.Font.Bold = true;
            wsb.Cells[1, 2].Value = "Player";
            wsb.Cells[1, 2].Style.Font.Bold = true;
            wsb.Cells[1, 3].Value = "Team";
            wsb.Cells[1, 3].Style.Font.Bold = true;
            wsb.Cells[1, 4].Value = "Matches";
            wsb.Cells[1, 4].Style.Font.Bold = true;
            wsb.Cells[1, 5].Value = "Overs";
            wsb.Cells[1, 5].Style.Font.Bold = true;
            wsb.Cells[1, 6].Value = "Maidens";
            wsb.Cells[1, 6].Style.Font.Bold = true;
            wsb.Cells[1, 7].Value = "Runs Conceded";
            wsb.Cells[1, 7].Style.Font.Bold = true;
            wsb.Cells[1, 8].Value = "Wickets";
            wsb.Cells[1, 8].Style.Font.Bold = true;
            wsb.Cells[1, 9].Value = "Wides";
            wsb.Cells[1, 9].Style.Font.Bold = true;
            wsb.Cells[1, 10].Value = "No Balls";
            wsb.Cells[1, 10].Style.Font.Bold = true;
            wsb.Cells[1, 11].Value = "5+ Wickets";
            wsb.Cells[1, 1].Style.Font.Bold = true;
            wsb.Cells[1, 12].Value = "Economy";
            wsb.Cells[1, 12].Style.Font.Bold = true;
            wsb.Cells[1, 13].Value = "Average";
            wsb.Cells[1, 13].Style.Font.Bold = true;

            //Column Value Section
            i = 0;
            LstBowling.ToList().ForEach(x =>
            {
                wsb.Cells[2 + i, 1].Value = x.KSCAUID;
                wsb.Cells[2 + i, 2].Value = x.Player.PlayerName;
                wsb.Cells[2 + i, 3].Value = x.TeamName;
                wsb.Cells[2 + i, 4].Value = x.Matches;
                wsb.Cells[2 + i, 5].Value = x.OversBowled;
                wsb.Cells[2 + i, 6].Value = x.Maidens;
                wsb.Cells[2 + i, 7].Value = x.RunsConceded;
                wsb.Cells[2 + i, 8].Value = x.TotalWickets;
                wsb.Cells[2 + i, 9].Value = x.Wides;
                wsb.Cells[2 + i, 10].Value = x.NoBalls;
                wsb.Cells[2 + i, 11].Value = x.FivePlusWickets;
                wsb.Cells[2 + i, 12].Value = x.Economy;
                wsb.Cells[2 + i, 13].Value = x.Average;
                i++;
            });

            var wsc = pck.Workbook.Worksheets.Add("AllRounder Stats");
            ws.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            wsc.Cells[1, 1].Value = "KSCAUID";
            wsc.Cells[1, 1].Style.Font.Bold = true;
            wsc.Cells[1, 2].Value = "Player";
            wsc.Cells[1, 2].Style.Font.Bold = true;
            wsc.Cells[1, 3].Value = "Team";
            wsc.Cells[1, 3].Style.Font.Bold = true;
            wsc.Cells[1, 4].Value = "Matches";
            wsc.Cells[1, 4].Style.Font.Bold = true;
            wsc.Cells[1, 5].Value = "Innings";
            wsc.Cells[1, 5].Style.Font.Bold = true;
            wsc.Cells[1, 6].Value = "Total Runs Scored";
            wsc.Cells[1, 6].Style.Font.Bold = true;
            wsc.Cells[1, 7].Value = "Batting Average";
            wsc.Cells[1, 7].Style.Font.Bold = true;
            wsc.Cells[1, 8].Value = "Overs Bowled";
            wsc.Cells[1, 8].Style.Font.Bold = true;
            wsc.Cells[1, 9].Value = "Runs Conceded";
            wsc.Cells[1, 9].Style.Font.Bold = true;
            wsc.Cells[1, 10].Value = "Wickets";
            wsc.Cells[1, 10].Style.Font.Bold = true;
            wsc.Cells[1, 11].Value = "5+ Wickets";
            wsc.Cells[1, 1].Style.Font.Bold = true;
            wsc.Cells[1, 12].Value = "Economy";
            wsc.Cells[1, 12].Style.Font.Bold = true;
            wsc.Cells[1, 13].Value = "Bowling Average";
            wsc.Cells[1, 13].Style.Font.Bold = true;

            //Column Value Section
            i = 0;
            LstAllRounder.ToList().ForEach(x =>
            {
                wsc.Cells[2 + i, 1].Value = x.ObjBatting.KSCAUID;
                wsc.Cells[2 + i, 2].Value = x.ObjBatting.Player.PlayerName;
                wsc.Cells[2 + i, 3].Value = x.ObjBatting.TeamName;
                wsc.Cells[2 + i, 4].Value = x.ObjBatting.Matches;
                wsc.Cells[2 + i, 5].Value = x.ObjBatting.InningsPlayed;
                wsc.Cells[2 + i, 6].Value = x.ObjBatting.TotalRuns;
                wsc.Cells[2 + i, 7].Value = x.ObjBatting.Average;
                wsc.Cells[2 + i, 8].Value = x.ObjBowling.OversBowled;
                wsc.Cells[2 + i, 9].Value = x.ObjBowling.RunsConceded;
                wsc.Cells[2 + i, 10].Value = x.ObjBowling.TotalWickets;
                wsc.Cells[2 + i, 11].Value = x.ObjBowling.FivePlusWickets;
                wsc.Cells[2 + i, 12].Value = x.ObjBowling.Economy;
                wsc.Cells[2 + i, 13].Value = x.ObjBowling.Average;
                i++;
            });

            byte[] fileText = pck.GetAsByteArray();

            System.Windows.Forms.SaveFileDialog dialog = new System.Windows.Forms.SaveFileDialog()
            {
                Filter = "Excel Worksheets (*.xlsx)|*.xlsx"
            };

            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                File.WriteAllBytes(dialog.FileName, fileText);
                throw new Exception("Exported To " + dialog.FileName);
            }
        }
    }

    public class AllRounder : ViewModelBase
    {
        public Batting ObjBatting { get; set; }
        public Bowling ObjBowling { get; set; }
    }
    public class Batting:ViewModelBase
    {
        public PlayerEntity Player { get; set; } 
        public Guid TeamId { get; set; }      
        public string TeamName { get; set; }
        public int Matches { get; set; }
        public int InningsPlayed { get; set; }
        public int NotOuts { get; set; }
        public int Hundreds { get; set; }
        public int Fifties { get; set; }
        public int HighestScore { get; set; }
        public int TotalRuns { get; set; }
        public double Average { get; set; }
        public int Fours { get; set; }
        public int Sixes { get; set; }
        public string KSCAUID { get; set; }
        public int BallsFaced { get; set; }
        public double StrikeRate { get; set; }
    }

    public class Bowling:ViewModelBase
    {
        public PlayerEntity Player { get; set; }
        public Guid TeamId { get; set; }
        public string TeamName { get; set; }
        public int Matches { get; set; }
        public int BallsBowled { get; set; }
        public int RunsConceded { get; set; }
        public int FivePlusWickets { get; set; }
        public int TotalWickets { get; set; }
        public double Economy { get; set; }
        public int Maidens { get; set; }
        public int NoBalls { get; set; }
        public int Wides { get; set; }
        public string KSCAUID { get; set; }
        public double Average { get; set; }
        public double OversBowled { get; set; }
    }
}
