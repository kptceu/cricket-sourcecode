﻿using Cricket.DAL;
using Cricket.Model;
using Cricket.Report_Models;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cricket.ViewModel
{
    public class FilteredFixturesVM:ViewModelBase
    {
        public FilteredFixturesVM()
        {

        }

        private ObservableCollection<FixtureEntity> _LstFixture = new ObservableCollection<FixtureEntity>();
        public ObservableCollection<FixtureEntity> LstFixture
        {
            get { return _LstFixture; }
            set { _LstFixture = value;InvokePropertyChanged("LstFixture"); }
        }

        public void LoadFilteredFixtures(DateTime fromdate,DateTime todate)
        {
            FixtureEntityManager ObjFixem = new FixtureEntityManager();
            LstFixture = ObjFixem.getListByFromAndToDate(Database.getDBContext(),fromdate, todate);
        }

        public ObservableCollection<FilterFixturesPrint> GenerateReports()
        {
            ObservableCollection<FilterFixturesPrint> LstForPrint = new ObservableCollection<FilterFixturesPrint>();
            LstFixture.ToList().ForEach(x =>
            {
                FilterFixturesPrint obj = new FilterFixturesPrint();
                if (x.Division.NumberOfDays == 2)
                    obj.DateWithDay = x.FromDate.Value.Date + " & " + x.ToDate.Value.Date + " " + x.FromDate.Value.DayOfWeek + " & " + x.ToDate.Value.DayOfWeek+ " - " + x.FromDate.Value.Year;
                else
                    obj.DateWithDay=x.FromDate.Value.Date + " "+ x.FromDate.Value.DayOfWeek + " - " + x.FromDate.Value.Year;
                obj.Division = x.Division.DivisionName;
                if(x.Scorer!=null)
                obj.scorer = x.Scorer.Name;
                obj.Season = x.Season.SeasonName;
                obj.teamone = x.TeamOne.TeamName;
                obj.teamtwo = x.TeamTwo.TeamName;
                if(x.UmpireOne!=null)
                obj.umpireone = x.UmpireOne.Name;
                if(x.UmpireTwo!=null)
                obj.umpiretwo = x.UmpireTwo.Name;
                if(x.Location!=null)
                obj.venue = x.Location.StadiumName;
                obj.Zone = x.Zone.ZoneName;
                obj.FixtureId = x.FixtureId.ToString();
                LstForPrint.Add(obj);
            });
            LstForPrint = new ObservableCollection<FilterFixturesPrint>(LstForPrint.OrderBy(p => p.DateWithDay));
            return LstForPrint;
        }

        public void GenerateExcel(DateTime from,DateTime to)
        {
            ExcelPackage pck = new ExcelPackage();
            pck.Workbook.Properties.Author = "Cricket Project";
            pck.Workbook.Properties.Title = User.Zone.ZoneName + "-" + User.Season.SeasonName;
            pck.Workbook.Properties.Company = "Infinite Strings";

            var ws = pck.Workbook.Worksheets.Add(User.Zone.ZoneName + "-" + User.Season.SeasonName) ;
            ws.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            ws.Cells[1, 1].Value = "Zone";
            ws.Cells[1, 1].Style.Font.Bold = true;
            ws.Cells[1, 2].Value = User.Zone.ZoneName;
            ws.Cells[1, 2].Style.Font.Bold = true;

            ws.Cells[2, 1].Value = "Season";
            ws.Cells[2, 1].Style.Font.Bold = true;
            ws.Cells[2, 2].Value = User.Season.SeasonName;
            ws.Cells[2, 2].Style.Font.Bold = true;

            ws.Cells[3, 1].Value = "From Date";
            ws.Cells[3, 1].Style.Font.Bold = true;
            ws.Cells[3, 2].Value = from.ToShortDateString();
            ws.Cells[3, 2].Style.Font.Bold = true;

            ws.Cells[4, 1].Value = "To Date";
            ws.Cells[4, 1].Style.Font.Bold = true;
            ws.Cells[4, 2].Value = to.ToShortDateString();
            ws.Cells[4, 2].Style.Font.Bold = true;

            ws.Cells[6, 1].Value = "From Date";
            ws.Cells[6, 1].Style.Font.Bold = true;
            ws.Cells[6, 2].Value = "MatchId";
            ws.Cells[6, 2].Style.Font.Bold = true;
            ws.Cells[6, 3].Value = "Division";
            ws.Cells[6, 3].Style.Font.Bold = true;
            ws.Cells[6, 4].Value = "Group";
            ws.Cells[6, 4].Style.Font.Bold = true;
            ws.Cells[6, 5].Value = "Teams";
            ws.Cells[6, 5].Style.Font.Bold = true;
            ws.Cells[6, 6].Value = "Ground";
            ws.Cells[6, 6].Style.Font.Bold = true;
            ws.Cells[6, 7].Value = "Umpire1";
            ws.Cells[6, 7].Style.Font.Bold = true;
            ws.Cells[6, 8].Value = "Umpire2";
            ws.Cells[6, 8].Style.Font.Bold = true;
            ws.Cells[6, 9].Value = "Scorer";
            ws.Cells[6, 9].Style.Font.Bold = true;
            //Column Value Section
            int i = 0;
            LstFixture.ToList().ForEach(x =>
            {
                ws.Cells[7 + i, 1].Value = x.FromDate.Value.ToShortDateString();
                ws.Cells[7 + i, 2].Value = x.MatchKeyId;
                ws.Cells[7 + i, 3].Value = x.Division.DivisionName;
                ws.Cells[7 + i, 4].Value = x.GroupName;
                ws.Cells[7 + i, 5].Value = x.Teams;
                if (x.Location != null)
                    ws.Cells[7 + i, 6].Value = x.Location.LocationName;
                if (x.UmpireOne != null)
                    ws.Cells[7 + i, 7].Value = x.UmpireOne.Name;
                if (x.UmpireTwo != null)
                    ws.Cells[7 + i, 8].Value = x.UmpireTwo.Name;
                if (x.Scorer != null)
                    ws.Cells[7 + i, 9].Value = x.Scorer.Name;
                i++;
            });

            byte[] fileText = pck.GetAsByteArray();

            System.Windows.Forms.SaveFileDialog dialog = new System.Windows.Forms.SaveFileDialog()
            {
                Filter = "Excel Worksheets (*.xlsx)|*.xlsx"
            };

            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                File.WriteAllBytes(dialog.FileName, fileText);
                throw new Exception("Exported To " + dialog.FileName);
            }
        }
    }
}
