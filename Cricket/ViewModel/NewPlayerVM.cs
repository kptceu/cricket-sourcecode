﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Cricket.Model;
using Cricket.DAL;

namespace Cricket.ViewModel
{
    public class NewPlayerVM : ViewModelBase 
    {
        private ObservableCollection<ZoneEntity> _zones = new ObservableCollection<ZoneEntity>();
        private ObservableCollection<TeamEntity> _teams = new ObservableCollection<TeamEntity>();
        private ObservableCollection<PlayerEntity> _players = new ObservableCollection<PlayerEntity>();
        private ObservableCollection<PlayerEntity> _currentzoneseasondivisionplayers = new ObservableCollection<PlayerEntity>();
        private ObservableCollection<TeamEntity> _teamsincurrentzone = new ObservableCollection<TeamEntity>();

        private ObservableCollection<PlayerEntity> _playersincurrentteam = new ObservableCollection<PlayerEntity>();
        private PlayerEntity _currentplayer;

        public NewPlayerVM()
        {
            InitiateNewPlayerVM();
            
        }

        public ObservableCollection<ZoneEntity> ActiveZones
        {
            get { return _zones; }
            set { _zones = value; InvokePropertyChanged("ActiveZones"); }
        }

        public ObservableCollection<TeamEntity > TeamsinCurrentZone
        {
            get { return _teamsincurrentzone ; }
            set { _teamsincurrentzone = value; InvokePropertyChanged("TeamsinCurrentZone"); }
        }

        public ObservableCollection<TeamEntity> ActiveTeams
        {
            get { return _teams; }
            set { _teams = value; InvokePropertyChanged("ActiveTeams"); }
        }

        public ObservableCollection <PlayerEntity> ActivePlayers
        {
            get { return _players; }
            set { _players = value;  InvokePropertyChanged("ActivePlayers"); }
        }
        public PlayerEntity CurrentPlayer
        {
            get { return _currentplayer; }
            set { _currentplayer = value; InvokePropertyChanged("CurrentPlayer"); }
        }

        public ObservableCollection<PlayerEntity> CurrentZoneDivisionSeasonPlayers
        {
            get { return _currentzoneseasondivisionplayers; }
            set { _currentzoneseasondivisionplayers = value; InvokePropertyChanged("CurrentZoneDivisionSeasonPlayers"); }
        }

        public ObservableCollection<PlayerEntity> PlayersinCurrentTeam
        {
            get { return _playersincurrentteam; }
            set { _playersincurrentteam = value; InvokePropertyChanged("PlayersinCurrentTeam"); }
        }




        public void InitiateNewPlayerVM()
        {

            LoadZones();
            LoadTeams();
            LoadPlayers();
            
                
               // ActivePlayers = Database.GetEntityListwithReferences<Player>(session);
               // CurrentPlayer = Database.getNewEntity<PlayerEntity>();
            
        }

        public bool LoadZones()
        {
            ActiveZones = Database.GetEntityList<ZoneEntity>();
            if (ActiveZones.Count>0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool LoadTeams()
        {
            ActiveTeams  = Database.GetEntityList<TeamEntity >();
            if (ActivePlayers.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool LoadPlayers()
        {
            PlayerEntityManager objPEM = new PlayerEntityManager();
            using (var Context = Database.getDBContext())
                ActivePlayers = objPEM.getEntityListwithReferences(Context);
            if (ActiveTeams.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

         public void getCurrentSeasonZoneDivisionPlayers(ZoneEntity objZone, SeasonEntity objSeason, DivisionEntity ObjDivision,TeamEntity CurrentTeam=null  )
        {
            PlayerEntityManager ObjPlayerEM = new PlayerEntityManager();
            using (var Context = Database.getDBContext())
            {
                if (CurrentTeam==null )
                    CurrentZoneDivisionSeasonPlayers = ObjPlayerEM.getPlayersfromZoneDidvisionSeasonwithReferences(Context, objZone, objSeason, ObjDivision);
                else
                    CurrentZoneDivisionSeasonPlayers = ObjPlayerEM.getPlayersfromTeamID (Context, CurrentTeam.TeamId );
            }
        }

        public bool getTeamsinCurrentZone(CricketContext context,ZoneEntity objZone)
        {
            TeamEntityManager objTeamEm = new TeamEntityManager();
            TeamsinCurrentZone = objTeamEm.getTeamsinCurrentZone(context, objZone);
            return true;
        }
       

    }
}

