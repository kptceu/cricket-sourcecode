﻿using Cricket.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Cricket.ViewModel
{
    public class MatchScoringVM:ViewModelBase
    {
        public MatchScoringVM()
        {
            LoadDefaultValues();
        }

        private Visibility _T1FirstInnings = Visibility.Hidden;
        public Visibility T1FirstInnings
        {
            get { return _T1FirstInnings; }
            set { _T1FirstInnings = value;InvokePropertyChanged("T1FirstInnings"); }
        }

        private Visibility _T2FirstInnings = Visibility.Hidden;
        public Visibility T2FirstInnings
        {
            get { return _T2FirstInnings; }
            set { _T2FirstInnings = value; InvokePropertyChanged("T2FirstInnings"); }
        }

        private Visibility _T2SecondInnings = Visibility.Hidden;
        public Visibility T2SecondInnings
        {
            get { return _T2SecondInnings; }
            set { _T2SecondInnings = value; InvokePropertyChanged("T2SecondInnings"); }
        }

        private Visibility _T1SecondInnings = Visibility.Hidden;
        public Visibility T1SecondInnings
        {
            get { return _T1SecondInnings; }
            set { _T1SecondInnings = value; InvokePropertyChanged("T1SecondInnings"); }
        }

        public void LoadDefaultValues()
        {
            LstPlayedMatchDetailTeamOne = User.LoadPlayedMatchDetailsTeamOne();
            LstPlayedMatchDetailTeamOne = new ObservableCollection<PlayedMatchDetailEntity>(LstPlayedMatchDetailTeamOne.OrderBy(p => p.BattingOrder));
            LstPlayedMatchDetailTeamTwo = User.LoadPlayedMatchDetailsTeamTwo();
            LstPlayedMatchDetailTeamTwo = new ObservableCollection<PlayedMatchDetailEntity>(LstPlayedMatchDetailTeamTwo.OrderBy(p => p.BattingOrder));
            ObjPlayedMatch = User.ObjPlayedMatch;
            ObjFixture = User.Fixture;
        }

        private ObservableCollection<PlayedMatchDetailEntity> _lstplayedmatchdetailteamone = new ObservableCollection<PlayedMatchDetailEntity>();
        public ObservableCollection<PlayedMatchDetailEntity> LstPlayedMatchDetailTeamOne
        {
            get { return _lstplayedmatchdetailteamone; }
            set { _lstplayedmatchdetailteamone = value;InvokePropertyChanged("LstPlayedMatchDetailTeamOne"); }
        }

        private ObservableCollection<PlayedMatchDetailEntity> _lstplayedmatchdetailteamtwo = new ObservableCollection<PlayedMatchDetailEntity>();
        public ObservableCollection<PlayedMatchDetailEntity> LstPlayedMatchDetailTeamTwo
        {
            get { return _lstplayedmatchdetailteamtwo; }
            set { _lstplayedmatchdetailteamtwo = value; InvokePropertyChanged("LstPlayedMatchDetailTeamTwo"); }
        }

        private PlayedMatchEntity _objplayedmatch = new PlayedMatchEntity();
        public PlayedMatchEntity ObjPlayedMatch
        {
            get { return _objplayedmatch; }
            set { _objplayedmatch = value;InvokePropertyChanged("ObjPlayedMatch"); }
        }

        private FixtureEntity _objfixture = new FixtureEntity();
        public FixtureEntity ObjFixture
        {
            get { return _objfixture; }
            set { _objfixture = value; InvokePropertyChanged("ObjFixture"); }
        }
    }
}
