﻿using Cricket.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.IO;

namespace Cricket.ViewModel
{
    public class PointsTableVM:ViewModelBase
    {
        public PointsTableVM()
        {
            LstLoadDivision = User.LoadDivisionsWithTeams();
            LstFixtures = User.LoadFixtures();
            LstPlayedMatch = User.LoadPlayedMatches();           
            LoadPointsTable();
            Groupcount = LstLoadDivision.Select(p => p.Group.GroupName).Distinct().Count();

        }

        public int Groupcount = 0;

        private ObservableCollection<LoadDivisionEntity> _LstLoadDivision = new ObservableCollection<LoadDivisionEntity>();
        public ObservableCollection<LoadDivisionEntity> LstLoadDivision
        {
            get { return _LstLoadDivision; }
            set { _LstLoadDivision = value; InvokePropertyChanged("LstLoadDivision"); }
        }

        private ObservableCollection<FixtureEntity> _LstFixtures = new ObservableCollection<FixtureEntity>();
        public ObservableCollection<FixtureEntity> LstFixtures
        {
            get { return _LstFixtures; }
            set { _LstFixtures = value;InvokePropertyChanged("LstFixtures"); }
        }

        private ObservableCollection<PlayedMatchEntity> _LstPlayedMatch = new ObservableCollection<PlayedMatchEntity>();
        public ObservableCollection<PlayedMatchEntity> LstPlayedMatch
        {
            get { return _LstPlayedMatch; }
            set { _LstPlayedMatch = value; InvokePropertyChanged("LstPlayedMatch"); }
        }

        private ObservableCollection<PointsTableClass> _LstPointsTableGroupA = new ObservableCollection<PointsTableClass>();
        public ObservableCollection<PointsTableClass> LstPointsTableGroupA
        {
            get { return _LstPointsTableGroupA; }
            set { _LstPointsTableGroupA = value; InvokePropertyChanged("LstPointsTableGroupA"); }
        }

        private ObservableCollection<PointsTableClass> _LstPointsTableGroupB = new ObservableCollection<PointsTableClass>();
        public ObservableCollection<PointsTableClass> LstPointsTableGroupB
        {
            get { return _LstPointsTableGroupB; }
            set { _LstPointsTableGroupB = value; InvokePropertyChanged("LstPointsTableGroupB"); }
        }

        private ObservableCollection<PointsTableClass> _LstPointsTableGroupC = new ObservableCollection<PointsTableClass>();
        public ObservableCollection<PointsTableClass> LstPointsTableGroupC
        {
            get { return _LstPointsTableGroupC; }
            set { _LstPointsTableGroupC = value; InvokePropertyChanged("LstPointsTableGroupC"); }
        }

        private ObservableCollection<PointsTableClass> _LstPointsTableGroupD = new ObservableCollection<PointsTableClass>();
        public ObservableCollection<PointsTableClass> LstPointsTableGroupD
        {
            get { return _LstPointsTableGroupD; }
            set { _LstPointsTableGroupD = value; InvokePropertyChanged("LstPointsTableGroupD"); }
        }

        public void LoadGroups(string group,ObservableCollection<PointsTableClass> Lst)
        {
            LstLoadDivision.Where(p => p.Group.GroupName == group).ToList().ForEach(x =>
            {
                PointsTableClass objnew = new PointsTableClass();
                objnew.Team = x.Team;
                objnew.OversFaced = "0";
                objnew.OversBowled = "0";
                Lst.Add(objnew);
            });
        }



        private int GetTotalBalls(decimal FirstinningsOvers, decimal SecondInningsOvers)
        {
            int oversinFirstInnings = 0, ballsinfirstinnings = 0, totalballsinFirstInnings = 0;
            int oversinSecondInnings = 0, ballsinSecondinnings = 0, totalballsinSecondInnings = 0;

            oversinFirstInnings = (int)(Math.Floor(FirstinningsOvers));
            oversinSecondInnings = (int)(Math.Floor(SecondInningsOvers));
            if (FirstinningsOvers.ToString().Contains("."))
            {
                string[] ballsfaced = FirstinningsOvers.ToString().Split('.');
                ballsinfirstinnings = Convert.ToInt16(ballsfaced[1]);
            }

            if (SecondInningsOvers.ToString().Contains("."))
            {
                string[] ballsfaced = SecondInningsOvers.ToString().Split('.');
                ballsinSecondinnings = Convert.ToInt16(ballsfaced[1]);
            }

            totalballsinFirstInnings = (oversinFirstInnings * 6) + ballsinfirstinnings;
            totalballsinSecondInnings = (oversinSecondInnings * 6) + ballsinSecondinnings;
            return (totalballsinFirstInnings + totalballsinSecondInnings);

        }


        private string getOvers(int balls)
        {
            return Math.Floor((decimal)Convert.ToInt16(balls) / 6).ToString() + "." + (Convert.ToInt16(balls) % 6).ToString();
        }

        public void LoadPoints(ObservableCollection<PointsTableClass> Lst)
        {
            Lst.ToList().ForEach(x =>
            {
                LstFixtures.Where(p => (p.TeamOneId == x.Team.TeamId || p.TeamTwoId == x.Team.TeamId) && (p.GroupName != "Semi-Final" && p.GroupName != "Final")).ToList().ForEach(y =>
                         {
                             PlayedMatchEntity obj = LstPlayedMatch.Where(p => p.FixtureId == y.FixtureId).SingleOrDefault();
                             if (obj != null)
                             {
                                 if (!(obj.TeamOnePoints == 0 && obj.TeamTwoPoints == 0))
                                 {
                                     x.MatchesPlayed = x.MatchesPlayed + 1;
                                     if (obj.Result == Guid.Empty)
                                         x.MatchesDrawn = x.MatchesDrawn + 1;
                                     else if (obj.Result == x.Team.TeamId)
                                         x.MatchesWon = x.MatchesWon + 1;
                                     else
                                         x.MatchesLost = x.MatchesLost + 1;

                                     if (string.IsNullOrEmpty(obj.TeamOneFirstInningsOvers))
                                         obj.TeamOneFirstInningsOvers = "0";
                                     if (string.IsNullOrEmpty(obj.TeamOneSecondInningsOvers))
                                         obj.TeamOneSecondInningsOvers = "0";
                                     if (string.IsNullOrEmpty(obj.TeamTwoFirstInningsOvers))
                                         obj.TeamTwoFirstInningsOvers = "0";
                                     if (string.IsNullOrEmpty(obj.TeamTwoSecondInningsOvers))
                                         obj.TeamTwoSecondInningsOvers = "0";

                                     if (obj.TeamOneId == x.Team.TeamId)
                                     {

                                         x.Points = x.Points + obj.TeamOnePoints;
                                         x.RunsScored = x.RunsScored + obj.TeamOneFirstInningsScore + obj.TeamOneSecondInningsScore;
                                         x.RunsConceded = x.RunsConceded + obj.TeamTwoFirstInningsScore + obj.TeamTwoSecondInningsScore;


                                         if (obj.TeamOneFirstInningsTotalWickets == 10)
                                             obj.TeamOneFirstInningsOvers = obj.MaximumOvers.ToString();
                                         if (obj.TeamOneSecondInningsTotalWickets == 10)
                                             obj.TeamOneSecondInningsOvers = obj.MaximumOvers.ToString();

                                         if (obj.TeamTwoFirstInningsTotalWickets == 10)
                                             obj.TeamTwoFirstInningsOvers = obj.MaximumOvers.ToString();
                                         if (obj.TeamTwoSecondInningsTotalWickets == 10)
                                             obj.TeamTwoSecondInningsOvers = obj.MaximumOvers.ToString();


                                         x.OversFaced = (Convert.ToInt16(x.OversFaced) + GetTotalBalls(Convert.ToDecimal(obj.TeamOneFirstInningsOvers), Convert.ToDecimal(obj.TeamOneSecondInningsOvers))).ToString();
                                         x.OversBowled = (Convert.ToInt16(x.OversBowled) + GetTotalBalls(Convert.ToDecimal(obj.TeamTwoFirstInningsOvers), Convert.ToDecimal(obj.TeamTwoSecondInningsOvers))).ToString();


                                     }
                                     else
                                     {
                                         x.Points = x.Points + obj.TeamTwoPoints;
                                         x.RunsScored = x.RunsScored + obj.TeamTwoFirstInningsScore + obj.TeamTwoSecondInningsScore;
                                         x.RunsConceded = x.RunsConceded + obj.TeamOneFirstInningsScore + obj.TeamOneSecondInningsScore;

                                         if (obj.TeamTwoFirstInningsTotalWickets == 10)
                                             obj.TeamTwoFirstInningsOvers = obj.MaximumOvers.ToString();
                                         if (obj.TeamTwoSecondInningsTotalWickets == 10)
                                             obj.TeamTwoSecondInningsOvers = obj.MaximumOvers.ToString();

                                         if (obj.TeamOneFirstInningsTotalWickets == 10)
                                             obj.TeamOneFirstInningsOvers = obj.MaximumOvers.ToString();
                                         if (obj.TeamOneSecondInningsTotalWickets == 10)
                                             obj.TeamOneSecondInningsOvers = obj.MaximumOvers.ToString();


                                         x.OversFaced = (Convert.ToInt16(x.OversFaced) + GetTotalBalls(Convert.ToDecimal(obj.TeamTwoFirstInningsOvers), Convert.ToDecimal(obj.TeamTwoSecondInningsOvers))).ToString();

                                         x.OversBowled = (Convert.ToInt16(x.OversBowled) + GetTotalBalls(Convert.ToDecimal(obj.TeamOneFirstInningsOvers), Convert.ToDecimal(obj.TeamOneSecondInningsOvers))).ToString();
                                         
                                     }
                                 }
                             }
                         });
                x.RunRate = (((Convert.ToDouble ((x.RunsScored / Convert.ToDouble (x.OversFaced)))) - Convert.ToDouble ((x.RunsConceded / Convert.ToDouble (x.OversBowled)))) * 6);
                x.RunRate = Math.Round(x.RunRate, 3);
                x.OversBowled = getOvers(Convert.ToInt16(x.OversBowled));
                x.OversFaced = getOvers(Convert.ToInt16(x.OversFaced ));
            });
        }
        
        public void LoadPointsTable()
        {
            LoadGroups("A", LstPointsTableGroupA);
            LoadGroups("B", LstPointsTableGroupB);
            LoadGroups("C", LstPointsTableGroupC);
            LoadGroups("D", LstPointsTableGroupD);

            LoadPoints(LstPointsTableGroupA);
            LstPointsTableGroupA =new ObservableCollection<PointsTableClass>( LstPointsTableGroupA.OrderByDescending(p => p.Points).ThenByDescending (p => p.RunRate).ThenByDescending(p => p.MatchesWon).ThenByDescending(p=>p.MatchesPlayed ));
            LoadPoints(LstPointsTableGroupB);
            LstPointsTableGroupB = new ObservableCollection<PointsTableClass>(LstPointsTableGroupB.OrderByDescending(p => p.Points).ThenByDescending(p => p.RunRate).ThenByDescending(p => p.MatchesWon).ThenByDescending(p => p.MatchesPlayed));
            LoadPoints(LstPointsTableGroupC);
            LstPointsTableGroupC = new ObservableCollection<PointsTableClass>(LstPointsTableGroupC.OrderByDescending(p => p.Points).ThenByDescending(p => p.RunRate).ThenByDescending(p => p.MatchesWon).ThenByDescending(p => p.MatchesPlayed));
            LoadPoints(LstPointsTableGroupD);
            LstPointsTableGroupD = new ObservableCollection<PointsTableClass>(LstPointsTableGroupD.OrderByDescending(p => p.Points).ThenByDescending(p => p.RunRate).ThenByDescending(p => p.MatchesWon).ThenByDescending(p => p.MatchesPlayed));
        }

        public bool ExportPointsTabletoExcel()
        {
            using (ExcelPackage pck = new ExcelPackage())
            {
                if (Groupcount == 1)
                {
                    ExportGroup(LstPointsTableGroupA, pck, "GroupA");
                }
                else if (Groupcount == 2)
                {
                    ExportGroup(LstPointsTableGroupA, pck, "GroupA");
                    ExportGroup(LstPointsTableGroupB, pck, "GroupB");
                }
                else if (Groupcount == 3)
                {
                    ExportGroup(LstPointsTableGroupA, pck, "GroupA");
                    ExportGroup(LstPointsTableGroupB, pck, "GroupB");
                    ExportGroup(LstPointsTableGroupC, pck, "GroupC");
                }
                else if (Groupcount == 4)
                {
                    ExportGroup(LstPointsTableGroupA, pck, "GroupA");
                    ExportGroup(LstPointsTableGroupB, pck, "GroupB");
                    ExportGroup(LstPointsTableGroupC, pck, "GroupC");
                    ExportGroup(LstPointsTableGroupD, pck, "GroupD");
                }
                System.Windows.Forms.SaveFileDialog dialog = new System.Windows.Forms.SaveFileDialog()
                {
                    Filter = "Excel Worksheets (*.xlsx)|*.xlsx"
                };

                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    byte[] filetext = pck.GetAsByteArray();
                    File.WriteAllBytes(dialog.FileName, filetext);
                }
            
        }

            return true;

        }
        private bool ExportGroup(ObservableCollection <PointsTableClass > lstpointsTable,ExcelPackage EPACKG,string GroupName)
        {

            
            EPACKG.Workbook.Properties.Author = "Points Table";
            EPACKG.Workbook.Properties.Title = "InfiniteStrings";
            EPACKG.Workbook.Properties.Company = "Infinite Strings";
            if (lstpointsTable.Count > 0)
            {
                var ws = EPACKG.Workbook.Worksheets.Add(GroupName);
                ws.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                //int cn = vm.LstForEmpCount.Count() + 4;

                ws.Cells[1, 1].Value = "Team Name";
                ws.Cells[1, 1].Style.Font.Bold = true;

                ws.Cells[1, 2].Value = "MatchesPlayed";
                ws.Cells[1, 2].Style.Font.Bold = true;

                ws.Cells[1, 3].Value = "MatchesWon";
                ws.Cells[1, 3].Style.Font.Bold = true;

                ws.Cells[1, 4].Value = "MatchesLost";
                ws.Cells[1, 4].Style.Font.Bold = true;

                ws.Cells[1, 5].Value = "MatchesDrawn";
                ws.Cells[1, 5].Style.Font.Bold = true;

                ws.Cells[1, 6].Value = "Points";
                ws.Cells[1, 6].Style.Font.Bold = true;

                ws.Cells[1, 7].Value = "RunRate";
                ws.Cells[1, 7].Style.Font.Bold = true;

                for (int j = 2; j < lstpointsTable.Count + 1; j++)
                {
                    ws.Cells[1 + j, 1].Value = lstpointsTable[j - 2].Team.TeamName;
                    ws.Cells[1 + j, 2].Value = lstpointsTable[j - 2].MatchesPlayed;
                    ws.Cells[1 + j, 3].Value = lstpointsTable[j - 2].MatchesWon;
                    ws.Cells[1 + j, 4].Value = lstpointsTable[j - 2].MatchesLost;
                    ws.Cells[1 + j, 5].Value = lstpointsTable[j - 2].MatchesDrawn;
                    ws.Cells[1 + j, 6].Value = lstpointsTable[j - 2].Points;
                    ws.Cells[1 + j, 7].Value = lstpointsTable[j - 2].RunRate;

                }
            }
                return true;
        }
    }

    public class PointsTableClass:ViewModelBase
    {
        public TeamEntity Team { get; set; }
        public int MatchesPlayed { get; set; } 
        public int MatchesWon { get; set; }
        public int MatchesLost { get; set; }
        public int MatchesDrawn { get; set; }
        public int Points { get; set; }
        public int RunsScored { get; set; }
        public int RunsConceded { get; set; }
        public string  OversFaced { get; set; }
        public string  OversBowled { get; set; }
        public double RunRate { get; set; }
    }

   

}
