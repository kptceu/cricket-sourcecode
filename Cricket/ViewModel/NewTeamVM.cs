﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using Cricket.Model;
using Cricket.DAL;
using System.Collections.ObjectModel;


namespace Cricket.ViewModel
{
   public class NewTeamVM : ViewModelBase 
    {
        private ZoneEntity _currentTeamZone;

        private ObservableCollection<TeamEntity> _teams;
        private ObservableCollection<ZoneEntity> _zones;
        private ObservableCollection<TeamEntity> _teamsincurrentzone;
        private TeamEntity _currentteam;

            public NewTeamVM ()
        {

            InitiateVM();
        
        }

        public void InitiateVM()
        {

            LoadTeams();
            //ActiveZones = Database.GetEntityList<ZoneEntity>();
            LoadZones();
                //CurrentTeam = Database.getNewEntity<TeamEntity>();
                
            
        }

        public ObservableCollection <TeamEntity> ActiveTeams
        {
            get { return _teams; }
            set { _teams = value; InvokePropertyChanged("ActiveTeams"); }
        }

        public ObservableCollection<TeamEntity> TeamsinCurrentZone
        {
            get { return _teamsincurrentzone ; }
            set { _teamsincurrentzone  = value; InvokePropertyChanged("TeamsinCurrentZone"); }
        }

        public ObservableCollection <ZoneEntity> ActiveZones
        {
            get { return _zones; }
            set { _zones = value; InvokePropertyChanged("ActiveZones"); }
        }
        public TeamEntity CurrentTeam
        {
            get { return _currentteam; }
            set { _currentteam = value; InvokePropertyChanged("CurrentTeam"); }
        }

        public ZoneEntity CurrentTeamZone
        {
            get { return _currentTeamZone; }
            set { _currentTeamZone = value; InvokePropertyChanged("CurrentTeamZone");
                if(CurrentTeam != null)
                    CurrentTeam.CZone = CurrentTeamZone; }
        }

        public bool LoadTeams()
        {
            ActiveTeams = Database.GetEntityList<TeamEntity>();
            if (ActiveTeams.Count > 0)
                return true;
            else
                return false;
        }

        public bool LoadZones()
        {
            ActiveZones = Database.GetEntityList<ZoneEntity >();
            if (ActiveZones.Count > 0)
                return true;
            else
                return false;
        }


        public bool getTeamsinCurrentZone(CricketContext Context, ZoneEntity objZone)
        {
            TeamEntityManager ObjTeamEm = new TeamEntityManager();
            TeamsinCurrentZone = ObjTeamEm.getTeamsinCurrentZone(Context, objZone);
            return true;
        }

    }
}
