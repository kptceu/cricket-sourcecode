﻿using Cricket.DAL;
using Cricket.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Cricket.ViewModel
{
    public class SelectPlayersForMatchVM : ViewModelBase
    {

        public SelectPlayersForMatchVM()
        {
            LoadPlayersInTeam();
            ObjFixture = User.Fixture;
            LstTeam.Add(ObjFixture.TeamOne);
            LstTeam.Add(ObjFixture.TeamTwo);
            LstElectedTo.Add("BAT");
            LstElectedTo.Add("BOWL");
            LstOfficial = User.LoadOfficials();
            LstLocation = User.LoadLocations();

            PlayedMatchEntityManager objplayedem = new PlayedMatchEntityManager();
            PlayedMatchEntity obj = objplayedem.GetByFixtureId(Database.getDBContext(), User.Fixture.FixtureId);
            if (obj != null)
                ObjPlayedMatch = obj;            
        }

        public bool checkGround(string grdname)
        {
            LocationEntityManager objlocem = new LocationEntityManager();
            if ((objlocem.CheckGround(Database.getDBContext(), grdname)) == false)
                return true;
            else
                return false;
        }

        public bool checkofficial(string offname)
        {
            OfficialEntityManager objoffem = new OfficialEntityManager();
            if ((objoffem.CheckOfficial(Database.getDBContext(), offname)) == false)
                return true;
            else
                return false;
        }

        public bool checkplayer(string playername,Guid TeamId)
        {
            PlayerEntityManager objplayem = new PlayerEntityManager();
            if ((objplayem.CheckPlayer(Database.getDBContext(), playername, TeamId)) == false)
                return true;
            else
                return false;
        }

        public void LoadPlayersInTeam()
        {
            PlayerEntityManager objplayerem = new PlayerEntityManager();
            using (var dbContext = Database.getDBContext())
            {
                LstPlayerTeamOne = objplayerem.GetByTeamId(dbContext, User.Fixture.TeamOne.TeamId);
                LstPlayerTeamTwo = objplayerem.GetByTeamId(dbContext, User.Fixture.TeamTwo.TeamId);
            }
        }


        private ObservableCollection<string> _lstelectedto = new ObservableCollection<string>();
        public ObservableCollection<string> LstElectedTo
        {
            get { return _lstelectedto; }
            set { _lstelectedto = value; InvokePropertyChanged("LstElectedTo"); }
        }

        public ObservableCollection<TeamEntity> _lstteam = new ObservableCollection<TeamEntity>();
        public ObservableCollection<TeamEntity> LstTeam
        {
            get { return _lstteam; }
            set { _lstteam = value;InvokePropertyChanged("LstTeam"); }
        }


        private ObservableCollection<LocationEntity> _lstlocation = new ObservableCollection<LocationEntity>();
        public ObservableCollection<LocationEntity> LstLocation
        {
            get { return _lstlocation; }
            set { _lstlocation = value;InvokePropertyChanged("LstLocation"); }
        }

        private ObservableCollection<OfficialEntity> _lstofficial = new ObservableCollection<OfficialEntity>();
        public ObservableCollection<OfficialEntity> LstOfficial
        {
            get { return _lstofficial; }
            set { _lstofficial = value; InvokePropertyChanged("LstOfficial"); }
        }

        private PlayedMatchEntity _objplayedmatch = Database.getNewEntity<PlayedMatchEntity>();
        public PlayedMatchEntity ObjPlayedMatch
        {
            get { return _objplayedmatch; }
            set { _objplayedmatch = value; InvokePropertyChanged("ObjPlayedMatch"); }

        }

        private ObservableCollection<PlayedMatchDetailEntity> _lstplayedmatchdetail = new ObservableCollection<PlayedMatchDetailEntity>();
        public ObservableCollection<PlayedMatchDetailEntity> LstPlayedMatchDetail
        {
            get { return _lstplayedmatchdetail; }
            set { _lstplayedmatchdetail = value; InvokePropertyChanged("LstPlayedMatchDetail"); }

        }

        private FixtureEntity _objfixture = new FixtureEntity();
        public FixtureEntity ObjFixture
        {
            get { return _objfixture; }
            set { _objfixture = value;InvokePropertyChanged("ObjFixture"); }
        }

        private ObservableCollection<PlayerEntity> _lstplayerteamone = new ObservableCollection<PlayerEntity>();
        public ObservableCollection<PlayerEntity> LstPlayerTeamOne
        {
            get { return _lstplayerteamone; }
            set { _lstplayerteamone = value; InvokePropertyChanged("LstPlayerTeamOne"); }
        }

        private ObservableCollection<PlayerEntity> _lstplayerteamtwo = new ObservableCollection<PlayerEntity>();
        public ObservableCollection<PlayerEntity> LstPlayerTeamTwo
        {
            get { return _lstplayerteamtwo; }
            set { _lstplayerteamtwo = value; InvokePropertyChanged("LstPlayerTeamTwo"); }
        }

        private ObservableCollection<PlayerEntity> _lstplaying11teamone = new ObservableCollection<PlayerEntity>();
        public ObservableCollection<PlayerEntity> LstPlaying11TeamOne
        {
            get { return _lstplaying11teamone; }
            set { _lstplaying11teamone = value; InvokePropertyChanged("LstPlaying11TeamOne"); }
        }

        private ObservableCollection<PlayerEntity> _lstplaying11teamtwo = new ObservableCollection<PlayerEntity>();
        public ObservableCollection<PlayerEntity> LstPlaying11TeamTwo
        {
            get { return _lstplaying11teamtwo; }
            set { _lstplaying11teamtwo = value; InvokePropertyChanged("LstPlaying11TeamTwo"); }
        }


    }
}
