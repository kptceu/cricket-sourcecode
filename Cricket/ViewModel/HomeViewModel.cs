﻿using Cricket.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cricket.ViewModel
{
   public class HomeViewModel : ViewModelBase 
    {
        private ObservableCollection<SeasonEntity> _lstSeason = new ObservableCollection<SeasonEntity>();
        private ObservableCollection<ZoneEntity> _lstZone = new ObservableCollection<ZoneEntity>();
        private ObservableCollection<DivisionEntity> _lstDivision = new ObservableCollection<DivisionEntity>();


        public HomeViewModel ()
        {
            getSeasonList();
            getDivisionList();
            getZoneList();
            User.HomePageVM = this;
           
        }
        public ObservableCollection<SeasonEntity> SeasonList
        {
            get { return _lstSeason; }
            set { _lstSeason = value;InvokePropertyChanged("SeasonList"); }
        }

        public ObservableCollection<ZoneEntity > ZoneList
        {
            get { return _lstZone; }
            set { _lstZone = value; InvokePropertyChanged("ZoneList"); }
        }
        public ObservableCollection<DivisionEntity > DivisionList
        {
            get { return _lstDivision; }
            set { _lstDivision = value; InvokePropertyChanged("DivisionList"); }
        }

        public bool getSeasonList()
        {
            SeasonList = User.LoadSeason();
            return true;
        }

        public bool getDivisionList()
        {
            DivisionList = User.LoadDivision();
            return true;
        }

        public bool getZoneList()
        {
            
            ZoneList = User.LoadZones();
            return true;
        }
    }
}
