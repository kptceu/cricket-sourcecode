﻿using Cricket.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cricket.DAL;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace Cricket.ViewModel
{
    public class ViewFixtureVM : ViewModelBase 
    {
        public ViewFixtureVM()
        {
            LoadFixtures();
        }

        private ObservableCollection<FixtureEntity> _lstfixture = new ObservableCollection<FixtureEntity>();
        public ObservableCollection<FixtureEntity> LstFixture
        {
            get { return _lstfixture; }
            set { _lstfixture = value;InvokePropertyChanged("LstFixture"); }
        }

        public void LoadFixtures()
        {
            FixtureEntityManager objfixem = new FixtureEntityManager();
            using (var dbContext = Database.getDBContext())
            {
                LstFixture = User.LoadFixtures();
                LstFixture = new ObservableCollection<FixtureEntity>(LstFixture.OrderBy(p => p.SerialNumber));
            }
        }
        public void Search(string srchtxt)
        {
            if(srchtxt=="")
            {
                LstFixture.ToList().ForEach(x => x.Visibility = System.Windows.Visibility.Visible);
            }
            else
            {
                LstFixture.ToList().ForEach(x =>
                {                    
                    if (x.Teams.ToUpper().Contains(srchtxt))
                        x.Visibility = System.Windows.Visibility.Visible;
                    else
                        x.Visibility = System.Windows.Visibility.Collapsed;
                });
            }
        }

        public void GenerateExcel()
        {
            ExcelPackage pck = new ExcelPackage();
            pck.Workbook.Properties.Author = "Cricket Project";
            pck.Workbook.Properties.Title =User.Zone.ZoneName+ "-"+ User.Season.SeasonName + "-" + User.Division.DivisionName + " Fixtures";
            pck.Workbook.Properties.Company = "Infinite Strings";

            var ws = pck.Workbook.Worksheets.Add(User.Zone.ZoneName + "-" + User.Season.SeasonName + "-" + User.Division.DivisionName + " Fixtures");
            ws.Cells.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            ws.Cells[1, 1].Value = "Zone";
            ws.Cells[1, 1].Style.Font.Bold = true;
            ws.Cells[1, 2].Value = User.Zone.ZoneName;
            ws.Cells[1, 2].Style.Font.Bold = true;

            ws.Cells[2, 1].Value = "Season";
            ws.Cells[2, 1].Style.Font.Bold = true;
            ws.Cells[2, 2].Value = User.Season.SeasonName;
            ws.Cells[2, 2].Style.Font.Bold = true;

            ws.Cells[3, 1].Value = "Division";
            ws.Cells[3, 1].Style.Font.Bold = true;
            ws.Cells[3, 2].Value = User.Division.DivisionName;
            ws.Cells[3, 2].Style.Font.Bold = true;






            ws.Cells[5, 1].Value = "MatchId";
            ws.Cells[5, 1].Style.Font.Bold = true;
            ws.Cells[5, 2].Value = "Group";
            ws.Cells[5, 2].Style.Font.Bold = true;
            ws.Cells[5, 3].Value = "Teams";
            ws.Cells[5, 3].Style.Font.Bold = true;
            ws.Cells[5, 4].Value = "Ground";
            ws.Cells[5, 4].Style.Font.Bold = true;
            ws.Cells[5, 5].Value = "Umpire1";
            ws.Cells[5, 5].Style.Font.Bold = true;
            ws.Cells[5, 6].Value = "Umpire2";
            ws.Cells[5, 6].Style.Font.Bold = true;
            ws.Cells[5, 7].Value = "Scorer";
            ws.Cells[5, 7].Style.Font.Bold = true;
            //Column Value Section
            int i = 0;
            LstFixture.ToList().ForEach(x =>
            {
                ws.Cells[6 + i, 1].Value = x.MatchKeyId;
                ws.Cells[6 + i, 2].Value = x.GroupName;
                ws.Cells[6 + i, 3].Value = x.Teams;
                if(x.Location!=null)
                ws.Cells[6 + i, 4].Value = x.Location.LocationName;
                if(x.UmpireOne!=null)
                ws.Cells[6 + i, 5].Value = x.UmpireOne.Name;
                if(x.UmpireTwo!=null)
                ws.Cells[6 + i, 6].Value = x.UmpireTwo.Name;
                if(x.Scorer!=null)
                ws.Cells[6 + i, 7].Value = x.Scorer.Name;
                i++;
            });

            byte[] fileText = pck.GetAsByteArray();

            System.Windows.Forms.SaveFileDialog dialog = new System.Windows.Forms.SaveFileDialog()
            {
                Filter = "Excel Worksheets (*.xlsx)|*.xlsx"
            };

            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                File.WriteAllBytes(dialog.FileName, fileText);
                throw new Exception("Exported To " + dialog.FileName);
            }
        }
    }
}
