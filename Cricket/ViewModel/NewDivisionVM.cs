﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Cricket.DAL;
using Cricket.Model;
using Cricket.ViewModel;
using System.Collections.ObjectModel;




namespace Cricket.ViewModel
{
    public class NewDivisionVM : ViewModelBase 
    {
        private ObservableCollection<DivisionEntity> LstDivisions = new ObservableCollection<DivisionEntity>();
        private DivisionEntity _currentdivision = new DivisionEntity();

        public NewDivisionVM()
        {
            loadDivisionstolist();
        }

        public void loadDivisionstolist()
        {
            
                LstDivisions = Database.GetEntityList<DivisionEntity>();
            
        }

        public ObservableCollection<DivisionEntity> DivisionList
        {
            get{ return LstDivisions; }
            set { LstDivisions = value; InvokePropertyChanged("DivisionList");}
        }

        public DivisionEntity CurrentDivision
        {
            get { return _currentdivision; }
            set { _currentdivision = value; InvokePropertyChanged("CurrentDivision"); }
        }
    }
}
