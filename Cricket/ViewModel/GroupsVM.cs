﻿using Cricket.BLL;
using Cricket.DAL;
using Cricket.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cricket.ViewModel
{
    public class GroupsVM:ViewModelBase
    {
        public GroupsVM()
        {
            GetActiveLoadDivisions();
            LoadUnaddedTeams();

            ObservableCollection<FixtureEntity> Lstfixture = User.LoadFixtures();
            if (Lstfixture.Count() > 0)
            {
                CheckStatus = false;
                VisibleStatus = System.Windows.Visibility.Hidden;
                InvisibleStatus = System.Windows.Visibility.Visible;
            }
        }

        public bool GetActiveLoadDivisions()
        {
            ActiveLoadDivisions = User.LoadDivisionsWithTeams();
            return true;
        }

        public bool _checkstatus = true;
        public bool CheckStatus
        {
            get { return _checkstatus; }
            set { _checkstatus = value; InvokePropertyChanged("CheckStatus"); }
        }
        public System.Windows.Visibility _visiblestatus = System.Windows.Visibility.Visible;
        public System.Windows.Visibility VisibleStatus
        {
            get { return _visiblestatus; }
            set { _visiblestatus = value; InvokePropertyChanged("VisibleStatus"); }
        }
        public System.Windows.Visibility _invisiblestatus = System.Windows.Visibility.Hidden;
        public System.Windows.Visibility InvisibleStatus
        {
            get { return _invisiblestatus; }
            set { _invisiblestatus = value; InvokePropertyChanged("InvisibleStatus"); }
        }

        public int GroupCount { get; set; }

        private ObservableCollection<TeamEntity> _activeteams = new ObservableCollection<TeamEntity>();

        public ObservableCollection<TeamEntity> ActiveTeams
        {
            get { return _activeteams; }
            set { _activeteams = value; InvokePropertyChanged("ActiveTeams"); }
        }

        private ObservableCollection<LoadDivisionEntity> _activeloaddivisions = new ObservableCollection<LoadDivisionEntity>();

        public ObservableCollection<LoadDivisionEntity> ActiveLoadDivisions
        {
            get { return _activeloaddivisions; }
            set { _activeloaddivisions = value; InvokePropertyChanged("ActiveLoadDivisions"); }
        }

        private ObservableCollection<LoadDivisionEntity> _lstgroupa = new ObservableCollection<LoadDivisionEntity>();

        public ObservableCollection<LoadDivisionEntity> LstGroupA
        {
            get { return _lstgroupa; }
            set { _lstgroupa = value; InvokePropertyChanged("LstGroupA"); }
        }

        private ObservableCollection<LoadDivisionEntity> _lstgroupb = new ObservableCollection<LoadDivisionEntity>();

        public ObservableCollection<LoadDivisionEntity> LstGroupB
        {
            get { return _lstgroupb; }
            set { _lstgroupb = value; InvokePropertyChanged("LstGroupB"); }
        }

        private ObservableCollection<LoadDivisionEntity> _lstgroupc = new ObservableCollection<LoadDivisionEntity>();

        public ObservableCollection<LoadDivisionEntity> LstGroupC
        {
            get { return _lstgroupc; }
            set { _lstgroupc = value; InvokePropertyChanged("LstGroupC"); }
        }

        private ObservableCollection<LoadDivisionEntity> _lstgroupd = new ObservableCollection<LoadDivisionEntity>();

        public ObservableCollection<LoadDivisionEntity> LstGroupD
        {
            get { return _lstgroupd; }
            set { _lstgroupd = value; InvokePropertyChanged("LstGroupD"); }
        }

        public void AddtoGroup(ObservableCollection<LoadDivisionEntity> LstLoadDivision,TeamEntity Objteam,GroupEntity objgroup)
        {
            ActiveTeams.Remove(Objteam);
            LoadDivisionEntity ObjLoadDivison = Database.getNewEntity<LoadDivisionEntity>();
            ObjLoadDivison.Zone = User.Zone;
            ObjLoadDivison.Division = User.Division;
            ObjLoadDivison.Season = User.Season;
            ObjLoadDivison.Team = Objteam;
            ObjLoadDivison.Group = objgroup;
            LstLoadDivision.Add(ObjLoadDivison);
            LstLoadDivision = new ObservableCollection<LoadDivisionEntity>(LstLoadDivision.OrderBy(p => p.Team.TeamName));
        }

        public void RemoveFromGroup(ObservableCollection<LoadDivisionEntity> LstLoadDivision,LoadDivisionEntity ObjLoadDivision)
        {
            ActiveTeams.Add(ObjLoadDivision.Team);
            ActiveTeams = new ObservableCollection<TeamEntity>(ActiveTeams.OrderBy(p => p.TeamName));
            if(ObjLoadDivision.IsNew==false)
            Database.DeleteEntity<LoadDivisionEntity>(ObjLoadDivision);
            LstLoadDivision.Remove(ObjLoadDivision);
            LstLoadDivision = new ObservableCollection<LoadDivisionEntity>(LstLoadDivision.OrderBy(p => p.Team.TeamName));
        }

        public bool LoadUnaddedTeams()
        {
            ObservableCollection<TeamEntity> LstTeams = User.LoadTeams();
            LstTeams.ToList().ForEach(x =>
            {
                if (ActiveLoadDivisions.Where(p => p.TeamId == x.TeamId).Count() == 0)
                    ActiveTeams.Add(x);
            });

            if(ActiveLoadDivisions.Count>0)
            {
                LstGroupA =new ObservableCollection<LoadDivisionEntity>(ActiveLoadDivisions.Where(p => p.Group.GroupName == "A"));
                LstGroupB = new ObservableCollection<LoadDivisionEntity>(ActiveLoadDivisions.Where(p => p.Group.GroupName == "B"));
                LstGroupC = new ObservableCollection<LoadDivisionEntity>(ActiveLoadDivisions.Where(p => p.Group.GroupName == "C"));
                LstGroupD = new ObservableCollection<LoadDivisionEntity>(ActiveLoadDivisions.Where(p => p.Group.GroupName == "D"));
            }

            GroupCount = ActiveLoadDivisions.Select(p => p.Group.GroupName).Distinct().Count();
            return true;
        }

        public void Search(string srchtxt)
        {

        }

        public bool Save(int groups)
        {
            List<EntityCollection> lsttosave = new List<EntityCollection>();
            if (groups == 1)
                Database.NewSaveEntity<LoadDivisionEntity>(LstGroupA, lsttosave);
            else if (groups == 2)
            {
                Database.NewSaveEntity<LoadDivisionEntity>(LstGroupA, lsttosave);
                Database.NewSaveEntity<LoadDivisionEntity>(LstGroupB, lsttosave);
            }
            else if (groups == 3)
            {
                Database.NewSaveEntity<LoadDivisionEntity>(LstGroupA, lsttosave);
                Database.NewSaveEntity<LoadDivisionEntity>(LstGroupB, lsttosave);
                Database.NewSaveEntity<LoadDivisionEntity>(LstGroupC, lsttosave);
            }
            else if (groups == 4)
            {
                Database.NewSaveEntity<LoadDivisionEntity>(LstGroupA, lsttosave);
                Database.NewSaveEntity<LoadDivisionEntity>(LstGroupB, lsttosave);
                Database.NewSaveEntity<LoadDivisionEntity>(LstGroupC, lsttosave);
                Database.NewSaveEntity<LoadDivisionEntity>(LstGroupD, lsttosave);
            }
            else
                throw new Exception("Select No. Of Groups");
            using (var dbContext = Database.getDBContext())
                Database.CommitChanges(lsttosave, dbContext);

            return true;
        }

        public void GenerateFixtures(int groupcount)
        {
            string divletter = User.Division.DivisionName;
            string zoneno = User.Zone.ZoneNumber.ToString();
            string seasonno = User.Season.SeasonName;
            seasonno = seasonno.Substring(2, 2);
            int groups = groupcount;
            
            FixtureGeneration objfix = new FixtureGeneration();

            DataTable dt = new DataTable();



            ObservableCollection<TeamNameandId> collteam_A = new ObservableCollection<TeamNameandId>();
            ObservableCollection<TeamNameandId> collteam_B = new ObservableCollection<TeamNameandId>();
            ObservableCollection<TeamNameandId> collteam_C = new ObservableCollection<TeamNameandId>();
            ObservableCollection<TeamNameandId> collteam_D = new ObservableCollection<TeamNameandId>();
            //Gropu A




            if (((LstGroupA.Count) % 2) == 1)
            {
                foreach (LoadDivisionEntity obj in LstGroupA)
                {
                    generatelistforgroups(obj.Team, collteam_A);
                }
                generatelistforgroups("Dummy A", collteam_A);
            }
            else
            {
                foreach (LoadDivisionEntity obj in LstGroupA)
                {
                    generatelistforgroups(obj.Team, collteam_A);
                }
            }
            //Gropu B
            if (((LstGroupB.Count) % 2) == 1)
            {
                foreach (LoadDivisionEntity obj in LstGroupB)
                {
                    generatelistforgroups(obj.Team, collteam_B);
                }
                generatelistforgroups("Dummy B", collteam_B);
            }
            else
            {
                foreach (LoadDivisionEntity obj in LstGroupB)
                {
                    generatelistforgroups(obj.Team, collteam_B);
                }
            }
            //Gropu C
            if (((LstGroupC.Count) % 2) == 1)
            {
                foreach (LoadDivisionEntity obj in LstGroupC)
                {
                    generatelistforgroups(obj.Team, collteam_C);
                }
                generatelistforgroups("Dummy C", collteam_C);
            }
            else
            {
                foreach (LoadDivisionEntity obj in LstGroupC)
                {
                    generatelistforgroups(obj.Team, collteam_C);
                }
            }
            //Gropu D
            if (((LstGroupD.Count) % 2) == 1)
            {
                foreach (LoadDivisionEntity obj in LstGroupD)
                {
                    generatelistforgroups(obj.Team, collteam_D);
                }
                generatelistforgroups("Dummy D", collteam_D);
            }
            else
            {
                foreach (LoadDivisionEntity obj in LstGroupD)
                {
                    generatelistforgroups(obj.Team, collteam_D);
                }
            }
            DataTable das = new DataTable();
            das = objfix.fixture(collteam_A, collteam_B, collteam_C, collteam_D, zoneno, seasonno, groups, divletter);


            save(das);
        }

        private void save(DataTable das)
        {
            ObservableCollection<FixtureEntity> LstFixture = new ObservableCollection<FixtureEntity>();
            foreach (DataRowView dr in das.DefaultView)
            {
                FixtureEntity objfix = Database.getNewEntity<FixtureEntity>();
                objfix.SerialNumber =Convert.ToInt32(dr["SerialNo"].ToString());
                objfix.Day = dr["Day"].ToString();
                objfix.Teams = dr["Teams"].ToString();
                objfix.MatchKeyId = dr["MatchId"].ToString();
                objfix.GroupName = dr["Group"].ToString();

                if (objfix.GroupName == "SemiFinal")
                    objfix.MatchType = MatchType.SemiFinal;
                else if (objfix.GroupName == "Final")
                    objfix.MatchType = MatchType.Final;
                else
                    objfix.MatchType = MatchType.League;

                objfix.Season = User.Season;
                objfix.Division = User.Division;
                objfix.Zone = User.Zone;
                if (dr["TeamOne"].ToString() != "")
                    objfix.TeamOne = Database.GetEntity<TeamEntity>(Guid.Parse(dr["TeamOne"].ToString()));
                if (dr["TeamTwo"].ToString() != "")
                    objfix.TeamTwo = Database.GetEntity<TeamEntity>(Guid.Parse(dr["TeamTwo"].ToString()));
                LstFixture.Add(objfix);
            }
            List<EntityCollection> lsttosave = new List<EntityCollection>();
            Database.NewSaveEntity<FixtureEntity>(LstFixture, lsttosave);

            


            using (var dbContext = Database.getDBContext())
                Database.CommitChanges(lsttosave, dbContext);
        }        

        private void generatelistforgroups(TeamEntity objteam, ObservableCollection<TeamNameandId> lstteamnameandid)
        {
            TeamNameandId objteamnameandid = new TeamNameandId();
            objteamnameandid.Id = objteam.TeamId.ToString();
            objteamnameandid.Name = objteam.TeamName;
            lstteamnameandid.Add(objteamnameandid);
        }

        private void generatelistforgroups(string team, ObservableCollection<TeamNameandId> lstteamnameandid)
        {
            TeamNameandId objteamnameandid = new TeamNameandId();
            objteamnameandid.Id = Guid.Empty.ToString();
            objteamnameandid.Name = team;
            lstteamnameandid.Add(objteamnameandid);
        }

    }
}
