using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Cricket.Model
/// <summary>
/// BAse Class that handles Property Changed,Data Validations
/// </summary>
/// <seealso cref="System.ComponentModel.INotifyPropertyChanged" />
/// <seealso cref="System.ComponentModel.INotifyDataErrorInfo" />
{
    [Serializable]
    public abstract class BaseEntity : INotifyPropertyChanged, INotifyDataErrorInfo
    {
        /// <summary>
        /// Occurs when [property changed].
        /// </summary>
        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;


        /// <summary>
        /// The property errors
        /// </summary>
        protected Dictionary<string, List<string>> propErrors = new Dictionary<string, List<string>>();
        /// <summary>
        /// Invokes the property changed.
        /// </summary>
        /// <param name="PropertyName">Name of the property.</param>
        public void InvokePropertyChanged(string PropertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (this.GetType().GetProperty(PropertyName).GetCustomAttribute(typeof(IgnoreSaveAttribute)) == null)
            {
                if (this.GetType().GetProperty(PropertyName).PropertyType.IsSubclassOf(typeof(BaseEntity)) == true)
                {
                    ForeignKeyAttribute FKe = this.GetType().GetProperty(PropertyName).GetCustomAttribute(typeof(ForeignKeyAttribute)) as ForeignKeyAttribute;
                    if (FKe != null)
                    {
                        if (DirtyProperties.Find(p => p.ToString() == PropertyName) == null)
                            DirtyProperties.Add(FKe.Name);
                    }
                }
                else if (this.GetType().GetProperty(PropertyName).PropertyType.GetInterfaces().Any(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(ICollection<>)) && PropertyName != "Rowversion" && PropertyName != "DirtyProperties")
                {
                    //Do Nothing
                }
                else
                {
                    if (DirtyProperties.Find(p => p.ToString() == PropertyName) == null)
                        DirtyProperties.Add(PropertyName);
                }
            }
            if (handler != null)
            {

                handler(this, new PropertyChangedEventArgs(PropertyName));
                //Console.WriteLine(PropertyName);
                //if(this.GetType()==typeof(PlayedMatchEntity ))
                //{
                //    Console.WriteLine(this.GetType().GetProperty("PlayedMatchId").GetValue(this));
                //}
                Validate(PropertyName);
                PropertyInfo ParentProperty = this.GetType().GetProperties().Where(p => p.GetCustomAttribute(typeof(ParentAttribute)) != null).SingleOrDefault();
                if (ParentProperty != null)
                {
                    object ParentObject = (object)ParentProperty.GetValue(this);
                    if (ParentObject != null)
                    {

                        if ((bool)ParentObject.GetType().GetProperty("IsDirty").GetValue(ParentObject) == false)
                            ParentObject.GetType().GetProperty("IsDirty").SetValue(ParentObject, true);
                    }
                }
            }

        }

        /// <summary>
        /// The _isdirty
        /// </summary>
        private bool _isdirty;
        /// <summary>
        /// Gets or sets a value indicating whether this instance is dirty.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is dirty; otherwise, <c>false</c>.
        /// </value>
        [IgnoreSave]
        public bool IsDirty
        {
            get { return _isdirty; }
            set { _isdirty = value; InvokePropertyChanged("IsDirty"); }
        }

        /// <summary>
        /// The _dirtyproperties
        /// </summary>
        private List<string> _dirtyproperties = new List<string>();
        /// <summary>
        /// Gets or sets the dirty properties.
        /// </summary>
        /// <value>
        /// The dirty properties.
        /// </value>
        [IgnoreSave]
        public List<string> DirtyProperties
        {
            get { return _dirtyproperties; }
            set { _dirtyproperties = value; InvokePropertyChanged("DirtyProperties"); }
        }

        /// <summary>
        /// The _isnew
        /// </summary>
        private bool _isnew;
        /// <summary>
        /// Gets or sets a value indicating whether this instance is new.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is new; otherwise, <c>false</c>.
        /// </value>
        [IgnoreSave]
        public bool IsNew
        {
            get { return _isnew; }
            set { _isnew = value; InvokePropertyChanged("IsNew"); }
        }

        /// <summary>
        /// The _ active indicator
        /// </summary>
        /// 
        
        private bool _ActiveIndicator;
        /// <summary>
        /// Gets or sets a value indicating whether [active indicator].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [active indicator]; otherwise, <c>false</c>.
        /// </value>
        /// 
        [IgnoreSave]
        public bool ActiveIndicator
        {
            get { return _ActiveIndicator; }
            set { _ActiveIndicator = value; InvokePropertyChanged("ActiveIndicator"); IsDirty = true; }
        }

        /// <summary>
        /// The _ is enabled
        /// </summary>
        private bool _IsEnabled = true;
        /// <summary>
        /// Gets or sets a value indicating whether this instance is enabled.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is enabled; otherwise, <c>false</c>.
        /// </value>
        [IgnoreSave]
        public bool IsEnabled
        {
            get { return _IsEnabled; }
            set { _IsEnabled = value; InvokePropertyChanged("IsEnabled"); }
        }

        /// <summary>
        /// The _ is selected
        /// </summary>
        private bool _IsSelected;
        /// <summary>
        /// Gets or sets a value indicating whether this instance is selected.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is selected; otherwise, <c>false</c>.
        /// </value>
        [IgnoreSave]
        public bool IsSelected
        {
            get { return _IsSelected; }
            set { _IsSelected = value; InvokePropertyChanged("IsSelected"); }
        }


        private RecordStatus _indicator;
        public RecordStatus Indicator
        {
            get { return _indicator; }
            set { _indicator = value;InvokePropertyChanged("Indicator");IsDirty = true; }
        }

        

        /// <summary>
        /// The _ visibility
        /// </summary>
        private System.Windows.Visibility _Visibility;
        /// <summary>
        /// Gets or sets the visibility.
        /// </summary>
        /// <value>
        /// The visibility.
        /// </value>
        [IgnoreSave]
        public System.Windows.Visibility Visibility
        {
            get { return _Visibility; }
            set { _Visibility = value; InvokePropertyChanged("Visibility"); }

        }

        /// <summary>
        /// The _ created date time
        /// </summary>
        private DateTime? _CreatedDateTime;
        /// <summary>
        /// Gets or sets the created date time.
        /// </summary>
        /// <value>
        /// The created date time.
        /// </value>
        public DateTime? CreatedDateTime
        {
            get { return _CreatedDateTime; }
            set { _CreatedDateTime = value; InvokePropertyChanged("CreatedDateTime"); IsDirty = true; }

        }

        /// <summary>
        /// The _ updated date time
        /// </summary>
        private DateTime? _UpdatedDateTime;
        /// <summary>
        /// Gets or sets the updated date time.
        /// </summary>
        /// <value>
        /// The updated date time.
        /// </value>
        public DateTime? UpdatedDateTime
        {
            get { return _UpdatedDateTime; }
            set { _UpdatedDateTime = value; InvokePropertyChanged("UpdatedDateTime"); IsDirty = true; }

        }

        /// <summary>
        /// The _ created by
        /// </summary>
        private Guid? _CreatedBy;
        /// <summary>
        /// Gets or sets the created by.
        /// </summary>
        /// <value>
        /// The created by.
        /// </value>
        public Guid? CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; InvokePropertyChanged("CreatedBy"); IsDirty = true; }

        }

        /// <summary>
        /// The _ updated by
        /// </summary>
        private Guid? _UpdatedBy;
        /// <summary>
        /// Gets or sets the updated by.
        /// </summary>
        /// <value>
        /// The updated by.
        /// </value>
        public Guid? UpdatedBy
        {
            get { return _UpdatedBy; }
            set { _UpdatedBy = value; InvokePropertyChanged("UpdatedBy"); IsDirty = true; }

        }

        /// <summary>
        /// Gets or sets the rowversion.
        /// </summary>
        /// <value>
        /// The rowversion.
        /// </value>
        [Timestamp]
        public byte[] Rowversion { get; set; }

        /// <summary>
        /// Gets the objectto save.
        /// </summary>
        /// <returns></returns>
        public object getObjecttoSave()
        {
            return this.MemberwiseClone();
        }

        #region INotifyDataErrorInfo

        /// <summary>
        /// Occurs when the validation errors have changed for a property or for the entire entity.
        /// </summary>
        [field: NonSerialized]
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        /// <summary>
        /// Called when [property errors changed].
        /// </summary>
        /// <param name="p">The p.</param>
        public void OnPropertyErrorsChanged(string p)
        {
            if (ErrorsChanged != null)
                ErrorsChanged.Invoke(this, new DataErrorsChangedEventArgs(p));
        }

        /// <summary>
        /// Gets the validation errors for a specified property or for the entire entity.
        /// </summary>
        /// <param name="propertyName">The name of the property to retrieve validation errors for; or null or <see cref="F:System.String.Empty" />, to retrieve entity-level errors.</param>
        /// <returns>
        /// The validation errors for the property or entity.
        /// </returns>
        public System.Collections.IEnumerable GetErrors(string propertyName)
        {
            List<string> errors = new List<string>();
            if (propertyName != null)
            {
                propErrors.TryGetValue(propertyName, out errors);
                return errors;
            }

            else
                return null;
        }
        /// <summary>
        /// Gets a value that indicates whether the entity has validation errors.
        /// </summary>
        [IgnoreSave]
        public bool HasErrors
        {
            get
            {
                try
                {
                    var propErrorsCount = propErrors.Values.FirstOrDefault(r => r.Count > 0);
                    if (propErrorsCount != null)
                        return true;
                    else
                        return false;
                }
                catch { }
                return true;
            }
        }

        #endregion

        /// <summary>
        /// Validates the specified property name.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        private void Validate(string propertyName)
        {
            if(propertyName!="IsDirty")
            Task.Run(() => ValidateModel(propertyName));
        }

        /// <summary>
        /// Validates the model.
        /// </summary>
        /// <param name="propertName">Name of the propert.</param>
        /// <returns></returns>
        public virtual bool ValidateModel(string propertName = "")
        {
            return true;
        }

    }


    /// <summary>
    /// Stores Status Of the Employee
    /// </summary>
    public enum EmployeeStatus
    {
        Active, Deleted, Deceased, Retired
    };

    /// <summary>
    /// Stores the Tasks
    /// </summary>
    public enum Tasks
    {
        Read, Create, Edit, Print, Export, SaveAndPrint
    };

    /// <summary>
    /// Specifies as  a parent attribute
    /// </summary>
    /// <seealso cref="System.Attribute" />
    public class ParentAttribute : Attribute
    {

    }

    /// <summary>
    /// Specifies as an identifier
    /// </summary>
    /// <seealso cref="System.Attribute" />
    public class IdentifierAttribute : Attribute
    {

    }

    /// <summary>
    /// Ignores Field during save
    /// </summary>
    /// <seealso cref="System.Attribute" />
    public class IgnoreSaveAttribute : Attribute
    {

    }

    /// <summary>
    /// Stores EntityKey,Original Entity And The Entity to be saved
    /// </summary>
    public class EntityCollection
    {
        public Guid EntityKey
        {
            get; set;
        }
        /// <summary>
        /// Gets or sets the entity.
        /// </summary>
        /// <value>
        /// The entity.
        /// </value>
        public object Entity
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the original entity.
        /// </summary>
        /// <value>
        /// The original entity.
        /// </value>
        public object OriginalEntity
        {
            get; set;
        }
    }

    
    


    public enum RecordStatus
    {

        AddedNotSaved, Added, EditedNotSaved, Edited, DeletedNotSaved, Deleted
    }

    public enum MatchType
    { League, SemiFinal, Final }

    public enum PitchType
    {
        Matting, Turf
    }
    public enum Gender
    {
        Male, Female
    }

}


