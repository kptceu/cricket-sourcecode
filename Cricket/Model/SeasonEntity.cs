using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Data.OleDb;
using System.ComponentModel.DataAnnotations;

namespace Cricket.Model
{
    [Serializable]
    public class SeasonEntity : BaseEntity
    {
        private Guid _seasonid;
        private string  _seasonname;
        private DateTime? _startdate;
        private DateTime? _enddate;
        private bool _selectedseason;
        [Key]
        public Guid SeasonId
        {
            get
            {
                return _seasonid;
            }
            set
            {
               
                        _seasonid = value;
                        IsDirty = true;
                this.InvokePropertyChanged("SeasonId");
            }
        }
        [Identifier]
        public string SeasonName
        {
            get
            {
                return _seasonname;
            }
            set
            {
                
                        _seasonname = value;
                        IsDirty = true;
                this.InvokePropertyChanged("SeasonName");
            }
        }
        public DateTime? StartDate
        {
            get
            {
                return _startdate;
            }
            set
            {
               
                        _startdate = value;
                        IsDirty = true;
                this.InvokePropertyChanged("StartDate");
            }
        }
        public DateTime? EndDate
        {
            get
            {
                return _enddate;
            }
            set
            {
                
                        _enddate = value;
                        IsDirty = true;
                this.InvokePropertyChanged("EndDate");
            }
        }
        public bool SelectedSeason
        {
            get
            {
                return _selectedseason;
            }
            set
            {
                        _selectedseason = value;
                        IsDirty = true;
                this.InvokePropertyChanged("SelectedSeason");
            }
        }
        public virtual bool ValidateEntity()
        {
            if (this.Indicator != RecordStatus.Deleted && this.Indicator != RecordStatus.DeletedNotSaved)
            {
                if (_seasonid == null || _seasonid == Guid.Empty)
                {
                    throw new Exception("No reference set or key filed is empty for SeasonId ,enter value to continue");
                }
            }
            return true;
        }
    }
}