using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Data.OleDb;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.RegularExpressions;

namespace Cricket.Model
{
    [Serializable]
    public class PlayerEntity : BaseEntity
    {
        private Guid _playerid;
        private string _playername;
        private string _mobilenumber;
        private string _emailid;
        private ZoneEntity _zone;
        private Guid _zoneid;
        private DateTime?  _dateofbirth;
        private string _address;
        private string _kscauid;
        private string _battingstyle;
        private string _bowlingstyle;
        private string _gender;
        private bool _wicketkeeper;
        private TeamEntity _team;
        private Guid _teamid;
        private string _fathername;
        private string _playerimagepath;
        private string _bowlingsubstyle;

        [Key]
        public Guid PlayerId
        {
            get
            {
                return _playerid;
            }
            set
            {
               
                        _playerid = value;
                        IsDirty = true;
                this.InvokePropertyChanged("PlayerId");
            }
        }

        public string PlayerName
        {
            get
            {
                return _playername;
            }
            set
            {
                
                        _playername = value;
                        IsDirty = true;
                this.InvokePropertyChanged("PlayerName");
            }
        }
        public string MobileNumber
        {
            get
            {
                return _mobilenumber;
            }
            set
            {
                
                        _mobilenumber = value;
                        IsDirty = true;
                this.InvokePropertyChanged("MobileNumber");
            }
        }
        public string EmailId
        {
            get
            {
                return _emailid;
            }
            set
            {
                
                        _emailid = value;
                        IsDirty = true;
                this.InvokePropertyChanged("EmailId");
            }
        }

        [ForeignKey("ZoneId")]
       
        public virtual ZoneEntity CZone
        {
            get
            {
                return _zone;
            }
            set
            { 
                            _zone = value;
                             IsDirty = true;
                this.InvokePropertyChanged("CZone");
                
            }
        }
        [Required]
        public Guid ZoneId
        {
            get
            {
                return _zoneid;
            }
            set
            {
                _zoneid = value;
                IsDirty = true;
                this.InvokePropertyChanged("ZoneId");
            }
        }
        public DateTime?  DateOfBirth
        {
            get
            {
                return _dateofbirth;
            }
            set
            {
               
                        _dateofbirth = value;
                        IsDirty = true;
                this.InvokePropertyChanged("DateOfBirth");
            }
        }
        public string Address
        {
            get
            {
                return _address;
            }
            set
            {
                
                        _address = value;
                        IsDirty = true;
                this.InvokePropertyChanged("Address");
            }
        }
        [Identifier]
        public string KSCAUID
        {
            get
            {
                return _kscauid;
            }
            set
            {
                
                        _kscauid = value;
                        IsDirty = true;
                this.InvokePropertyChanged("KSCAUID");
            }
        }
        public string BattingStyle
        {
            get
            {
                return _battingstyle;
            }
            set
            {
                
                        _battingstyle = value;
                        IsDirty = true;
                this.InvokePropertyChanged("BattingStyle");
            }
        }
        public string BowlingStyle
        {
            get
            {
                return _bowlingstyle;
            }
            set
            {
                
                        _bowlingstyle = value;
                        IsDirty = true;
                this.InvokePropertyChanged("BowlingStyle");
            }
        }
        public string Gender
        {
            get
            {
                return _gender;
            }
            set
            {
                
                        _gender = value;
                        IsDirty = true;
                this.InvokePropertyChanged("Gender");
            }
        }
        public bool WicketKeeper
        {
            get
            {
                return _wicketkeeper;
            }
            set
            {
               
                        _wicketkeeper = value;
                        IsDirty = true;
                this.InvokePropertyChanged("WicketKeeper");
            }
        }
        [ForeignKey("TeamId")]
       
        public virtual TeamEntity Team
        {
            get
            {
                return _team;
            }
            set
            {
                            _team = value;
                            IsDirty = true;
                this.InvokePropertyChanged("Team");
               
            }
        }
        [Required]
        public Guid TeamId
        {
            get
            {
                return _teamid;
            }
            set
            {
                _teamid = value;
                IsDirty = true;
                this.InvokePropertyChanged("TeamId");
            }
        }
        public string FatherName
        {
            get
            {
                return _fathername;
            }
            set
            {
               
                        _fathername = value;
                        IsDirty = true;
                this.InvokePropertyChanged("FatherName");
            }
        }
        public string PlayerImagePath
        {
            get
            {
                return _playerimagepath;
            }
            set
            {
                
                        _playerimagepath = value;
                        IsDirty = true;
                this.InvokePropertyChanged("PlayerImagePath");
            }
        }
        public string BowlingSubStyle
        {
            get
            {
                return _bowlingsubstyle;
            }
            set
            {
               
                        _bowlingsubstyle = value;
                        IsDirty = true;
                this.InvokePropertyChanged("BowlingSubStyle");
            }
        }
        public virtual bool ValidateEntity()
        {

            if (this.Indicator != RecordStatus.Deleted && this.Indicator != RecordStatus.DeletedNotSaved)
            {
                if (_playerid == null || _playerid == Guid.Empty)
                {
                    throw new Exception("No reference set or key filed is empty for PlayerId ,enter value to continue");
                }

                if( (CZone==null))
                {
                    throw new Exception("Zone not selected,Select Zone to Continue");
                }
                if ((Team  == null))
                {
                    throw new Exception("Team not selected,Select Team to Continue");
                }
                if (!(String.IsNullOrEmpty(PlayerName)))
                {
                    if (PlayerName.Length > 255)
                    {
                        throw new Exception("Filed length for PlayerName exceeds max length, maxlenth is 255");
                    }
                }
                else
                {
                    throw new Exception("Player Name not entered, enter player name to continue..");
                }
                //if (!(String.IsNullOrEmpty(MobileNumber)))
                //{
                //    if (MobileNumber.Length > 255)
                //    {
                //        throw new Exception("Filed length for MobileNumber exceeds max length, maxlenth is 255");
                //    }
                //    string MobilePattern = @"^\d{10}$"; //"^9\d{9}$";
                //    string newnumber=string.Empty ;
                //   // Regex R= new Regex(MobilePattern);
                //    if(!Regex.IsMatch (MobileNumber,MobilePattern ) )
                //    {
                //        throw new Exception("Invalid mobile number format, Please recheck the number entered");
                //    }
                    
                //}
                //else
                //{
                //    throw new Exception("Player mobilenumber not entered, enter player mobile number to continue..");
                //}
                //if (!(String.IsNullOrEmpty(EmailId)))
                //{
                //    if (EmailId.Length > 255)
                //    {
                //        throw new Exception("Filed length for EmailId exceeds max length, maxlenth is 255");
                //    }
                //}
                //else
                //{
                //    throw new Exception("Player Email Id not entered, enter player Email Id to continue..");
                //}
                if (!(String.IsNullOrEmpty(KSCAUID)))
                {
                    if (KSCAUID.Length > 255)
                    {
                        throw new Exception("Filed length for KSCAUID exceeds max length, maxlenth is 255");
                    }
                }
                else
                {
                    throw new Exception("Player KSCAUID not entered, reselect Team to continue..");
                }
                //if (!(String.IsNullOrEmpty(BattingStyle)))
                //{
                //    if (BattingStyle.Length > 255)
                //    {
                //        throw new Exception("Filed length for BattingStyle exceeds max length, maxlenth is 255");
                //    }
                //}
                //else
                //{
                //    throw new Exception("Player batting Style not entered, enter Batting Style name to continue..");
                //}
                //if (!(String.IsNullOrEmpty(BowlingStyle)))
                //{
                //    if (BowlingStyle.Length > 255)
                //    {
                //        throw new Exception("Filed length for BowlingStyle exceeds max length, maxlenth is 255");
                //    }
                //}
                //else
                //{
                //    throw new Exception("Player Bowling style not entered, enter player Bowling style to continue..");
                //}
                //if (!(String.IsNullOrEmpty(Gender)))
                //{
                //    if (Gender.Length > 255)
                //    {
                //        throw new Exception("Filed length for Gender exceeds max length, maxlenth is 255");
                //    }
                //}
                //if (!(String.IsNullOrEmpty(FatherName)))
                //{
                //    if (FatherName.Length > 255)
                //    {
                //        throw new Exception("Filed length for FatherName exceeds max length, maxlenth is 255");
                //    }
                //}
                //else
                //{
                //    FatherName = "";
                //}
                //if (string.IsNullOrEmpty(Address))
                //{
                //    throw new Exception("Address not entered, enter player's Address to continue..");
                //}
                //if(string.IsNullOrEmpty (Gender ))
                //{
                //    throw new Exception("Gender not selected, select player's gender to continue..");
                //}
                //if(string.IsNullOrEmpty(BowlingSubStyle ))
                //{
                //    throw new Exception("BowlingSubStyle not selected, select player's BowlingSubStyle to continue..");
                //}
                //if(string.IsNullOrEmpty (PlayerImagePath ))
                //{
                //    PlayerImagePath = "";
                //}
                //if(this.DateOfBirth ==null)
                //{
                //    throw new Exception("Date of birth not entered, Please enter player's date of birth to continue..");
               // }
            }
            return true;
        }
    }
}


