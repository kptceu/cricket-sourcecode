using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Data.OleDb;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cricket.Model
{
    [Serializable]
    public class LoadDivisionEntity : BaseEntity
    {
        private Guid _loaddivisionid;
        private SeasonEntity _season;
        private Guid _seasonid;
        private bool _onhold;
        private string _teamname;
        private DivisionEntity _division;
        private Guid _divisionid;
        private Guid _teamid;
        private TeamEntity _team;
        private Guid _groupid;
        [Required]
        public Guid GroupId
        {
            get { return _groupid; }
            set { _groupid = value;InvokePropertyChanged("GroupId");IsDirty = true; }
        }

        private GroupEntity _group;
        [ForeignKey("GroupId")]
        public GroupEntity Group
        {
            get { return _group; }
            set { _group = value; InvokePropertyChanged("Group"); IsDirty = true; }

        }

        private Guid _zoneid;
        [Required]
        public Guid ZoneId
        {
            get { return _zoneid; }
            set { _zoneid = value;InvokePropertyChanged("ZoneId");IsDirty = true; }
        }

        private ZoneEntity _zone;
        [ForeignKey("ZoneId")]
        public ZoneEntity Zone
        {
            get { return _zone; }
            set { _zone = value; InvokePropertyChanged("Zone"); IsDirty = true; }

        }

        [Key]
        public Guid LoadDivisionId
        {
            get
            {
                return _loaddivisionid;
            }
            set
            {

                _loaddivisionid = value;
                IsDirty = true;
                this.InvokePropertyChanged("LoadDivisionId");

            }
        }
        [ForeignKey("SeasonId")]
       
        public virtual SeasonEntity Season
        {
            get
            {
                return _season;
            }
            set
            {

                _season = value;
                IsDirty = true;
                this.InvokePropertyChanged("Season");

            }
        }
        [Required]
        public Guid SeasonId
        {
            get
            {
                return _seasonid;
            }
            set
            {
                _seasonid = value;
                IsDirty = true;
                this.InvokePropertyChanged("SeasonId");
            }
        }
        public bool OnHold
        {
            get
            {
                return _onhold;
            }
            set
            {
                _onhold = value;
                IsDirty = true;
                this.InvokePropertyChanged("OnHold");

            }
        }
        public string TeamName
        {
            get
            {
                return _teamname;
            }
            set
            {

                _teamname = value;
                IsDirty = true;
                this.InvokePropertyChanged("TeamName");

            }
        }
        [ForeignKey("DivisionId")]
       
        public virtual DivisionEntity Division
        {
            get
            {
                return _division;
            }
            set
            {

                _division = value;
               IsDirty = true;
                this.InvokePropertyChanged("Division");

            }
        }
        [Required]
        public Guid DivisionId
        {
            get
            {
                return _divisionid;
            }
            set
            {
                _divisionid = value;
                IsDirty = true;
                this.InvokePropertyChanged("DivisionId");
            }
        }

        [ForeignKey("TeamId")]
       
        public virtual TeamEntity Team
        {
            get
            {
                return _team;
            }
            set
            {

                _team = value;
                 IsDirty = true;
                this.InvokePropertyChanged("Team");

            }
        }

        [Required]
        public Guid TeamId
        {
            get
            {
                return _teamid;
            }
            set
            {

                _teamid = value;
                IsDirty = true;
                this.InvokePropertyChanged("TeamId");

            }
        }
        public virtual bool ValidateEntity()
        {
            if (this.Indicator != RecordStatus.Deleted && this.Indicator != RecordStatus.DeletedNotSaved)
            {
                if (_loaddivisionid == null || _loaddivisionid == Guid.Empty)
                {
                    throw new Exception("No reference set or key filed is empty for LoadDivisionId ,enter value to continue");
                }
            }
            return true;
        }
    }
}