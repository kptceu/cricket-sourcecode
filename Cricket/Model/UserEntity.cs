using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Data.OleDb;
using System.ComponentModel.DataAnnotations;

namespace Cricket.Model
{
    [Serializable]
    public class UserEntity : BaseEntity
    {
        private Guid _userid;
        private string _username;
        private string _password;
        [Key]
        public Guid UserId
        {
            get
            {
                return _userid;
            }
            set
            {
                
                        _userid = value;
                        IsDirty = true;
                this.InvokePropertyChanged("UserId");
            }
        }
        [Identifier]
        public string UserName
        {
            get
            {
                return _username;
            }
            set
            {
                
                        _username = value;
                        IsDirty = true;
                this.InvokePropertyChanged("UserName");
            }
        }
        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                
                        _password = value;
                        IsDirty = true;
                this.InvokePropertyChanged("Password");
            }
        }
        public virtual bool ValidateEntity()
        {
            if (this.Indicator != RecordStatus.Deleted && this.Indicator != RecordStatus.DeletedNotSaved)
            {
                if (_userid == null || _userid == Guid.Empty)
                {
                    throw new Exception("No reference set or key filed is empty for UserId ,enter value to continue");
                }
                if (String.IsNullOrEmpty(UserName))
                {
                    throw new Exception("UserName is a mandatory filed,enter value to continue");
                }
                if (!(String.IsNullOrEmpty(UserName)))
                {
                    if (UserName.Length > 10)
                    {
                        throw new Exception("Filed length for UserName exceeds max length, maxlenth is 10");
                    }
                }
                if (String.IsNullOrEmpty(Password))
                {
                    throw new Exception("Password is a mandatory filed,enter value to continue");
                }
                if (!(String.IsNullOrEmpty(Password)))
                {
                    if (Password.Length > 10)
                    {
                        throw new Exception("Filed length for Password exceeds max length, maxlenth is 10");
                    }
                }
            }
            return true;
        }
    }
}
