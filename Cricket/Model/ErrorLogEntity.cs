using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Data.OleDb;
using System.ComponentModel.DataAnnotations;

namespace Cricket.Model
{
    [Serializable]
    public class ErrorLogEntity : BaseEntity
    {
        private Guid _errorlogid;
        private string _errorcontent;
        private string _pagename;
        private string _trycatchnumber;
        private DateTime _errortime;
        [Key]
        public Guid ErrorLogId
        {
            get
            {
                return _errorlogid;
            }
            set
            {

                _errorlogid = value;
                IsDirty = true;
                this.InvokePropertyChanged("ErrorLogId");
            }
        }
        public string ErrorContent
        {
            get
            {
                return _errorcontent;
            }
            set
            {

                _errorcontent = value;
                IsDirty = true;
                this.InvokePropertyChanged("ErrorContent");
            }
        }
        public string PageName
        {
            get
            {
                return _pagename;
            }
            set
            {

                _pagename = value;
                IsDirty = true;
                this.InvokePropertyChanged("PageName");
            }
        }
        public string TryCatchNumber
        {
            get
            {
                return _trycatchnumber;
            }
            set
            {

                _trycatchnumber = value;
                IsDirty = true;
                this.InvokePropertyChanged("TryCatchNumber");
            }
        }
        public DateTime ErrorTime
        {
            get
            {
                return _errortime;
            }
            set
            {

                _errortime = value;
                IsDirty = true;
                this.InvokePropertyChanged("ErrorTime");
            }
        }
        public virtual bool ValidateEntity()
        {
            if (this.Indicator != RecordStatus.Deleted && this.Indicator != RecordStatus.DeletedNotSaved)
            {
                if (_errorlogid == null || _errorlogid == Guid.Empty)
                {
                    throw new Exception("No reference set or key filed is empty for ErrorLogId ,enter value to continue");
                }
            }
            return true;
        }
    }
}
