using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Data.OleDb;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.RegularExpressions;

namespace Cricket.Model
{
    [Serializable]
    public class OfficialEntity : BaseEntity
    {
        private Guid _officialid;
        private string _officialkeyid;
        private string _name;
        private string _place;
        private string _mobilenumber;
        private DateTime?  _dateofbirth;
        private string _emailid;
        private string _gender;
        private ZoneEntity _zone;
        private  Guid _zoneid;
        [Key]
        public Guid OfficialId
        {
            get
            {
                return _officialid;
            }
            set
            {
               
                        _officialid = value;
                        IsDirty = true;
                        this.InvokePropertyChanged ("OfficialId");
                    
            }
        }

        private bool _isselectable;
        [IgnoreSave]
        public bool IsSelectable
        {
            get { return _isselectable; }
            set { _isselectable = value; InvokePropertyChanged("IsSelectable");IsDirty = true; }
        }

        private string _allowedcolor;
        [IgnoreSave]
        public string AllowedColor
        {
            get { return _allowedcolor; }
            set { _allowedcolor = value;InvokePropertyChanged("AllowedColor"); }  
        }

        [Identifier]
        public string OfficialKeyId
        {
            get
            {
                return _officialkeyid;
            }
            set
            {
                
                        _officialkeyid = value;
                        IsDirty = true;
                        this.InvokePropertyChanged ("OfficialKeyId");
                    
            }
        }
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                
                        _name = value;
                        IsDirty = true;
                        this.InvokePropertyChanged ("Name");
                    
            }
        }
        public string Place
        {
            get
            {
                return _place;
            }
            set
            {
                
                        _place = value;
                        IsDirty = true;
                        this.InvokePropertyChanged ("Place");
                    
            }
        }
        public string MobileNumber
        {
            get
            {
                return _mobilenumber;
            }
            set
            {
                
                        _mobilenumber = value;
                        IsDirty = true;
                        this.InvokePropertyChanged ("MobileNumber");
                    
            }
        }
        public DateTime?  DateOfBirth
        {
            get
            {
                return _dateofbirth;
            }
            set
            {
               
                        _dateofbirth = value;
                        IsDirty = true;
                        this.InvokePropertyChanged ("DateOfBirth");
                   
            }
        }
        public string EmailId
        {
            get
            {
                return _emailid;
            }
            set
            {
                
                        _emailid = value;
                        IsDirty = true;
                        this.InvokePropertyChanged ("EmailId");
                    
            }
        }
        public string Gender
        {
            get
            {
                return _gender;
            }
            set
            {
               
                        _gender = value;
                        IsDirty = true;
                        this.InvokePropertyChanged ("Gender");
                    
            }
        }
        [ForeignKey("ZoneId")]
          public virtual ZoneEntity CZone
        {
            get
            {
                return _zone;
            }
            set
            {
               
                    _zone = value;
                   IsDirty = true;
                    this.InvokePropertyChanged("CZone");
               
                        
                
            }
        }
        [Required]
        public Guid ZoneId
        {
            get
            {
                return _zoneid;
            }
            set
            {
                _zoneid = value;
                IsDirty = true;
                this.InvokePropertyChanged("ZoneId");
            }
        }
        public virtual bool ValidateEntity()
        {
            if (this.Indicator != RecordStatus.Deleted && this.Indicator != RecordStatus.DeletedNotSaved)
            {
                if (_officialid == null || _officialid == Guid.Empty)
                {
                    throw new Exception("No reference set or key filed is empty for OfficialId ,enter value to continue");
                }
                if (!(String.IsNullOrEmpty(OfficialKeyId)))
                {
                    if (OfficialKeyId.Length > 255)
                    {
                        throw new Exception("Filed length for OfficialKeyId exceeds max length, maxlenth is 255");
                    }
                }
                else
                {
                    throw new Exception("Official Key Id not updated, update to continue");
                }
                if (!(String.IsNullOrEmpty(Name)))
                {
                    if (Name.Length > 255)
                    {
                        throw new Exception("Filed length for Name exceeds max length, maxlenth is 255");
                    }
                }
                else
                {
                    throw new Exception("Official Name not updated, update to continue");
                }
                if (!(String.IsNullOrEmpty(Place)))
                {
                    if (Place.Length > 255)
                    {
                        throw new Exception("Filed length for Place exceeds max length, maxlenth is 255");
                    }
                }
                else
                {
                    throw new Exception("Place not updated, update to continue");
                }
                if (!(String.IsNullOrEmpty(MobileNumber)))
                {
                    if (MobileNumber.Length > 255)
                    {
                        throw new Exception("Filed length for MobileNumber exceeds max length, maxlenth is 255");
                    }
                    string MobilePattern = @"^\d{10}$"; //"^9\d{9}$";
                    string newnumber = string.Empty;
                    // Regex R= new Regex(MobilePattern);
                    if (!Regex.IsMatch(MobileNumber, MobilePattern))
                    {
                        throw new Exception("Invalid mobile number format, Please recheck the number entered");
                    }

                }
                else
                {
                    throw new Exception("Mobile Number updated, update to continue");
                }
                if (!(String.IsNullOrEmpty(EmailId)))
                {
                    if (EmailId.Length > 255)
                    {
                        throw new Exception("Filed length for EmailId exceeds max length, maxlenth is 255");
                    }
                }
                else
                {
                    throw new Exception("Email Id updated, update to continue");
                }
                if (!(String.IsNullOrEmpty(Gender)))
                {
                    if (Gender.Length > 255)
                    {
                        throw new Exception("Filed length for Gender exceeds max length, maxlenth is 255");
                    }
                }
                else
                {
                    throw new Exception("Gender not selected, update to continue");
                }
            }
            return true;
        }
    }
}
