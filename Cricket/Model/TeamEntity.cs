using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Data.OleDb;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cricket.Model
{
    [Serializable]
    public class TeamEntity : BaseEntity
    {
        private Guid _teamid;
        private string _teamname;
        private string _teamintname;
        private ZoneEntity _zone;
        private Guid _zoneid;
        private string _teamkeyid;
        [Key]
        public Guid TeamId
        {
            get
            {
                return _teamid;
            }
            set
            {
               
                        _teamid = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamId");
            }
        }

        public string TeamName
        {
            get
            {
                return _teamname;
            }
            set
            {
               
                        _teamname = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamName");
            }
        }

        public virtual string TeamintName
        {
            get
            {
                return _teamintname;
            }
            set
            {
                
                        _teamintname = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamintName");
            }
        }

        [ForeignKey("ZoneId")]
       
        public virtual ZoneEntity CZone
        {
            get
            {
                return _zone;
            }
            set
            {
               
                        _zone = value;

                IsDirty = true;
                this.InvokePropertyChanged("CZone");
            }
        }

        [Required]
        public Guid ZoneId
        {
            get
            {
                return _zoneid;
            }
            set
            {
                _zoneid = value;
                IsDirty = true;
                this.InvokePropertyChanged("ZoneId");
            }
        }

        [Identifier]
        public string TeamKeyId
        {
            get
            {
                return _teamkeyid;
            }
            set
            {
                
                        _teamkeyid = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamKeyId");
            }
        }
        public virtual bool ValidateEntity()
        {
            if (this.Indicator != RecordStatus.Deleted && this.Indicator != RecordStatus.DeletedNotSaved)
            {
                if (_teamid == null || _teamid == Guid.Empty)
                {
                    throw new Exception("No reference set or key filed is empty for TeamId ,enter value to continue");
                }
                if (_zone  == null )
                {
                    throw new Exception("No reference set to Zone or Zone not selected,Select Zone to continue");
                }
                if (!(String.IsNullOrEmpty(this._teamname )))
                {
                    if (TeamName.Length > 255)
                    {
                        throw new Exception("Filed length for TeamName exceeds max length, maxlenth is 255");
                    }
                }
                else
                {
                    throw new Exception("TeamName not updated...");
                }
                if (!(String.IsNullOrEmpty(this._teamintname )))
                {
                    if (TeamintName.Length > 255)
                    {
                        throw new Exception("Filed length for TeamintName exceeds max length, maxlenth is 255");
                    }
                }
                else
                {
                    throw new Exception("Team intName not updated...");
                }
                if (!(String.IsNullOrEmpty(TeamKeyId)))
                {
                    if (TeamKeyId.Length > 255)
                    {
                        throw new Exception("Filed length for TeamKeyId exceeds max length, maxlenth is 255");
                    }
                }
                else
                {
                    throw new Exception("TeamKeyId not updated...");
                }
            }
            return true;
        }
    }
}
