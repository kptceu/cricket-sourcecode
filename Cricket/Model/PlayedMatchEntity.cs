using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Data.OleDb;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cricket.Model
{
    [Serializable]
    public class PlayedMatchEntity : BaseEntity
    {
        private Guid _playedmatchid;
        private FixtureEntity _fixture;
        private Guid _fixtureid;
        private string _tosswonby;
        private string _electedto;
        private string _teamonefirstinningscommencedat;
        private string _teamonefirstinningsclosedat;
        private string _teamonefirstinningsduration;
        private Guid _result;
        private bool _isteamonefirstinningsnotplayed;
        private int _teamonefirstinningsscore;
        private bool _isteamonesecondinningsnotplayed;

        private Guid? _teamonecaptainid;
        public Guid? TeamOneCaptainId
        {
            get { return _teamonecaptainid; }
            set { _teamonecaptainid = value;InvokePropertyChanged("TeamOneCaptainId"); IsDirty = true; }
        }

        private PlayerEntity _teamonecaptain;
        [ForeignKey("TeamOneCaptainId")]
        public PlayerEntity TeamOneCaptain
        {
            get { return _teamonecaptain; }
            set { _teamonecaptain = value; InvokePropertyChanged("TeamOneCaptain"); IsDirty = true; }
        }

        private Guid? _teamtwocaptainid;
        public Guid? TeamtwoCaptainId
        {
            get { return _teamtwocaptainid; }
            set { _teamtwocaptainid = value; InvokePropertyChanged("TeamtwoCaptainId"); IsDirty = true; }
        }

        private PlayerEntity _teamtwocaptain;
        [ForeignKey("TeamtwoCaptainId")]
        public PlayerEntity TeamTwoCaptain
        {
            get { return _teamtwocaptain; }
            set { _teamtwocaptain = value; InvokePropertyChanged("TeamTwoCaptain"); IsDirty = true; }
        }

        private Guid? _teamtwokeeperid;
        public Guid? TeamtwoKeeperId
        {
            get { return _teamtwokeeperid; }
            set { _teamtwokeeperid = value; InvokePropertyChanged("TeamtwoKeeperId"); IsDirty = true; }
        }

        private PlayerEntity _teamtwokeeper;
        [ForeignKey("TeamtwoKeeperId")]
        public PlayerEntity TeamTwoKeeper
        {
            get { return _teamtwokeeper; }
            set { _teamtwokeeper = value; InvokePropertyChanged("TeamTwoKeeper"); IsDirty = true; }
        }

        private Guid? _teamonekeeperid;
        public Guid? TeamOneKeeperId
        {
            get { return _teamonekeeperid; }
            set { _teamonekeeperid = value; InvokePropertyChanged("TeamOneKeeperId"); IsDirty = true; }
        }

        private PlayerEntity _teamonekeeper;
        [ForeignKey("TeamOneKeeperId")]
        public PlayerEntity TeamOneKeeper
        {
            get { return _teamonekeeper; }
            set { _teamonekeeper = value; InvokePropertyChanged("TeamOneKeeper"); IsDirty = true; }
        }


        private int _teamonesecondinningsscore;

        private bool _isteamtwofirstinningsnotplayed;
        private bool _isteamtwosecondinningsnotplayed;

        private bool _iswalkoverforteamone;
        private bool _iswalkoverforteamtwo;

        private int _teamtwosecondinningsscore;
        private int _teamtwofirstinningsscore;
        private int _teamonefirstinningsbyes;
        private int _teamonesecondinningsbyes;
        private int _teamtwosecondinningsbyes;
        private int _teamtwofirstinningsbyes;
        private int _teamonefirstinningslegbyes;
        private int _teamonesecondinningslegbyes;
        private int _teamtwosecondinningslegbyes;
        private int _teamtwofirstinningslegbyes;
        private int _teamonefirstinningsp;
        private int _teamonesecondinningsp;
        private int _teamtwosecondinningsp;
        private int _teamtwofirstinningsp;
        private int _teamonefirstinningsro;
        private int _teamonesecondinningsro;
        private int _teamtwosecondinningsro;
        private int _teamtwofirstinningsro;
        private int _teamonefirstinningstotalextras;
        private int _teamonesecondinningstotalextras;
        private int _teamtwosecondinningstotalextras;
        private int _teamtwofirstinningstotalextras;
        private Guid _teamoneid;
        private TeamEntity _teamone;
        private Guid _teamtwoid;
        private TeamEntity _teamtwo;
        private int _teamonepoints;
        private int _teamtwopoints;
        private string _teamtwofirstinningscommencedat;
        private string _teamtwofirstinningsclosedat;
        private string _teamtwofirstinningsduration;
        private string _teamonesecondinningscommencedat;
        private string _teamonesecondinningsclosedat;
        private string _teamonesecondinningsduration;
        private string _teamtwosecondinningscommencedat;
        private string _teamtwosecondinningsclosedat;
        private string _teamtwosecondinningsduration;
        private string _teamonefirstinningsovers;
        private string _teamtwofirstinningsovers;
        private string _teamtwosecondinningsovers;
        private string _teamonesecondinningsovers;
        private string _resultremarks;
        private int _teamonefirstinningstotalwickets;
        private int _teamtwofirstinningstotalwickets;
        private int _teamtwosecondinningstotalwickets;
        private int _teamonesecondinningstotalwickets;
        private int _teamonefirstinningswides;
        private int _teamtwofirstinningswides;
        private int _teamtwosecondinningswides;
        private int _teamonesecondinningswides;
        private int _teamonefirstinningsnoballs;
        private int _teamtwofirstinningsnoballs;
        private int _teamtwosecondinningsnoballs;
        private int _teamonesecondinningsnoballs;
        private int _teamonefirstinningscatches;
        private int _teamtwofirstinningscatches;
        private int _teamtwosecondinningscatches;
        private int _teamonesecondinningscatches;
        private int _teamonefirstinningslbws;
        private int _teamtwofirstinningslbws;
        private int _teamtwosecondinningslbws;
        private int _teamonesecondinningslbws;
        private int _teamonesecondinningsbowled;
        private int _teamtwosecondinningsbowled;
        private int _teamtwofirstinningsbowled;
        private int _teamonefirstinningsbowled;
        private bool _completestatus;
        private int _teamonefirstinningsstumped;
        private int _teamtwofirstinningsstumped;
        private int _teamtwosecondinningsstumped;
        private int _teamonesecondinningsstumped;
        private int _teamonesecondinningshitwickets;
        private int _teamtwosecondinningshitwickets;
        private int _teamtwofirstinningshitwickets;
        private int _teamonefirstinningshitwickets;
        private int _teamonefirstinningsretiredhurt;
        private int _teamonesecondinningsretiredhurt;
        private int _teamtwofirstinningsretiredhurt;
        private int _teamtwosecondinningsretiredhurt;
        private int _maximumovers;
        public int MaximumOvers
        {
            get { return _maximumovers; }
            set { _maximumovers = value;InvokePropertyChanged("MaximumOvers"); }
        }

        public int TeamOneFirstInningsRetiredHurt
        {
            get { return _teamonefirstinningsretiredhurt; }
            set { _teamonefirstinningsretiredhurt = value;InvokePropertyChanged("TeamOneFirstInningsRetiredHurt");IsDirty = true; }
        }

        public int TeamTwoFirstInningsRetiredHurt
        {
            get { return _teamtwofirstinningsretiredhurt; }
            set { _teamtwofirstinningsretiredhurt = value; InvokePropertyChanged("TeamTwoFirstInningsRetiredHurt"); IsDirty = true; }
        }

        public int TeamOneSecondInningsRetiredHurt
        {
            get { return _teamonesecondinningsretiredhurt; }
            set { _teamonesecondinningsretiredhurt = value; InvokePropertyChanged("TeamOneSecondInningsRetiredHurt"); IsDirty = true; }
        }

        public int TeamTwoSecondInningsRetiredHurt
        {
            get { return _teamtwosecondinningsretiredhurt; }
            set { _teamtwosecondinningsretiredhurt = value; InvokePropertyChanged("TeamTwoSecondInningsRetiredHurt"); IsDirty = true; }
        }


        [Key]
        public Guid PlayedMatchId
        {
            get
            {
                return _playedmatchid;
            }
            set
            {
               
                        _playedmatchid = value;
                        IsDirty = true;
                        this.InvokePropertyChanged ("PlayedMatchId");
                   
            }
        }

        [ForeignKey("FixtureId")]
       [Parent]
        public virtual FixtureEntity Fixture
        {
            get
            {
                return _fixture;
            }
            set
            {
               
                        _fixture = value;
                      IsDirty = true;
                        this.InvokePropertyChanged ("Fixture");
                  
            }
        }
        [Required]
        public Guid FixtureId
        {
            get
            {
                return _fixtureid;
            }
             set
            {
                _fixtureid = value;
                IsDirty = true;
                this.InvokePropertyChanged("FixtureId");
            }
        }
        public string TossWonBy
        {
            get
            {
                return _tosswonby;
            }
            set
            {
                
                        _tosswonby = value;
                        IsDirty = true;
                        this.InvokePropertyChanged ("TossWonBy");
                    
            }
        }
        public string ElectedTo
        {
            get
            {
                return _electedto;
            }
            set
            {
                
                        _electedto = value;
                        IsDirty = true;
                        this.InvokePropertyChanged ("ElectedTo");
                    
            }
        }
        public string TeamOneFirstInningsCommencedAt
        {
            get
            {
                return _teamonefirstinningscommencedat;
            }
            set
            {
                
                        _teamonefirstinningscommencedat = value;
                        IsDirty = true;
                        this.InvokePropertyChanged ("TeamOneFirstInningsCommencedAt");
                  
            }
        }
        public string TeamOneFirstInningsClosedAt
        {
            get
            {
                return _teamonefirstinningsclosedat;
            }
            set
            {
                
                        _teamonefirstinningsclosedat = value;
                        IsDirty = true;
                        this.InvokePropertyChanged ("TeamOneFirstInningsClosedAt");
                    
            }
        }
        public string TeamOneFirstInningsDuration
        {
            get
            {
                return _teamonefirstinningsduration;
            }
            set
            {
                
                        _teamonefirstinningsduration = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOneFirstInningsDuration");
            }
        }

        public Guid Result
        {
            get
            {
                return _result;
            }
            set
            {
               
                        _result = value;
                        IsDirty = true;
                this.InvokePropertyChanged("Result");
            }
        }

        public bool IsTeamOneFirstInningsNotPlayed
        {
            get { return _isteamonefirstinningsnotplayed; }
            set { _isteamonefirstinningsnotplayed = value; IsDirty = true; InvokePropertyChanged("IsTeamOneFirstInningsNotPlayed"); }
        }

        public bool IsTeamTwoFirstInningsNotPlayed
        {
            get { return _isteamtwofirstinningsnotplayed ; }
            set { _isteamtwofirstinningsnotplayed = value; IsDirty = true; InvokePropertyChanged("IsTeamTwoFirstInningsNotPlayed"); }
        }

        public bool IsTeamOneSecondInningsNotPlayed
        {
            get { return _isteamonesecondinningsnotplayed ; }
            set { _isteamonesecondinningsnotplayed = value; IsDirty = true; InvokePropertyChanged("IsTeamOneSecondInningsNotPlayed"); }
        }


        public bool IsTeamTwoSecondInningsNotPlayed
        {
            get { return _isteamtwosecondinningsnotplayed; }
            set { _isteamtwosecondinningsnotplayed = value; IsDirty = true; InvokePropertyChanged("IsTeamTwoSecondInningsNotPlayed"); }
        }


        public bool IsWalkOverForTeamOne
        {
            get { return _iswalkoverforteamone ; }
            set { _iswalkoverforteamone = value; IsDirty = true; InvokePropertyChanged("IsWalkOverForTeamOne"); }
        }


        public bool IsWalkOverForTeamTwo
        {
            get { return _iswalkoverforteamtwo ; }
            set { _iswalkoverforteamtwo = value; IsDirty = true; InvokePropertyChanged("IsWalkOverForTeamTwo"); }
        }


        public int TeamOneFirstInningsScore
        {
            get
            {
                return _teamonefirstinningsscore;
            }
            set
            {
               
                        _teamonefirstinningsscore = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOneFirstInningsScore");
            }
        }
        public int TeamOneSecondInningsScore
        {
            get
            {
                return _teamonesecondinningsscore;
            }
            set
            {
               
                        _teamonesecondinningsscore = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOneSecondInningsScore");
            }
        }
        public int TeamTwoSecondInningsScore
        {
            get
            {
                return _teamtwosecondinningsscore;
            }
            set
            {
               
                        _teamtwosecondinningsscore = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoSecondInningsScore");
            }
        }
        public int TeamTwoFirstInningsScore
        {
            get
            {
                return _teamtwofirstinningsscore;
            }
            set
            {
               
                        _teamtwofirstinningsscore = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoFirstInningsScore");
            }
        }
        public int TeamOneFirstInningsByes
        {
            get
            {
                return _teamonefirstinningsbyes;
            }
            set
            {
              
                        _teamonefirstinningsbyes = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOneFirstInningsByes");
            }
        }
        public int TeamOneSecondInningsByes
        {
            get
            {
                return _teamonesecondinningsbyes;
            }
            set
            {
               
                        _teamonesecondinningsbyes = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOneSecondInningsByes");
            }
        }
        public int TeamTwoSecondInningsByes
        {
            get
            {
                return _teamtwosecondinningsbyes;
            }
            set
            {
               
                        _teamtwosecondinningsbyes = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoSecondInningsByes");
            }
        }
        public int TeamTwoFirstInningsByes
        {
            get
            {
                return _teamtwofirstinningsbyes;
            }
            set
            {
                
                        _teamtwofirstinningsbyes = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoFirstInningsByes");
            }
        }
        public int TeamOneFirstInningsLegByes
        {
            get
            {
                return _teamonefirstinningslegbyes;
            }
            set
            {
                
                        _teamonefirstinningslegbyes = value;
                        IsDirty = true;
                this.InvokePropertyChanged ("TeamOneFirstInningsLegByes");
            }
        }
        public int TeamOneSecondInningsLegByes
        {
            get
            {
                return _teamonesecondinningslegbyes;
            }
            set
            {
               
                        _teamonesecondinningslegbyes = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOneSecondInningsLegByes");
            }
        }
        public int TeamTwoSecondInningsLegByes
        {
            get
            {
                return _teamtwosecondinningslegbyes;
            }
            set
            {
               
                        _teamtwosecondinningslegbyes = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoSecondInningsLegByes");
                    
            }
        }
        public int TeamTwoFirstInningsLegByes
        {
            get
            {
                return _teamtwofirstinningslegbyes;
            }
            set
            {

                _teamtwofirstinningslegbyes = value;
                IsDirty = true;
                this.InvokePropertyChanged("TeamTwoFirstInningsLegByes");
            }
        }
        public int TeamOneFirstInningsP
        {
            get
            {
                return _teamonefirstinningsp;
            }
            set
            {
               
                        _teamonefirstinningsp = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOneFirstInningsP");
            }
        }
        public int TeamOneSecondInningsP
        {
            get
            {
                return _teamonesecondinningsp;
            }
            set
            {
                
                        _teamonesecondinningsp = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOneSecondInningsP");
            }
        }
        public int TeamTwoSecondInningsP
        {
            get
            {
                return _teamtwosecondinningsp;
            }
            set
            {
                
                        _teamtwosecondinningsp = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoSecondInningsP");
            }
        }
        public int TeamTwoFirstInningsP
        {
            get
            {
                return _teamtwofirstinningsp;
            }
            set
            {
                        _teamtwofirstinningsp = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoFirstInningsP");
            }
        }
        public int TeamOneFirstInningsRO
        {
            get
            {
                return _teamonefirstinningsro;
            }
            set
            {
                
                        _teamonefirstinningsro = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOneFirstInningsRO");
            }
        }
        public int TeamOneSecondInningsRO
        {
            get
            {
                return _teamonesecondinningsro;
            }
            set
            {
                
                        _teamonesecondinningsro = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOneSecondInningsRO");
            }
        }
        public int TeamTwoSecondInningsRO
        {
            get
            {
                return _teamtwosecondinningsro;
            }
            set
            {
               
                        _teamtwosecondinningsro = value;
                        IsDirty = true;
                this.InvokePropertyChanged ("TeamTwoSecondInningsRO");
            }
        }
        public int TeamTwoFirstInningsRO
        {
            get
            {
                return _teamtwofirstinningsro;
            }
            set
            {
               
                        _teamtwofirstinningsro = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoFirstInningsRO");
            }
        }
        public int TeamOneFirstInningsTotalExtras
        {
            get
            {
                return _teamonefirstinningstotalextras;
            }
            set
            {
               
                        _teamonefirstinningstotalextras = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOneFirstInningsTotalExtras");
            }
        }
        public int TeamOneSecondInningsTotalExtras
        {
            get
            {
                return _teamonesecondinningstotalextras;
            }
            set
            {
               
                        _teamonesecondinningstotalextras = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOneSecondInningsTotalExtras");
            }
        }
        public int TeamTwoSecondInningsTotalExtras
        {
            get
            {
                return _teamtwosecondinningstotalextras;
            }
            set
            {
                
                        _teamtwosecondinningstotalextras = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoSecondInningsTotalExtras");
            }
        }
        public int TeamTwoFirstInningsTotalExtras
        {
            get
            {
                return _teamtwofirstinningstotalextras;
            }
            set
            {
                
                        _teamtwofirstinningstotalextras = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoFirstInningsTotalExtras");
            }
        }
        [Required]
        public Guid TeamOneId
        {
            get
            {
                return _teamoneid;
            }
            set
            {
               
                        _teamoneid = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOneId");
            }
        }
        [ForeignKey("TeamOneId")]
       
        public virtual TeamEntity TeamOne
        {
            get
            {
                return _teamone ;
            }
            set
            {
               
                        _teamone = value;
                         IsDirty = true;
                this.InvokePropertyChanged("TeamOne");
            }
        }
        [ForeignKey("TeamTwoId")]
       
        public virtual TeamEntity TeamTwo
        {
            get
            {
                return _teamtwo;
            }
            set
            {
                
                        _teamtwo = value;
                      IsDirty = true;
                this.InvokePropertyChanged("TeamTwo");
            }
        }

        [Required]
        public Guid TeamTwoId
        {
            get
            {
                return _teamtwoid;
            }
            set
            {
               
                        _teamtwoid = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoId");
            }
        }
        public int TeamOnePoints
        {
            get
            {
                return _teamonepoints;
            }
            set
            {
                
                        _teamonepoints = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOnePoints");
            }
        }
        public int TeamTwoPoints
        {
            get
            {
                return _teamtwopoints;
            }
            set
            {
                
                        _teamtwopoints = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoPoints");
            }
        }
        public string TeamTwoFirstInningsCommencedAt
        {
            get
            {
                return _teamtwofirstinningscommencedat;
            }
            set
            {
                
                        _teamtwofirstinningscommencedat = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoFirstInningsCommencedAt");
                    
            }
        }
        public string TeamTwoFirstInningsClosedAt
        {
            get
            {
                return _teamtwofirstinningsclosedat;
            }
            set
            {
                
                        _teamtwofirstinningsclosedat = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoFirstInningsClosedAt");
            }
        }
        public string TeamTwoFirstInningsDuration
        {
            get
            {
                return _teamtwofirstinningsduration;
            }
            set
            {
                
                        _teamtwofirstinningsduration = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoFirstInningsDuration");
            }
        }
        public string TeamOneSecondInningsCommencedAt
        {
            get
            {
                return _teamonesecondinningscommencedat;
            }
            set
            {
                
                        _teamonesecondinningscommencedat = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOneSecondInningsCommencedAt");
                   
            }
        }
        public  string TeamOneSecondInningsClosedAt
        {
            get
            {
                return _teamonesecondinningsclosedat;
            }
            set
            {
               
                        _teamonesecondinningsclosedat = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOneSecondInningsClosedAt");
            }
        }
        public string TeamOneSecondInningsDuration
        {
            get
            {
                return _teamonesecondinningsduration;
            }
            set
            {
               
                        _teamonesecondinningsduration = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOneSecondInningsDuration");
            }
        }
        
        public string TeamTwoSecondInningsCommencedAt
        {
            get
            {
                return _teamtwosecondinningscommencedat;
            }
            set
            {
               
                        _teamtwosecondinningscommencedat = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoSecondInningsCommencedAt");
            }
        }
        public string TeamTwoSecondInningsClosedAt
        {
            get
            {
                return _teamtwosecondinningsclosedat;
            }
            set
            {
                
                        _teamtwosecondinningsclosedat = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoSecondInningsClosedAt");
            }
        }
        public string TeamTwoSecondInningsDuration
        {
            get
            {
                return _teamtwosecondinningsduration;
            }
            set
            {
                
                        _teamtwosecondinningsduration = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoSecondInningsDuration");
            }
        }
        public string TeamOneFirstInningsOvers
        {
            get
            {
                return _teamonefirstinningsovers;
            }
            set
            {
               
                        _teamonefirstinningsovers = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOneFirstInningsOvers");
            }
        }
        public string TeamTwoFirstInningsOvers
        {
            get
            {
                return _teamtwofirstinningsovers;
            }
            set
            {
               
                        _teamtwofirstinningsovers = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoFirstInningsOvers");
            }
        }
        public string TeamTwoSecondInningsOvers
        {
            get
            {
                return _teamtwosecondinningsovers;
            }
            set
            {
               
                        _teamtwosecondinningsovers = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoSecondInningsOvers");
            }
        }
        public string TeamOneSecondInningsOvers
        {
            get
            {
                return _teamonesecondinningsovers;
            }
            set
            {
               
                        _teamonesecondinningsovers = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOneSecondInningsOvers");
                   
            }
        }
        public string ResultRemarks
        {
            get
            {
                return _resultremarks;
            }
            set
            {
                
                        _resultremarks = value;
                        IsDirty = true;
                this.InvokePropertyChanged("ResultRemarks");
            }
        }
        public int TeamOneFirstInningsTotalWickets
        {
            get
            {
                return _teamonefirstinningstotalwickets;
            }
            set
            {
                
                        _teamonefirstinningstotalwickets = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOneFirstInningsTotalWickets");
            }
        }
        public int TeamTwoFirstInningsTotalWickets
        {
            get
            {
                return _teamtwofirstinningstotalwickets;
            }
            set
            {
               
                        _teamtwofirstinningstotalwickets = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoFirstInningsTotalWickets");
            }
        }
        public int TeamTwoSecondInningsTotalWickets
        {
            get
            {
                return _teamtwosecondinningstotalwickets;
            }
            set
            {
                
                        _teamtwosecondinningstotalwickets = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoSecondInningsTotalWickets");
            }
        }
        public int TeamOneSecondInningsTotalWickets
        {
            get
            {
                return _teamonesecondinningstotalwickets;
            }
            set
            {
                        _teamonesecondinningstotalwickets = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOneSecondInningsTotalWickets");
            }
        }
        public int TeamOneFirstInningsWides
        {
            get
            {
                return _teamonefirstinningswides;
            }
            set
            {
                
                        _teamonefirstinningswides = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOneFirstInningsWides");
            }
        }
        public int TeamTwoFirstInningsWides
        {
            get
            {
                return _teamtwofirstinningswides;
            }
            set
            {
                
                        _teamtwofirstinningswides = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoFirstInningsWides");
            }
        }
        public int TeamTwoSecondInningsWides
        {
            get
            {
                return _teamtwosecondinningswides;
            }
            set
            {
                
                        _teamtwosecondinningswides = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoSecondInningsWides");
            }
        }
        public int TeamOneSecondInningsWides
        {
            get
            {
                return _teamonesecondinningswides;
            }
            set
            {
               
                        _teamonesecondinningswides = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOneSecondInningsWides");
            }
        }
        public int TeamOneFirstInningsNoBalls
        {
            get
            {
                return _teamonefirstinningsnoballs;
            }
            set
            {
                
                        _teamonefirstinningsnoballs = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOneFirstInningsNoBalls");
            }
        }
        public int TeamTwoFirstInningsNoBalls
        {
            get
            {
                return _teamtwofirstinningsnoballs;
            }
            set
            {
                
                        _teamtwofirstinningsnoballs = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoFirstInningsNoBalls");
            }
        }
        public int TeamTwoSecondInningsNoBalls
        {
            get
            {
                return _teamtwosecondinningsnoballs;
            }
            set
            {
                
                        _teamtwosecondinningsnoballs = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoSecondInningsNoBalls");
            }
        }
        public int TeamOneSecondInningsNoBalls
        {
            get
            {
                return _teamonesecondinningsnoballs;
            }
            set
            {
               
                        _teamonesecondinningsnoballs = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOneSecondInningsNoBalls");
            }
        }
        public int TeamOneFirstInningsCatches
        {
            get
            {
                return _teamonefirstinningscatches;
            }
            set
            {
               
                        _teamonefirstinningscatches = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOneFirstInningsCatches");
            }
        }
        public int TeamTwoFirstInningsCatches
        {
            get
            {
                return _teamtwofirstinningscatches;
            }
            set
            {
               
                        _teamtwofirstinningscatches = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoFirstInningsCatches");
            }
        }
        public int TeamTwoSecondInningsCatches
        {
            get
            {
                return _teamtwosecondinningscatches;
            }
            set
            {
                
                        _teamtwosecondinningscatches = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoSecondInningsCatches");
            }
        }
        public int TeamOneSecondInningsCatches
        {
            get
            {
                return _teamonesecondinningscatches;
            }
            set
            {
                
                        _teamonesecondinningscatches = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOneSecondInningsCatches");
                    
            }
        }
        public int TeamOneFirstInningsLbws
        {
            get
            {
                return _teamonefirstinningslbws;
            }
            set
            {
                
                        _teamonefirstinningslbws = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOneFirstInningsLbws");
            }
        }
        public int TeamTwoFirstInningsLbws
        {
            get
            {
                return _teamtwofirstinningslbws;
            }
            set
            {
                
                        _teamtwofirstinningslbws = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoFirstInningsLbws");
            }
        }
        public int TeamTwoSecondInningsLbws
        {
            get
            {
                return _teamtwosecondinningslbws;
            }
            set
            {
                
                        _teamtwosecondinningslbws = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoSecondInningsLbws");
            }
        }
        public int TeamOneSecondInningsLbws
        {
            get
            {
                return _teamonesecondinningslbws;
            }
            set
            {
                
                        _teamonesecondinningslbws = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOneSecondInningsLbws");
            }
        }
        public int TeamOneSecondInningsBowled
        {
            get
            {
                return _teamonesecondinningsbowled;
            }
            set
            {
                
                        _teamonesecondinningsbowled = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOneSecondInningsBowled");
            }
        }
        public int TeamTwoSecondInningsBowled
        {
            get
            {
                return _teamtwosecondinningsbowled;
            }
            set
            {
               
                        _teamtwosecondinningsbowled = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoSecondInningsBowled");
            }
        }
        public int TeamTwoFirstInningsBowled
        {
            get
            {
                return _teamtwofirstinningsbowled;
            }
            set
            {
                
                        _teamtwofirstinningsbowled = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoFirstInningsBowled");
            }
        }
        public int TeamOneFirstInningsBowled
        {
            get
            {
                return _teamonefirstinningsbowled;
            }
            set
            {
               
                        _teamonefirstinningsbowled = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOneFirstInningsBowled");
            }
        }
        public bool CompleteStatus
        {
            get
            {
                return _completestatus;
            }
            set
            {
                
                        _completestatus = value;
                        IsDirty = true;
                this.InvokePropertyChanged("CompleteStatus");
            }
        }
        public int TeamOneFirstInningsStumped
        {
            get
            {
                return _teamonefirstinningsstumped;
            }
            set
            {
                
                        _teamonefirstinningsstumped = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOneFirstInningsStumped");
                    
            }
        }
        public int TeamTwoFirstInningsStumped
        {
            get
            {
                return _teamtwofirstinningsstumped;
            }
            set
            {
                
                        _teamtwofirstinningsstumped = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoFirstInningsStumped");
            }
        }
        public int TeamTwoSecondInningsStumped
        {
            get
            {
                return _teamtwosecondinningsstumped;
            }
            set
            {
                
                        _teamtwosecondinningsstumped = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoSecondInningsStumped");
            }
        }
        public int TeamOneSecondInningsStumped
        {
            get
            {
                return _teamonesecondinningsstumped;
            }
            set
            {
               
                        _teamonesecondinningsstumped = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOneSecondInningsStumped");
            }
        }
        public int TeamOneSecondInningsHitWickets
        {
            get
            {
                return _teamonesecondinningshitwickets;
            }
            set
            {
                
                        _teamonesecondinningshitwickets = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOneSecondInningsHitWickets");
                    
            }
        }
        public int TeamTwoSecondInningsHitWickets
        {
            get
            {
                return _teamtwosecondinningshitwickets;
            }
            set
            {
               
                        _teamtwosecondinningshitwickets = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoSecondInningsHitWickets");
            }
        }
        public int TeamTwoFirstInningsHitWickets
        {
            get
            {
                return _teamtwofirstinningshitwickets;
            }
            set
            {
               
                        _teamtwofirstinningshitwickets = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamTwoFirstInningsHitWickets");
            }
        }
        public int TeamOneFirstInningsHitWickets
        {
            get
            {
                return _teamonefirstinningshitwickets;
            }
            set
            {
                
                        _teamonefirstinningshitwickets = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamOneFirstInningsHitWickets");
            }
        }

        public virtual bool ValidateEntity()
        {
            if (this.Indicator != RecordStatus.Deleted && this.Indicator != RecordStatus.DeletedNotSaved)
            {
                if (_playedmatchid == null || _playedmatchid == Guid.Empty)
                {
                    throw new Exception("No reference set or key filed is empty for PlayedMatchId ,enter value to continue");
                }
            }
            return true;
        }
    }
}
