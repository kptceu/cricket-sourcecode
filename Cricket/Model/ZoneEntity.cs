using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Data.OleDb;
using System.ComponentModel.DataAnnotations;

namespace Cricket.Model
{
    [Serializable]
    public class ZoneEntity : BaseEntity
    {
        private Guid _zoneid;
        private string _zonename;
        private int _zonenumber;
        [Key]
        public Guid ZoneId
        {
            get
            {
                return _zoneid;
            }
            set
            {
                
                        _zoneid = value;
                        IsDirty = true;
                this.InvokePropertyChanged("ZoneId");
            }
        }


        public string ZoneName
        {
            get
            {
                return _zonename;
            }
            set
            {
               
                        _zonename = value;
                        IsDirty = true;
                this.InvokePropertyChanged("ZoneName");
            }
        }

        [Identifier]
        public int ZoneNumber
        {
            get
            {
                return _zonenumber;
            }
            set
            {
                
                        _zonenumber = value;
                        IsDirty = true;
                this.InvokePropertyChanged("ZoneNumber");
            }
        }
        public virtual bool ValidateEntity()
        {
            if (this.Indicator != RecordStatus.Deleted && this.Indicator != RecordStatus.DeletedNotSaved)
            {
                if (_zoneid == null || _zoneid == Guid.Empty)
                {
                    throw new Exception("No reference set or key filed is empty for ZoneId ,enter value to continue");
                }
                if (!(String.IsNullOrEmpty(ZoneName)))
                {
                    if (ZoneName.Length > 255)
                    {
                        throw new Exception("Filed length for ZoneName exceeds max length, maxlenth is 255");
                    }
                }
            }
            return true;
        }
    }
}
