using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Data.OleDb;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cricket.Model
{
    [Serializable]
    public class PlayedMatchDetailEntity : BaseEntity
    {
        private Guid _playedmatchdetailid;
        private FixtureEntity _fixture;
        private Guid _fixtureid;
        private PlayerEntity _player;
        private Guid _playerid;
        private string _firstinningsdismissal;
        private int _firstinningsrunsscored;
        private int _firstinningsminutes;
        private int _firstinningsballsfaced;
        private int _firstinningsfours;
        private int _firstinningssixes;
        private int _battingorder;
        private double _firstinningsoversbowled;
        private int _firstinningsmaidens;
        private int _firstinningsrunsgiven;
        private int _firstinningswickets;
        private int _firstinningsnoballs;
        private int _firstinningswides;
        private string _secondinningsdismissal;
        private int _secondinningsrunsscored;
        private int _secondinningsminutes;
        public int SecondInningsMinutes
        {
            get { return _secondinningsminutes; }
            set { _secondinningsminutes = value;InvokePropertyChanged("SecondInningsMinutes"); }
        }
        private int _secondinningsballsfaced;
        private int _secondinningsfours;
        private int _secondinningssixes;
        private double _secondinningsoversbowled;
        private int _secondinningsmaidens;
        private int _secondinningsrunsgiven;
        private int _secondinningswickets;
        private int _secondinningsnoballs;
        private int _secondinningswides;
        private double _firstinningseconomy;
        private double _secondinningseconomy;
        private string _playername;
        private Guid _teamid;
        private TeamEntity _team;

        [Key]
        public Guid PlayedMatchDetailId
        {
            get
            {
                return _playedmatchdetailid;
            }
            set
            {
               
                        _playedmatchdetailid = value;
                        IsDirty = true;
                this.InvokePropertyChanged("PlayedMatchDetailId");
            }
        }
        [ForeignKey("FixtureId")]
       [Parent ]
        public virtual FixtureEntity Fixture
        {
            get
            {
                return _fixture;
            }
            set
            {
                
                        _fixture = value;
                       IsDirty = true;
                this.InvokePropertyChanged("Fixture");
            }
        }
        [Required]
        public Guid FixtureId
        {
            get
            {
                return _fixtureid;
            }
            set
            {
                _fixtureid = value;
            }
        }
        [ForeignKey("PlayerId")]
       
        public virtual PlayerEntity Player
        {
            get
            {
                return _player;
            }
            set
            {
                
                        _player = value;
                        IsDirty = true;
                this.InvokePropertyChanged("Player");
            }
        }
        [Required]
        public Guid PlayerId
        {
            get
            {
                return _playerid;
            }
            set
            {
                _playerid = value;
            }
        }
        public string FirstInningsDismissal
        {
            get
            {
                return _firstinningsdismissal;
            }
            set
            {
               
                        _firstinningsdismissal = value;
                        IsDirty = true;
                this.InvokePropertyChanged("FirstInningsDismissal");
            }
        }
        public int FirstInningsRunsScored
        {
            get
            {
                return _firstinningsrunsscored;
            }
            set
            {
               
                        _firstinningsrunsscored = value;
                        IsDirty = true;
                this.InvokePropertyChanged("FirstInningsRunsScored");
            }
        }
        public int FirstInningsMinutes
        {
            get
            {
                return _firstinningsminutes;
            }
            set
            {
                
                        _firstinningsminutes = value;
                        IsDirty = true;
                this.InvokePropertyChanged("FirstInningsMinutes");
            }
        }
        public int FirstInningsBallsFaced
        {
            get
            {
                return _firstinningsballsfaced;
            }
            set
            {
               
                        _firstinningsballsfaced = value;
                        IsDirty = true;
                this.InvokePropertyChanged("FirstInningsBallsFaced");
            }
        }
        public int FirstInningsFours
        {
            get
            {
                return _firstinningsfours;
            }
            set
            {
                
                        _firstinningsfours = value;
                        IsDirty = true;
                this.InvokePropertyChanged("FirstInningsFours");
            }
        }
        public int FirstInningsSixes
        {
            get
            {
                return _firstinningssixes;
            }
            set
            {
                
                        _firstinningssixes = value;
                        IsDirty = true;
                this.InvokePropertyChanged("FirstInningsSixes");
            }
        }
        public int BattingOrder
        {
            get
            {
                return _battingorder;
            }
            set
            {
                
                        _battingorder = value;
                        IsDirty = true;
                this.InvokePropertyChanged("BattingOrder");
            }
        }
        public double FirstInningsOversBowled
        {
            get
            {
                return _firstinningsoversbowled;
            }
            set
            {
                
                        _firstinningsoversbowled = value;
                        IsDirty = true;
                this.InvokePropertyChanged("FirstInningsOversBowled");
            }
        }
        public int FirstInningsMaidens
        {
            get
            {
                return _firstinningsmaidens;
            }
            set
            {
                
                        _firstinningsmaidens = value;
                        IsDirty = true;
                this.InvokePropertyChanged("FirstInningsMaidens");
            }
        }
        public int FirstInningsRunsGiven
        {
            get
            {
                return _firstinningsrunsgiven;
            }
            set
            {
                
                        _firstinningsrunsgiven = value;
                        IsDirty = true;
                this.InvokePropertyChanged("FirstInningsRunsGiven");
            }
        }
        public int FirstInningsWickets
        {
            get
            {
                return _firstinningswickets;
            }
            set
            {
                
                        _firstinningswickets = value;
                        IsDirty = true;
                this.InvokePropertyChanged("FirstInningsWickets");
            }
        }
        public int FirstInningsNoBalls
        {
            get
            {
                return _firstinningsnoballs;
            }
            set
            {
                
                        _firstinningsnoballs = value;
                        IsDirty = true;
                this.InvokePropertyChanged("FirstInningsNoBalls");
            }
        }
        public int FirstInningsWides
        {
            get
            {
                return _firstinningswides;
            }
            set
            {
               
                        _firstinningswides = value;
                        IsDirty = true;
                this.InvokePropertyChanged("FirstInningsWides");
            }
        }
        public string SecondInningsDismissal
        {
            get
            {
                return _secondinningsdismissal;
            }
            set
            {
                
                        _secondinningsdismissal = value;
                        IsDirty = true;
                this.InvokePropertyChanged("SecondInningsDismissal");
            }
        }
        public int SecondInningsRunsScored
        {
            get
            {
                return _secondinningsrunsscored;
            }
            set
            {
                
                        _secondinningsrunsscored = value;
                        IsDirty = true;
                this.InvokePropertyChanged("SecondInningsRunsScored");
            }
        }
        public int SecondInningsBallsFaced
        {
            get
            {
                return _secondinningsballsfaced;
            }
            set
            {
                
                        _secondinningsballsfaced = value;
                        IsDirty = true;
                this.InvokePropertyChanged("SecondInningsBallsFaced");
            }
        }
        public int SecondInningsFours
        {
            get
            {
                return _secondinningsfours;
            }
            set
            {
                
                        _secondinningsfours = value;
                        IsDirty = true;
                this.InvokePropertyChanged("SecondInningsFours");
            }
        }
        public int SecondInningsSixes
        {
            get
            {
                return _secondinningssixes;
            }
            set
            {
                
                        _secondinningssixes = value;
                        IsDirty = true;
                this.InvokePropertyChanged("SecondInningsSixes");
            }
        }
        public double SecondInningsOversBowled
        {
            get
            {
                return _secondinningsoversbowled;
            }
            set
            {
                
                        _secondinningsoversbowled = value;
                        IsDirty = true;
                this.InvokePropertyChanged("SecondInningsOversBowled");
            }
        }
        public int SecondInningsMaidens
        {
            get
            {
                return _secondinningsmaidens;
            }
            set
            {
                
                        _secondinningsmaidens = value;
                        IsDirty = true;
                this.InvokePropertyChanged("SecondInningsMaidens");
            }
        }
        public int SecondInningsRunsGiven
        {
            get
            {
                return _secondinningsrunsgiven;
            }
            set
            {
                
                        _secondinningsrunsgiven = value;
                        IsDirty = true;
                this.InvokePropertyChanged("SecondInningsRunsGiven");
            }
        }
        public int SecondInningsWickets
        {
            get
            {
                return _secondinningswickets;
            }
            set
            {
                
                        _secondinningswickets = value;
                        IsDirty = true;
                this.InvokePropertyChanged("SecondInningsWickets");
            }
        }
        public int SecondInningsNoBalls
        {
            get
            {
                return _secondinningsnoballs;
            }
            set
            {
                
                        _secondinningsnoballs = value;
                        IsDirty = true;
                this.InvokePropertyChanged("SecondInningsNoBalls");
            }
        }
        public int SecondInningsWides
        {
            get
            {
                return _secondinningswides;
            }
            set
            {
                
                        _secondinningswides = value;
                        IsDirty = true;
                this.InvokePropertyChanged("SecondInningsWides");
            }
        }
        public double FirstInningsEconomy
        {
            get
            {
                return _firstinningseconomy;
            }
            set
            {
                
                        _firstinningseconomy = value;
                        IsDirty = true;
                this.InvokePropertyChanged("FirstInningsEconomy");
            }
        }
        public double SecondInningsEconomy
        {
            get
            {
                return _secondinningseconomy;
            }
            set
            {
                
                        _secondinningseconomy = value;
                        IsDirty = true;
                this.InvokePropertyChanged("SecondInningsEconomy");
            }
        }
        public string PlayerName
        {
            get
            {
                return _playername;
            }
            set
            {
                
                        _playername = value;
                        IsDirty = true;
                this.InvokePropertyChanged("PlayerName");
            }
        }
        [Required]
        public Guid TeamId
        {
            get
            {
                return _teamid;
            }
            set
            {
                
                        _teamid = value;
                        IsDirty = true;
                this.InvokePropertyChanged("TeamId");
            }
        }

        [ForeignKey("TeamId")]
       
        public virtual TeamEntity Team
        {
            get
            {
                return _team ;
            }
            set
            {
                        _team = value;
                        IsDirty = true;
                this.InvokePropertyChanged("Team");
            }
        }
        
        
       
      


        public virtual bool ValidateEntity()
        {
            if (this.Indicator != RecordStatus.Deleted && this.Indicator != RecordStatus.DeletedNotSaved)
            {
                if (_playedmatchdetailid == null || _playedmatchdetailid == Guid.Empty)
                {
                    throw new Exception("No reference set or key filed is empty for PlayedMatchDetailId ,enter value to continue");
                }
            }
            return true;
        }
    }
}
