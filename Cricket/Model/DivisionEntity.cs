using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Data.OleDb;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cricket.Model
{
    [Serializable]
    public class DivisionEntity : BaseEntity
    {
        private Guid _divisionid;
        private Guid _seasonid;
        private string _divisionname;
        private int _numberofinningsperside;
        private int _maxoverperinnings;
        private int _numberofdays;
        private int _ballsrequiredpermatch;
        private string _ballname;
        private int _serial;
        

        [Key]
        public Guid DivisionId
        {
            get
            {
                return _divisionid;
            }
            set
            {

                _divisionid = value;
                IsDirty = true;
                this.InvokePropertyChanged("DivisionId");

            }
        }

        public string DivisionName
        {
            get
            {
                return _divisionname;
            }
            set
            {
                _divisionname = value;
                IsDirty = true;
                this.InvokePropertyChanged("DivisionName");
            }
        }
        public int NumberOfInningsPerSide
        {
            get
            {
                return _numberofinningsperside;
            }
            set
            {
                _numberofinningsperside = value;
                IsDirty = true;
                this.InvokePropertyChanged("NumberOfInningsPerSide");
            }
        }
        public int MaxOverPerInnings
        {
            get
            {
                return _maxoverperinnings;
            }
            set
            {

                _maxoverperinnings = value;
                IsDirty = true;
                this.InvokePropertyChanged("MaxOverPerInnings");

            }
        }
        public int NumberOfDays
        {
            get
            {
                return _numberofdays;
            }
            set
            {

                _numberofdays = value;
                IsDirty = true;
                this.InvokePropertyChanged("NumberOfDays");

            }
        }
        public int BallsRequiredPerMatch
        {
            get
            {
                return _ballsrequiredpermatch;
            }
            set
            {

                _ballsrequiredpermatch = value;
                IsDirty = true;
                this.InvokePropertyChanged("BallsRequiredPerMatch");

            }
        }
        public string BallName
        {
            get
            {
                return _ballname;
            }
            set
            {

                _ballname = value;
                IsDirty = true;
                this.InvokePropertyChanged("BallName");

            }
        }
        public int Serial
        {
            get
            {
                return _serial;
            }
            set
            {

                _serial = value;
                IsDirty = true;
                this.InvokePropertyChanged("Serial");

            }
        }
        public virtual bool ValidateEntity()
        {
            if (this.Indicator != RecordStatus.Deleted && this.Indicator != RecordStatus.DeletedNotSaved)
            {
                if (_divisionid == null || _divisionid == Guid.Empty)
                {
                    throw new Exception("No reference set or key filed is empty for DivisionId ,enter value to continue");
                }
                if (string.IsNullOrEmpty(this.DivisionName))
                {
                    throw new Exception("Division Name not entered ,enter Division Name to continue");
                }
                if (this.MaxOverPerInnings == 0)
                {
                    throw new Exception("Maximum number of overs for an innings not entered,enter  MaxOverPerInnings continue");
                }

                if (this.NumberOfDays == 0)
                {
                    throw new Exception("Number of days not entered,enter  Number of Days");
                }

                if (this.NumberOfInningsPerSide == 0)
                {
                    throw new Exception("Number of Innings per side not...");
                }

                if (string.IsNullOrEmpty(this.BallName))
                {
                    throw new Exception("Ball Name not entered...");
                }

                if (this.BallsRequiredPerMatch == 0)
                {
                    throw new Exception("number of Balls required per match not entered...");
                }


            }
            return true;
        }
    }
}
