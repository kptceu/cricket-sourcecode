using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Data.OleDb;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cricket.Model
{
    [Serializable]
    public class LocationEntity : BaseEntity
    {
        private Guid _locationid;
        private string _stadiumname;
        private string _locationname;
        private Guid _zoneid;
        private ZoneEntity _zone;
        private string _address;
        private PitchType _pitchtype;
        private string _groundkeyid;
        [Key]
        public Guid LocationId
        {
            get
            {
                return _locationid;
            }
            set
            {

                _locationid = value;
                IsDirty = true;
                this.InvokePropertyChanged("LocationId");

            }
        }

        private bool _isselectable;
        [IgnoreSave]
        public bool IsSelectable
        {
            get { return _isselectable; }
            set { _isselectable = value; InvokePropertyChanged("IsSelectable"); IsDirty = true; }
        }

        private string _allowedcolor;
        [IgnoreSave]
        public string AllowedColor
        {
            get { return _allowedcolor; }
            set { _allowedcolor = value; InvokePropertyChanged("AllowedColor"); }
        }

        public string StadiumName
        {
            get
            {
                return _stadiumname;
            }
            set
            {

                _stadiumname = value;
                IsDirty = true;
                this.InvokePropertyChanged("StadiumName");

            }
        }
        public string LocationName
        {
            get
            {
                return _locationname;
            }
            set
            {

                _locationname = value;
                IsDirty = true;
                this.InvokePropertyChanged("LocationName");

            }
        }
        [ForeignKey("ZoneId")]
       
        public virtual ZoneEntity CZone
        {
            get
            {
                return _zone;
            }
            set
            {

                _zone = value;
                 IsDirty = true;
                this.InvokePropertyChanged("CZone");


            }
        }
        [Required]
        public Guid ZoneId
        {
            get
            {
                return _zoneid;
            }
            set
            {
                _zoneid = value;
                IsDirty = true;
                this.InvokePropertyChanged("ZoneId");
            }
        }
        public string Address
        {
            get
            {
                return _address;
            }
            set
            {

                _address = value;
                IsDirty = true;
                this.InvokePropertyChanged("Address");

            }
        }
        public PitchType PitchType
        {
            get
            {
                return _pitchtype;
            }
            set
            {

                _pitchtype = value;
                IsDirty = true;
                this.InvokePropertyChanged("PitchType");

            }
        }
        [Identifier]
        public string GroundKeyId
        {
            get
            {
                return _groundkeyid;
            }
            set
            {

                _groundkeyid = value;
                IsDirty = true;
                this.InvokePropertyChanged("GroundKeyId");

            }
        }
        public virtual bool ValidateEntity()
        {
            if (this.Indicator != RecordStatus.Deleted && this.Indicator != RecordStatus.DeletedNotSaved)
            {
                if (_locationid == null || _locationid == Guid.Empty)
                {
                    throw new Exception("No reference set or key filed is empty for LocationId ,enter value to continue");
                }
                if (!(String.IsNullOrEmpty(StadiumName)))
                {
                    if (StadiumName.Length > 255)
                    {
                        throw new Exception("Filed length for StadiumName exceeds max length, maxlenth is 255");
                    }
                }
                else
                {
                    throw new Exception("StadiumName is not set or null, enter Stadium Name");
                }
                if (!(String.IsNullOrEmpty(LocationName)))
                {
                    if (LocationName.Length > 255)
                    {
                        throw new Exception("Filed length for LocationName exceeds max length, maxlenth is 255");
                    }
                }
                else
                {
                    throw new Exception("LocationName is not set or null, enter LocationName");
                }

                if (!(String.IsNullOrEmpty(Address)))
                {
                    if (Address.Length > 255)
                    {
                        throw new Exception("Filed length for Address exceeds max length, maxlenth is 255");
                    }
                }
                else
                {
                    throw new Exception("Address is not set or null, enter Address");
                }
                if (!(String.IsNullOrEmpty(PitchType.ToString())))
                {
                    if (PitchType.ToString().Length > 255)
                    {
                        throw new Exception("Filed length for PitchType exceeds max length, maxlenth is 255");
                    }
                }
                else
                {
                    throw new Exception("PitchType is not set or null, enter PitchType");
                }
                if (!(String.IsNullOrEmpty(GroundKeyId)))
                {
                    if (GroundKeyId.Length > 255)
                    {
                        throw new Exception("Filed length for GroundKeyId exceeds max length, maxlenth is 255");
                    }
                }
                else
                {
                    throw new Exception("GroundKeyId is not set or null, enter GroundKeyId");
                }
            }
            return true;
        }
    }
}
