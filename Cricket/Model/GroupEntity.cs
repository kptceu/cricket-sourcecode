﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cricket.Model
{
    [Serializable]
   public class GroupEntity:BaseEntity
    {
        public GroupEntity() : base()
        {

        }

        private Guid _groupid;
        [Key]
        public Guid GroupId
        {
            get { return _groupid; }
            set { _groupid = value;InvokePropertyChanged("GroupId");IsDirty = true; }
        }

        private string _groupname;
        [Identifier]
        public string GroupName
        {
            get { return _groupname; }
            set { _groupname = value; InvokePropertyChanged("GroupName"); IsDirty = true; }
        }

        
    }
}
