using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Data.OleDb;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cricket.Model
{
    [Serializable]
    public class FixtureEntity : BaseEntity
    {
        private Guid _fixtureid;
        private int _serialnumber;
        private string _groupname;
        //private Guid _groupid;
        //[Required]
        //public Guid GroupId
        //{
        //    get { return _groupid; }
        //    set { _groupid = value;InvokePropertyChanged("GroupId");IsDirty = true; }
        //}

        //private GroupEntity _group;
        //[ForeignKey("GroupId")]
        //public GroupEntity Group
        //{
        //    get { return _group; }
        //    set { _group = value; InvokePropertyChanged("Group"); IsDirty = true; }
        //}

        private Guid? _locationid;
        private LocationEntity _location;
        private Guid? _umpireoneid;
        private OfficialEntity _umpireone;
        private Guid? _umpiretwoid;
        private OfficialEntity _umpiretwo;
        private Guid? _scorerid;
        private OfficialEntity _scorer;
        private SeasonEntity _season;
        private Guid _seasonid;
        private DivisionEntity _division;
        private Guid _divisionid;
        private MatchType _matchtype;
        private DateTime? _fromdate;
        private DateTime? _todate;
        private string _matchkeyid;
        private string _day;
        private string _teams;
        private string _assigndate;
        private Guid? _teamtwoid;
        private Guid? _teamoneid;
        private TeamEntity _teamone;
        private TeamEntity _teamtwo;
        private Guid _zoneid;
        [Required]
        public Guid ZoneId
        {
            get { return _zoneid; }
            set { _zoneid = value;InvokePropertyChanged("ZoneId"); }
        }

        private ZoneEntity _zone;
        [ForeignKey("ZoneId")]
        public ZoneEntity Zone
        {
            get { return _zone; }
            set { _zone = value;InvokePropertyChanged("Zone");IsDirty = true; }
        }

        [Key]
        public Guid FixtureId
        {
            get
            {
                return _fixtureid;
            }
            set
            {

                _fixtureid = value;
                IsDirty = true;
                this.InvokePropertyChanged("FixtureId");
            }
        }
        public int SerialNumber
        {
            get
            {
                return _serialnumber;
            }
            set
            {

                _serialnumber = value;
                IsDirty = true;
                this.InvokePropertyChanged("SerialNumber");

            }
        }
        public string GroupName
        {
            get
            {
                return _groupname;
            }
            set
            {

                _groupname = value;
                IsDirty = true;
                this.InvokePropertyChanged("GroupName");

            }
        }
        [ForeignKey("LocationId")]
       
        public virtual LocationEntity Location
        {
            get
            {
                return _location;
            }
            set
            {
                _location = value;
                IsDirty = true;
                this.InvokePropertyChanged("Location");
            }
        }
        public Guid? LocationId
        {
            get
            {
                return _locationid;
            }
            set
            {

                _locationid = value;
                IsDirty = true;
                this.InvokePropertyChanged("LocationId");

            }
        }
        public Guid? UmpireOneId
        {
            get
            {
                return _umpireoneid;
            }
            set
            {

                _umpireoneid = value;
                IsDirty = true;
                this.InvokePropertyChanged("UmpireOneId");

            }
        }
        [ForeignKey("UmpireOneId")]
       
        public virtual OfficialEntity UmpireOne
        {
            get { return _umpireone; }
            set
            {

                _umpireone = value;
                IsDirty = true;
                this.InvokePropertyChanged("UmpireOne");

            }
        }
        public Guid? UmpireTwoId
        {
            get
            {
                return _umpiretwoid;
            }
            set
            {

                _umpiretwoid = value;
                IsDirty = true;
                this.InvokePropertyChanged("UmpireTwoId");

            }
        }
        [ForeignKey("UmpireTwoId")]
       
        public virtual OfficialEntity UmpireTwo
        {
            get { return _umpiretwo; }
            set
            {

                _umpiretwo = value;
                IsDirty = true;
                this.InvokePropertyChanged("UmpireTwo");

            }
        }

        public Guid? ScorerId
        {
            get
            {
                return _scorerid;
            }
            set
            {
                _scorerid = value;
                IsDirty = true;
                this.InvokePropertyChanged("ScorerId");

            }
        }
        [ForeignKey("ScorerId")]
       

        public virtual OfficialEntity Scorer
        {
            get
            {
                return _scorer;
            }
            set
            {

                _scorer = value;
                IsDirty = true;
                this.InvokePropertyChanged("Scorer");

            }
        }
        [ForeignKey("SeasonId")]
       
        public virtual SeasonEntity Season
        {
            get
            {
                return _season;
            }
            set
            {
               
                {
                    _season = value;
                    IsDirty = true;
                    this.InvokePropertyChanged("Season");
                }
            }
        }
        [Required]
        public Guid SeasonId
        {
            get
            {
                return _seasonid;
            }
            set
            {
                _seasonid = value;
                IsDirty = true;
                InvokePropertyChanged("SeasonId");
            }
        }

        [ForeignKey("DivisionId")]
       
        public virtual DivisionEntity Division
        {
            get
            {
                return _division;
            }
            set
            {

                _division = value;
                IsDirty = true;
                this.InvokePropertyChanged("Division");

            }
        }
        [Required]
        public Guid DivisionId
        {
            get
            {
                return _divisionid;
            }
            set
            {
                _divisionid = value;
                IsDirty = true;
                InvokePropertyChanged("DivisionId");
            }
        }
        public MatchType MatchType
        {
            get
            {
                return _matchtype;
            }
            set
            {

                _matchtype = value;
                IsDirty = true;
                this.InvokePropertyChanged("MatchType");

            }
        }
        public DateTime? FromDate
        {
            get
            {
                return _fromdate;
            }
            set
            {

                _fromdate = value;
                IsDirty = true;
                this.InvokePropertyChanged("FromDate");

            }
        }
        public DateTime? ToDate
        {
            get
            {
                return _todate;
            }
            set
            {

                _todate = value;
                IsDirty = true;
                this.InvokePropertyChanged("ToDate");

            }
        }

        [Identifier]
        public  string MatchKeyId
        {
            get
            {
                return _matchkeyid;
            }
            set
            {

                _matchkeyid = value;
                IsDirty = true;
                this.InvokePropertyChanged("MatchKeyId");

            }
        }
        public string Day
        {
            get
            {
                return _day;
            }
            set
            {

                _day = value;
                IsDirty = true;
                this.InvokePropertyChanged("Day");

            }
        }
        public string Teams
        {
            get
            {
                return _teams;
            }
            set
            {

                _teams = value;
                IsDirty = true;
                this.InvokePropertyChanged("Teams");

            }
        }
        public string AssignDate
        {
            get
            {
                return _assigndate;
            }
            set
            {

                _assigndate = value;
                IsDirty = true;
                this.InvokePropertyChanged("AssignDate");

            }
        }
        public Guid? TeamTwoId
        {
            get
            {
                return _teamtwoid;
            }
            set
            {

                _teamtwoid = value;
                IsDirty = true;
                this.InvokePropertyChanged("TeamTwoId");

            }
        }
        public Guid? TeamOneId
        {
            get
            {
                return _teamoneid;
            }
            set
            {

                _teamoneid = value;
                IsDirty = true;
                this.InvokePropertyChanged("TeamOneId");

            }
        }
        [ForeignKey("TeamTwoId")]
       
        public virtual TeamEntity TeamTwo
        {
            get
            {
                return _teamtwo;
            }
            set
            {

                _teamtwo = value;
                IsDirty = true;
                this.InvokePropertyChanged("TeamTwo");

            }
        }
        [ForeignKey("TeamOneId")]
       
        public virtual TeamEntity TeamOne
        {
            get
            {
                return _teamone;
            }
            set
            {

                _teamone = value;
                IsDirty = true;
                this.InvokePropertyChanged("TeamOne");

            }
        }

        public virtual bool ValidateEntity()
        {
            if (this.Indicator != RecordStatus.Deleted && this.Indicator != RecordStatus.DeletedNotSaved)
            {
                if (_fixtureid == null || _fixtureid == Guid.Empty)
                {
                    throw new Exception("No reference set or key filed is empty for FixtureId ,enter value to continue");
                }
            }
            return true;
        }
    }
}
