﻿using Cricket.DAL;
using Cricket.Model;
using Cricket.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cricket
{
    public static class User
    { 
       
       

        public static ObservableCollection<OfficialEntity> LstOfficial = new ObservableCollection<OfficialEntity>();
        public static ObservableCollection<FixtureEntity> lsTFixture = new ObservableCollection<FixtureEntity>();
        private static ObservableCollection<ZoneEntity> _LstZones = new ObservableCollection<ZoneEntity>();
        public static ObservableCollection<ZoneEntity> LstZones
        {
            get { return _LstZones; }
            set { _LstZones = value;
                
            }
        }

       
        public static ObservableCollection<GroupEntity> Lstgroups = new ObservableCollection<GroupEntity>(Database.GetEntityList<GroupEntity>());
        public static FixtureEntity Fixture { get; set; }
        public static UserEntity LoggedInUser { get; set; }
        public static SeasonEntity Season { get; set; }
        public static DivisionEntity Division { get; set; }
        public static ZoneEntity Zone { get; set; }
        private static string _playerimagepath;
        public static PlayedMatchEntity ObjPlayedMatch { get; set; }
        public static string Dismissal { get; set; }

        public static void Created(BaseEntity obj)
        {
            if (LoggedInUser != null)
            {
                obj.CreatedBy = LoggedInUser.UserId;
                obj.CreatedDateTime = DateTime.Now;
            }
        }

        public static void Updated(BaseEntity obj)
        {
            if (LoggedInUser != null)
            {
                obj.UpdatedBy = LoggedInUser.UserId;
                obj.UpdatedDateTime = DateTime.Now;
            }
        }

        public static string PlayerImagePath
        {
            get { return _playerimagepath; }
            set { _playerimagepath = value; }
        }
        public static ObservableCollection<ZoneEntity> LoadZones()
        {
            ObservableCollection<ZoneEntity> LstZones = Database.GetEntityList<ZoneEntity>();
            LstZones = new ObservableCollection<ZoneEntity>(LstZones.OrderBy(p => p.ZoneNumber));
            
            return LstZones;
        } 

        public static ObservableCollection<DivisionEntity> LoadDivision()
        {
            ObservableCollection<DivisionEntity> LstDivision = Database.GetEntityList<DivisionEntity>();
            LstDivision = new ObservableCollection<DivisionEntity>(LstDivision.OrderBy(p => p.DivisionName));
            return LstDivision;
        }

        public static ObservableCollection<SeasonEntity> LoadSeason()
        {
            ObservableCollection<SeasonEntity> LstSeason = Database.GetEntityList<SeasonEntity>();
            LstSeason = new ObservableCollection<SeasonEntity>(LstSeason.OrderByDescending(p => p.SeasonName));
            return LstSeason;

        }

        public static ObservableCollection<LocationEntity> LoadLocations()
        {
            ObservableCollection<LocationEntity> LstLocation = Database.GetEntityList<LocationEntity>();
            LstLocation = new ObservableCollection<LocationEntity>(LstLocation.OrderBy(p => p.StadiumName));
            return LstLocation;

        }

        public static ObservableCollection<OfficialEntity> LoadOfficials()
        {
            LstOfficial = Database.GetEntityList<OfficialEntity>();
            LstOfficial = new ObservableCollection<OfficialEntity>(LstOfficial.OrderBy(p => p.Name));
            return LstOfficial;
        }

        public static ObservableCollection<FixtureEntity> LoadFixtures()
        {
            lsTFixture = Database.GetEntityList<FixtureEntity>();
            lsTFixture = new ObservableCollection<FixtureEntity>(lsTFixture.OrderBy(p => p.Day));
            return lsTFixture;
        }

        public static ObservableCollection<LoadDivisionEntity> LoadDivisionsWithTeams()
        {
            ObservableCollection<LoadDivisionEntity> LstLoadDivision = Database.GetEntityList<LoadDivisionEntity>();
            LstLoadDivision = new ObservableCollection<LoadDivisionEntity>(LstLoadDivision.OrderBy(p => p.Team.TeamName));
            return LstLoadDivision;
        }

        public static ObservableCollection<TeamEntity> LoadTeams()
        {
            ObservableCollection<TeamEntity> LsTTeam = Database.GetEntityList<TeamEntity>();
            LsTTeam = new ObservableCollection<TeamEntity>(LsTTeam.OrderBy(p => p.TeamName));
            return LsTTeam;
        }

        public static ObservableCollection<PlayedMatchDetailEntity> LoadPlayedMatchDetailsTeamOne()
        {
            PlayedMatchDetailEntityManager ObjPlayedDetailEm = new PlayedMatchDetailEntityManager();
            ObservableCollection<PlayedMatchDetailEntity> LstPlayedMatchDetails = ObjPlayedDetailEm.GetByFixtureIdAndTeamId(Database.getDBContext(), User.Fixture.FixtureId,User.Fixture.TeamOneId.Value);
            LstPlayedMatchDetails = new ObservableCollection<PlayedMatchDetailEntity>(LstPlayedMatchDetails.OrderBy(p => p.PlayerName));
            return LstPlayedMatchDetails;
        }

        public static ObservableCollection<PlayedMatchDetailEntity> LoadPlayedMatchDetailsTeamTwo()
        {
            PlayedMatchDetailEntityManager ObjPlayedDetailEm = new PlayedMatchDetailEntityManager();
            ObservableCollection<PlayedMatchDetailEntity> LstPlayedMatchDetails = ObjPlayedDetailEm.GetByFixtureIdAndTeamId(Database.getDBContext(), User.Fixture.FixtureId,User.Fixture.TeamTwoId.Value);
            LstPlayedMatchDetails = new ObservableCollection<PlayedMatchDetailEntity>(LstPlayedMatchDetails.OrderBy(p => p.PlayerName));
            return LstPlayedMatchDetails;
        }

        public static ObservableCollection<PlayedMatchEntity> LoadPlayedMatches()
        {
            ObservableCollection<PlayedMatchEntity> LstPlayedMatches = Database.GetEntityList<PlayedMatchEntity>();
            return LstPlayedMatches;
        }

        public static int ConvertToBalls(decimal Overs)
        {
            int oversinFirstInnings = 0, ballsinfirstinnings = 0, totalballsinFirstInnings = 0;
            oversinFirstInnings = (int)(Math.Floor(Overs));
            if (Overs.ToString().Contains("."))
            {
                string[] ballsfaced = Overs.ToString().Split('.');
                ballsinfirstinnings = Convert.ToInt16(ballsfaced[1]);
            }
            totalballsinFirstInnings = (oversinFirstInnings * 6) + ballsinfirstinnings;
            return totalballsinFirstInnings;
        }

        public static int GetTotalBalls(decimal FirstinningsOvers, decimal SecondInningsOvers)
        {
            int oversinFirstInnings = 0, ballsinfirstinnings = 0, totalballsinFirstInnings = 0;
            int oversinSecondInnings = 0, ballsinSecondinnings = 0, totalballsinSecondInnings = 0;

            oversinFirstInnings = (int)(Math.Floor(FirstinningsOvers));
            oversinSecondInnings = (int)(Math.Floor(SecondInningsOvers));
            if (FirstinningsOvers.ToString().Contains("."))
            {
                string[] ballsfaced = FirstinningsOvers.ToString().Split('.');
                ballsinfirstinnings = Convert.ToInt16(ballsfaced[1]);
            }

            if (SecondInningsOvers.ToString().Contains("."))
            {
                string[] ballsfaced = SecondInningsOvers.ToString().Split('.');
                ballsinSecondinnings = Convert.ToInt16(ballsfaced[1]);
            }

            totalballsinFirstInnings = (oversinFirstInnings * 6) + ballsinfirstinnings;
            totalballsinSecondInnings = (oversinSecondInnings * 6) + ballsinSecondinnings;
            return (totalballsinFirstInnings + totalballsinSecondInnings);

        }

        public static string getOvers(int balls)
        {
            return Math.Floor((decimal)Convert.ToInt16(balls) / 6).ToString() + "." + (Convert.ToInt16(balls) % 6).ToString();
        }

        public static HomeViewModel HomePageVM = new HomeViewModel();


    }
    
}
