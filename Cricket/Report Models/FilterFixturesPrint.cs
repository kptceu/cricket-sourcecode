﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cricket.Report_Models
{
    public class FilterFixturesPrint
    {
        public string DateWithDay { get; set; }
        public  string teamone { get; set; }
        public string teamtwo { get; set; }
        public  string venue { get; set; }
        public string umpireone { get; set; }
        public string umpiretwo { get; set; }
        public string scorer { get; set; }
        public  string Division { get; set; }
        public string Zone { get; set; }
        public string Season { get; set; }
        public string FixtureId { get; set; }

    }
}
